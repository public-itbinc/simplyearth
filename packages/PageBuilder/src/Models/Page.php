<?php

namespace PageBuilder\PageBuilder\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pagebuilder_pages';

    protected $fillable = [
        'title',
        'slug',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function contents()
    {
        return $this->belongsToMany(Template::class, 'pagebuilder_contents', 'template_id', 'page_id');
    }
}
