<?php

namespace PageBuilder\PageBuilder\Models\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Content extends Pivot
{
    protected $table = 'pagebuilder_contents';

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
