<?php

namespace PageBuilder\PageBuilder\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'pagebuilder_templates';

    protected $fillable = [
      'name',
      'position_inverse',
      'preview',
      'structure',
      'type',
    ];

    protected $casts = [
        'preview' => 'array',
        'structure' => 'array',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
