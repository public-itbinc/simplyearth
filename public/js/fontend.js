/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 908);
/******/ })
/************************************************************************/
/******/ ({

/***/ 740:
/***/ (function(module, exports) {

$(function () {
  window.SocialShareKit.init();

  $('.blog-detail .body blockquote > p').prepend("<i class='fa fa-quote-left fa-2x blue-text'></i>");
  $('.blog-detail .body blockquote > p').append('<i class="fa fa-quote-right fa-2x blue-text"></i>');

  var iframe = document.querySelectorAll('.blog-detail .body p > iframe');

  if (iframe.length > 0) {
    var iframe_wrapper = document.createElement('div');
    var iframe_parent = iframe[0].parentNode;
    iframe_wrapper.className = 'iframe-wrapper';
    iframe_wrapper.appendChild(iframe[0]);
    iframe_parent.appendChild(iframe_wrapper);
  }

  /* Share on tweeter ---------------------------------------------------------------------------*/
  /*let div = $(".blog-detail > .body > div");
     for (var i = 0; i < div.length; i++) {
      let idiv = $(div[i]);
       if (div[i].innerText.length > 1) {
        let appendHtml =
        '<p class="actn">' +
        '<a href="#" class="tweet" data-text="'+ div[i].innerText +'">CLICK TO TWEET <span class="ti-twitter-alt"></span></a>' +
        '</p>';
         idiv.append(appendHtml);
      } else {
        idiv.remove();
      }
    }*/

  var shareURL = 'http://twitter.com/share?';

  $(document).on('click', '.tweet', function () {
    var forTweeting = $(this).data('text');

    var params = {
      url: window.location.origin,
      text: forTweeting,
      hashtags: 'simplyearth'
    };

    for (var prop in params) {
      shareURL += '&' + prop + '=' + encodeURIComponent(params[prop]);
    }window.open(shareURL, '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
  });
  /*-------------------------------------------------------------------------------------------------- */
});

/***/ }),

/***/ 908:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(740);


/***/ })

/******/ });
//# sourceMappingURL=fontend.js.map