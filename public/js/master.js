/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 780);
/******/ })
/************************************************************************/
/******/ ({

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hoverintent__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hoverintent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_hoverintent__);
/*!
 * [Project Name]
 * Simply Earth
 */



var simplyEarth = function () {
  'use strict';

  function init() {
    var $searchForm = $('.search-form');
    // Your code here
    // $('.main-carousel').slick({
    //           slide: 'article',
    //           speed: 500,
    //           slidesToShow: 1,
    //           slidesToScroll: 1,
    //           autoplay: true,
    //           arrows: true
    //       });

    $('ul.pagination').hide();
    var options = {
      autoTrigger: true,
      debug: true,
      loadingHtml: '<img class="center-block" src="http://www.arimalasers.com/ridea/image/zh-Hant/spinner9.gif" style="width:50px;" />',
      padding: 0,
      nextSelector: '.pagination li.active + li a:last',
      contentSelector: '.infinite-scroll',
      // pixelsFromNavToBottom:-,
      pagingSelector: 'li a',
      callback: function callback() {
        $('ul.pagination').remove();
      }
    };
    $('.infinite-scroll').jscroll(options).fadeIn(3000);

    //Accordion auto open
    if (window.location.hash) {
      var $target = $('.se-accordion').find(window.location.hash);
      if ($target.length > 0) {
        $target.collapse('show').siblings().addClass('collapsed');
        $('html, body').animate({
          scrollTop: $target.offset().top - 100
        }, 1000);
      }
    }

    $('#burger-menu').click(function () {
      $(this).toggleClass('open');

      var $menu = $('.main-menu');
      if ($(this).hasClass('open')) {
        $menu.slideDown();
        $searchForm.addClass('active');
      } else {
        $menu.slideUp('fast', function () {
          $searchForm.removeClass('active');
        });
      }
    });

    function footerBoxToggle() {
      var $footerBox = $('.footer-box');
      var $boxToggle = $('.box-toggle');
      $('.footer-box .footer-title').click(function () {
        $(this).closest($footerBox).toggleClass('open');
      });
    }
    footerBoxToggle();

    $(".videoPlayers").fitVids();

    function initializeVideo($videoId, $videoType) {
      var $player = '<iframe src="https://www.youtube.com/embed/' + $videoId + '?autoplay=1&showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
      return $player;
    }

    function initializeVideo640($videoId, $videoType) {
      var $player = '<iframe width="640" height="360" src="https://www.youtube.com/embed/' + $videoId + '?autoplay=1&showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
      return $player;
    }

    $('.play-video').click(function (e) {
      e.preventDefault();
      clear_video_player();
      var $player = $(this).parent().find('.videoPlayer');
      var $vidId = $player.data('video-id');
      var $videoType = $player.data('video-type');

      var result = initializeVideo($vidId, $videoType);

      $player.append(result);
    });

    $('.pop-video').click(function (e) {
      e.preventDefault();
      var $this = $(this);
      var video_id = $(this).data('video-id');

      if (video_id == '') {
        return;
      }

      if ($('#player' + video_id).length == 0) {
        $('body').append('<div id="player' + video_id + '" class="modal video-modal">\n        <div class="modal-dialog">\n          <div class="modal-body"></div>\n        </div>\n        </div>');
      }

      var player = initializeVideo640(video_id, 'youtube');
      var video_container = $('<div class="videoPlayers" />').append(player);

      var modal = $('#player' + video_id).modal().on('hidden.bs.modal', function (e) {
        modal.find('.modal-body').html('');
      });

      modal.find('.modal-body').append(video_container);
    });

    //subscription box
    var box_slick = $('.slick-shopping-box').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 1,
      centerMode: true,
      infinite: false,
      variableWidth: true,
      draggable: false,
      arrows: false,
      cssEase: 'linear'
    });

    box_slick.find('.slick-prev').click(function () {
      box_slick.slick('slickPrev');
    });
    box_slick.find('.slick-next').click(function () {
      box_slick.slick('slickNext');
    });

    // START: subscription Page Slider for testimonial 
    var testiMonialSlider = $('#testimonial-slider');

    testiMonialSlider.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });

    $('.slick-prev', testiMonialSlider).click(function () {
      testiMonialSlider.slick('slickPrev');
    });
    $('.slick-next', testiMonialSlider).click(function () {
      testiMonialSlider.slick('slickNext');
    });

    var variants = $('.variant-attribute', '.variant-images');

    $('.slider-for').on('init', function () {

      //Variant


      if (variants.length > 0) {
        var first_slide = $('.slick-current', '.slider-main');

        variants.click(function () {
          first_slide.find('img').attr('src', $(this).addClass('active').find('img').attr('src'));
          variants.not(this).removeClass('active');
          $('.slick-current .image', '.slider-thumb').css({
            'background-image': 'url(' + $(this).addClass('active').find('img').attr('src') + ')'
          });
          $('.slider-for').slick('slickGoTo', 0);
        });

        var $first_variant = variants.first();
        var image_src = $first_variant.addClass('active').find('img').attr('src');

        first_slide.find('img').attr('src', image_src);
      }

      // end variant

      $('.product-gallery').removeClass('invisible');
    }).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: false,
      adaptiveHeight: true,
      infinite: false,
      useTransform: true,
      speed: 400,
      cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)'
    });

    $('.slider-nav').on('init', function (event, slick) {
      $('.slider-nav .slick-slide.slick-current').addClass('is-active');

      if (variants.length > 0) {

        var $first_variant = variants.first();
        var image_src = $first_variant.addClass('active').find('img').attr('src');
        $('.slick-current .image', '.slider-thumb').css({
          'background-image': 'url(' + image_src + ')'
        });
      }
    }).slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrows: false,
      focusOnSelect: false,
      infinite: false,
      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      }, {
        breakpoint: 640,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      }, {
        breakpoint: 420,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }]
    });

    $('.slider-for').on('afterChange', function (event, slick, currentSlide) {
      $('.slider-nav').slick('slickGoTo', currentSlide);
      var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
      $('.slider-nav .slick-slide.is-active').removeClass('is-active');
      $(currrentNavSlideElem).addClass('is-active');
    });

    $('.slider-nav').on('click', '.slick-slide', function (event) {
      event.preventDefault();
      var goToSingleSlide = $(this).data('slick-index');

      $('.slider-for').slick('slickGoTo', goToSingleSlide);
    });

    $('.preview-img').hide();
    $('.loader').show();
    $('.preview-img').imagesLoaded(function () {
      $(this).show();
      $('.loader').hide();
    });

    $('.nav-tabs a').click(function () {
      clear_video_player();
      $(this).tab('show');
    });

    $('.box-tab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      box_slick.slick('slickGoTo', 1);
    });

    var productBox = $('#products .thumb-box');
    productBox.hover(function (e) {
      if (e.type == "mouseenter") {
        $(this).addClass('hovered');
      } else {
        $(this).removeClass('hovered');
      }
    });

    // var $panelTab = $('.panel-tab .tab li');
    // var $tabContent = $('.panel-tab .tab-content');
    // $panelTab.click(function() {
    //  $panelTab.removeClass('active');
    //  $tabContent.removeClass('active');

    //  $(this).addClass('active');

    //  var $target = $(this).data('content-target');
    //  $($target).addClass('active');


    // });

    function clear_video_player() {
      $('.videoPlayer').html("");
    }

    var $pmLabel = $('.payment-method-selection label');
    $pmLabel.on('click', function (e) {
      e.preventDefault();
      $pmLabel.parent().removeClass('checked');
      if ($('input[name="payment-method"]:checked')) {
        $(this).parent().addClass('checked');
      }
      var $content = $(this).parent().find('input').val();
      var $paypalContainer = $('.paypal-content');
      var $ccContainer = $('.cc-content');

      var $contentMethod = $('.content-method');
      $contentMethod.removeClass('active');
      if ($content == "paypal") {
        $paypalContainer.addClass('active');
      } else {
        $ccContainer.addClass('active');
      }
    });

    $('.se-accordion .panel-title a').click(function (e) {
      e.preventDefault();
    });

    $('.checkout-wizard ul li').click(function (e) {
      e.preventDefault();
      var target = $(this).data('target');
      var content = $('.wizard-content');
      content.removeClass('active');

      $(this).addClass('active');
      $('#' + target).addClass('active');
    });

    $('.main-menu .has-sub').each(function () {
      __WEBPACK_IMPORTED_MODULE_0_hoverintent___default()(this, function () {
        $(this).addClass('open');
      }, function () {
        $(this).removeClass('open');
      }).options({
        timeout: 300
      });
    });

    var text_max = $('#textarea').data('max');
    $('#wordcounter').html(text_max + ' characters remaining');

    $('#textarea').keyup(function () {
      var text_length = $('#textarea').val().length;
      var text_remaining = text_max - text_length;

      $('#wordcounter').html(text_remaining + ' characters remaining');
    });

    $('.submit-search').click(function () {
      $searchForm.addClass('active');
    });

    $('#forgotbox').on('show.bs.modal', function () {
      $('#login').modal('hide');
    });

    $('[data-toggle="tooltip"]').tooltip();
  }

  return {
    init: init
  };
}();

jQuery(document).ready(function ($) {
  simplyEarth.init();
});

function setModalMaxHeight(element) {
  this.$element = $(element);
  this.$content = this.$element.find('.modal-content');
  var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
    'overflow': 'hidden'
  });

  this.$element.find('.modal-body').css({
    'max-height': maxHeight,
    'overflow-y': 'auto'
  });
}

$('.modal').on('show.bs.modal', function () {
  $(this).show();
  //setModalMaxHeight(this);
});

$(window).resize(function () {
  if ($('.modal.in').length != 0) {
    //setModalMaxHeight($('.modal.in'));
  }
});

// END: Subscription Page Slider for testimonial 


// Footer Arrangement in Mobile
function arrangeFooter() {
  var row1 = $('.footer-bottom .col-sm-4:eq(0)');
  var row2 = $('.footer-bottom .col-sm-4:eq(1)');
  var row3 = $('.footer-bottom .col-sm-4:eq(2)');
  var guaranteeIcons = $('.guarantee-icons');

  var socialIcons = $('footer .social-icons');

  if ($(window).width() < 769) {
    socialIcons.insertAfter('.guarantee-box');
    guaranteeIcons.insertBefore('.top-box p');
  }
  if ($(window).width() > 768) {
    guaranteeIcons.insertBefore('.guarantee-box p:eq(0)');
    socialIcons.insertAfter('.newsletter-form');
  }
}
arrangeFooter();
$(window).resize(function () {
  arrangeFooter();
});
// $(window).on('load', function () {
//   $('#masonry').masonry({
//     itemSelector: '.grid-item'
//   }).imagesLoaded(function () {
//     $('#masonry').masonry('reload');
//   });
// });

/***/ }),

/***/ 237:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var extend = __webpack_require__(238);

module.exports = function(el, onOver, onOut) {
  var x, y, pX, pY;
  var h = {},
    state = 0,
    timer = 0;

  var options = {
    sensitivity: 7,
    interval: 100,
    timeout: 0
  };

  function delay(el, e) {
    if (timer) timer = clearTimeout(timer);
    state = 0;
    return onOut.call(el, e);
  }

  function tracker(e) {
    x = e.clientX;
    y = e.clientY;
  }

  function compare(el, e) {
    if (timer) timer = clearTimeout(timer);
    if ((Math.abs(pX - x) + Math.abs(pY - y)) < options.sensitivity) {
      state = 1;
      return onOver.call(el, e);
    } else {
      pX = x;
      pY = y;
      timer = setTimeout(function() {
        compare(el, e);
      }, options.interval);
    }
  }

  // Public methods
  h.options = function(opt) {
    options = extend({}, options, opt);
    return h;
  };

  function dispatchOver(e) {
    if (timer) timer = clearTimeout(timer);
    el.removeEventListener('mousemove', tracker, false);

    if (state !== 1) {
      pX = e.clientX;
      pY = e.clientY;

      el.addEventListener('mousemove', tracker, false);

      timer = setTimeout(function() {
        compare(el, e);
      }, options.interval);
    }

    return this;
  }

  function dispatchOut(e) {
    if (timer) timer = clearTimeout(timer);
    el.removeEventListener('mousemove', tracker, false);

    if (state === 1) {
      timer = setTimeout(function() {
        delay(el, e);
      }, options.timeout);
    }

    return this;
  }

  h.remove = function() {
    if (!el) return;
    el.removeEventListener('mouseover', dispatchOver, false);
    el.removeEventListener('mouseout', dispatchOut, false);
  };

  if (el) {
    el.addEventListener('mouseover', dispatchOver, false);
    el.addEventListener('mouseout', dispatchOut, false);
  }

  return h;
};


/***/ }),

/***/ 238:
/***/ (function(module, exports) {

module.exports = extend

var hasOwnProperty = Object.prototype.hasOwnProperty;

function extend() {
    var target = {}

    for (var i = 0; i < arguments.length; i++) {
        var source = arguments[i]

        for (var key in source) {
            if (hasOwnProperty.call(source, key)) {
                target[key] = source[key]
            }
        }
    }

    return target
}


/***/ }),

/***/ 780:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(236);


/***/ })

/******/ });
//# sourceMappingURL=master.js.map