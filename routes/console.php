<?php

use App\Shop\Customers\Account;
use App\Shop\Importer\BraintreeImports;
use App\Shop\Importer\ShopifyOrder;
use App\Shop\Orders\Order;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
 */

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('admin:create {name} {email} {password}', function ($name, $email, $password) {
    $user = App\User::create(['name' => $name, 'email' => $email, 'password' => bcrypt($password)]);
    $user->assignRole('admin');
    $this->comment('Admin user created');

});

Artisan::command('admin:create-super {name} {email} {password}', function ($name, $email, $password) {
    $user = App\User::create(['name' => $name, 'email' => $email, 'password' => bcrypt($password)]);
    $user->assignRole('super-admin');
    $this->comment('SUper Admin user created');

});

Artisan::command('products:generate-slugs', function () {
    $products = App\Shop\Products\Product::where('slug', '')
        ->orWhere('slug', null)->get();

    $products->each(function ($product) {
        $product->slug = $product->generateSlug();
        $product->save();
    });

});

Artisan::command('install:generate', function () {
    $products = factory(App\Shop\Products\Product::class, 12)->create();
    $this->subscription_monthly = factory(App\Shop\Products\Product::class)->create(['type' => 'subscription', 'sku' => 'subscription-monthly', 'price' => 39]);
    $this->subscription_quarterly = factory(App\Shop\Products\Product::class)->create(['type' => 'subscription', 'sku' => 'subscription-quarterly', 'price' => 45]);
    $this->subscription_quarterly->subscription_months = 3;
    $this->subscription_quarterly->save();
});

Artisan::command('box:run {schedule?}', function ($schedule = null) {

    if (!env('RUN_CRON_JOB', true)) {
        return;
    }

    if ($schedule) {
        $schedule_date = Carbon::createFromFormat('Y-m-d', $schedule);
        Carbon::setTestNow($schedule_date);
    } else {
        $schedule_date = Carbon::today();
    }

    Log::info('Running box for schedule:' . $schedule_date->format('F j, Y'));
    foreach (App\Shop\Customers\Account::subscribedUsers()
        ->whereHas('subscription', function ($query) use ($schedule_date) {
            $query->where('override_schedule', $schedule_date->format('d'));
            $query->orWhere(function ($query) use ($schedule_date) {
                $query->where('schedule', $schedule_date->format('d'))
                ->whereNull('override_schedule');
            });
        })
        ->whereHas('customer')
        ->get() as $account) {
        try {
            Log::info('Starting:' . $account->email);
            $nextBox = $account->nextBox();

            if ($nextBox && $nextBox->date->isToday()) {
                if (!$nextBox->skipped()) {
                    Log::info('Initiating Box Process:' . $account->email);
                    processBox($nextBox);
                } else {
                    Log::info('Skipped:' . $account->email);
                }
            }
        } catch (\Throwable $e) {
            Log::error($account->id . ':' . $account->email . ' ' . $e->getMessage());
        }

    }
    Log::info('End running box for schedule:' . $schedule_date->format('F j, Y'));
});

Artisan::command('box:trial {schedule?}', function ($schedule = null) {

    if ($schedule) {
        $schedule_date = Carbon::createFromFormat('Y-m-d', $schedule);
        Carbon::setTestNow($schedule_date);
    } else {
        $schedule_date = Carbon::today()->addDays(2);
        Carbon::setTestNow($schedule_date);
    }

    Log::info('Running box trial for schedule:' . $schedule_date->format('F j, Y'));

    $error_message = '';
    foreach (App\Shop\Customers\Account::subscribedUsers()
        ->whereHas('subscription', function ($query) use ($schedule_date) {
            $query->where('override_schedule', $schedule_date->format('d'));
            $query->orWhere(function ($query) use ($schedule_date) {
                $query->where('schedule', $schedule_date->format('d'))
                ->whereNull('override_schedule');
            });
        })
        ->whereHas('customer')
        ->get() as $account) {
        try {
            Log::info('Starting:' . $account->email);
            $nextBox = $account->nextBox();

            if ($nextBox && $nextBox->date->isToday()) {
                if (!$nextBox->skipped()) {
                    Log::info('Initiating Box Process:' . $account->email);
                    Log::info('Box processed' . $account->subscription->id);
                } else {
                    Log::info('Skipped:' . $account->email);
                }
            }
        } catch (\Throwable $e) {
            $error_message .= $account->email . ': ' . $e->getMessage() . PHP_EOL;
            Log::error($account->id . ':' . $account->email . ' ' . $e->getMessage());
        }
    }
    if (!empty($error_message)) {
        Mail::to(env('FAILED_JOB_ADMIN_EMAIL'))->send(new App\Mail\BoxProcessTrialError($schedule_date->format('F j, Y'), $error_message));
    }
    Log::info('End running box trial for schedule:' . $schedule_date->format('F j, Y'));
});

Artisan::command('box:process {email} {schedule?}', function ($email, $schedule = null) {

    if ($schedule) {
        $schedule_date = Carbon::createFromFormat('Y-m-d', $schedule);
        Carbon::setTestNow($schedule_date);
    }

    echo 'Box process start' . PHP_EOL;
    try {
        $account = App\Shop\Customers\Account::where('email', $email)->first();

        if (!$account) {
            echo 'Account not found' . PHP_EOL;
            return;
        }

        $nextBox = $account->nextBox();

        if ($nextBox && $nextBox->date->isToday() && !$nextBox->skipped()) {
            Log::info('Initiating Box Process:' . $account->email);
            processBox($nextBox);
        }
    } catch (\Exception $e) {
        Log::error($e->getMessage());
    }

    echo 'Box process end' . PHP_EOL;
});

Artisan::command('gift:notify', function () {

    if (!env('RUN_CRON_JOB', true)) {
        return;
    }

    echo 'Gift Notification start' . PHP_EOL;

    $gifts = App\Shop\Subscriptions\SubscriptionGift::with(['subscription'])->emailGiftToday()->get();

    echo 'Count:' . $gifts->count() . PHP_EOL;

    $gifts->each(function ($gift) {
        App\Jobs\NotifyBoxGift::dispatch($gift);
    });
    echo 'Gift Notification end' . PHP_EOL;
});

Artisan::command('card:expiring-notify', function () {

    if (!env('RUN_CRON_JOB', true)) {
        return;
    }

    echo 'Card expiring Notificaiton start' . PHP_EOL;
    $now = Carbon::today();
    $expiring = App\Shop\Customers\Account::subscribedUsers()
        ->expiringCards($now->format('Y-m-d'))
        ->whereNull('expiring_notified_at')
        ->get();
    $expiring->each(function ($account) {
        echo 'Sending expiring notification: ' . $account->email . PHP_EOL;
        App\Jobs\NotifyExpiringCard::dispatch($account);
    });
    echo 'Card expiring Notificaiton end';
});

Artisan::command('order:process-giftcard {order_id}', function ($order_id) {

    $order = App\Shop\Orders\Order::find($order_id);

    $order->processGiftCards();

});

Artisan::command('import:recharge-subscriptions', function () {
    (new App\Shop\Importer\ImportRecharge())->process();
});

Artisan::command('import:recharge-failed-subscriptions', function () {
    (new App\Shop\Importer\ImportRechargeFailed())->process();
});

Artisan::command('import:account-payment-method {email?}', function ($email = null) {
    if ($email) {
        $accounts = Account::where('email', $email)->get();
    } else {
        $accounts = Account::whereNull('braintree_id')->get();
    }

    if (count($accounts)) {
        foreach ($accounts as $account) {
            BraintreeImports::importAccountPaymentMethod($account);
            echo '.';
        }
    }
});

Artisan::command('braintree:check {email}', function ($email) {

    $customers = Braintree\Customer::search([Braintree_CustomerSearch::id()->is($email)]);

    foreach ($customers as $customer) {
        dump($customers);
    }

});

//Import Customers from shopify

Artisan::command('import:shopify-orders', function () {

    $orders_count = Facades\App\Shop\Importer\Shopify::getOrdersCount();
    echo 'Total orders: ' . $orders_count . PHP_EOL;

    sleep(1);

    $page = 1;
    $limit = 200;
    $count = 0;
    $loops = ceil($orders_count / (float) $limit);

    $tags = [];

    while ($page <= $loops) {
        $orders = Facades\App\Shop\Importer\Shopify::getOrders($page, $limit);
        $count = count($orders);
        echo 'Count:' . $count . PHP_EOL;
        foreach ($orders as $order) {

            (new ShopifyOrder($order))->import();

        }
        $page++;
    }
});

//Fix orders in march

Artisan::command('shopify:march-fix', function () {

    $orders_count = Facades\App\Shop\Importer\Shopify::getOrdersMarchCount();
    echo 'Total orders: ' . $orders_count . PHP_EOL;

    sleep(1);

    $page = 1;
    $limit = 200;
    $count = 0;
    $loops = ceil($orders_count / (float) $limit);

    $tags = [];

    while ($page <= $loops) {
        $orders = Facades\App\Shop\Importer\Shopify::getOrdersMarch($page, $limit);
        $count = count($orders);
        echo 'Count:' . $count . PHP_EOL;
        foreach ($orders as $order) {

            (new \App\Shop\Importer\ShopifyMarchFix($order))->fix();

        }
        $page++;
    }
});

Artisan::command('import:shopify-customers', function () {

    echo 'Connecting to Shopify' . PHP_EOL;

    $customers_count = Facades\App\Shop\Importer\Shopify::getCustomersCount();
    echo 'Total customers: ' . $customers_count . PHP_EOL;

    sleep(1);

    $page = 1;
    $limit = 200;
    $count = 0;
    $loops = ceil($customers_count / (float) $limit);

    $tags = [];
    $errors = [];
    while ($page <= $loops) {

        $customers = Facades\App\Shop\Importer\Shopify::getCustomers($page, $limit);
        $count = count($customers);

        //echo 'Count:' . $count.PHP_EOL;
        App\Shop\Customers\Customer::unsetEventDispatcher();
        App\Shop\Customers\Account::unsetEventDispatcher();
        foreach ($customers as $customer) {

            //try{
            $real_customer = App\Shop\Customers\Customer::firstOrNew(['id' => $customer->id]);
            $real_customer->forceFill([
                'id' => $customer->id,
                'email' => $customer->email,
                'phone' => $customer->phone,
                'first_name' => $customer->first_name,
                'last_name' => $customer->last_name,
                'created_at' => Carbon::createFromFormat(DateTime::ISO8601, $customer->created_at),
                'updated_at' => Carbon::createFromFormat(DateTime::ISO8601, $customer->updated_at),
            ])->save();

            if ($customer->state == 'enabled' && $customer->email) {

                if (!$real_customer->account) {
                    $real_customer->account()->create(['password' => bcrypt(str_random(16))]);
                }
            }

            if (count($customer->addresses)) {
                foreach ($customer->addresses as $address) {

                    $real_customer->addresses()->firstOrCreate([
                        "first_name" => $address->first_name,
                        "last_name" => $address->last_name,
                        "company" => $address->company,
                        "address1" => $address->address1,
                        "address2" => $address->address2,
                        "city" => $address->city,
                        "region" => $address->province_code,
                        "country" => $address->country_code,
                        "zip" => $address->zip,
                        "phone" => $address->phone,
                        "primary" => $address->default,
                    ]);
                }
            }

            if ($customer->tags) {

                $customer_tags = [];

                foreach (explode(',', $customer->tags) as $tag) {

                    $slug_tag = str_slug($tag);

                    if (!isset($tags[$slug_tag])) {
                        $tags[$slug_tag] = App\Shop\Tags\Tag::firstOrCreate(['name' => $slug_tag]);
                    }

                    $customer_tags[] = $tags[$slug_tag]->id;
                }

                $real_customer->tags()->sync($customer_tags);
            }

            /* } catch(\Exception $e) {
            $errors[] = $e->getMessage();
            echo 'F';
            }*/

            echo '.';
        }

        $page++;
    }

    echo implode(PHP_EOL, $errors);

});

Artisan::command('import:shopify-giftcards', function () {

    (new App\Shop\Importer\ImportGiftCards())->process();

});

Artisan::command('notify:imported-giftcards', function () {

    $gift_cards = App\Shop\Discounts\GiftCard::where('remaining', '>', 0)->get();

    foreach ($gift_cards as $gift_card) {
        Mail::to($gift_card->owner)->send(new App\Mail\NewGiftCard($gift_card));
        echo '.';
    }

});

Artisan::command('notify:imported-accounts {email?}', function ($email = null) {

    if ($email) {
        $accounts = Account::where('email', $email)->get();
    } else {
        $accounts = App\Shop\Customers\Account::whereNotNull('email')->get();
    }

    foreach ($accounts as $account) {
        Mail::to($account)->send(new App\Mail\MailNewAccount1($account));
        echo '.';
    }

});

Artisan::command('products:calculate-total-sales', function () {
    App\Shop\Products\ProductProcessor::calculateTotalSales();
});

//Reports

//Daily Boxes
Artisan::command('report:boxes {start_date?} {end_date?}', function ($start_date, $end_date = null) {
    (new App\Shop\Reports\DailyBoxes($start_date, $end_date))->process();
});


//Installments
Artisan::command('installments:run', function () {
    (new App\Shop\Processes\ProcessInstallments)->run();
});

//Gnerate missing sharecodes
Artisan::command('sharecodes:generate', function () {
    $accounts = App\Shop\Customers\Account::subscribedUsers()->with(['customer'])->get();
    $accounts->each(function($account){
        if ($account->customer){
            $account->customer->prepareShareCode();
        }
    });
});