<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('page-builder')->group(function(){
    Route::get('categories', 'Api\Pagebuilder\CategoryController');

    Route::prefix('page/')->namespace('Api\Pagebuilder')->group(function(){
        Route::post('loadImage', 'TemplateController@loadImage');
        Route::prefix('content/')->group(function(){
            Route::get('{page}/', 'TemplateController@show');
            Route::post('{page}/update', 'TemplateController@store');

        });

    });


});
