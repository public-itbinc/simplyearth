<?php

use Illuminate\Support\Facades\Auth;
use App\Shop\Orders\Order;
use App\Shop\Customers\Account;
use App\Shop\Exporter\CSVExporter;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Shipstation

Route::get('/shipstation', 'ShipstationController@export');
Route::post('/shipstation', 'ShipstationController@notify');

Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['namespace' => 'Customers'], function () {
        Route::get('/orders/{token}', 'CustomerOrdersController@show')->name('orders.show');
    });
});

//Generate page
Route::get('/page/{slug}', 'PageController')->name('page');

$lock_config = env('APP_LOCK') ? ['middleware' => 'auth.basic'] : [];

/*Route::get('testnextbox', function(){

    $account  = App\Shop\Customers\Account::where('email', 'mharkrollen@gmail.com')->first();
    $order = (new App\Shop\ShoppingBoxes\ShoppingBoxBuilder($account->nextBox()))->setData(['status' => App\Shop\Orders\Order::ORDER_PROCESSING])->build();
    event(new App\Events\OrderCreated($order));
});*/
/*Route::get('testgiftcard', function(){

    $order  = App\Shop\Orders\Order::find(10007);
    Mail::to($order)->send(new App\Mail\GiftCard($order, App\Shop\Discounts\GiftCard::find(1)));

    //$order = App\Shop\Orders\Order::find(10007);

    //$order->processGiftCards();

});*/

/*Route::get('/testmail', function(){	
    return new App\Mail\OrderProcessed(App\Shop\Orders\Order::first());	
});

Route::get('/teststamped', function(){	
    $stamped = new App\Shop\Conversion\Stamped();
    $conversion = new App\Shop\Checkout\ConversionTracker(Order::first());
    $stamped->track($conversion->stamped());
});*/

Route::group(['middleware' => $lock_config], function () {

/** ADMIN */
    Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/admin/login', 'Auth\LoginController@login');
    Route::post('/admin/logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
    Route::get('/admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/admin/password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/search', 'SearchController@search');

/** END ADMIN */

    Route::group(['middleware' => ['auth', 'role:super-admin', 'ip_address'], 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
        Route::namespace ('Administrators')->group(function () {

            Route::get('/search/administrators', 'AdministratorsController@search');
            Route::post('/administrators/adduser', 'AdministratorsController@adduser')->name('administrators.adduser');
            Route::post('/administrators/store', 'AdministratorsController@store')->name('administrators.store');            
            Route::get('/administrators/{user}', 'AdministratorsController@detail')->name('dashboard');
            Route::patch('/administrators/{user}', 'AdministratorsController@update')->name('dashboard');
            Route::delete('/administrators/{$user}', 'AdministratorsController@destroy')->name('administrators.delete');

        });

    });

    Route::group(['middleware' => ['auth', 'role:super-admin|admin', 'ip_address'], 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

        Route::get('/testmail/{day}', function($day){

            $account = App\Shop\Customers\Account::first();
            switch ($day) {
                case 1:
                    return new App\Mail\FailedPaymentCharge($account);
                    break;
                case 2:
                    return new App\Mail\FailedPaymentCharge2($account);
                    break;
                case 3:
                    return new App\Mail\FailedPaymentCharge3($account);
                    break;
                case 4:
                    return new App\Mail\FailedPaymentCharge4($account);
                    break;
                case 5:
                    return new App\Mail\FailedPaymentCharge5($account);
                    break;
                case 6:
                    return new App\Mail\FailedPaymentCharge6($account);
                    break;

            }	
        });

        Route::namespace ('Discounts')->group(function () {
            Route::get('/discounts/search', 'DiscountsController@search')->name('admin.discounts.search');
            Route::get('/discounts/download/{filename}', 'DiscountsController@downloadDiscounts');
            Route::resource('discounts', 'DiscountsController');
        });

        Route::namespace('Recipes')->group(function (){
            Route::get('/recipes/search', 'RecipeController@search')->name('admin.recipes.search');
            Route::resource('/recipes', 'RecipeController');
        });

        Route::namespace ('Importer')->group(function () {

            Route::get('/import/productids', 'ImporterController@productids');
            Route::get('/import/update', 'ImporterController@update');
        });

        Route::namespace ('Customers')->group(function () {
            Route::get('/customers', 'CustomersController@index')->name('customers.index');
            Route::get('/customers/settings', 'CustomersController@settings')->name('customers.settings');
            Route::post('/customers', 'CustomersController@store')->name('customers.store');
            Route::get('/customers/search', 'CustomersController@search')->name('customers.search');
            Route::get('/customers/{customer}', 'CustomersController@show')->name('customers.show');
            Route::patch('/customers/{customer}', 'CustomersController@update')->name('customers.update');
            Route::patch('/customers/{customer}/addresses', 'CustomersController@address_update')->name('customers.address_update');
            Route::delete('/customers/{customer}/addresses', 'CustomersController@address_delete')->name('customers.address_delete');
            Route::post('/customers/sendinvite/{customer}', 'CustomersController@send_invite')->name('customers.sendinvite');
            Route::post('/customers/changepassword/{customer}', 'CustomersController@changePassword')->name('customers.changepassword');
            Route::post('/customers/createaccount/{customer}', 'CustomersController@createAccount')->name('customers.createaccount');
            Route::delete('/customers/{customer}/account', 'CustomersController@disable_account')->name('customers.sendinvite');
            Route::post('/customers/{customer}/logas', 'CustomersController@loginAs');
            Route::post('/customers/{customer}/tax/upload', 'CustomersController@uploadTaxDocument');
            Route::post('/customers/{customer}/tax/exempt_toggle', 'CustomersController@exemptToggle');
            Route::get('/tax_documents/{document_id}/download', 'CustomersController@downloadTaxDocument');
            Route::delete('/tax_documents/{document_id}', 'CustomersController@deleteTaxDocument');
            //tags
            Route::patch('/customers/{customer}/tags', 'CustomersController@updateTags');
            Route::patch('/customers/{customer}/schedule', 'CustomersController@overrideSchedule');
            Route::patch('/customers/{customer}/cancel', 'CustomersController@cancelSubscription');
            Route::get('/customers/tags/search/{tag}', 'CustomersController@searchTags');
        });

        Route::namespace ('Users')->group(function () {
            Route::get('/users/search', 'UsersController@search')->name('users.search');
        });

        Route::namespace ('Products')->group(function () {
            Route::resource('products', 'ProductController');
            Route::post('/products/{product}/images', 'ProductController@images')->name('products.images');
        });

        Route::namespace ('ShoppingBoxes')->group(function () {
            Route::resource('shopping-boxes', 'ShoppingBoxController');
        });

        Route::namespace ('Categories')->group(function () {
            Route::get('categories/search', 'CategoryController@search');
            Route::resource('categories', 'CategoryController');
            Route::post('/categories/{category}/images', 'CategoryController@images')->name('categories.images');
        });

        Route::namespace ('Tags')->group(function () {
            Route::resource('tags', 'TagController');
        });

        Route::namespace ('Orders')->group(function () {
            Route::get('orders/search', 'OrderAdminController@search')->name('orders.search');
            Route::get('orders/settings', 'OrderAdminController@settings')->name('orders.settings');
            Route::resource('orders', 'OrderAdminController');
            Route::post('orders/compute', 'OrderAdminController@compute')->name('orders.compute');
            Route::patch('orders/{order}/edit', 'OrderAdminController@update');
            Route::patch('orders/{order}/contact', 'OrderAdminController@update_contact');
            Route::patch('orders/{order}/referrer', 'OrderAdminController@update_referrer');
            Route::delete('orders/{order}/referrer', 'OrderAdminController@remove_referrer');
            Route::patch('orders/{order}/shipping', 'OrderAdminController@update_shipping');
            Route::patch('orders/{order}/status', 'OrderAdminController@updateStatus');
            Route::patch('orders/{order}/approve', 'OrderAdminController@approve')->name('order.approve');
            Route::post('orders/{order}/resend-order-email', 'OrderAdminController@resendOrderEmail');
        });

        Route::namespace ('Media')->group(function () {
            Route::post('/media/upload', 'MediaController@upload');
            Route::get('/media', 'MediaController@index');
            Route::post('/media', 'MediaController@store');
            Route::delete('/media/{media}', 'MediaController@destroy');
        });

        Route::namespace ('Shipping')->group(function () {
            Route::get('/store/shipping', 'ShippingController@index');
            Route::get('/store/shipping/settings', 'ShippingController@settings');
            Route::get('/store/shipping/settings', 'ShippingController@settings');
            Route::post('/store/settings/store_address', 'ShippingController@update_store_address');
            Route::resource('shipping-zones', 'ShippingZoneController');
        });

        Route::namespace ('Settings')->group(function () {
            Route::get('/store/settings/wholesale', 'SettingsController@wholesale');
            Route::patch('/store/settings/wholesale', 'SettingsController@wholesaleUpdate')->name('settings.wholesale.update');
        });

        Route::namespace ('Tools')->group(function () {
            Route::get('/store/tools/export-subscribers', 'ToolsController@export_subscribers')->name('tools.export_subscribers');
        });

        Route::namespace ('Blogs')->group(function () {
            Route::name('blogs.search')->post('/blogs/search', 'BlogSearchController@search');
            Route::name('blogs.get.gallery')->get('/blogs/gallery/{offset}/{skip}', 'BlogAxiosController@getBlog');
            Route::resources([
                'blogs' => 'BlogController',
                'blogtag' => 'BlogTagController',
                'blogtagsgroups' => 'BlogTagsGroupsController',
                'blogdraft' => 'BlogDraftController',
                'blogcategory' => 'BlogCategoryController',
                'bloggallery' => 'BlogImageGalleryController',
                'blogsubcategory' => 'BlogSubCategoryController',
                'blogauthor' => 'BlogAuthorController',
                'blogcategorygroup' => 'BlogCategoryGroupController',
            ]);
        });

        Route::namespace ('Store')->group(function () {
            Route::resources([
                'storelocator' => 'StoreLocatorController',
                'storelocatorimport' => 'StoreImportController',
                'storelocatorshow' => 'StoreShowController',
            ]);
        });
        

        Route::namespace('GiftCards')->group(function () {
            Route::get('/search/gift-cards', 'GiftCardAdminController@index');
            Route::post('/gift-cards/{gift_card}/resend', 'GiftCardAdminController@resend');
            Route::delete('/gift-cards/{gift_card}', 'GiftCardAdminController@remove');
        });

        Route::namespace ('Dashboard')->group(function () {
            Route::get('/', 'AdminController@index')->name('dashboard');
            Route::get('/dashboard/stats', 'AdminController@stats')->name('dashboard.stats');
        });

        Route::namespace ('Blogs')->group(function () {
            Route::name('blogs.search')->post('/blogs/search', 'BlogSearchController@search');
            Route::name('blogs.get.gallery')->get('/blogs/gallery/{offset}/{skip}', 'BlogAxiosController@getBlog');
            Route::resources([
                'blogs' => 'BlogController',
                'blogtag' => 'BlogTagController',
                'blogtagsgroups' => 'BlogTagsGroupsController',
                'blogdraft' => 'BlogDraftController',
                'blogcategory' => 'BlogCategoryController',
                'bloggallery' => 'BlogImageGalleryController',
                'blogsubcategory' => 'BlogSubCategoryController',
                'blogauthor' => 'BlogAuthorController',
                'blogcategorygroup' => 'BlogCategoryGroupController',
            ]);
        });

        Route::namespace ('Pagebuilder')->group(function () {
            Route::get('pagebuilder/editpage/{page}','PagebuilderController@edit_template')->name('pagebuilder.edit_template');
            Route::resource('pagebuilder', 'PagebuilderController',['parameters' => [
                'pagebuilder' => 'page'
            ]]);
        });

    });

    //API

    Route::prefix('api')->group(function(){
        Route::get('productsproducts', function(){
            return App\Shop\Products\Product::active()->get()->map(function($product){
                return [
                    'id' => $product->id,
                    'sku' => $product->sku,
                    'name' => $product->name,
                ];
            });
        });
    });

    Route::group(['namespace' => 'Frontend'], function () {

        Route::namespace ('Pages')->group(function () {

            Route::get('/', 'PageController@index')->name('home');

            Route::prefix('pages')->group(function () {
                Route::get('faqs', 'PageController@faqs')->name('faqs');
                Route::get('become-an-ambassador', 'PageController@ambassador')->name('become-an-ambassador');
                Route::get('wholesale', 'PageController@wholesale_program')->name('wholesale');
                Route::get('our-cause', 'PageController@our_cause')->name('our-cause');
                Route::get('be-a-team-member', 'PageController@careers')->name('be-a-team-member');
                Route::get('dillution-rates', 'PageController@dillution_rates')->name('dillution-rates');
                Route::get('ebook', 'PageController@e_book')->name('ebook');
                Route::get('comparison-blends', 'PageController@comparison_chart')->name('comparison-blends');
                Route::get('6-steps-to-purity', 'PageController@six_steps_to_pure')->name('6-steps-to-purity');
                Route::get('our-story', 'PageController@our_story')->name('our-story');
                Route::get('privacy-policy', 'PageController@privacy_policy')->name('privacy-policy');
                Route::get('terms-of-service', 'PageController@terms_of_use')->name('terms-of-service');
                Route::get('ask-our-aromatherapist', 'PageController@our_aromatherapist')->name('ask-our-aromatherapist');
                Route::get('pure-natural-products', 'PageController@simply_pure_promise')->name('pure-natural-products');
                Route::get('free-returns', 'PageController@free_shipping')->name('free-returns');
                Route::get('terms-of-use', 'PageController@terms-of-use')->name('terms-of-use');
                Route::get('new-wholesaler', 'PageController@wholesalerSalesPage')->name('wholesaler.sales');
            });

            Route::get('blogs/stories/', 'BlogController@index')->name('all.blog');
            Route::get('blogs/stories/{slug}', 'BlogController@show')->name('single.post');
            Route::get('blogs/stories/category/{category}', 'BlogController@catFilter')->name('blog.cat.filter');
            Route::get('blogs/stories/tag/{tag}', 'BlogController@tagFilter')->name('blog.tag.filter');
        });

        Route::namespace ('Store')->group(function () {
            Route::name('store.search')->get('/store/search', 'StoreSearchController@search');
            Route::get('/store-locator', 'StoreLocator@index')->name('store-locator');
            Route::get('/store/get/xml', 'StoreLocator@getXml');
        });

        Route::namespace ('Customers')->group(function () {

            Route::get('login', 'CustomerLoginController@showLoginForm')->name('customer.login');
            Route::post('login', 'CustomerLoginController@login');
            Route::post('logout', 'CustomerLoginController@logout')->name('customer.logout');

            //Activation
            Route::get('activation/{token}', 'CustomerRegisterController@activation')->name('activation');

            Route::get('password/reset', 'CustomerForgotPasswordController@showLinkRequestForm')->name('customer.password.request');
            Route::post('password/email', 'CustomerForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
            Route::get('password/reset/{token}', 'CustomerResetPasswordController@showResetForm')->name('customer.password.reset');
            Route::post('password/reset', 'CustomerResetPasswordController@reset')->name('customer.password.reset_submit');

            // Registration Routes...
            Route::get('register', 'CustomerRegisterController@showRegistrationForm')->name('customer.register');
            Route::post('register', 'CustomerRegisterController@register');

            //Profile

            Route::get('/profile', 'CustomerProfileController@show')->name('profile');
            Route::patch('/profile/address', 'CustomerProfileController@updateAddress')->name('profile.update.address');
            Route::patch('/profile/email', 'CustomerProfileController@updateEmail')->name('profile.update.email');
            Route::patch('/profile/password', 'CustomerProfileController@updatePassword')->name('profile.update.password');
            Route::patch('/profile/payment', 'CustomerProfileController@updatePayment')->name('profile.update.payment');

            // Orders
            Route::get('/past-orders', 'CustomerOrdersController@index')->name('past-orders');
            Route::get('/past-orders/{order}', 'CustomerOrdersController@order')->name('past-orders.order');
            Route::get('/future-orders', 'CustomerOrdersController@future')->name('future-orders');

            //Rewards
            Route::get('/rewards', 'CustomerProfileController@rewards')->name('rewards');
            Route::post('/rewards/{reward}/claim', 'CustomerProfileController@claimReward')->name('rewards');
            Route::post('/rewards/claim/freebox', 'CustomerProfileController@claimFreebox')->name('rewards');
            Route::post('/rewards/unclaim/freebox', 'CustomerProfileController@unclaimFreebox')->name('rewards');

            //Wholesale
            Route::get('/wholesaler/form', 'WholesalerController@form')->name('wholesaler.form');
            Route::post('/wholesaler/form', 'WholesalerController@checkout')->name('wholesaler.form.checkout');
            Route::get('/wholesaler/create', 'WholesalerController@typeform');
            Route::get('/wholesaler/verify/{email}/{token}', 'WholesalerController@verify')->name('wholesaler.verify');
        });

        Route::namespace ('Products')->group(function () {
            Route::get('/products-feed.xml', 'ProductsController@feed');
            Route::get('/products', 'ProductsController@index')->name('products');
            Route::get('/products/{product}/gcms/{lot?}', 'ProductsController@gcms')->name('products.gcms');
            Route::get('/products/{param}', 'ProductsController@show')->name('products.show');
        });

        Route::namespace ('Categories')->group(function () {

            Route::get('/categories/{category}', 'CategoriesController@show')->name('products.categories');

        });

        Route::namespace ('Cart')->group(function () {
            Route::get('/cart', 'CartController@cart');
            Route::get('/cart/{product}/add', 'CartController@add')->name('cart.add');
            Route::get('/cart/{product}/addplan', 'CartController@addPlan')->name('cart.addplan');
            Route::patch('/cart/{product}/edit', 'CartController@update');
            Route::patch('/cart/shipping', 'CartController@updateShipping');
            Route::post('/cart/promocode', 'CartController@promocode');
            Route::post('/cart/gift-card', 'CartController@giftCard');
            Route::post('/cart/shippingaddress', 'CartController@shippingAddress');
            Route::delete('/cart/promocode', 'CartController@promocodeDelete');
            Route::delete('/cart/gift-card', 'CartController@giftCardDelete');
        });

        Route::namespace ('Checkout')->group(function () {

            Route::get('/checkout', 'CheckoutController@index')->name('checkout');
            Route::get('/checkout/settings', 'CheckoutController@settings');
            Route::post('/checkout', 'CheckoutController@store');
            Route::post('/checkout-validate', 'CheckoutController@check');
            Route::get('thankyou', 'CheckoutController@thankyou')->name('checkout.thankyou');

        });

        Route::namespace('Referrals')->middleware(['auth:customer'])->group(function (){
            Route::get('/pages/referral/invites', 'ReferralController@invites')->name('referral-invites');
            Route::post('/pages/referral/send-invitation', 'ReferralController@sendInvitation')->name('referral-send-invitation');
            Route::get('/pages/invites/{customer_id}', 'ReferralController@getInvites')->name('referral-get-invites');
            Route::get('/pages/invites/conversion/{customer_id}', 'ReferralController@getConversion')->name('referral-get-invites-conversion');
            
            Route::get('/pages/invites/referral_clicks/{customer_id}', 'ReferralController@getReferralClicks')->name('referral-get-invites-referral-click');

            Route::get('/pages/save-invites', 'ReferralController@saveInvites')->name('referral-save-invites');

            Route::post('/pages/save-shares', 'ReferralController@storeShare')->name('referral-save-shares');
            
        });

        Route::namespace ('Subscriptions')->group(function () {

            Route::get('/pages/subscription-box', 'SubscriptionController@show')->name('subscription');
            //Route::get('/pages/subscription-box1', 'SubscriptionController@show')->name('subscription-theme-plans');
            //Route::get('/pages/subscription-box2', 'SubscriptionController@show2')->name('subscription-theme-legacy');
            //Route::get('/pages/subscription-box/plans', 'SubscriptionController@plans')->name('subscription-choose');
            Route::get('/subscribe', 'SubscriptionController@subscribe')->name('subscribe');
            Route::post('/email_subscribe', 'SubscriptionController@emailSubscribe')->name('subscribe.email');

            Route::middleware(['auth:customer'])->group(function () {
                Route::get('/subscription/{monthKey}/', 'SubscriptionController@box')->name('subscription.box');
                Route::get('/subscription/{monthKey}/future', 'SubscriptionController@boxFuture')->name('subscription.box.future');
                Route::post('/subscription/{monthKey}/skip', 'SubscriptionController@skipMonth')->name('subscription.skipMonth');
                Route::post('/subscription/{monthKey}/resume', 'SubscriptionController@resumeMonth')->name('subscription.resumeMonth');
                Route::post('/subscription/{monthKey}/gift', 'SubscriptionController@giftMonth')->name('subscription.giftMonth');
                Route::post('/subscription/gift-card', 'SubscriptionController@applyGiftCard');
                Route::delete('/subscription/gift-card/{subscription_gift_card}', 'SubscriptionController@removeGiftCard');
                Route::post('/subscription/discount', 'SubscriptionController@applyDiscount');
                Route::delete('/subscription/discount', 'SubscriptionController@removeDiscount');
                Route::delete('/subscription/{monthKey}/gift', 'SubscriptionController@giftDelete')->name('subscription.giftMonth.delete');
                Route::post('/subscription/stop', 'SubscriptionController@stop')->name('subscription.stop');
                Route::post('/subscription/resume', 'SubscriptionController@resume')->name('subscription.resume');
                Route::post('/subscription/unpause', 'SubscriptionController@unpause')->name('subscription.unpause');
                Route::post('/subscription/nextbox-continue', 'SubscriptionController@nextboxContinue')->name('subscription.nextbox_continue');
                Route::patch('/subscription/schedule', 'SubscriptionController@updateSchedule')->name('subscription.schedule.update');
                //Route::patch('/subscription/plan', 'SubscriptionController@updatePlan')->name('subscription.plan.update');
            });
        });

        Route::namespace ('ShoppingBoxes')->group(function () {

            Route::middleware(['auth:customer'])->group(function () {
                Route::get('/box/{monthKey}/exchange/{from}', 'ShoppingBoxController@exchange')->name('box.exchange.show');
                Route::patch('/box/{monthKey}/addon/{product}', 'ShoppingBoxController@updateAddon')->name('box.addon.update');
                Route::patch('/box/{monthKey}/exchange/{from}', 'ShoppingBoxController@updateExchange')->name('box.exchange.update');
            });
        });

        //Discounts & Gift card
        Route::namespace ('Discounts')->group(function () {
            Route::get('gift-card/{gift_card}/{token}', 'GiftCardController@show')->name('gift-card');
        });
    });
});
