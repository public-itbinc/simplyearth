<script type="text/javascript">

    $(document).ready(function () {

        tinymce.init({
            selector: '.post-editor',
            plugins: ['paste', 'link', 'autoresize'],
            valid_elements : "*[*]",
            extended_valid_elements : "*[*],script[charset|defer|language|src|type],style",
            custom_elements: "*[*],script[charset|defer|language|src|type],style",
            valid_children : "+body[style],+body[script]",
            verify_html : false,
media_strict: false

        });

        $('.date-picker').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    })

</script>