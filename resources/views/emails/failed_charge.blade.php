@component('mail::message')

Hi, {{ $account->customer->first_name }}!

<p>You know that all we want is for you to have a natural home. And that means not missing out on your essential oil recipe box!</p>

<p>Well, your card was just declined, thus delaying your box. 💔 It's ok! You can fix this! All you need to do is <a href="{{ route('profile') }}">update your details</a>.  😍</p>


@component('mail::center')

![The Box][logo]

[logo]: {{asset('mail/brown-box.jpg')}} "The Box"
@endcomponent


We at Simply Earth are always here to help you out and make your life easier! As soon as you update your card, we'll be sending the box your way! 

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature2')

@endcomponent

P.S. Got questions? Don't hesitate to shoot us an email at help@simplyearth.com 💙

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
