@component('mail::message')

Hi {{$customer->first_name}},

Did you request to reset your password? If you did, click the button below and reset! If you didn't send in the request, just ignore this email.

@component('mail::button', ['url' => route('customer.password.reset', ['token' => $token, 'email' => $customer->email]) , 'color' => 'green']) Reset Password @endcomponent

@component('mail::signature')
@endcomponent

P.S. Now that you can log into your account, start sharing and tracking your referral code to earn free recipe boxes! (coming soon)

@endcomponent

