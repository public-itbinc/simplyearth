@component('mail::message')

Hi, {{ $customer->first_name }}!

We've invited you to create an account at <a href="{{ route('home') }}">SimplyEarth.com</a>. To activate your account and set your password, click the button below:

@component('mail::button', ['url' => $activation->link, 'color' => 'green']) ACTIVATE ACCOUNT @endcomponent

If the account was created in error, you can visit the link above and select 'Decline' to decline this invitation.

Please do not hesitate to give us a call on 866-330-8165 or send an email to help@simplyearth.com if you have any questions.


@component('mail::signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent