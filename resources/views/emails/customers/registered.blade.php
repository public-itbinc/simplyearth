@component('mail::message')

Hi, {{ $customer->first_name }}!

You've made a great choice in signing up with Simply Earth! Congratulations!

We've made it easier for you to be able to do all kinds of amazing things with your new account. 

- Your loyalty drops rewards are all in one place. (coming soon)
- Referring friends and earning FREE monthly boxes has never been easier. (coming soon)
- If you are a recipe box subscriber, you can manage your future orders, gift future orders, and even exchange oils in your box. 

@component('mail::center')

![Hurray][logo]

[logo]: {{asset('mail/welcome.gif')}} "Hurray"
@endcomponent

@component('mail::signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

P.S. Got Questions? Feel free to send us an email at help@simplyearth.com or give us a call (866) 330-8165.

@endcomponent


