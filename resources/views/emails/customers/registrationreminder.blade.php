@component('mail::message')

Hi, {{ $customer->first_name }}!

Thank you for ordering from Simply Earth and making your home more natural. 

You can create an account at SimplyEarth.com to see your order status and get access to our loyalty program. 

[Create your account]({{ route('customer.register') }})

@component('mail::center')

![Hurray][logo]

[logo]: {{asset('mail/yipee.gif')}} "Yipee"
@endcomponent

By creating your account, we will automatically give you 50 drops. "What are drops?" you ask.  They are your Simply Earth reward points. 

300 drops and you earn a free oil!

@component('mail::button', ['url' => route('customer.register') , 'color' => 'green']) Create Account @endcomponent

@component('mail::signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent


