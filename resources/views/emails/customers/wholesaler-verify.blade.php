@component('mail::message')

Hi, {{ $registration->data['first_name'] }}

We've pre-approved your Simply Earth wholesale account but before you can order, you need to activate your account and set your password.   

Please do not hesitate to give us a call on 866-330-8165 or reply to this email if you have any questions. A real person will answer you.

@component('mail::button', ['url' => route('wholesaler.verify', ['email' => $registration->email, 'token' => $registration->token]) , 'color' => 'green']) Activate My Account @endcomponent

Once you activate your account, you will be able to order our starter kit and see wholesaler prices on simplyearth.com. We will be personally reaching out to you to answer any questions you have and we will double check your account to make sure you qualify for wholesale pricing before shipping out your first order. All it takes to qualify is to be a real business. 

We've had 100s of small business add thousands to their bottom line by selling our 100% pure oils.  You can be one of them. 

@component('mail::button', ['url' => route('wholesaler.verify', ['email' => $registration->email, 'token' => $registration->token]) , 'color' => 'green']) Activate My Account @endcomponent

@component('vendor.mail.html.signature-shilah')

@endcomponent

P.S. We love our oils so much, we have backed them with our 100-day guarantee! No risk to you 😍.

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent