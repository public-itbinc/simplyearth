@component('mail::message')

Hi, {{ $gift_card->owner->first_name }}!

<p>Nothing beats a Simply Earth gift card! With it, you'll be able to make your home full of natural goodness.</p> 

<p><b>{{ SEPriceFormat($gift_card->remaining) }}</b> of gift card is headed your way! Treat yourself!</p> 

<p>Here is your code: <span class="gift-card-code"><b>{{ $gift_card->code }}</b></span></p>


@component('mail::center')

![Gift][logo]

[logo]: {{asset('mail/giftcard.jpg')}} "Gift"
@endcomponent


Don't let that collect cobwebs in your account. It's basically {{ SEPriceFormat($gift_card->remaining) }}; so go have fun with it!

@component('mail::button', ['url' => route('products') , 'color' => 'green']) Use Your Gift Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. Make sure to save this email as this is where your code is stored. If you do lose it, our customer success team is always there to help. 

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
