@component('mail::message')

<p>HOORAY!</p>

<p>All the clicking and sharing you've been doing has finally paid off! You’ve successfully referred three of your friends which earns you a FREE Essential Oil Recipe Box.</p><br>

<p>You can claim your reward by visiting your invite friends page. We've created a coupon code just for you!</p> <br>

<p>Don’t forget your coupon code and link!</p><br>


<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/4c94c85e-9bfc-4b80-a34c-ccc2d0a605bd.jpeg" width="589px" height="471px"><br>

<p>But hey, {{ $earn_data['name'] }}, nobody said you have to stop at the first one? We even have subscribers who have over a whole year’s worth of FREE boxes just waiting patiently in their account. That could happen to you too!</p><br>

<p>Here is your code: <strong>{{ $earn_data['code'] }}</strong></p>
<br>

<p>And we’ve made sure to make it super easy for you to share.</p>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Continue Sharing! @endcomponent

<p>Till next time!</p>
<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/5bf84f1f-392e-4407-998f-b614dd59059d.jpeg" width="140px">

<i>Katie & Glo - Chief Recipe Makers, Simply Earth</i><br>

@endcomponent
