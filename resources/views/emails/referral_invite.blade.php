@component('mail::message')

<h1><strong>{{ $referral_data['name'] }} has gifted you a $20 eGift card</strong></h1> 

<p>{{ $referral_data['name'] }} is already making their home natural with 4 essential oils plus extras each month.</p> 

<p>Use <span class="gift-card-code"><b>{{ $referral_data['code'] }}</b></span> code at checkout to get your $20 eGift card when you subscribe. <b>Just use it before "30 days from when the person signed up".</b></p>

@component('mail::button', ['url' => route('subscription', ['discount' => $referral_data["code"], 'utm_source' => 'referral', 'utm_medium' => 'affiliate', 'utm_campaign' => 'invitefriends', 'utm_content' => 'emailfooter']) , 'color' => 'green']) Claim Your $20 Gift Card @endcomponent

<p>Till next time!</p>
![Gift][logo]

[logo]: {{asset('mail/logo.jpg')}} "Gift"

<i>Simply Earth - Helping you make your home fun & natural.</i>

P.S. Don't let {{ $referral_data['name'] }} down. They want to make all natural recipes with you :) 

@endcomponent
