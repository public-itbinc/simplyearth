@component('mail::message')

Hi, {{ $account->customer->first_name }}!

We've just updated our website, simplyearth.com so that we can provide you better value!  

What that means is that we need you to update your password in order to log on!

<a href="{{ route('customer.password.request') }}">Update My Password</a>


@component('mail::center')

![Stripes][logo]

[logo]: {{asset('mail/stripes.jpg')}} "Stripes"
@endcomponent


When our loyalty program launches in September 2018, we will automatically give you 50 drops. "What are drops?" you ask.  They are your Simply Earth reward points. 

300 drops and you earn a free oil!

@component('mail::button', ['url' => route('customer.password.request') , 'color' => 'green']) Update My Password @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. Not sure where to start with Essential Oils?  Check out our Essential Oils for Beginners book!

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
