@component('mail::message')

Hi, {{ $account->customer->first_name }}!

<p>Here's the thing - your card has been declined a couple of times, and because of that your Simply Earth wholesale account is now inactive.</p>

<p>Don't panic! We're here to make your life easier. Click the button below to update your card so you can continue adding to your bottom line and serving your customers with our pure essential oils!</p>


@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature-shilah')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
