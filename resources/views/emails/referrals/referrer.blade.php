@component('mail::message')

Congratulations,

{{ $customer->first_name }} has invited you to join in on all the Simply Earth Essential Oil Recipe Box Fun and is giving you your first month free when you <a href="{{ $customer->share_link }}">sign up today!</a>

@component('mail::center')

![Celebrate][logo]

[logo]: {{asset('mail/celebrate.gif')}} "Celebrate"

@endcomponent

@component('mail::button', ['url' => $customer->share_link , 'color' => 'green']) Accept The Invitation! @endcomponent

What's in a Simply Earth Recipe Box you ask? 

* Four full-sized bottles of essential oil. That's an over $150 value when compared to the big oil companies.
* Fun extras like dryer balls, melt and pour soap, and kaolin clay!
* Your first month FREE (just cover shipping the first month).

Don't forget to use your friend's code <strong>{{ $customer->share_code }}</strong> when you subscribe so you get your first month free with a multi-monthly plan.

@component('mail::button', ['url' => $customer->share_link , 'color' => 'green']) Accept The Invitation! @endcomponent

Just use it before "30 days from when your friend signed up". 

@component('vendor.mail.html.signature2')

@endcomponent

P.S. Got questions? Feel free to shoot us an email and we will take care of you!

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent