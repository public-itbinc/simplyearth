@component('mail::message')

Hi, {{ $account->customer->first_name }}!

<p>You know that all we want is for you to serve your customer be selling them pure essential oils. And that means you need to be able to purchase more.</p>

<p>Well, your card was just declined for your starter kit payment plan, thus holding you back from purchasing more oils at wholesale prices. 💔 It's ok! You can fix this! All you need to do is <a href="{{ route('profile') }}">update your details</a>.  😍</p>


We at Simply Earth are always here to help you out and make your life easier! As soon as you update your card, we'll activate your wholesale account again.

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature-shilah')

@endcomponent

P.S. What else can they do to be successful.

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
