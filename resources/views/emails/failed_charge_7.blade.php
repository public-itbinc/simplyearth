@component('mail::message')

Hi, {{ $account->customer->first_name }}!

Here's the thing - your card has been declined a couple of times, and because of that your subscription is now inactive. 

Don't panic! We're here to make your life easier. Click the button below to update your card and get your all-natural essential oil goodness shipped right to your door!

@component('mail::center')

![Box with flower][logo]

[logo]: {{asset('mail/box-with-flower2.jpg')}} "Box with flower"
@endcomponent


If you have had any sort of issues with us in the past and don't want your subscription anymore, talk to us. Chances are there's something that we can do about it. Our customer service team is awesome. Send us an email at help@simplyearth.com with any concerns. We're here to help!

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
