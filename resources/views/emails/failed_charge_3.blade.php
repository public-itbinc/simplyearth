@component('mail::message')

Hi, {{ $account->customer->first_name }}!

Are you missing your box? We have it all set and ready to go! If you're wondering why you haven't received it is because the card we have on file has been declined ☹️

No worries though because we've decided to send 25 FREE bonus DROPS just for updating your card. We'll try to process the payment tomorrow and send out your box as soon as we can. Just let us know if that is okay!


@component('mail::center')

![Oil set][logo]

[logo]: {{asset('mail/oil-set.jpg')}} "Oil set"
@endcomponent


Update your card and get those free oils!

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. If you have any questions, feel free to send them our way. 

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
