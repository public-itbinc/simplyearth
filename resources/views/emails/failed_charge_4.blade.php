@component('mail::message')

Hi, {{ $account->customer->first_name }}!

We just wanted to let you know that your card was declined again when we were trying to process your monthly box. Unfortunately your Simply Earth Essential Oil Recipe box hasn't been shipped yet! 

It's okay; we completely understand that life can get a little overwhelming sometimes. Things like these can be overlooked, which is why we're giving you a little gift for the extra effort putting into updating your card.

@component('mail::center')

![Box with flower][logo]

[logo]: {{asset('mail/box-with-flower.jpg')}} "Box with flower"
@endcomponent


We want to help you have an all-natural chemical-free home!

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. We'll be trying your card again in a week. Just let us know if there is anything we can do to help!

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
