@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

You have successfully stopped your Essential Oil Recipe Box subscription until {{ $stopped_date->format('F dS') }}. That means your next box of happiness and all-natural goodness will ship on {{ $continue_date->format('F dS') }}!

You can <a href="{{ route('profile') }}">login to your account</a> to get your box sooner or to make changes to your subscription at any time!

Please contact our customer service team with any of your questions at help@simplyearth.com. We're here for you!

@component('mail::button', ['url' => route('profile'), 'color' => 'green']) Login To Your Account @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent