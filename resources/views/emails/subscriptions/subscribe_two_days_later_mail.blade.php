@component('mail::message')

Hi, {{ $subscriber_data['name'] }}!

<p>Did you know you can earn a FREE Essential Oil Recipe box just by referring three friends?</p>
<p>They get a $20 gift card when they subscribe. They just need to use your coupon code: </p>
<p>{{ $subscriber_data['code'] }}</p><br>

<p>We made this little video if you still have questions. </p>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/0b928e1f-1ff1-4699-83b2-897cba1bcda0.gif" width="480px"><br>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Start Earning Free Boxes @endcomponent

<p>Till next time!</p>
![Gift][logo]

[logo]: {{asset('mail/logo.jpg')}} "Gift"

<p><i>Simply Earth - Helping you make your home fun & natural. </i></p><br>

<p>P.S. There is no limit on the number of FREE boxes you can earn. Share you code today!</p><br>

@endcomponent
