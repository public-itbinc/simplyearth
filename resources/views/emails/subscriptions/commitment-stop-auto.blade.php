@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

You have successfully stopped your auto-renew. Your last box will ship on {{ $subscription->commitment_last_box->getDate()->format('F dS') }}

And as always, we’re here to help and make your life more natural! Please don't hesitate to reach out to us with questions or concerns at help@simplyearth.com. 

@component('vendor.mail.html.signature2')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
