@component('mail::message')

Hi, {{ $subscriber_data['name'] }}!

<b>This is it-- the last week for the bonus referral month at Simply Earth!</b>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/aaa55848-caff-4b7e-b2cd-4c2f9ef3c971.png" width="582px"><br>

<p>Remember, <b>for every friend that subscribed to our recipe box with your code, you immediately get a free box</b> credited to your account! One for One. Unlimited. You could earn years of free boxes!</p><br>

<p>Here is a link you can share: {{ $subscriber_data['link'] }}. It will automatically add your referral code for your friends which gives them a $20 eGift card after they subscribe. A pretty great deal for them too!</p>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Share Your Code @endcomponent

<p>Till next time!</p>
![Gift][logo]

[logo]: {{asset('mail/logo.jpg')}} "Gift"

<p><i>Simply Earth - Helping you make your home fun & natural. </i></p><br>

<p>P.S. Don't forget to remind your friends of your unique coupon code in person! I've found this is the best! INSERT COUPON CODE</p><br>

@endcomponent
