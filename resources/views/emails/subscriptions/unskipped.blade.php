@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

You have successfully unskipped your Essential Oil Recipe Box! We are doing a little dance! That means your next box of happiness and all-natural goodness will ship on: {{ $subscription->owner->nextBox()->getDate()->format('F dS') }}!

@component('mail::center')

![Hurray][logo]

[logo]: {{asset('mail/welcome.gif')}} "Hurray"
@endcomponent

Your home is going to love you for getting your monthly box!

If you have questions, please contact our customer service team with any of your questions at help@simplyearth.com. We're here for you! 

@component('vendor.mail.html.signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent