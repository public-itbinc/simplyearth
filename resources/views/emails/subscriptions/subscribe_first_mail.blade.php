@component('mail::message')

Hi, {{ $subscriber_data['name'] }}!

<b>BONUS!!! INVITE YOUR FRIENDS. EARN A FREE BOX. </b>
<p>For 30 days only: for every friend you refer to our Essential Oil Recipe Box that uses your code or link to subscribe, you immediately get a free recipe box credited to your account. How awesome is that! </p><br>

<p>Just give your friends your referral code and they will receive a $20 eGift card emailed to them once they've subscribed.</p> <br>

<p>{{ $subscriber_data['code'] }}</p><br>

<p>But don't wait too long, you only have until the end of this month for this super-powered bonus.</p><br>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/aaa55848-caff-4b7e-b2cd-4c2f9ef3c971.png" width="582px"><br>

<p>Once this one-month countdown is up, you'll earn  FREE Essential Oil Recipe Box for every 3 friends you invite. People have earned over a year of free boxes from that offer, imagine how many free boxes you could earn...</p><br>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Share Your Code @endcomponent

<p>Till next time!</p>
![Gift][logo]

[logo]: {{asset('mail/logo.jpg')}} "Gift"

<p><i>Simply Earth - Helping you make your home fun & natural. </i></p><br>
<p>P.S. Don't be afraid to invite a lot of people. Your friends will thank you for their natural homes. {{ $subscriber_data['code'] }}</p><br>

@endcomponent
