@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

You have successfully RESUMED your Essential Oil Recipe Box! We are doing a little dance here at Simply Earth. 
That means your next box of happiness and all-natural goodness will ship on {{ $subscription->owner->nextBox()->getDate()->format('F dS') }}.

@component('mail::center')

![Yipee][logo]

[logo]: {{asset('mail/yipee.gif')}} "Yipee"
@endcomponent

@component('mail::button', ['url' => route('home'), 'color' => 'green']) Get Ready For Natural Goodness! @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
