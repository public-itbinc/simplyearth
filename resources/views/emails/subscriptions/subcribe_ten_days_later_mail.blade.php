@component('mail::message')

What's up, {{ $subscriber_data['name'] }}?!

<p>Can you believe that hundreds of free boxes have been sent out by the Simply Earth team thanks to our referral system?  All our amazing subscribers had to do was just share and after three friends subscribe, SURPRISE a FREE BOX on your doorstep!</p><br>

<ul>
	<li>You get a FREE Essential Oil Recipe Box for every 3 friends you refer.</li>
	<li>Your friends each get a FREE $20 gift card when they use your coupon code.</li>
</ul>


<p>Simply share your coupon code with your friends: {{ $subscriber_data['code'] }}</p><br>

<p>Now sit back, relax, and wait for those FREE boxes to start coming in!</p><br>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/e9ba2ca4-5d00-4512-b9be-bc1c633cc785.gif" width="480px"><br>

<p>Achieving a pure and natural home has never been easier, especially considering how it literally costs you nothing but a few clicks and shares!</p><br>

<p>So, what are you waiting for? The sooner you start making an effort to achieve that natural home, the better!</p><br>
<p>Get your friends to use your coupon code at checkout  😊 😊</p><br>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) 
Share To Friends! @endcomponent

<p>Till next time!</p>
<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/5bf84f1f-392e-4407-998f-b614dd59059d.jpeg" width="140px"><br>
<p><i>Katie & Glo - Chief Recipe Makers, Simply Earth</i></p><br>

<p>P.S. UNLIMITED free boxes if you keep on referring your friends!</p><br>

@endcomponent
