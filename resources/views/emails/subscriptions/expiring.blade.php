@component('mail::message')

Hi, {{ $account->customer->first_name }}!

The credit card you registered in our files is expiring within this month!

To ensure that there is no delay in your subscription, please update your card here! We don't want your 100% pure oils to get to you late.

<a href="{{ route('profile') }}">Update your card</a>.

@component('mail::center')

![Sad Owl][logo]

[logo]: {{asset('mail/owl.jpg')}} "Sad Owl"
@endcomponent

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update Your Card Today @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. Reply to this email if you have any questions! We're here to help!

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
