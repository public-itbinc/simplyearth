@component('mail::message')

Hi, {{ $subscriber_data['name'] }}!

<p>Just 2 days left to <b>earn a free recipe box for every friend you refer</b> to Simply Earth. That's still a lot of time to share your referral code with your friends. It also gives them a $20 eGift card when they subscribe. </p>

<p>{{ $subscriber_data['code'] }}</p><br>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/aaa55848-caff-4b7e-b2cd-4c2f9ef3c971.png" width="582px"><br>

<p>It's a win-win! Imagine the fun you guys can have sharing natural home tips and tricks when using the Simply Earth Recipe Box.</p><br>

<p>Plus you'll get a bunch of free recipe boxes when they subscribe 😉</p>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Invite Your Friends @endcomponent

<p>Till next time!</p>
![Gift][logo]

[logo]: {{asset('mail/logo.jpg')}} "Gift"

<p><i>Simply Earth - Helping you make your home fun & natural. </i></p><br>

<p>P.S. You can earn an unlimited number of free boxes!</p><br>

@endcomponent
