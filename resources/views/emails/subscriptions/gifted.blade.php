@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

{{ $gift->first_name }} is going to be so excited that you decided to gift this monthly Essential Oil Recipe Box. 💝

As per your decision, your box will be shipped to {{ $gift->first_name }} on {{ $gift->gift_date->format('F dS') }}. {{ $gift->first_name }} will also be receiving an email on {{ $gift->email_date->format('F dS') }} letting him/her know the all-natural goodness that will come in the Essential Oil Recipe Box!

@component('mail::center')

![Celebrate][logo]

[logo]: {{asset('mail/celebrate.gif')}} "Celebrate"
@endcomponent

We know they are gonna love the box as much as you do, and will love you even more for being such an awesome friend!

@component('vendor.mail.html.signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent