@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

You have successfully skipped your Essential Oil Recipe Box scheduled for {{ $old_box }}. Your next monthly box of essential oil goodness will ship on: {{ $new_box->getDate()->format('F dS') }}!

You can <a href="{{ route('profile') }}">login to your account</a> to get your pure essential oils sooner or to make changes to your plan at any time!

Please contact our customer service team if you have any questions. help@simplyearth.com We're here to help! 😊

@component('mail::button', ['url' => route('profile'), 'color' => 'green']) Login To Your Account @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent