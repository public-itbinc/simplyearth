@component('mail::message')
# Thank you for subscribing

@component('mail::button', ['url' => route('profile')])
Manage Subscription
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
