@component('mail::message')

Hi, {{ $subscriber_data['name'] }}!

<p>The first week of the Bonus Referral Month at Simply Earth  has just passed, where for every friend you refer to our recipe box, you get a free box credited to your account! One for one. That has a lot of earning potential. </p>

<p>Keep sharing your referral code to your friends. Along with getting you a free box, it gives your friends a $20 eGift card after they subscribe too. A pretty great deal for the both of you, huh?</p><br>

<p>{{ $subscriber_data['code'] }}</p><br>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/aaa55848-caff-4b7e-b2cd-4c2f9ef3c971.png" width="582px"><br>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Share Your Code @endcomponent

<p>{{ $subscriber_data['code'] }}</p><br>

<p>Till next time!</p>
![Gift][logo]

[logo]: {{asset('mail/logo.jpg')}} "Gift"

<p><i>Simply Earth - Helping you make your home fun & natural. </i></p><br>
<p>P.S. Here is a ready-to-share FB post too. We are here to help!</p><br>

@endcomponent
