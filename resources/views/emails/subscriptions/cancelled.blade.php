@component('mail::message')

Hi, {{ $subscription->owner->customer->first_name }}!

We're sorry to see you go but we hope to have you back as a subscriber soon!

And as always, we’re here to help and make your life more natural! Please don't hesitate to reach out to us with questions or concerns at help@simplyearth.com. 

@component('vendor.mail.html.signature2')

@endcomponent

P.S. Miss us already? It's okay, we don't blame you  😋  <a href="{{ route('home') }}">Click here</a> to resubscribe today.

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
