@component('mail::message')

Hey, {{ $gift_card->owner->first_name }}!

With an updated simplyearth.com website, we needed to reissue giftcards to everyone. So on this email, you'll see your new gift card number! If you have questions, just reply to this email and we will help you out!

<b>{{ SEPriceFormat($gift_card->remaining) }}</b> of gift card is headed your way! Treat yourself!

<p>Here is your code: <span class="gift-card-code"><b>{{ $gift_card->code }}</b></span></p>


@component('mail::center')

![Gift][logo]

[logo]: {{asset('mail/giftcard.jpg')}} "Gift"
@endcomponent


Don't let that collect cobwebs in your account.

@component('mail::button', ['url' => route('products') , 'color' => 'green']) Use Your Gift Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. Make sure to save this email as this is where your code is stored. If you do lose it, our customer success team is always there to help. 

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
