@component('mail::message')

Hi, {{ $order->customer->first_name }}!

Natural goodness is on its way! Get your DIY hacks ready because your home is about to become all-natural!

<a href="{{ route('past-orders') }}">Track package</a>


@component('vendor.mail.html.order_table', ['order' => $order]) @endcomponent


If you haven't joined yet, we have an awesome <a href="www.facebook.com/fromsimplyearth/">facebook page</a> where share extra recipes, and answer any questions you might have! It's the best community!

@component('mail::button', ['url' => 'https://www.facebook.com/fromsimplyearth/', 'color' => 'green']) Join Our Facebook Community @endcomponent

@component('vendor.mail.html.signature2')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
