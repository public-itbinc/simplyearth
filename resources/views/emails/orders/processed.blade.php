@component('mail::message')

Hi, {{ $order->customer->first_name }}!

Thank you so much for your purchase. 😍 We couldn’t be more excited about you joining us on your journey towards the all-natural home, as well as helping us fight human trafficking!

You can see your Order **#{{ $order->id }}** below.


@component('vendor.mail.html.order_table', ['order' => $order]) @endcomponent


If you ever need anything, just reply to this email or give us a call at 866.330.8165.

@component('vendor.mail.html.signature2')

@endcomponent

P.S. I'm so excited that you are filling up your home with natural products!

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
