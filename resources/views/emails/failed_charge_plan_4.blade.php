@component('mail::message')

Hi, {{ $account->customer->first_name }}!

<p>Please don't feel embarrassed, but we tried your card on file again and it didn't go through for the starter kit payment plan.</p>

<p>Go update your card today so we can reactivate your Simply Earth wholesale account.</p>


@component('mail::center')

![The Box][logo]

[logo]: {{asset('mail/box-with-flower3.jpg')}} "The Box"
@endcomponent


Please don't hesitate to email us at wholesale@simplyearth.com if you have any questions about your recurring orders.

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature-shilah')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
