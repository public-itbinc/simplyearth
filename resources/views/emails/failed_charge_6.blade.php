@component('mail::message')

Hi, {{ $account->customer->first_name }}!

Please don't feel embarrassed, but we tried your card on file again and it didn't go through.

The is the last chance for you to update your card info and get your monthly box. If you don't you'll have to start all over again and it'll be confusing on your end and on our end. Do us a favor and update your card information for us, please?  We'll take care of the rest and make sure you achieve your natural and toxin-free home. 

@component('mail::center')

![Box with flower][logo]

[logo]: {{asset('mail/fun.jpg')}} "Fun"
@endcomponent


Please don't hesitate to email us at help@simplyearth.com if you have any questions about your recurring orders.

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

P.S. This is your last chance to get Essential Oil Goodness! Update your card today! 

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
