@component('mail::message')

Hi, {{ $reward_data['name'] }}!

<p>WE HEARD THE GOOD NEWS!</p>
<p>One of your friends subscribed to Simply Earth’s recipe box with your referral code. This means you’re just {{ $reward_data['remain_referral'] }} of referrals away from a FREE BOX!</p><br>

<p>Now, what are you waiting for, share away.</p> <br>

<p>Don’t forget your coupon code and <a href="{{route('subscription', ['discount' => $reward_data['code'], 'utm_source' => 'referral', 'utm_medium' => 'affiliate', 'utm_campaign' => 'invitefriends', 'utm_content' => 'emailfooter']) }}">link!</a></p><br>

<p>Code: {{ $reward_data['code'] }}</p><br>

<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/99dde993-ba97-400c-80aa-6bd753d51539.gif" width="480px"><br>

<p>What’s better than a pure and natural home? A FREE one!</p><br>
<ul>
	<li>You get a FREE Essential Oil Recipe box for every 3 friends you refer</li>
	<li>PLUS your friends get a FREE $20 gift card each when they subscribe!</li>
</ul>
<br>
<p>Don’t forget your code: </p>

@component('mail::button', ['url' => route('referral-invites'), 'color' => 'green']) Refer Friends @endcomponent

<p>Till next time!</p>
<img src="https://d3k81ch9hvuctc.cloudfront.net/company/h5X6XF/images/5bf84f1f-392e-4407-998f-b614dd59059d.jpeg" width="140px">

<p>Katie & Glo - Chief Recipe Makers, Simply Earth</p><br>
<p>P.S. UNLIMITED free boxes if you keep on referring your friends!</p><br>
<p>P.S.S. We make it hassle-free to track your rewards with our easy to use referral page.</p>

@endcomponent
