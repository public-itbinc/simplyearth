@component('mail::message')

Hi, {{ $gift->first_name }}!

Your friend, {{ $subscription->owner->customer->first_name }} wanted to let you know just how much (s)he appreciates you and your friendship, so (s)he's decided to send over an Essential Oil Recipe Box!

Here's a little message from {{ $subscription->owner->customer->first_name }}

{{ $gift->message }}

@component('mail::center')

![Celebrate][logo]

[logo]: {{asset('mail/celebrate.gif')}} "Celebrate"
@endcomponent

Don't forget to review your existing info to make sure that we have your correct email and shipping addresses.  We want to make sure you get your box and exclusive subscriber offers every month. 💌

Just to get you psyched, here's what's in the Recipe Box:

4 Bottles of Essential Oils
Fun Extras
Recipes to Make Your Home Natural
I hope you're excited--because I am! Simply Earth's Essential Oil Recipe Box is well on its way to help you make your home pure and natural, just the way it should be. 🌿

@component('vendor.mail.html.signature2')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent