@component('mail::message')

Hi, {{ $account->customer->first_name }}!

Your credit card on file has been declined!! We want to send you your monthly essential oil box, but we can't send it until you updated your information :( 


@component('mail::center')

![Dog][logo]

[logo]: {{asset('mail/dog.jpg')}} "Dog"
@endcomponent


But don't be embarrassed; this happens a lot! You can easily avoid this mishap from happening again. Just update your credit card info here. 

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Manage My Credit Card Info @endcomponent

@component('vendor.mail.html.signature2')

@endcomponent

P.S. Having a hard time? Just send your questions our way! We're here to help. 

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
