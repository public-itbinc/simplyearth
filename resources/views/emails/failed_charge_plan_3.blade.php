@component('mail::message')

Hi, {{ $account->customer->first_name }}!

<p>We just wanted to let you know that your card was declined again when we were trying to process your starter kit payment plan. Unfortunately, that means you can't order more oils!</p>

<p>It's okay; we completely understand that life can get a little overwhelming sometimes. Things like these can be overlooked. Just <a href="{{ route('profile') }}">update your details</a> and all is well 😍</p>


We want to help you have an all-natural chemical-free home!

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature-shilah')

@endcomponent

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
