@component('mail::message')

Hi, {{ $account->customer->first_name }}!

You're running out of time. Don't miss out on making your home all-natural and chemical-free.

Go update your card today so we can get your next box shipped to you!

@component('mail::center')

![Box with flower][logo]

[logo]: {{asset('mail/box-with-flower3.jpg')}} "Box with flower"
@endcomponent


We can only do so much for you to update your card. Meet us halfway and click that button 💕

@component('mail::button', ['url' => route('profile') , 'color' => 'green']) Update My Card @endcomponent

@component('vendor.mail.html.signature')

@endcomponent

​P.S. All you have to do is update your card and we'll take care of the rest.

@component('mail::footer')
Simply Earth W4228 Church Rd, Waldo Wisconsin 53093 United States
@endcomponent

@endcomponent
