<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title',config('app.name', 'Simply Earth Laravel'))</title>
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}"> @stack('meta')
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff"> @php \JavaScript::put([ 'user_tags'
	=> customer() ? [array_merge(['default'], customer()->tags->pluck('name')->toArray())] : ['default'] ]); 
@endphp

	<!-- Styles -->
	<link href="{{ mix('css/app.css') }}" rel="stylesheet"> @if(env('APP_ENV')=='production')
	@include('frontend.layouts.googletag_head') @endif
	@include('frontend.layouts.javascript')
	@include('frontend.layouts.meta')
</head>

<body class="@yield('body_class', 'bg-default')" style="overflow-x: hidden;">

	@if(env('APP_ENV')=='production')
	@include('frontend.layouts.googletag_body') @endif
	@if(isset($_GET['t']) && $_GET['t'] === 'true')
		@include('frontend.pages.timer-include')
	@endif
	<div class="site" id="app">
		<popups></popups>
	@include('flash::message') @yield('content') @yield('sales-footer')
	</div>
	<!-- .wrapper -->

	@yield('modals')

	<!-- TYPEKIT -->
	<script type="text/javascript">
		try {
			Typekit.load();
		} catch(e){}
	</script>


	
@section('footer_base_scripts') @push('footer_scripts')
	<script src="{{ mix('js/app.js') }}"></script>


	
@endpush @stack('footer_scripts') @show @yield('js')
	@include('frontend.layouts.klaviyo')
	@if(in_array(Route::currentRouteName(),['products.show', 'products','products.categories',
	'wholesaler.sales']))

	<script type="text/javascript" src="https://cdn-stamped-io.azureedge.net/files/widget.min.js"></script>
	<script type="text/javascript">
		//<![CDATA[ 
	StampedFn.init({ apiKey: '{{ env("STAMPED_CLIENT_ID","") }}', storeUrl: '{{ env("STAMPED_STOR_URL","") }}' }); 
	// ]]>
	</script>

	@endif @if(in_array(Route::currentRouteName(),['products.show']))
	<script>
		$(document).ready(function(){
			if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
				return $('a[data-toggle="tab"]').on('shown', function(e) {
				return location.hash = $(e.target).attr('href').substr(1);
			});
		})
			@if(isset($product))
			$(document).on('stamped:reviews:loaded', function(){
				var ratingCount = $('meta[itemprop="reviewCount"]').attr('content');
				var ratingValue = $('meta[itemprop="ratingValue"]').attr('content');
				
				var price = "{{ $product->price }}";
				var richSnippet = {
				"@context": "http://schema.org",
				"@type": "Product",
				"name": "{{ $product->name }}",
				"image": "{{ $product->cover }}",
				"brand": {
				"@type": "Thing",
				"url": "{{ route('home') }}",
				"name": "Simply Earth"
				
				}
				}
				
				if (price) {
				richSnippet.offers = {
				"@type": "Offer",
				"price": price,
				"priceCurrency": "USD",
				"itemCondition": "https://schema.org/NewCondition",
				}
				}
				
				if (parseInt(ratingValue) > 0){
				richSnippet.aggregateRating = {
				"@type": "AggregateRating",
				"ratingValue": ratingValue,
				"reviewCount": ratingCount
				}
				}
				var script = document.createElement('script');
				script.type = 'application/ld+json';
				script.innerHTML = JSON.stringify(richSnippet);
				document.getElementsByTagName('head')[0].appendChild(script);
				})

				@endif
	</script>
	@endif	
	@include('frontend.layouts.refersion')
</body>

</html>
