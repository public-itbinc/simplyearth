<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}"> @stack('meta')
	<title>@yield('title',config('app.name', 'Simply Earth Laravel'))</title>
	<!-- Styles -->
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
	@if(env('APP_ENV')=='production')
		@include('frontend.layouts.googletag_head')
	@endif
</head>

<body class="@yield('body_class','bg-default')">
	@include('frontend.layouts.loginas')
	@if(env('APP_ENV')=='production')
		@include('frontend.layouts.googletag_body')
	@endif
	<div class="site" id="app">
		@yield('header')
		<popups></popups>
		<div class="container">
			<div class="flash-message-container">
	@include('flash::message')
			</div>
		</div>
		@yield('content')
	</div>

	<div class="container">
		<footer class="plain-footer">
			<div class="text-center">
				<ul class="footer-links">
					<li>&copy; Simply Earth. All Rights Reserved</li>
					<li>By  placing this order, you are committing to the length of your Recipe Box plan and agree the <a href="/pages/terms-of-service">Terms & Conditions</a> & <a href="/pages/privacy-policy">Privacy Policy</a></li>
				</ul>
			</div>

		</footer>
	</div>
	<!-- .wrapper -->
	<!-- TYPEKIT -->
	<script type="text/javascript" src="//use.typekit.net/hkd3jik.js"></script>
	<script type="text/javascript">
		try{Typekit.load();}catch(e){}
	</script>





	
@section('footer_base_scripts') @push('footer_scripts')
	<script src="{{ mix('js/app.js') }}"></script>




	
@endpush @stack('footer_scripts') @show
	@include('frontend.layouts.klaviyo')
	@include('frontend.layouts.refersion')
</body>

</html>