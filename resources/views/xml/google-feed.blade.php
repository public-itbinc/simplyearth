
@php echo '
<?xml version="1.0" encoding="UTF-8"?>'; @endphp
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
    <channel>
        <title>Simply Earth Products Feed</title>
        <description>Simply Earth Products RSS Feed</description>
        <link>https://simplyearth.com</link>
        @foreach ($products as $product)
        <item>
            <g:id>{{ $product->sku }}</g:id>
            <title>{{ $product->name }}</title>
            <description>{{ $product->short_description }}</description>
            <link>{{ $product->link() }}</link>
            <g:price>{{ $product->price }} USD</g:price>
            <g:condition>new</g:condition>
            <g:availability>in stock</g:availability>
            <g:image_link>{{ asset($product->cover) }}</g:image_link>
            <g:google_product_category>6104</g:google_product_category>
            <g:product_type>{{ $product->default_category->name }}</g:product_type>
            <g:brand>Simply Earth</g:brand>
            <g:mpn>{{ $product->sku }}</g:mpn>
        </item>
        @endforeach
    </channel>
</rss>