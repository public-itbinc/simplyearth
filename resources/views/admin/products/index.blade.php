@extends('admin.dashboard')

@section('title', 'Products')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Product List</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li class="active">
                <strong>Product list</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

            
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">


<div class="ibox-content m-b-sm border-bottom">
    <div class="row">
        <div class="col-sm-6">
            <form action="" class="form-inline">
                <div class="form-group">
                    <?= Form::text('q', null, ['class' => 'form-control','placeholder' => 'Product Name or SKU']); ?>
                </div>
                <div class="form-group">

                    <?= Form::select('status', collect(\App\Shop\Products\Product::statuses())->prepend('All status',''), null, ['class' => 'form-control']); ?>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
        <div class="col-sm-6">
            <div class="text-right">
                    <a href="{{ url('/admin/products/create') }}/" class="btn btn-primary"><i class="fa fa-plus"></i> New Product</a>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">

                <table id="product-list" class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                    <thead>
                    <tr>

                        <th>Image</th>
                        <th data-toggle="true">Product Name</th>
                        <th data-toggle="true">Type</th>
                        <th data-toggle="true">SKU</th>
                        <th data-hide="phone">Price</th>
                        <th data-hide="phone,tablet" >Quantity</th>
                        <th data-hide="phone">Status</th>
                        <th class="text-right" data-sort-ignore="true">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($products as $product)    

                    <tr>
                        <td>
                            @if($cover = $product->image('thumb'))
                            <img class="img-sm" src="{{ $cover }}">
                            @endif
                        </td>
                        <td>
                           {{ $product->name }}
                        </td>
                        <td>
                            {{ $product->type }}
                        </td>
                        <td>
                            {{ $product->sku }}
                        </td>
                        <td>
                            {{ $product->priceString }}
                        </td>
                        <td>
                            {{ $product->quantity }}
                        </td>
                        <td>
                            @include('admin.products._status')
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <a href="{{ $product->link() }}" target="_blank" class="btn-white btn btn-xs">View</a>
                                <a href="{{ $product->editLink() }}" class="btn-white btn btn-xs">Edit</a>
                            </div>
                        </td>
                    </tr>

                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="6">
                            <div class="pull-right">
                                {{ $products->appends(request()->query())->links()  }}
                            </div>
                        </td>
                    </tr>
                    </tfoot>

                </table>

            </div>
        </div>
    </div>
</div>


</div>

@endsection

@section('footer_base_scripts')
<script src="{{ mix('js/admin-products.js') }}"></script>
@endsection
