<div class="row" id="product-edit-container">
    <div class="col-lg-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#info">Product info</a></li>
                <li class=""><a data-toggle="tab" href="#data">Data</a></li>
                <li class=""><a data-toggle="tab" href="#visibility">Visibility</a></li>
                <li class=""><a data-toggle="tab" href="#seo">SEO</a></li>
                @if(isset($product->id))
                <li class=""><a data-toggle="tab" href="#images">Product Images & Videos</a></li>
                @endif @if(isset($product->setup) && $product->setup=='variable')
                <li class=""><a data-toggle="tab" href="#variants">Variants</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="visibility" class="tab-pane">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Product only visible for customers with tags:</label>
                                <div class="col-sm-10">
                                    <tags-input :tags={{ $tags }}></tags-input>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="info" class="tab-pane active">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('name', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('slug', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">SKU:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('sku', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Price:</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <?= Form::text('price', null, ['class' => 'form-control', 'placeholder' => '0.00']) ?>
                                    </div>
                                </div>
                            </div>

                            <wholesale-pricing :init="{{ $wholesale->toJson() }}"></wholesale-pricing>

                        <product-plans :available_plans="{{ isset($product) ? collect($product->available_plans)->toJson() : json_encode([]) }}"></product-plans>

                            <div class="form-group"><label class="col-sm-2 control-label">Short Description:</label>
                                <div class="col-sm-10">
                                    <?= Form::textarea('short_description', null, ['class' => 'form-control', 'rows' => 5]) ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                <div class="col-sm-10">
                                    <?= Form::textarea('description', null, ['class' => 'form-control post-editor']) ?>
                                </div>
                            </div>

                            <product-setup></product-setup>
                            <subscription-input :subscription_type="'{{ isset($product) && $product->type ? $product->type : 'default' }}'" :subscription_months="'{{ isset($product) && $product->subscription_months ? $product->subscription_months : 1 }}'">
                            </subscription-input>

                            <div class="form-group"><label class="col-sm-2 control-label">Status:</label>
                                <div class="col-sm-2">
                                    <?= Form::select('status', \App\Shop\Products\Product::statuses(), null, ['class' => 'form-control']); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="data" class="tab-pane">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Quantity:</label>
                                        <div class="col-sm-10">
                                            <?= Form::number('quantity', null, ['class' => 'form-control', 'step' => 1, 'min' => 0]) ?>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Sort order:</label>
                                        <div class="col-sm-10">
                                            <?= Form::number('sort_order', null, ['class' => 'form-control', 'step' => 1, 'min' => 0]) ?>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Quality Certification Title:</label>
                                        <div class="col-sm-10">
                                            <?= Form::text('quality_certification_title', null, ['class' => 'form-control']) ?>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Quality Certification Text:</label>
                                        <div class="col-sm-10">
                                            <?= Form::textarea('quality_certification', null, ['class' => 'form-control post-editor']) ?>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Quality Certification Image:</label>
                                        <div class="col-sm-3">
                                            <MediaLibrary :image_url="'{{ isset($product) ? $product->quality_certification_image : '' }}'" :media_id="'quality-certification-image'">
                                                <div slot="front" slot-scope="image">

                                                    <div class="dropzone">

                                                        <img :src="image.preview" class="" />

                                                        <p>Add or replace image</p>
                                                        <input type="hidden" :name="'quality_certification_image'" v-model="image.cover" class="form-control" />
                                                    </div>

                                                </div>
                                            </MediaLibrary>
                                        </div>
                                    </div>

                                    <product-recipe :init="{{ isset($product) ? collect([
                                        'recipe_title' => $product->recipe_title,
                                        'show_recipe' => $product->show_recipe,
                                        'recipe' => $product->recipe
                                        ])->toJson() : collect([]) }}"></product-recipe>

                                    <div class="form-group"><label class="col-sm-2 control-label">Add Recipe:</label>
                                        <div class="col-sm-5">
                                            <?= Form::select('recipe_id', isset($recipes) ? $recipes->toArray() : [], isset($product) ? $product->recipe_id : null,['class' => 'form-control']) ?>
                                        </div>
                                    </div>

                                    <shipping-input :shipping="{{ isset($product) ? $product->shipping :0 }}" :weight="{{ isset($product) ? $product->weight :0 }}">
                                    </shipping-input>

                                    <div class="form-group"><label class="col-sm-2 control-label">Categories:</label>
                                        <div class="col-sm-10">
    @include('admin.products._categories')
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <ProductAttribute :attributes="{{ isset($product) ? collect($product->extra_attributes)->toJson() : collect([]) }}"><label>Product Attributes</label></ProductAttribute>
                                <ProductRelated :related-products="{{ isset($product->related_products) ? collect($product->related_products)->toJson() : collect([]) }}" :products="{{ isset($products) ? $products->toJson() : collect([]) }}"><label>Related Products</label></ProductRelated>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="seo" class="tab-pane">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <div class="form-group"><label class="col-sm-2 control-label">Meta Tag Title:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('meta_title', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Meta Tag Description:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('meta_description', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Meta Tag Keywords:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('meta_keywords', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                @if(isset($product->id))
                <div id="images" class="tab-pane">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <Droppable url="{{ url('/admin/products/'.$product->id.'/images') }}" :images="{{ json_encode($product->images('thumb')) }}"></Droppable>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="videos">
                        <Repeatable :list="{{ collect($product->videos) }}" :blank="''">
                            <template slot="add-button" scope="props">
                                <div class="form-group">
                                <button type="button" class="btn btn-primary" @click="props.addItem"><i class="fa fa-plus"></i> Add new video</button>
                                </div>
                            </template>
                            <template slot="list" scope="props">
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <MediaLibrary :image_url="props.item.cover" :media_id="'cover' + props.index">
                                        <div slot="front" slot-scope="image">
                                        
                                        <div  class="dropzone">
            
                                            <img :src="image.preview" class="" /> 
                                            
                                             <p>Add or replace video cover</p>
                                            <input type="hidden" :name="'videos[' + props.index +'][cover]'" v-model="image.cover" class="form-control" />
                                        </div> 
                                        
                                       </div>
                                    </MediaLibrary></div>
                                    <div class="col-xs-4"><input type="text" :name="'videos[' + props.index +'][id]'" placeholder="Video ID" v-model="props.item.id" class="form-control" /> </div>
                                    <div class="col-xs-4">
                                        <button type="button" class="btn btn-danger" @click="props.removeItem(props.index)"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div> 
                            </div>
                        </template>
                        </Repeatable>
                    </div>
                </div>
                @endif @if(isset($product->setup) && $product->setup=='variable')
                <div id="variants" class="tab-pane">
                    <div class="videos m-t-lg">
                        <Repeatable :list="{{ collect($product->variants) }}" :blank="{{ collect([
                        'price' => $product->price
                    ]) }}">
                            <template slot="add-button" scope="props">
                                <div class="form-group">
                                <button type="button" class="btn btn-primary" @click="props.addItem"><i class="fa fa-plus"></i> Add new variant</button>
                                </div>
                            </template>
                            <template slot="list" scope="props">
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <MediaLibrary :image_url="props.item.variant_cover" :media_id="'variant_cover' + props.index">
                                        <div slot="front" slot-scope="image">
                                        
                                        <div  class="dropzone">
            
                                            <img :src="image.preview" class="" /> 
                                            
                                             <p>Add or replace cover</p>
                                            <input type="hidden" :name="'variants[' + props.index +'][variant_cover]'" v-model="image.cover" class="form-control" />
                                        </div> 
                                       </div>
                                    </MediaLibrary></div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                                <div class="row">
                                                        <div class="col-xs-6"><input type="text" :name="'variants[' + props.index +'][sku]'" placeholder="SKU" v-model="props.item.sku" class="form-control" /> </div>
                                                        <div class="col-xs-6">
                                                                <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        <input type="text" :name="'variants[' + props.index +'][price]'" placeholder="0.0" v-model="props.item.price" class="form-control" /> 
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                        
                                        <input type="text" :name="'variants[' + props.index +'][variant_attributes]'" placeholder="e.g. Color:Red|Size:10ml" v-model="props.item.variant_attributes" class="form-control" />
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="hidden" :name="'variants[' + props.index +'][id]'" v-model="props.item.id" />
                                        <button type="button" class="btn btn-danger" @click="props.removeItem(props.index)"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div> 
                            </div>
                        </template>
                        </Repeatable>
                    </div>
                </div>
                @endif



                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="text-right">
                        <button class="btn btn-danger pull-left" type="button" onclick="
                        swal({
                            title: 'Are you sure you want to delete this product?',
                            icon: 'warning',
                            buttons: true,
                            dangerMode: true
                          }).then(function(willDelete) {
                            if (willDelete) {
                                let form = document.getElementById('form-product');
                                form._method.value = 'DELETE';
                                form.submit();
                            }
                          })">Delete product</button>
                        <a href="{{ route('admin.products.index') }}" class="btn btn-white" type="button">Cancel</a>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
