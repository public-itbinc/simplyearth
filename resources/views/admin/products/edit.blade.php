@extends('admin.dashboard')

@section('title', 'Product edit')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-8">
        <h2>Product edit</h2>
        <ol class="breadcrumb">
            <li>
                    <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
                    <a href="{{ url('/admin/products') }}">Product List</a>
            </li>
            <li class="active">
                <strong>Product edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-xs-4 text-right">        
            <div class="m-t-lg">
            <a href="{{ $product->link() }}" target="_blank" class="btn btn-primary">View Product</a>
            </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    @if(count($errors))

    <div class="alert alert-danger">


        @foreach ($errors->all() as $message) 

        {{ $message }} <br />

        @endforeach


    </div>

    @endif

    {{ Form::model( $product,[ 'route' => ['admin.products.update',$product], 'method' => 'patch', 'enctype' => 'multipart/form-data', 'id' => 'form-product' ] ) }}
    
        @include('admin.products._fields',[
            'attributes' => $product->extra_attributes,
            'wholesale' => collect($product->only(['wholesale_pricing', 'wholesale_price']))
        ])
    
    {{ Form::close() }}

</div>



@endsection

@section('footer_base_scripts')
<script src="{{ mix('js/admin-products.js') }}"></script>
@endsection