@extends('admin.dashboard')

@section('title', 'Add new product')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add new product</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
                    <a href="{{ url('/admin/products') }}">Product List</a>
            </li>
            <li class="active">
                <strong>Add new product</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    @include('admin.layouts.errors')

    {{ Form::open( [ 'route' => 'admin.products.store', 'method' => 'post', 'id' => 'form-product' ] ) }}
    
        @include('admin.products._fields',[
            'attributes' => [],
            'wholesale' => collect([])
        ])
    
    {{ Form::close() }}

</div>

@endsection

@section('footer_base_scripts')
<script src="{{ mix('js/admin-products.js') }}"></script>
@endsection