@foreach($categories as $key => $category)
    @if(in_array($category->id, $productCategories))
        <div class="checkbox checkbox-primary">
            <input id="category-checkbox-{{ $category->id }}" type="checkbox" checked="checked" name="categories[]" value="{{ $category->id ?: old('name') }}">
            <label for="category-checkbox-{{ $category->id }}">
                {{ $category->name }}
            </label>
        </div>
    @else
    <div class="checkbox checkbox-primary">
            <input id="category-checkbox-{{ $category->id }}" type="checkbox" name="categories[]" value="{{ $category->id ?: old('name') }}">
            <label for="category-checkbox-{{ $category->id }}">
                {{ $category->name }}
            </label>
        </div>
    @endif
@endforeach