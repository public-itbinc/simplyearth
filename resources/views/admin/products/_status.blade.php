@if($product->status=='active')

<span class="label label-primary">{{ $product->status }}</span>

@else

<span class="label label-default">{{ $product->status }}</span>

@endif