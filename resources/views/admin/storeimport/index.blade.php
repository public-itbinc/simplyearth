@extends('admin.dashboard')

@section('title', 'Stores Import')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Stores</h2>
        <ol class="breadcrumb">
            <li> <a href="{{ url('/admin') }}">Admin</a> </li>
            <li class=""> <a href="#">Store</a></li>
            <li class="active"> <a href="#">Import</a></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @if (session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
  @endif

  <div class="ibox-content m-b-sm border-bottom">
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" method="POST" action="{{ route('admin.storelocatorimport.store') }}" enctype="multipart/form-data">
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
            <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

            <div class="col-md-6">
              <input id="csv_file" type="file" class="form-control" name="csv_file" required>

              @if ($errors->has('csv_file'))
                <span class="help-block">
                  <strong>{{ $errors->first('csv_file') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
              <button type="submit" class="btn btn-primary">
                Import
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
    </div>
  </div>
</div>

@endsection
