<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-10">
                <MediaLibrary :image_url="'{{ $box->cover }}'" :media_id="'box-cover'">
                    <div slot="front" slot-scope="image">

                        <div class="dropzone" style="min-height:400px;">

                            <img :src="image.preview" class="" />

                            <p>Add or replace box image</p>
                            <input type="hidden" name="cover" v-model="image.cover" class="form-control" />
                        </div>

                    </div>
                </MediaLibrary>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-2 control-label">Name:</label>
            <div class="col-sm-10">
                <?= Form::text('name', null, ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-2 control-label">Key:</label>
            <div class="col-sm-10">
                <?= Form::text('key', null, ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
            <div class="col-sm-10">
                <?= Form::textarea('description', null, ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-2 control-label">Available Stock:</label>
            <div class="col-sm-10">
                <?= Form::number('stock', null, ['class' => 'form-control', 'min' => 0, 'step' => 1]) ?>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-2 control-label">Features:</label>
            <div class="col-sm-10">
                <Repeatable :list="{{ json_encode($box->features ?? []) }}" :blank="''">
                    <template slot="add-button" scope="props">
                                <div class="">
                                <button type="button" class="btn btn-primary btn-sm" @click="props.addItem"><i class="fa fa-plus"></i></button>
                                </div>
                            </template>
                    <template slot="list" scope="props">
                            <div class="m-t-lg m-b-lg">
                                <div class="row">
                                    
                                    <div class="col-xs-10"><input type="text" :name="'features[' + props.index +']'" v-model="props.repeatable[props.index]" class="form-control" /> </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn-danger" @click="props.removeItem(props.index)"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div> 
                            </div>
                        </template>
                </Repeatable>
            </div>
        </div>

    </div>

    <div class="col-sm-4">
        <div class="ibox">
            <div class="ibox-title">
                <h3>Products</h3>
            </div>
            <div class="ibox-content">
                <SearchLister :init="{{ $box->products }}" :search_url="'/admin/products'" :placeholder="'Search a product'">
                    <template slot="option" slot-scope="option">
                <div class="d-center">
                    <img :src="option.option.cover" class="img-sm" />
                    <span v-text="option.option.name"></span>
                </div>
            </template>

                    <template slot="list" slot-scope="option">
                <img :src="option.list_item.cover" class="img-sm" />
                <a href="#"><span v-text="option.list_item.name"></span></a>
                <input type="hidden" name="products[]" :value="option.list_item.id">
            </template>
                </SearchLister>
            </div>
        </div>

        <div class="ibox">
                <div class="ibox-title">
                    <h3>Video </h3>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-xs-5">
                            <MediaLibrary :image_url="'{{ $box->box_video_cover ?? '' }}'" :media_id="'box_video_cover'">
                                <div slot="front" slot-scope="image">
                                
                                <div  class="dropzone">
    
                                    <img :src="image.preview" class="" /> 
                                    
                                        <p>Add or replace box video cover</p>
                                    <input type="hidden" :name="'box_video_cover'" v-model="image.cover" class="form-control" />
                                </div> 
                                
                                </div>
                            </MediaLibrary>
                        </div>
                    <div class="col-xs-7"><input type="text" name="box_video_id" value="{{ $box->box_video_id }}" placeholder="Video ID" class="form-control" /> </div>
                    </div>
                </div>
            </div>

        <div class="ibox">
            <div class="ibox-title">
                <h3>Recipes</h3>
            </div>
            <div class="ibox-content">

                <div class="recipes">
                    <Repeatable :list="{{ $box->recipes }}" :blank="''">
                        <template slot="add-button" scope="props">
                                    <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-sm" @click="props.addItem"><i class="fa fa-plus"></i> Add new recipe</button>
                                    </div>
                                </template>
                        <template slot="list" scope="props">
                                    <div class="m-t-lg m-b-lg">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <MediaLibrary :image_url="props.item.cover" :media_id="'cover' + props.index">
                                            <div slot="front" slot-scope="image">
                                            
                                            <div  class="dropzone">
                
                                                <img :src="image.preview" class="" /> 
                                                
                                                 <p>Add or replace recipe cover</p>
                                                <input type="hidden" :name="'recipes[' + props.index +'][cover]'" v-model="image.cover" class="form-control" />
                                            </div> 
                                            
                                           </div>
                                        </MediaLibrary></div>
                                        <div class="col-xs-5"><input type="text" :name="'recipes[' + props.index +'][id]'" placeholder="Video ID" v-model="props.item.id" class="form-control" /> </div>
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-danger" @click="props.removeItem(props.index)"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div> 
                                    
                                    <div class="m-t-md">
                                        <input type="text" placeholder="Header" :name="'recipes[' + props.index +'][header]'"  v-model="props.item.header" class="form-control" />
                                    </div>
                                    <div class="m-t-md">
                                        <textarea rows="5" placeholder="Text" :name="'recipes[' + props.index +'][text]'"  v-model="props.item.text" class="form-control" ></textarea>
                                    </div>
                                

                                </div>
                            </template>
                    </Repeatable>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-8">
        <div class="text-right">
            <a href="{{ route('admin.shopping-boxes.index') }}" class="btn btn-white" type="submit">Cancel</a>
            <button class="btn btn-primary" type="submit">Save changes</button>
        </div>
    </div>
</div>