@extends('admin.dashboard') 
@section('title', 'Shopping Boxes') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{ $box->name }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
                <a href="{{ url('/admin/shopping-boxes') }}">Shopping Boxes</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">


    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    @if(count($errors))

    <div class="alert alert-danger">


        @foreach ($errors->all() as $message) {{ $message }} <br /> @endforeach


    </div>

    @endif 
    
    {{ Form::model( $box, [ 'route' => ['admin.shopping-boxes.update',$box], 'method' => 'patch', 'enctype' => 'multipart/form-data',
    'class' => 'form-horizontal' ] ) }}
    
    @include('admin.shoppingboxes._fields') 
    
    {{ Form::close() }}

</div>
@endsection