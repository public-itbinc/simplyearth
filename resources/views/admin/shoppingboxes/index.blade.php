@extends('admin.dashboard') 
@section('title', 'Shopping Boxes') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Shopping Boxes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li class="active">
                <strong>Shopping Boxes</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">


    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        @foreach($boxes as $box)
        <div class="col-sm-3">
            <div class="ibox">
                <div class="ibox-title text-center">
                    <h3>{{ $box->name }}</h3>
                </div>
                <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-6">
                                <small class="stats-label">Subscription Boxes</small>
                                <h4>{{ $box->remaining_boxes }}</h4>
                            </div>

                            <div class="col-xs-6">
                                <small class="stats-label">Stocks</small>
                                <h4>{{ $box->stock ?? 'No limit' }}</h4>
                            </div>
                        </div>
                    </div>
                <div class="ibox-content">
                    @if(!empty($box->cover))

                    <img src="{{ $box->cover }} " width="100%" />

                    @endif
                   
                
                </div>
                <div class="ibox-footer text-right">
                    <a href="{{ route('admin.shopping-boxes.edit',['shopping_box' => $box->id]) }}" class="btn btn-primary">Edit</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection