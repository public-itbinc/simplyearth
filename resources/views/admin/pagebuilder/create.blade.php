@extends('admin.dashboard')
@section('title', 'Create New Page')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Page</h2>
            <ol class="breadcrumb">
                <li><a href="{{ url('/admin') }}">Admin</a></li>
                <li><a href="{{ route('admin.pagebuilder.index') }}">Pages</a></li>
                <li class="active"><strong>Add new</strong></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('admin.layouts.errors') @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
                <a href="{{ route('admin.blogs.index') }}">
                    <button type="button" class="btn btn-default">All Pages</button>
                </a>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <form action="{{route('admin.pagebuilder.store')}}" method="POST">
                    <div class="tabs-container">
                        <div class="tab-content">
                            <div id="blog" class="tab-pane active">
                                <div class="panel-body">
                                    @include('admin/pagebuilder/_form')
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for=title""></label>
                                        <div class="col-sm-10">
                                            <a href="{{route('admin.pagebuilder.index')}}" class="btn btn-default pull-right">Back</a>
                                            <button type="submit" class="btn btn-primary pull-right m-r-sm">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('js')
{{--    <script src="{{ mix('js/admin-blog.js') }}" charset="utf-8"></script>--}}
@endsection