@extends('admin.dashboard') 
@section('title', 'All Post') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>All Post</h2>
    <ol class="breadcrumb">
      <li> <a href="{{ url('/admin') }}">Admin</a> </li>
      <li> <a href="#">Pages</a> </li>
      <li class="active"> <strong>All Pages</strong> </li>
    </ol>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @if (session('success'))
  <div class="alert alert-success">{{ session('success') }}</div>
  @endif @if (isset($success))
  <div class="alert alert-success">{{ $success }}</div>
  @endif

  <div id="delete-alert"></div>

  <div class="ibox-content m-b-sm border-bottom">
    <div class="row">
      <div class="col-sm-6">
        <form action="{{ route('admin.pagebuilder.index') }}" class="form-inline">
          <div class="form-group">
          <input class="form-control" placeholder="Search Page" name="q" value=" {{ request()->get('q', '') }}" type="text">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div>
        </form>
      </div>
      <div class="col-sm-6">
        <div class="text-right">
          <a href="{{ route('admin.pagebuilder.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New Page</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">
          <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
            <thead>
              <tr>
                <th>Title</th>
                <th>Page slug</th>
                <th width="250px"></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pages as $key => $page)
              <tr>
                <td>{{ ucwords(str_limit($page->title, 20)) }}</td>
                <td>{{$page->slug}}</td>
                <td>
                  <div>
                    <a href="{{route('page', $page->slug) }}" class="btn btn-outline btn-primary">Show</a>
                    <a href="{{route('admin.pagebuilder.edit', $page) }}" class="btn btn-primary">Edit</a>
                    <a href="{{route('admin.pagebuilder.destroy', $page) }}"
                       onclick="event.preventDefault();
                               if(confirm('Are you sure?')){
                               document.getElementById('delete-form-{{$page->id}}').submit();
                               }"
                       class="btn btn-danger">Delete</a>
                      <form id="delete-form-{{$page->id}}"
                            action="{{ route('admin.pagebuilder.destroy', $page) }}" method="POST"
                            style="display: none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <nav aria-label="Page navigation example">
            {{ $pages->links() }}
          </nav>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
 
@section('footer_base_scripts')
<script src="{{ asset('js/admin-blog.js') }}" charset="utf-8"></script>
{{--<script type="text/javascript">--}}
{{--  $(document).on('click', '.delete-blog-post', function() {--}}
{{--      let id = $(this).parents('li').data('id');--}}
{{--      let delete_id = "'delete-post'";--}}

{{--      let html =--}}
{{--      '<div class="alert alert-danger">' +--}}
{{--        '<p>You are about to delete a post </p>' +--}}
{{--        '<a href="#" class="btn btn-default" onclick="event.preventDefault(); document.getElementById('+ delete_id +').submit();">' +--}}
{{--          'YES' +--}}
{{--        '</a>' +--}}
{{--        '<a href="#" id="delete-no" class="btn btn-default">NO</a>' +--}}
{{--        '<form id="delete-post" action="/admin/blogs/'+ id +'" method="POST" style="display: none;">' +--}}
{{--          '<?php echo csrf_field(); ?>' +--}}
{{--          '<?php echo method_field('DELETE'); ?>' +--}}
{{--        '</form>'--}}
{{--      '</div>';--}}

{{--      $('#delete-alert').html(html);--}}
{{--    });--}}

{{--    $(document).on('click', '#delete-no', function() {--}}
{{--      $('#delete-alert').html('');--}}
{{--    });--}}

{{--</script>--}}
@endsection