<fieldset class="form-horizontal">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="col-sm-2 control-label" for=title"">Title:</label>
        <div class="col-sm-10">
            <input type="text" name="title" class="form-control" placeholder="Title"
                   value="{{old('title') ?? $page->title ?? ""}}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for=title"">Slug:</label>
        <div class="col-sm-10">
            <input type="text" name="slug" class="form-control" placeholder="Slug page"
                   value="{{old('slug', $page->slug ?? "")}}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for=title"">Meta title:</label>
        <div class="col-sm-10">
            <input type="text" name="meta_title" class="form-control" placeholder="Meta title"
                   value="{{old('meta_title', $page->meta_title->value ?? "")}}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for=title"">Meta description:</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="meta_description" id="" cols="30" rows="5"
                      placeholder="Meta description">{{old('meta_description', $page->meta_description->value ?? "")}}</textarea>
        </div>
    </div>
    @isset($page)
        <div class="form-group">
            <label class="col-sm-2 control-label" for=title"">Page content:</label>
            <div class="col-sm-10">
                <a href="{{route('admin.pagebuilder.edit_template', $page ?? collect())}}" class="btn btn-primary">Edit Content</a>
            </div>
        </div>
    @endisset
</fieldset>

@push('footer_scripts')
    <script>

        let parent;
        let elem;
        let clone;
        let list;
        // let divForm = document.createElement('div');
        // let divCol3 = document.createElement('div');
        // let divCol7 = document.createElement('div');
        // let label = document.createElement('label');
        // let inputName = document.createElement('input');
        // let inputLink = document.createElement('input');
        // divForm.classList.add('form-group redirect');
        let addLink;
        setTimeout(() => (
            addLink = document.querySelector('.add-link'),
                addLink.addEventListener('click', () => {
                    list = document.querySelectorAll('.redirect').length,
                        console.log(list);
                    parent = document.querySelector('.redirects');
                    elem = parent.querySelector('.redirect');
                    clone = elem.cloneNode(true);
                    clone.querySelectorAll('.form-control').forEach(function (e, index) {

                        e.setAttribute('name', 'redirects[' + list + '][' + (index == 0 ? 'utm_content' : 'url') + ']');
                        e.value = '';
                    });
                    console.log(clone);
                    parent.appendChild(clone);
                })
        ), 1000);


    </script>
@endpush


