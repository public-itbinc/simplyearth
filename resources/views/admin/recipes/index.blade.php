@extends('admin.dashboard')
@section('title', 'Recipes')
@section('content')
    <div id="app-orders">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Recipes</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ url('/admin') }}">Admin</a>
                    </li>
                    <li class="active">
                        <strong>Recipes</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <PageRecipe :products="{{ $products->toJson() }}"></PageRecipe>
        </div>
    </div>
@endsection
