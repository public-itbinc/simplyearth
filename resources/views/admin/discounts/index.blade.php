@extends('admin.dashboard') 
@section('title', 'Discounts') 
@section('content')
<div id="app-orders">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Discounts</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/admin') }}">Admin</a>
                </li>
                <li class="active">
                    <strong>Discounts</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">

        <PageDiscount></PageDiscount>

    </div>
</div>
@endsection