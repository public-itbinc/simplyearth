@extends('admin.dashboard') 
@section('title', 'Wholesale settings') 
@section('content')
<div id="app-settings-wholesale">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Wholesale settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/admin') }}">Admin</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li class="active">
                    <strong>Wholesale settings</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-md-6">
                <div class="ibox">
                    <div class="ibox-content">
                        {{ Form::open( [ 'route' => 'admin.settings.wholesale.update', 'method' => 'patch', 'class'=> 'form-horizontal'] ) }} {{
                        csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Minimum Order:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>

                                    <input type="text" name="settings_wholesale[wholesale_minimum_order]" value="{{ $settings['wholesale_minimum_order'] ?? 0 }}"
                                        class="form-control" />
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Shipping cost on all orders:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" name="settings_wholesale[wholesale_shipping_total]" value="{{ $settings['wholesale_shipping_total'] ?? 20 }}"
                                        class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="ibox-footer text-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection