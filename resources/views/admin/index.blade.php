@extends('admin.dashboard')
@section('content')
<transition name="slide">
    <router-view></router-view>
</transition>
@endsection