<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Simply Earth - @yield('title','Dashboard') </title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{!! mix('css/admin.css') !!}" />

        @yield('css')

    </head>
    <body>

        <!-- Wrapper-->
        <div id="wrapper">

            <!-- Navigation -->
            @include('admin.layouts.navigation')
            <!-- Page wraper -->
            <div id="page-wrapper" class="gray-bg">


                <div class="row flash-message-container">
                        @include('flash::message')
                </div>

                <!-- Page wrapper -->
                @include('admin.layouts.topnavbar')

                <div id="app">

                    <!-- Main view  -->
                    @yield('content')

                </div>

                <!-- Footer -->
                @include('admin.layouts.footer')

            </div>
            <!-- End page wrapper-->

        </div>
        <!-- End wrapper-->

        @yield('modals')

        @section('footer_base_scripts')
            @push('footer_scripts')
              <script src="{{ asset('js/admin.js') }}"></script>
            @endpush

            @stack('footer_scripts')
        @show

        @yield('js')

    </body>
</html>
