<div class="row">
    <div class="col-lg-8">
        <div class="ibox float-e-margins">
            <div class="ibox-content form-horizontal">
            <div class="form-group"><label class="col-sm-2 control-label">Category Name</label>
                    <div class="col-sm-10">
                        <?= Form::text('name', null, ['class' => 'form-control']) ?>
                    </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-10">
                    <?= Form::text('slug', null, ['class' => 'form-control']) ?>
                </div>
            </div>
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <?= Form::textarea('description', null, ['class' => 'form-control','rows' => 5]) ?>
                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Parent Category</label>
                    <div class="col-sm-10">

                        

                        <select class="form-control" name="parent">
                            <option value="" >No parent</option>
                            @foreach($categories as $item)

                            <option value="{{ $item->id }}" @if(isset($category) && $item->id == $category->parent_id) selected="selected" @endif>{{ $item->name }}</option>

                            @endforeach
                        </select>

                    </div>
                </div>

                 @if(isset($category->id))

                <div id="images" class="tab-pane">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-content">

                                 <Droppable url="{{ url('/admin/categories/'.$category->slug.'/images') }}" :images="{{ json_encode($category->images('thumb')) }}"></Droppable>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endif

               
                                

                    <div class="form-group">
                        <div class="text-right col-lg-12">
                                <button class="btn btn-danger pull-left" type="button" onclick="
                                swal({
                                    title: 'Are you sure you want to delete this category?',
                                    icon: 'warning',
                                    buttons: true,
                                    dangerMode: true
                                  }).then(function(willDelete) {
                                    if (willDelete) {
                                        let form = document.getElementById('form-category');
                                        form._method.value = 'DELETE';
                                        form.submit();
                                    }
                                  })">Delete category</button>
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-white" type="submit">Cancel</a>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

{{ csrf_field() }}

