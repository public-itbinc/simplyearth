@extends('admin.dashboard')

@section('title', 'Edit category')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit category</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
                    <a href="{{ url('/admin/categories') }}">Categories</a>
            </li>
            <li class="active">
                <strong>Edit category</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    @include('admin.layouts.errors')

     {{ Form::model( $category,[ 'route' => ['admin.categories.update',$category->id], 'method' => 'patch', 'id' => 'form-category' ] ) }}
    
        @include('admin.categories._fields')
    
    {{ Form::close() }}

</div>

@endsection