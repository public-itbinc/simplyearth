@extends('admin.dashboard')

@section('title', 'Categories')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Categories</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li class="active">
                <strong>Categories</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

            
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">


<div class="ibox-content m-b-sm border-bottom">
    <div class="row">
        <div class="col-sm-6">
            <form action="" class="form-inline">
                <div class="form-group">
                    <?= Form::text('q', null, ['class' => 'form-control','placeholder' => 'Category Name']); ?>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
        <div class="col-sm-6">
            <div class="text-right">
                    <a href="{{ url('/admin/categories/create') }}/" class="btn btn-primary"><i class="fa fa-plus"></i> New Category</a>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">

                <table id="product-list" class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                    <thead>
                    <tr>

                        <th>Image</th>
                        <th data-toggle="true">Category Name</th>
                        <th data-toggle="true">Slug</th>
                        <th class="text-right" data-sort-ignore="true">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($categories as $category)    

                    <tr>
                        <td>
                            @if($cover = $category->image('thumb'))
                            <img class="img-sm" src="{{ $cover }}">
                            @endif
                        </td>
                        <td>
                           {{ $category->name }}
                        </td>
                        <td>
                            {{ $category->slug }}
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <a href="{{ $category->link() }}" target="_blank" class="btn-white btn btn-xs">View</a>
                                <a href="{{ $category->editLink() }}" class="btn-white btn btn-xs">Edit</a>
                            </div>
                        </td>
                    </tr>

                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="6">
                            <div class="pull-right">
                                {{ $categories->appends(request()->query())->links()  }}
                            </div>
                        </td>
                    </tr>
                    </tfoot>

                </table>

            </div>
        </div>
    </div>
</div>


</div>

@endsection
