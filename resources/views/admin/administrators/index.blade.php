@extends('admin.dashboard')

@section('title', 'Products')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Staff</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li class="active">
                <strong>Staff</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

            
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <Administrators :administrators="{{ $administrators }}"></Administrators>
</div>
@endsection
