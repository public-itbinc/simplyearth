@extends('themes.frontend')

@section('content')
<section>
    <div class="container-fluid">
        <div class="blog-detail">
            <div class="header">
              <a href="{{ route('admin.blogs.edit', $post->id) }}" class="btn btn-default">
                Return to editor
              </a>

              <h1 class="blue-text">{{ ucwords($post->title) }}</h1>

              <div class="author">
                  <div class="image" style="background-image: url('theme/images/author.jpg')"></div>
                  <div class="content">
                      <label>By {{ ucwords($post->author->name) }}</label>
                      <span>Posted on {{ date('F d, Y') }}</span>
                  </div>
              </div>

              <div class="social-links">
                  <div class="links">
                      <a href="#"><span class="se-icon icon-fb-rec"></span></a>
                      <a href="#"><span class="se-icon icon-twitter-rec"></span></a>
                      <a href="#"><span class="se-icon icon-pinit-rec"></span></a>
                      <a href="#"><span class="se-icon icon-google-rec"></span></a>
                  </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="body">
                <img class="img-responsive centered wide" src="{{ asset("images/{$post->image}") }}">

                {!! $post->content !!}

                <div class="tweet-box">
                    <p>{{ $post->twitter_post }}</p>

                    <p class="actn"><a href="#">CLICK TO TWEET <span class="ti-twitter-alt"></span></a></p>
                </div>

                <div class="quote-box">
                  <p><span class="fa fa-quote-left fa-2x blue-text"></span> {{ $post->quote }} <i class="fa fa-quote-right fa-2x blue-text" aria-hidden="true"></i></p>
                </div>
            </div>
            <div class="footer">
                <div class="author">
                    <div class="image" style="background-image: url('{{ asset($post->author->avatar) }}')"></div>
                    <div class="content">
                        <label>Written by <a href="#">{{ ucwords($post->author->name) }}</a></label>
                    </div>
                </div>

                <div class="social-links">
                    <div class="links">
                        <a href="#"><span class="se-icon icon-fb-rec"></span></a>
                        <a href="#"><span class="se-icon icon-twitter-rec"></span></a>
                        <a href="#"><span class="se-icon icon-pinit-rec"></span></a>
                        <a href="#"><span class="se-icon icon-google-rec"></span></a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <p> {{ $post->author->about }} </p>
            </div>
            <div class="disqus-box">
                <div id="disqus_thread"></div>
            </div>
        </div>
    </div>
</section>

<script>
    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
        var d = document, s = d.createElement('script');

        // IMPORTANT: Replace EXAMPLE with your forum shortname!
        s.src = 'https://simplyearth.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
@endsection
