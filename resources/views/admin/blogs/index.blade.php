@extends('admin.dashboard') 
@section('title', 'All Post') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>All Post</h2>
    <ol class="breadcrumb">
      <li> <a href="{{ url('/admin') }}">Admin</a> </li>
      <li> <a href="#">Blogs</a> </li>
      <li class="active"> <strong>All Post</strong> </li>
    </ol>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @if (session('success'))
  <div class="alert alert-success">{{ session('success') }}</div>
  @endif @if (isset($success))
  <div class="alert alert-success">{{ $success }}</div>
  @endif

  <div id="delete-alert"></div>

  <div class="ibox-content m-b-sm border-bottom">
    <div class="row">
      <div class="col-sm-6">
        <form action="{{ route('admin.blogs.index') }}" class="form-inline">
          <div class="form-group">
          <input class="form-control" placeholder="Search Post" name="q" value=" {{ request()->get('q', '') }}" type="text">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div>
        </form>
      </div>
      <div class="col-sm-6">
        <div class="text-right">
          <a href="{{ route('admin.blogs.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New Post</a>
          <a href="{{ route('all.blog') }}" class="btn btn-primary"> All Post</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">
          <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
            <thead>
              <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Categories</th>
                <th>Tags</th>
                <th>SEO Title</th>
                <th>SEO Description</th>
                <th>SEO Keywords</th>
                <th>Date Created</th>
                <th>Date Updated</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($posts as $key => $post)
              <tr>
                <td>{{ ucwords(str_limit($post->title, 20)) }}</td>
                <td>{{ ucwords($post->author->name) }}</td>
                <td>
                  @if (isset($post->cat)) {!! $post->cat !!} @endif
                </td>
                <td>
                  @if (isset($post->posttag)) {!! $post->posttag !!} @endif
                </td>
                <td>{{ ucwords($post->seo_title) }}</td>
                <td>{{ ucwords($post->seo_description) }}</td>
                <td>{{ ucwords($post->seo_Keywords) }}</td>
                <td>{{ date('M d Y', strtotime($post->created_at)) }}</td>
                <td>{{ date('M d Y', strtotime($post->updated_at)) }}</td>
                <?php $post->status = $post->status == 'publish' ? 'published' : $post->status; ?>
                <td>{{ ucwords($post->status) }}</td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                          </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="{{ route('single.post', $post->slug) }}">VIEW</a></li>
                      <li><a href="{{ route('admin.blogs.edit', $post->id) }}">EDIT</a></li>
                      <li data-id="{{ $post->id }}">
                        <a href="#" class="delete-blog-post">DELETE</a>
                      </li>
                    </ul>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <nav aria-label="Page navigation example">
            {{ $posts->links() }}
          </nav>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
 
@section('footer_base_scripts')
<script src="{{ asset('js/admin-blog.js') }}" charset="utf-8"></script>
<script type="text/javascript">
  $(document).on('click', '.delete-blog-post', function() {
      let id = $(this).parents('li').data('id');
      let delete_id = "'delete-post'";

      let html =
      '<div class="alert alert-danger">' +
        '<p>You are about to delete a post </p>' +
        '<a href="#" class="btn btn-default" onclick="event.preventDefault(); document.getElementById('+ delete_id +').submit();">' +
          'YES' +
        '</a>' +
        '<a href="#" id="delete-no" class="btn btn-default">NO</a>' +
        '<form id="delete-post" action="/admin/blogs/'+ id +'" method="POST" style="display: none;">' +
          '<?php echo csrf_field(); ?>' +
          '<?php echo method_field('DELETE'); ?>' +
        '</form>'
      '</div>';

      $('#delete-alert').html(html);
    });

    $(document).on('click', '#delete-no', function() {
      $('#delete-alert').html('');
    });

</script>
<script async src="https://js.convertflow.co/production/websites/4040.js"></script>
@endsection