@extends('admin.dashboard') 
@section('title', 'Create New Blog') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Blog Post</h2>
    <ol class="breadcrumb">
      <li> <a href="{{ url('/admin') }}">Admin</a> </li>
      <li> <a href="{{ route('admin.blogs.index') }}">Blogs</a> </li>
      <li class="active"> <strong>Add new</strong> </li>
    </ol>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
  @include('admin.layouts.errors') @if (session('success'))
  <div class="alert alert-success">
    {{ session('success') }}
    <a href="{{ route('admin.blogs.index') }}"><button type="button" class="btn btn-default">All Post</button></a>
  </div>
  @endif

  <div class="row">
    <div class="col-lg-9">
      <div class="tabs-container">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#blog">Blog</a></li>
        </ul>
        <div class="tab-content">
          <div id="blog" class="tab-pane active">
            <div class="panel-body">
                {{ Form::open( [ 'route' => ['admin.blogs.store'], 'method' => 'post', 'enctype' => 'multipart/form-data',
                'id' => 'blog-form', 'class' => 'form' ] ) }}

                <div id="preview" class=""></div>
                <div id="draft" class=""></div>
                <div id="publish" class=""></div>
  @include('admin/blogs/_fields',[ 'attributes' => [] ])

                {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="ibox">
        <div class="ibox-title">
          <h3>Publish</h3>
        </div>
        <div class="ibox-content text-center">
          <button type="button" id="btn-draft" class="btn btn-default"> Save Draft </button>
          <a href="{{ route('all.blog') }}">
                <button type="button" id="btn-preview" class="btn btn-default"> All Blogs </button>
              </a>
        </div>
        <div class="ibox-footer text-right pull-left"><a href="#" id="blog-preview" class="btn btn-primary">Preview</a></div>
        <div class="ibox-footer text-right"><a href="#" id="blog-publish" class="btn btn-primary">Publish</a></div>
      </div>
      <div class="ibox">
        <div class="ibox-title">
          <h3>Categories</h3>
        </div>
        <div class="ibox-content" id="categories">
          <ul>
            @foreach ($categories as $key => $category)
            <li>
              <?php $checked = isset($category['postCategory']) ? 'checked' : ''; ?>
              <input type="checkbox" name="category[]" value="{{ $category['id'] }}" {{ $checked }}> {{ ucwords($category['name'])
              }} <br> @if (isset($category['child']))
              <?php sub_category($category['child']); ?> @endif
            </li>
            @endforeach
          </ul>
        </div>
        <div class="ibox-footer">
          <a href="{{ route('admin.blogcategory.create') }}">
                <button type="button" class="btn btn-default">New Category</button>
              </a>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
 
@section('modals')
<div class="modal fade" id="insert-image" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id=""></h4>
      </div>
      <div class="modal-body">
        <p>Click on the image it will automatically inserted to the content</p>
        <div class="grid" style="display: flex; flex-wrap: wrap;">
          @foreach ($gallery as $key => $gal)
          <div class="grid-item">
            <img class="img-responsive insert-to-editor" src="{{ asset($gal->image) }}" alt="{{ $gal->name }}">
            <div class="btn-copy-wrapper">
              <p>Name: {{ $gal->name }}</p>
            </div>
          </div>
          @endforeach
        </div>

        <div class="grid-load-more">
          <button type="button" class="btn btn-primary" id="load-more" data-taken="{{ $taken }}">Load More</button>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
 
@section('js')
<script src="{{ mix('js/admin-blog.js') }}" charset="utf-8"></script>
@endsection