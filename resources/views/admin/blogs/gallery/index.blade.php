@extends('admin.dashboard')

@section('title', 'Blog Gallery')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gallery</h2>
        <ol class="breadcrumb">
            <li> <a href="{{ url('/admin') }}">Admin</a> </li>
            <li class="active"> <a href="#">Gallery</a> </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @if (session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
  @endif

  <div id="delete-alert"></div>

  <div class="ibox-content m-b-sm border-bottom">
      <div class="row">
          <div class="col-sm-6">&nbsp;</div>
          <div class="col-sm-6">
              <div class="text-right">
                <a href="{{ route('admin.bloggallery.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New Image</a>
              </div>
          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-lg-12">
          <div class="ibox">
              <div class="ibox-content">
                <div class="grid">
                  @foreach ($galleries as $key => $gallery)
                    <div class="grid-item">
                      <img class="img-responsive" src="{{ asset($gallery->image) }}" alt="{{ $gallery->name }}">
                      <div class="btn-copy-wrapper">
                        <p>Name: {{ $gallery->name }}</p>
                        <p style="display:none;">{{ asset($gallery->image) }}</p>
                        <button type="button" class="btn btn-copy btn-default modal-btn-copy"> Copy URL </button>
                      </div>
                    </div>
                  @endforeach
                </div>

                {{ $galleries->links() }}
              </div>
          </div>
      </div>
  </div>

</div>

@endsection
