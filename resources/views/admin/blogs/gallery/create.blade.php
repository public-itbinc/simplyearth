@extends('admin.dashboard')

@section('title', 'Blog Gallery')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Blog Post</h2>
        <ol class="breadcrumb">
            <li> <a href="{{ url('/admin') }}">Admin</a> </li>
            <li> <a href="{{ route('admin.blogs.index') }}">Blogs</a> </li>
            <li class="active"> <strong>Add new</strong> </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    @include('admin.layouts.errors')

    @if (session('success'))
      <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <form action="{{ route('admin.bloggallery.store') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name here">
              </div>

              <div class="form-group">
                <label for="image">Chose Image</label>
                <input type="file" class="form-control" id="image" name="image">
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection
