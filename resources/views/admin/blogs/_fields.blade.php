<fieldset class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-2 control-label" for=title"">Title:</label>
        <div class="col-sm-10">
            <?= Form::text('title', null, ['class' => 'form-control']) ?>
        </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="slug">Slug</label>
      <div class="col-sm-10">
          <?= Form::text('slug', null, ['class' => 'form-control']) ?>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="content">Content</label>
      <div class="col-sm-10">
          <?= Form::textarea('content', null, ['class' => 'form-control post-editor', 'id' => 'content']) ?>
        <p class="help-block">
            To add bordered div click on Format tool > Blocks > Div. <br>
            To add quote click on Format tool > Blocks > blockquote <br>
            To insert image, click Image tool
        </p>
      </div>
    </div>
@if (isset($post->published_at))
    <div class="form-group">
      <label class="col-sm-2 control-label" for="date-published">Date Published</label>
      <div class="col-sm-10">
          <input type="date" class="form-control" id="date-published" name="published_at" value="{{ date('Y-m-d', strtotime($post->published_at)) }}" readonly="true">
      </div>
    </div>
@endif

    <div class="form-group">
      <label class="col-sm-2 control-label" for="date-created">Date Created</label>
      <div class="col-sm-10">
        @if (isset($post->created_at))
          <input type="date" class="form-control" id="date-created" name="created_at" value="{{ date('Y-m-d', strtotime($post->created_at)) }}">
        @else
          <input type="date" class="form-control" id="date-created" name="created_at" placeholder="Enter the date the blog post created">
        @endif
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="date-updatd">Date Updated</label>
      <div class="col-sm-10">
        @if (isset($post->updated_at))
          <input type="date" class="form-control" id="date-updatd" name="updated_at" value="{{ date('Y-m-d', strtotime($post->updated_at)) }}">
        @else
          <input type="date" class="form-control" id="date-updatd" name="updated_at" placeholder="Enter the date the blog post updated">
        @endif
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="author">Author</label>
      <div class="col-sm-10">
          <?= Form::select('user_id', $authors->pluck('name','id'), null, ['class' => 'form-control']); ?>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="excerpt">Excerpt Or Summary</label>
      <div class="col-sm-10">
        @if (isset($post->excerpt))
          <input type="text" class="form-control" id="excerpt" name="excerpt" placeholder="Enter short paragraph or summary of your content" value="{{ $post->excerpt }}">
        @else
          <input type="text" class="form-control" id="excerpt" name="excerpt" placeholder="Enter short paragraph or summary of your content" value="{{ old('excerpt') }}">
        @endif
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="excerpt">Tags</label>
      <div class="col-sm-10">
        @if (isset($post) AND ! is_null($post->tags))
          <input type="text" class="form-control" id="tag" name="tags" placeholder="Enter tags here" value="{{ $post->tags }}" data-role="tagsinput">
        @else
          <input type="text" class="form-control" id="tag" name="tags" placeholder="Enter tags here" value="" data-role="tagsinput">
        @endif
      </div>
    </div>

    <hr>

    <div class="form-group">
      <h3 class="col-sm-3 control-label">Search Engine Optimization</h3>
    </div>

    <br>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="seo-title">Title</label>
      <div class="col-sm-10">
        @if (isset($post->seo_title))
          <input type="text" class="form-control" id="seo-title" name="seo_title" placeholder="Enter meta title" value="{{ $post->seo_title }}">
        @else
          <input type="text" class="form-control" id="seo-title" name="seo_title" placeholder="Enter meta title" value="{{ old('seo_title') }}">
        @endif
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="seo-description">Description</label>
      <div class="col-sm-10">
        @if (isset($post->seo_description))
          <input type="text" class="form-control" id="seo-description" name="seo_description" placeholder="Enter meta description" value="{{ $post->seo_description }}">
        @else
          <input type="text" class="form-control" id="seo-description" name="seo_description" placeholder="Enter meta description" value="{{ old('seo_description') }}">
        @endif
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="seo-Keywords">Keywords</label>
      <div class="col-sm-10">
        @if (isset($post->seo_Keywords))
          <input type="text" class="form-control" id="Keywords" name="seo_Keywords" placeholder="Enter search keywords" value="{{ $post->seo_Keywords }}">
        @else
          <input type="text" class="form-control" id="Keywords" name="seo_Keywords" placeholder="Enter search keywords" value="{{ old('seo_Keywords') }}">
        @endif
        <p class="help-block">Note: Keywords is comma separated</p>
      </div>
    </div>
    -	
-    <hr>	
-	
-    <div class="form-group">	
-      <h3 class="col-sm-2 control-label">Upload Image</h3>	
-    </div>	
-	
-    <div class="form-group">	
-      <label class="col-sm-2 control-label" for="imgage">Image</label>	
-      <div class="col-sm-2">	
-        <MediaLibrary :image_url="'{{ isset($post) ? $post->image : '' }}'" :media_id="'blog-image'">
            <div slot="front" slot-scope="image">

                <div class="dropzone">

                    <img :src="image.preview" class="" />

                    <p>Add or replace image</p>
                    <input type="hidden" :name="'image'" v-model="image.cover" class="form-control" />
                </div>

            </div>
          </MediaLibrary>
-      </div>	
-    </div>
</fieldset>

@push('footer_scripts')
@endpush
