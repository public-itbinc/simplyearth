@extends('admin.dashboard') 
@section('title', 'All Post') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>All Post</h2>
    <ol class="breadcrumb">
      <li> <a href="{{ url('/admin') }}">Admin</a> </li>
      <li> <a href="#">Blogs</a> </li>
      <li class="active"> <strong>All Category</strong> </li>
    </ol>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @if (session('success'))
  <div class="alert alert-success">{{ session('success') }}</div>
  @endif @if (isset($success))
  <div class="alert alert-success">{{ $success }}</div>
  @endif @if (session('warning'))
  <div class="alert alert-warning">{{ session('warning') }}</div>
  @endif

  <div id="delete-alert"></div>

  <div class="ibox-content m-b-sm border-bottom">
    <div class="row">
      <div class="col-sm-6">
      </div>
      <div class="col-sm-6">
        <div class="text-right">
          <a href="{{ route('admin.blogcategory.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New Category</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">
          <table id="product-list" class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
            <thead>
              <tr>
                <th>Name</th>
                <th>Slug</th>
                <th>Desription</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($categories as $key => $category)
              <tr>
                <td>{{ ucwords($category->name) }}</td>
                <td>{{ ucwords($category->slug) }}</td>
                <td>{{ str_limit(ucfirst($category->description), 50) }}</td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              Action <span class="caret"></span>
                            </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="{{ route('admin.blogcategory.edit', $category->id) }}">Edit</a></li>
                      <li data-id="{{ $category->id }}"><a href="#" class="delete-blog-category">Delete</a></li>
                      <li><a href="#" class="sub-cat" data-toggle="modal" data-target="#make-sub-category" data-id="{{ $category->id }}">Make a sub-category</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <nav aria-label="Page navigation example">
            <ul class="pagination">
              @if ($categories->currentPage() > 1)
              <li class="page-item"><a class="page-link" href="{{ url('/admin/blogcategory') }}?page={{ $categories->currentPage() - 1 }}" style="background-color: #1ab394; color: white;">Previous</a></li>
              @else
              <li class="page-item"><a class="page-link" href="#">Previous</a></li>
              @endif

              <li class="page-item"><a class="page-link" href="#">Page {{ $categories->currentPage() }} of {{ $page }}</a></li>

              @if ($categories->currentPage()
              < $categories->lastPage())
                <li class="page-item"><a class="page-link" href="{{ url('/admin/blogcategory') }}?page={{ $categories->currentPage() + 1 }}" style="background-color: #1ab394; color: white;">Next</a></li>
                @else
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                @endif
            </ul>
          </nav>

        </div>
      </div>
    </div>
  </div>

</div>
@endsection
 
@section('modals')
<div class="modal fade" id="make-sub-category" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Make me a sub-category</h4>
      </div>
      <div class="modal-body">
        <form class="sub-category" action="{{ route('admin.blogsubcategory.store') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="parent">Chose Parent</label>
            <select class="form-control" name="parent">
                <option value="0">-- Select Parent Category --</option>
                @foreach ($catForModal as $key => $cat)
                  @if ($cat->name != 'uncategorized')
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                  @endif
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Set
              </button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
 
@section('footer_base_scripts')
  <script src="{{ asset('js/admin-blog.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('click', '.delete-blog-category', function() {
      let id = $(this).parents('li').data('id');
      let delete_id = "'delete-post'";

      let html =
      '<div class="alert alert-danger">' +
        '<p>You are about to delete a blog category </p>' +
        '<a href="#" class="btn btn-default" onclick="event.preventDefault(); document.getElementById('+ delete_id +').submit();">' +
          'YES' +
        '</a>' +
        '<a href="#" id="delete-no" class="btn btn-default">NO</a>' +
        '<form id="delete-post" action="/admin/blogcategory/'+ id +'" method="POST" style="display: none;">' +
          '<?php echo csrf_field(); ?>' +
          '<?php echo method_field('DELETE'); ?>' +
        '</form>'
      '</div>';

      $('#delete-alert').html(html);
    });

    $(document).on('click', '#delete-no', function() {
      $('#delete-alert').html('');
    });

    $(document).on('click', '.sub-cat', function() {
      let id = $(this).data('id');
      let input = '<input type="hidden" class="form-control" name="id" value="'+ id +'" placeholder="">';

      $('.sub-category').append(input);
    });
</script>
@endsection