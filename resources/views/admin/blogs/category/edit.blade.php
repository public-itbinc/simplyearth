@extends('admin.dashboard')

@section('title', 'Add new category')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add new category</h2>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}">Admin</a></li>
            <li> <a href="{{ route('admin.blogcategory.index') }}">Blogs</a> </li>
            <li class="active"><strong>Edit</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @include('admin.layouts.errors')

  @if (session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">
          <form class="" action="{{ route('admin.blogcategory.update', $category->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}" placeholder="Enter category name here" required>
            </div>
            <div class="form-group">
              <label for="slug">Slug</label>
              <input type="text" class="form-control" id="slug" name="slug" value="{{ $category->slug }}" placeholder="Enter slug here" required>
            </div>
            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" name="description" rows="8" cols="80" placeholder="Enter description here">{{ $category->description }}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@section('footer_base_scripts')
@endsection
