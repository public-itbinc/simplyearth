@extends('admin.dashboard') 
@section('title', 'Blog Author') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Blog Author</h2>
        <ol class="breadcrumb">
            <li> <a href="{{ url('/admin') }}">Admin</a> </li>
            <li> <a href="{{ route('admin.blogs.index') }}">Blogs</a> </li>
            <li class="active"> <strong>Update author info</strong> </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    @include('admin.layouts.errors') @if (session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#blog">Blog Author</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="blog" class="tab-pane active">
                        <div class="panel-body">
                            <form class="" action="{{ route('admin.blogauthor.store') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="description">Short description</label>
                                    <input type="text" class="form-control" id="description" name="description" placeholder="Enter short description about yourself">
                                </div>
                                <div class="form-group">
                                    <label for="avatar">Avatar</label>
                                    <input type="file" class="form-control" id="avatar" name="avatar" placeholder="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
 
@section('footer_base_scripts')
  <script src="{{ asset('js/admin-blog.js') }}" charset="utf-8"></script>
@endsection
