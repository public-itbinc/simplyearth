<nav class="navbar-default navbar-static-side" role="navigation">
  <div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
        <li class="{{ Request::is('admin/page-builder') ? 'active' : '' }}"><a href="/admin/pagebuilder"><i class="fa fa-users"></i> <span class="nav-label">Page builder</span></a></li>
    </ul>
  </div>
</nav>
