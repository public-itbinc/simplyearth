@extends('admin.dashboard') 
@section('title', 'Store Locator') 
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Stores</h2>
    <ol class="breadcrumb">
      <li> <a href="{{ url('/admin') }}">Admin</a> </li>
      <li> <a href="{{ route('admin.storelocator.create') }}">Store</a></li>
      <li class="active"> <strong>All Store</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

  @if (session('success'))
  <div class="alert alert-success">{{ session('success') }}</div>
  @endif @if (isset($success))
  <div class="alert alert-success">{{ $success }}</div>
  @endif

  <div id="delete-alert"></div>

  <div class="ibox-content m-b-sm border-bottom">
    <div class="row">
      <div class="col-sm-6">
        <form action="" method="post" class="form-inline">
          {{ csrf_field() }}
          <div class="form-group">
            {{ Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Search Store']) }}
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div>
        </form>
      </div>
      <div class="col-sm-6">
        <div class="text-right">
          <a href="{{ route('store-locator') }}" class="btn btn-primary"><i class="fa fa-eye"></i> View Store Locator</a>
          <a href="{{ route('admin.storelocator.create') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Store</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">
          <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
            <thead>
              <tr>
                <th>Name</th>
                <th>Priority</th>
                <th>Address</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($stores as $key => $store): ?>
              <tr>
                <td>{{ ucwords($store->name) }}</td>
                <td>
                  <input type="number" name="priority" value="1">
                </td>
                <td>{{ ucwords($store->address) }} {{ ucwords($store->city) }} {{ ucwords($store->country) }} {{ ucwords($store->zipcode)
                  }} {{ ucwords($store->state) }}</td>
                <td></td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              Action <span class="caret"></span>
                            </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="{{ route('admin.storelocator.edit', $store->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>                        </li>
                      <li data-id="{{ $store->id }}">
                        <a href="#" class="delete-store" @click="deleteStore({{ $store->id }})"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                      </li>
                    </ul>
                  </div>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div>
            {{ $stores->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection