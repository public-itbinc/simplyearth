@extends('admin.dashboard')

@section('title', 'Stores')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Stores</h2>
        <ol class="breadcrumb">
            <li> <a href="{{ url('/admin') }}">Admin</a> </li>
            <li> <a href="{{ route('admin.storelocator.create') }}">Store</a></li>
            <li class="active"><a href="#">Create</a></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">


  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  @if (session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
          <div class="ibox-content">
            <form class="" action="{{ route('admin.storelocator.update', $store->id) }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $store->name }}" placeholder="Enter name of the store here" required>
              </div>
              <div class="form-group">
                <label for="address">Street</label>
                <input type="text" class="form-control" id="address" name="address" value="{{ $store->address }}" placeholder="Enter street here" required>
              </div>
              <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" id="city" name="city" value="{{ $store->city }}" placeholder="Enter the City here" required>
              </div>
              <div class="form-group">
                <label for="zipcode">Zipcode</label>
                <input type="text" class="form-control" id="zipcode" name="zipcode" value="{{ $store->zipcode }}" placeholder="Enter zipcode here" required>
              </div>
              <div class="form-group">
                <label for="country">Country</label>
                <input type="text" class="form-control" id="country" name="country" value="{{ $store->country }}" placeholder="Enter country here" required>
              </div>
              <div class="form-group">
                <label for="state">State</label>
                <input type="text" class="form-control" id="state" name="state" value="{{ $store->state }}" placeholder="Enter state here" required>
              </div>
              <div class="form-group">
                <label for="latitude">Latitude</label>
                <input type="text" class="form-control" id="latitude" name="latitude" value="{{ $store->latitude }}" placeholder="Enter latitude here" required>
              </div>
              <div class="form-group">
                <label for="longitude">Longitude</label>
                <input type="text" class="form-control" id="longitude" name="longitude" value="{{ $store->longitude }}" placeholder="Enter longitude here" required>
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" id="description" name="description" value="{{ $store->description }}" placeholder="Enter description here">
              </div>
              <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" value="{{ $store->phone }}" placeholder="Enter phone here">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ $store->email }}" placeholder="Enter email here">
              </div>
              <div class="form-group">
                <label for="fax">Fax</label>
                <input type="text" class="form-control" id="fax" name="fax" value="{{ $store->fax }}" placeholder="Enter fax here">
              </div>
              <div class="form-group">
                <label for="web">Web</label>
                <input type="text" class="form-control" id="web" name="web" value="{{ $store->web }}" placeholder="Enter web here">
              </div>
              <div class="form-group">
                <label for="tag">Tag</label>
                <input type="text" class="form-control" id="tag" name="store_tag" value="{{ $store->tag }}" placeholder="Enter tag here">
              </div>
              <div class="form-group">
                <label for="schedule">Schedule</label>
                <input type="text" class="form-control" id="schedule" name="schedule" value="{{ $store->schedule }}" placeholder="Enter schedule here">
              </div>
              <div class="form-group">
                <label for="image">Store Image</label>
                <input type="file" class="form-control" id="store_image" name="store_image" value="" placeholder="">
              </div>
              <div class="form-group">
                <label for="marker_image">Marker Image</label>
                <input type="file" class="form-control" id="marker_image" name="marker_image" placeholder="">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </form>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection

@section('footer_base_scripts')
@endsection
