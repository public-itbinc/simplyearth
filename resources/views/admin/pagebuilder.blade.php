<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Simply Earth - @yield('title','Dashboard') </title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('css')

    </head>
    <body onkeydown=disableTab(event)>
    @yield('content')
        @yield('js')

    </body>
</html>
