<div class="row" id="product-edit-container">
    <div class="col-md-4">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#info">Tag</a></li>
            </ul>
            <div class="tab-content">
                <div id="info" class="tab-pane active">
                    <div class="panel-body">
                        <fieldset class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name:</label>
                                <div class="col-sm-10">
                                    <?= Form::text('name', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>



                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="text-right">

                        @if(isset($product))

                        <button class="btn btn-danger pull-left" type="button" onclick="
                        swal({
                            title: 'Are you sure you want to delete this tag?',
                            icon: 'warning',
                            buttons: true,
                            dangerMode: true
                          }).then(function(willDelete) {
                            if (willDelete) {
                                let form = document.getElementById('form-tag');
                                form._method.value = 'DELETE';
                                form.submit();
                            }
                          })">Delete tag</button>

                        @endif

                        <button class="btn btn-white" type="submit">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>