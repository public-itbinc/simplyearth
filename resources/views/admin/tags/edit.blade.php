@extends('admin.dashboard')

@section('title', 'Tags edit')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-8">
        <h2>Tag edit</h2>
        <ol class="breadcrumb">
            <li>
                    <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
                    <a href="{{ url('/admin/tags') }}">Tags List</a>
            </li>
            <li class="active">
                <strong>Tag edit</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    @if(count($errors))

    <div class="alert alert-danger">


        @foreach ($errors->all() as $message) 

        {{ $message }} <br />

        @endforeach


    </div>

    @endif

    {{ Form::model( $tag,[ 'route' => ['admin.tags.update', $tag], 'method' => 'patch', 'enctype' => 'multipart/form-data', 'id' => 'form-tag' ] ) }}
    
        @include('admin.tags._fields')
    
    {{ Form::close() }}

</div>



@endsection