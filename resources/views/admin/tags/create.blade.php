@extends('admin.dashboard')

@section('title', 'Add new tag')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add new tag</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
                    <a href="{{ url('/admin/tags') }}">Tag List</a>
            </li>
            <li class="active">
                <strong>Add new tag</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    @include('admin.layouts.errors')

    {{ Form::open( [ 'route' => 'admin.tags.store', 'method' => 'post', 'id' => 'form-tag' ] ) }}
    
        @include('admin.tags._fields')
    
    {{ Form::close() }}

</div>

@endsection