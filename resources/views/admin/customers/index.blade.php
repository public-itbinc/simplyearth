@extends('admin.dashboard') 
@section('title', 'Customers') 
@section('content')
<div id="app-customers">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Customers</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/admin') }}">Admin</a>
                </li>
                <li class="active">
                    <strong>Customers</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <transition name="slide">
            <router-view></router-view>
        </transition>
    </div>
</div>
@endsection
 
@section('footer_base_scripts')
<script src="{{ mix('js/admin-customers.js') }}"></script>
@endsection