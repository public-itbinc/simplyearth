@extends('themes.frontend') 
@section('body_class','home-page') @if(customer()) {{-- 
@section('facebook')
@endsection
 --}}

@section('content')
<div>
    <section class="pdtb40 bg-default">
        <div class="container-fluid">
            <div class="row referral-invite">

                <div class="col-md-7">

                    <div class="glass-box">
                        <h1 class="wide-title">Give a Free Box, Get a Free Box!</h1>
                        <p>Invite your friends to start making their homes natural! When they sign up for a recipe box, we'll
                            add a free extra month onto your current active subscription and their first month is free!</p>

                        <div class="form-inline">
                            <form>
                                <div class="form-group form-group-flex form-group-lg">
                                    <input type="email" v-model="invite_emails" id="referral_email" class="form-control referral-input-lg sm-9 cloudsponge-contacts" placeholder="Enter email addresses"
                                        required>
                                    
                                <button type="submit" value="submit" @click.prevent="sendInvitation"
                                        class="btn btn-primary btn-lg referral-btn-lg">Send Invites</button>
                                </div>&nbsp;&nbsp;

                            </form>
                        </div>
                        {{--<u class="referral-contacts">
                                    <p>Import from:<span class="cloudsponge-launch"><span class="text-blue">Gmail</span><span class="text-blue">Yahoo</span><span class="text-blue">Outlook</span></span></p>
                                </u>
                        <hr>--}}
                    </div>
                </div>
                <div class="col-md-5">

                    <div class="glass-box">
                        <h3 class="wide-title">Share your Coupon Code</h3>
                        <p>Your friends get their <strong>first month free!</strong> Your friends just cover shipping on their first box.</p>
                        <div class="input-group input-group-lg referral-coupon-code">
                            <input type="text" id="refercode2" class="form-control bg-white" placeholder="Coupon Code" value="{{$code}}" readonly>
                            <span class="input-group-btn">
                                    <button class="btn btn-default referral-code-btn" type="button" onclick="copy('refercode2')">COPY</button>
                                </span>
                        </div>
                        <div class="referral-flex">
                                <span onclick="shareFB()" class="text-pointer">
                                        <img src="{{asset('images/share-fb.png')}}" alt="facebook"> 
                                </span>
                                <span onclick="shareMessenger()" class="text-pointer text-right">
                                        <img src="{{asset('images/share-messenger.png')}}" alt="messenger">
                                </span>
                        </div>
                    </div>
                    {{--<div class="">
                        <h3 for="" class="">Your Referrals</h3>
                        <dl>
                            <dt>Earned</dt>

                            <dd>
                                1
                            </dd>
                            <dt>Pending</dt>

                            <dd>
                                1
                            </dd>
                            <dt>Invites</dt>

                            <dd>
                                1
                            </dd>
                        </dl>
                        <button class="btn btn-info btn-lg btn-wide">Show Invite Details</button>
                    </div>--}}
                </div>
            </div>
        </div>
    </section>

    <section class="bg-white">
        <div class="row">
            <div class="col-sm-12 text-center  mb-10">
                <h1 class="referral-program-label">How Does the Program Work?</h1>
            </div>
        </div>
        <div class="col-md-11 col-md-offset-1">
            <div class="col-md-3 col-md-offset-1 text-center">
                <img src="{{asset('images/share.png')}}" alt="Share Coupon">
                <h2 class="">Invite</h2>
                <p>Sign up and share your coupon code with friends.</p>
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('images/subscribe.png')}}" alt="Subscribe">
                <h2 class="">Subscribe</h2>
                <p>Your friend gets their first box FREE when they subscribe.</p>
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('images/claim.png')}}" alt="Claim Free Box">
                <h2 class="">Claim</h2>
                <p>You get a FREE BOX for every friend that subscribes after their 3rd month ships.</p>
            </div>
        </div>
    </section>

    <section class="bg-default">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">FREQUENTLY ASKED QUESTIONS</h2>
                    <div class="se-accordion panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                                <h4 class="panel-title ">
                                    <a href="#collapseOne">
                                        <p>Is there a limit to the number of people I can refer?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    Nope! It’s unlimited. Some people have referred so many friends that they have over a year of FREE boxes in their queue!
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#partnership">
                                <h4 class="panel-title ">
                                    <a href="#collapseOne">
                                        <p>What is my unique reward code?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="partnership" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    Your reward code allows you to earn rewards for yourself by sharing your code with your friends. Your friends get their <strong>first month free</strong> + shipping. You get a free box.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
                                <h4 class="panel-title ">
                                    <a href="#collapseTwo">
                                        <p>What do I get?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    <p>When you refer someone, we will add another free month to your subscription! How easy is that? And your friend gets their first month free with your unique coupon code.</p>

<p>You will get credited for your free box after your friend's third box in their subscription ships.</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-oils-produced">
                                <h4 class="panel-title ">
                                    <a href="#accordion-oils-produced">
                                        <p>What does my friend need to do?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordion-oils-produced" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    Your friends need to subscribe to our essential oil recipe box and use your unique coupon code at checkout. They get a big bonus box worth over $40 and their first month FREE plus $9.99 shipping. This special offer is reserved only for our referral program! 
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-how-to-dilute">
                                <h4 class="panel-title ">
                                    <a href="#accordion-how-to-dilute">
                                        <p>Can I refer a friend who is already a Simply Earth Recipe Box Subscriber?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordion-how-to-dilute" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    Unfortunately, no. To qualify for this program, your friend must be a new Simply Earth Recipe Box subscriber. If they have placed orders with us in the past but not subscribed to our essential oil recipe box then those friends will qualify as a referral.  
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-how-to-redeem">
                                <h4 class="panel-title ">
                                    <a href="#accordion-how-to-redeem">
                                        <p>How do I redeem my free month?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordion-how-to-redeem" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    <p>It will automatically be applied to your account once we have shipped out the new subscription box.</p>

                                    <p>As a reminder, we will credit your account right away if your friend signs up for a multi-month plan. If they order a 1-month plan, you will get credited after they order the third box in their subscription.</p> 
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-small-print">
                                <h4 class="panel-title ">
                                    <a href="#accordion-small-print">
                                        <p>Any Small Print?</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordion-small-print" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    Your reward code can't be used in conjunction with other offers as is the case with all our coupons. You can’t refer someone who lives in the same house as you.  Any transaction involving a fraudulent, fictitious, or otherwise non-bonafide account is ineligible for this program. All participation in this program is subject to the Simply Earth <a href="{{ route('terms-of-service') }}" class="text-blue text-underline">Terms of Service</a> and <a href="{{ route('privacy-policy') }}" class="text-blue text-underline">Privacy Policy</a>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="fb-root"></div>
@endsection
 @push('footer_scripts') 
@section('footer_base_scripts')
<script src="https://api.cloudsponge.com/widget/hCE7SnWtuPCmTZYpis5-SQ.js"></script>
<script>
    function copy(target) {
        var code = document.getElementById(target);
        code.select();
        document.execCommand("copy");
        $('.referral-code-btn').text('COPIED')
    }

</script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '{{ config("app.fb_app_id") }}',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.3'
    });

  };

  function shareFB()
  {
      

    FB.ui({
        method: 'share',
        href: "{{ url('/pages/subscription-box?utm_source=facebook&utm_medium=referral&utm_campaign=referral_program&utm_content='.customer()->share_code) }}",
        quote: "Get a FREE Simply Earth Essential Oil Box with my coupon code: {{ customer()->share_code }} | https://simplyearth.com/box"
        }, function(response){});
  }

  function shareMessenger()
  {
    FB.ui({
        method: 'send',
        link: "{{ url('/pages/subscription-box?utm_source=messenger&utm_medium=referral&utm_campaign=referral_program&utm_content='.customer()->share_code) }}",
    });
  }
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<script src="{{ mix('js/invites.js') }}"></script>
@endsection
 
@endpush @else 
@section('content')
<script>
    window.location = "/pages/referral";

</script>

<div class="row">
    <div class="container-fluid referral-invite">
        <div class="col-sm-12 text-center referral-login">
            <h1>Please Login to View this Page.</h1>
        </div>
    </div>
</div>
@endsection
 @endif