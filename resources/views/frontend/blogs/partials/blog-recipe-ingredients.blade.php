<section>
    <div class="container-fluid related-products-view">
        <ingredient-listing :collection="{{ $ingredients->toJson() }}"></ingredient-listing>
    </div>
</section>
