<section>
    <div class="container-fluid related-products-view">
        <ProductListingHorizontal :collection="{{ $related_products->toJson() }}"></ProductListingHorizontal>
    </div>
</section>