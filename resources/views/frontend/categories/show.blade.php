@extends('themes.frontend')

@section('content')


<section class="pdtb40 products-category">
			<div class="container-fluid">
				<div class="row products-category-container clearfix">
					<div class="col-sm-3 sidebar" id="main-sidebar">
						@include('frontend.layouts._product_sidebar')
					</div>
					<div class="col-sm-9 col-xs-12 product-listing">
						@include('frontend.layouts._product_listing')
					</div>
				</div>
			</div>
        </section>


@endsection