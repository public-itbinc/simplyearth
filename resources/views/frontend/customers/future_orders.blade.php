@extends('themes.frontend') 
@section('content')

<section class="pdt40 m-b20 store-orders upcoming-orders">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row-block">
                    <div class="order-header clearfix">
                        <h3 class="inner pull-left">Manage Upcoming Orders</h3>
                        <a href="{{ route('past-orders') }}" class="pull-right">Previous Orders</a>
                    </div>
                </div>

                @if(customer()->account->subscribed() && $box_groups) @if(customer()->account->subscription->isCommitment()) @foreach($box_groups
                as $box_group_key => $box_group) @if(count($box_group->filter(function($item){ return !is_null($item->getCommitment());
                })) > 0)

                <fieldset class="commitment-group {{ $box_group_key }}">

                    <legend>6 Month Commitment</legend>

                    @foreach($box_group as $box)

                    <FutureBox :init="{{ (new \App\Shop\ShoppingBoxes\FutureBoxDetail($box))->toJson() }}">

                    </FutureBox>

                    @endforeach
                </fieldset>

                @else

                <div class="text-center pd20-0 mb20 bg-white">

                    <form action="{{ route('subscription.unpause') }}" method="POST">
                        <button type="submit" class="btn btn-primary btn-lg">RESUME COMMITMENT</button>

                        {{ csrf_field() }}
                    </form>

                </div>

                @endif @endforeach @else @foreach($box_groups as $box_group)

                @foreach($box_group as $box)

                <FutureBox :init="{{ (new \App\Shop\ShoppingBoxes\FutureBoxDetail($box))->toJson() }}">

                </FutureBox>

                @endforeach

                @endforeach @endif @endif

            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer_base_scripts')
<script src="{{ mix('js/profile.js') }}"></script>
@endsection