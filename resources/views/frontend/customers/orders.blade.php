@extends('themes.frontend') 
@section('content')
<section class="pdt40 m-b20 store-orders recent-orders">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row-block">
                    <div class="order-header clearfix">
                        <h3 class="inner pull-left">Your Orders</h3>
                    </div>
                </div>
                @if(count($orders))
                <div class="row-block order-items">
                    @foreach($orders as $order)
                    <div class="item clearfix same-height">
                        <div class="col-md-4">
                            <p class="data">{{ $order->processed_at->format('F j, Y')}}</p>
                            @if($order->subscription)
                            <span>Subscription Purchase</span> @endif
                        </div>
                        <div class="col-md-4">
                            <div class="product-detail">

                                @if(count($order->order_items))
                                <img src="{{ $order->order_items->first()->cover }}" alt=""> @endif

                                <div class="order-no">
                                    <h4>Order No. {{ $order->order_number }}</h4>
                                    <span>{{ $order->order_items->sum('quantity') }} {{ $order->order_items->sum('quantity') > 1 ? 'items' : 'item' }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="order-detail-right text-pointer" @click="global.showOrderDetails({{ $order->id}})">
                                <p>${{ $order->total_price}}</p>
                                <span class="status {{ $order->status }}">{{ strtoupper($order->status) }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>
                {{ $orders->links() }} @endif
            </div>
        </div>
    </div>
</section>
@endsection