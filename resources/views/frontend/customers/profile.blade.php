@extends('themes.frontend') 
@section('content')

<section class="pdt40 account-boxes products-category">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <label for="name" class="greeting pull-left">Welcome, {{ customer()->first_name }}!</label> @if(customer()->account->subscribed())
                <label for="name" class="subscriber-date pull-right">Subscriber Since, {{ customer()->account->subscription->created_at->diffForHumans() }}</label>                @endif
            </div>
        </div>

        <div class="row">
            @if(customer()->account->subscribed())
            <div class="col-md-4">

                <ManageSubscription :next_box="{{ collect(new App\Http\Resources\FutureOrderMonthResource(customer()->account->nextBox()))->toJson() }}"
                    :next_boxes="{{ $boxes->map(function($item){
                    return new App\Http\Resources\FutureOrderMonthResource($item);}) }}" :available_subscriptions="{{ $available_subscriptions }}"
                    :subscription="{{ collect([
                        'schedule' => customer()->account->subscription->schedule,
                        'plan' => customer()->account->subscription->plan,
                        'is_commitment' => customer()->account->subscription->isCommitment(),
                        'commitment_last_box' => customer()->account->subscription->commitment_last_box ? new App\Http\Resources\FutureOrderMonthResource(customer()->account->subscription->commitment_last_box) : null,
                        'commitment_allowed_skips' => customer()->account->subscription->commitment_allowed_skips->map(function($item){
                    return new App\Http\Resources\FutureOrderMonthResource($item);})
                    ])->toJSON() }}"></ManageSubscription>

            </div>
            @elseif(customer()->account->paused())
            <div class="col-md-4">
                <div class="box">
                    <h1>{{ customer()->first_name }}, We're ready when you are.</h1>

                    <p>We have your subscription details saved in case you want to start up again at any point. You can still
                        make changes to your plan; we won't charge or ship until 1 days after you resume.</p>

                    <span>
                                                    <ResumeBox :first_name="'{{ strtoupper(customer()->first_name) }}'"></ResumeBox>
                                                </span>
                </div>
            </div>
            @else
            <div class="col-md-4">
                <div class="box flex-space-around">

                    <span>
                                                    <a href="{{ route('subscription') }}" class="btn btn-primary btn-wide-md btn-lg">SUBSCRIBE NOW</a>
                                            </span>
                </div>
            </div>
            @endif @if(customer()->isWholesaler())
            <div class="col-md-4">
                <div class="box">
                    <h2>QUICK ORDER FORM</h2>

                    <div class="object"><span class="se-icon icon-quick-order-form"></span></div>

                    <a href="{{ route('wholesaler.form') }}" class="keep-bot green-text">Place my wholesale order <span class="fa fa-angle-right"></span></a>
                </div>
            </div>
            @endif {{--
            <div class="col-md-4">
                <div class="box">
                    <h2>MONTHS TILL NEXT REWARD</h2>
                    <div class="object"><span class="month-count">2</span></div>

                    <p>A fun extra is soon to come.</p>
                    <a href="#">Your Points Summary <span class="fa fa-angle-right"></span></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <h2>Get a FREE Month for every 3 friends who join</h2>
                    <div class="object"><span class="se-icon icon-users-group"></span></div>
                    <a href="#" class="keep-bot"><strong>Invite Friends to Join</strong> <span class="fa fa-angle-right"></span></a>
                </div>
            </div> --}}
            <div class="col-md-4">
                <div class="box" style="background-image: url({{ asset('images/referral-box-button.jpg') }});padding:0;">
                    <a href="{{ route('referral-invites') }}"><img src='{{ asset('images/referral-box-button.jpg') }}' style="visibility: hidden;" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <CustomerAddress :address="{{ collect($default_address)->toJSON() }}">
                            </CustomerAddress>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <CustomerPaymentMethod :env="'{{ $env }}'" :client_token="'{{ $client_token }}'" :init="{{ collect(customer()->payment_method)->toJSON() }}"></CustomerPaymentMethod>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <CustomerEmail :email="'{{ customer()->email }}'"></CustomerEmail>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <CustomerPassword></CustomerPassword>
                        </div>
                    </div>
                    {{--
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <h2><a href="/pages/referral/invites">Invite Friends</a></h2>
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="col-sm-8 product-listing">
                <div class="product-header clearfix">
                    @if(customer()->account->subscribed())
                    <h3 class="pull-left">FREE SHIPPING on addons</h3>
                    @else
                    <h3 class="pull-left">Checkout What We Have To Offer</h3>
                    @endif
                    <a href="{{ route('products') }}" class="browse-all pull-right">Browse All</a>
                </div>

                <ProductListing :show_load_more="false" :collection="{{ $products->toJson() }}"></ProductListing>

            </div>
        </div>
    </div>
</section>

<section class="pd0 m-b20 store-orders recent-orders">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row-block">
                    <div class="order-header clearfix">
                        <h3 class="inner pull-left">Your Recent Orders</h3>
                        <a href="{{ route('past-orders') }}" class="pull-right">View All</a>
                    </div>
                </div>

                <div class="row-block order-items">
                    @if(count($recent_orders)) @foreach($recent_orders as $order)

                    <div class="item clearfix same-height">
                        <div class="col-md-4">
                            <p class="data">{{ $order->processed_at->format('F j, Y')}}</p>
                            @if($order->subscription)
                            <span>Subscription Purchase</span> @endif
                        </div>
                        <div class="col-md-4">
                            <div class="product-detail">
                                @if($order->order_items->first())
                                <img src="{{ $order->order_items->first()->cover }}" alt=""> @endif
                                <div class="order-no">
                                    <h4>Order No. {{ $order->order_number }}</h4>
                                    <span>{{ $order->order_items->sum('quantity') }} {{ $order->order_items->sum('quantity') > 1 ? 'items' : 'item' }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="order-detail-right text-pointer" @click="global.showOrderDetails({{ $order->id}})">
                                <p>${{ $order->total_price}}</p>
                                <span class="status {{ $order->status }}">{{ strtoupper($order->status) }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach @endif
                </div>
            </div>
        </div>
    </div>
</section>

@if(customer()->account->subscribed())
<section class="pd0 m-b20 store-orders upcoming-orders">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row-block">
                    <div class="order-header clearfix">
                        <h3 class="inner pull-left">Manage Upcoming Orders</h3>
                        <a href="{{ route('future-orders') }}" class="pull-right">View All</a>
                    </div>
                </div>

                @if($boxes) @foreach($boxes->take(2) as $box)

                <FutureBox :init="{{ (new \App\Shop\ShoppingBoxes\FutureBoxDetail($box))->toJson() }}">

                </FutureBox>

                @endforeach @endif

            </div>
        </div>
    </div>
</section>
@endif
@endsection
 
@section('footer_base_scripts')
<script src="https://www.paypalobjects.com/api/checkout.min.js" data-version-4></script>
<script src="{{ mix('js/profile.js') }}"></script>
@endsection