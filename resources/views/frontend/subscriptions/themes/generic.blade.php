@extends('themes.frontend') 
@section('body_class', 'bg-default se-theme-generic') 
@section('top-bar-header')

<div class="cta-section cta-section-xs text-center white-text" style="">
    <div class="container-fluid">
        <div class="cta-content"><strong>FREE Big Bonus Box!</strong> <span class="cta-subheader">Carrier oils and bottles valued at over $40.</span></div>
    </div>
</div>
@endsection
 
@section('header')
    @include('frontend.layouts.header-subscription')
@endsection
 
@section('content')

<section class="bg-white main-section">
    <div class="container-fluid">
        <div class="row eq-height image-with-text image-with-text-normal">

            <div class="col-sm-6 main-section-image section-image-wrapper section-image-wrapper-left">
                <div class="section-image">
                    <img class="max-width-100" src="{{$this_month_box->cover}}" alt="recipe box" width="100%" height="100%">
                </div>
                <div class="section-copy hidden-lg hidden-md hidden-sm">
                    <div class="speech-bubble-box">
                        <div class="speech-bubble">
                            <div class="speech-bubble-text">
                                With your first box, Gloria has a special offer for you... a <strong>FREE Big Bonus Box</strong>.
                                It has coconut oil, almond oil, beeswax, roller bottles, + more!
                            </div>
                        </div>
                        <div class="speech-bubble-person">
                            <img src="{{ asset('images/gloria-heart.png') }}" />
                        </div>
                    </div>
                    <a href="{{ route('subscription-choose').((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '') }}"
                        class="btn btn-primary btn-lg m-b-5 btn-wide-mobile">
                        CLAIM YOUR OFFER  
                        </a> @if(!$this_month_available)
                    <p><em>We are SOLD OUT of {{ $last_month->format('F') }}'s box. Reserve {{$this_month->format('F')}}'s box now to make sure you get your box! Mailed out on {{$this_month->format('F')}} 1st.</em></p>
                    @else
                    <p><em>Don't wait to make it yours. It WILL sell out!</em></p>
                    @endif
                </div>
            </div>

            <div class="col-sm-6 main-section-text">
                <div class="section-copy">
                    <h1 class="blue wide-title">The Best Essential Oil Recipe Box!</h1>
                    <p>Pure essential oils + natural ingredients every month.</p>

                    <div class="hidden-xs">
                        <div class="speech-bubble-box">
                            <div class="speech-bubble">
                                <div class="speech-bubble-text">
                                    With your first box, Gloria has a special offer for you... a <strong>FREE Big Bonus Box</strong>.
                                    It has coconut oil, almond oil, beeswax, roller bottles, + more!
                                </div>
                            </div>
                            <div class="speech-bubble-person">
                                <img src="{{ asset('images/gloria-heart.png') }}" />
                            </div>
                        </div>
                        <a href="{{ route('subscription-choose').((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '') }}"
                            class="btn btn-primary btn-lg m-b-5 btn-wide-mobile">
                            CLAIM YOUR OFFER  
                            </a>
                        <div class="mobile-centered">
                            @if(!$this_month_available)
                            <p><em>We are SOLD OUT of {{ $last_month->format('F') }}'s box. Reserve {{$this_month->format('F')}}'s box now to make sure you get your box! Mailed out on {{$this_month->format('F')}} 1st.</em></p>
                            @else
                            <p><em>Don't wait to make it yours. It WILL sell out!</em></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>

<section class="bg-white company-scroller-outer">
        <div class="container-fluid">
                <div class="company-scroller" style="background-image:url({{ asset('images/companies.png') }});">
        
                </div>
        </div>
</section>

<section class="section-padding bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text image-with-text-normal">
            <div class="col-sm-5 section-image-wrapper">
                <div class="section-image">
                    <div class="four-boxes">
                        <div class="four-boxes-box"><img src="{{$next_month_box->cover}}" alt="Monthly box"></div>
                        <div class="four-boxes-box"><img src="{{$this_month_box->cover}}" alt="Monthly box"></div>
                        <div class="four-boxes-box"><img src="{{$last_month_box->cover}}" alt="Monthly box"></div>
                        <div class="four-boxes-box"><img src="{{$last_last_month_box->cover}}" alt="Monthly box"></div>
                    </div>
                </div>
                <div class="section-copy hidden-lg hidden-md hidden-sm">
                    <div class="speech-bubble-box speech-bubble-box-2">
                        <div class="speech-bubble">
                            <div class="speech-bubble-text">
                                <strong>4 Essential Oils</strong>, <strong>Natural Ingredients</strong>, <strong>+ Fun Extras</strong>.
                                $150+ value starting at 39/month.
                            </div>
                        </div>
                        <div class="speech-bubble-person">
                            <img src="{{ asset('images/katie-heart.png') }}" />
                        </div>
                    </div>

                    <a href="{{ route('subscription-choose').((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '') }}"
                        class="btn btn-primary btn-lg m-b-5 btn-wide-mobile">
                                GET YOUR BOX TODAY 
                                </a>
                    <div class="mobile-centered">
                        @if(!$this_month_available)
                        <p><em>We are SOLD OUT of {{ $last_month->format('F') }}'s box. Reserve {{$this_month->format('F')}}'s box now to make sure you get your box! Mailed out on {{$this_month->format('F')}} 1st.</em></p>
                        @else
                        <p><em>Don't wait to make it yours. It WILL sell out!</em></p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-copy">
                    <h2 class="blue wide-title">Fun Monthly Boxes</h2>
                    <p>Every month is designed around a new theme with all the ingredients, containers and extras you need to
                        make 6 natural recipes... delivered right to your doorstep.</p>

                    <div class="hidden-xs">
                        <div class="speech-bubble-box speech-bubble-box-2">
                            <div class="speech-bubble">
                                <div class="speech-bubble-text">
                                    <strong>4 Essential Oils</strong>, <strong>Natural Ingredients</strong>, <strong>+ Fun Extras</strong>.
                                    $150+ value starting at 39/month.
                                </div>
                            </div>
                            <div class="speech-bubble-person">
                                <img src="{{ asset('images/katie-heart.png') }}" />
                            </div>
                        </div>
                        <a href="{{ route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')}}"
                            class="btn btn-primary btn-lg m-b-5 btn-wide-mobile">
                                GET YOUR BOX TODAY
                        </a>
                        <div class="mobile-centered">You don't want to miss out!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-padding bg-white">
    <div class="container-fluid">
        <div class="row eq-height image-with-text image-with-text-normal">
            <div class="col-md-6">
                <div class="section-copy visible-xs">
                    <h2 class="blue wide-title">Easy To Follow Recipes</h2>
                    <p>Our aromatherapy team designs the perfect recipes. Discover how fun and easy it is to make your home
                        all natural.</p>
                </div>

                <div class="section-image recipe-container">

                    <div class="recipe-gallery invisible">
                        <div class="slider-main slider-for">

                            @foreach( $recipes as $recipe)

                            <div class="recipe-video-container" style="background-repeat:no-repeat;background-size:cover;background-image:url({{ $recipe->cover }});height:auto">

                                <img alt="{{ $recipe->header }}" src="{{ $recipe->cover }}" style="visibility: hidden;" />
                                <div class="video-container">
                                    <span class="recipe-header">{{ $recipe->header }}</span>
                                    <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                                    <div class="videoPlayer" data-video-id="{{ $recipe->id }}" data-video-type="youtube"></div>
                                </div>

                            </div>


                            @endforeach

                        </div>
                        <div class="slider-thumb slider-nav">

                            @foreach($recipes->map(function($item) { return $item->cover; }) as $image)
                            <div class="slide-item">
                                <img src="{{ $image }}" />
                            </div>

                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-copy">
                    <div class="hidden-xs">
                        <h2 class="blue wide-title">Easy To Follow Recipes</h2>
                        <p>Our aromatherapy team designs the perfect recipes. Discover how fun and easy it is to make your home
                            all natural.</p>
                    </div>

                    <div class="reviewer-section bg-default">
                        <div class="reviewer-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <h5 class="review-title">So Simple!</h5>
                        <p>I love how easy it is, starting with oils can be SO OVERWHELMING, but this box has you covered!</p>
                        <div class="reviewer">
                            <img src="{{ secure_asset('images/patricia.png') }}" />
                            <div class="reviewer-name">Patricia B. <span class="verified">Verified Buyer</span><br /><span class="reviewer-country">United States</span></div>
                        </div>
                    </div>

                    <a href="{{ route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')}}"
                        class="btn btn-primary btn-lg m-b-5 btn-wide-mobile">
                                            GET YOUR BOX TODAY
                                    </a>
                    <div class="mobile-centered">Limited Quantities Available.</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-padding join-movement-section">
    <div class="container-fluid">
        <h2 class="wide-title"> Join a Movement</h2>
        <p>You can help end human trafficking. At Simply Earth, we share 13% of our profits with to organizations who help end
            this atrocity. Each month, we share who we are partnering with and how you can help too.</p>
    </div>
    </div>
</section>

<section class="section-padding bg-white">
    <div class="container-fluid">
        <div class="row eq-height image-with-text image-with-text-normal">
            <div class="col-md-6 section-image-wrapper hidden-xs">
                <div class="section-image ">

                    <img src="{{ asset('images/haiti-farm.jpg') }}" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-copy">
                    <h2 class="blue wide-title">Tested 100% Pure Essential Oils</h2>
                    <p>We are creating the standards for high quality 100% pure oils.</p>
                    <div class="section-image visible-xs">
                        <img src="{{ asset('images/haiti-farm.jpg') }}" />
                    </div>
                    <div class="reviewer-section bg-default">
                        <div class="reviewer-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <h5 class="review-title">3rd Party Tested!</h5>
                        <p>They are third party tested for purity so you can know you are getting a quality oil. That's something
                            I look for.</p>
                        <div class="reviewer">
                            <img src="{{ secure_asset('images/jessica.png') }}" />
                            <div class="reviewer-name">Jessica <span class="verified">Verified Buyer</span><br /><span class="reviewer-country">United States</span></div>
                        </div>
                    </div>

                    <a href="{{ route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')}}"
                        class="btn btn-primary btn-lg m-b-5 btn-wide-mobile">
                                                GET YOUR BOX TODAY
                                        </a>
                    <div class="mobile-centered">The best box ever!</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default section-testimonial">
    <div class="container-fluid">
        <div class="row eq-height testimonial-overview image-with-text image-with-text-normal">
            <div class="col-sm-6">
                <div class="flex-center">
                    <h2 class="wide-title blue-text"><strong>10,000+ Natural Homes Agree</strong></h2>
                    <p class="hidden-xs">Our fans love sharing how they are using Simply Earth to make their homes natural. Join the community
                        using #simplyearth.
                    </p>
                </div>
            </div>
            <div class="col-sm-6 testimonial-box">
                <div class="testimonial-image">

                    <div class="testimonial-inner">
                        <aside class="social-intro-featured active">
                            <figure class="social-intro-featured-item">
                                <div class="social-intro-inner">
                                    <div class="social-intro-avatar">
                                        <img src="{{ asset('images/testimonial-megan.jpg') }}" alt="@meganeacuna">
                                    </div>
                                    <figcaption>
                                        <span class="social-intro-user">@meganeacuna</span>
                                        <p>"I love it! Thanks @fromsimplyearth"</p>
                                    </figcaption>
                                </div>
                            </figure>
                        </aside>

                        <aside class="social-intro-featured">
                            <figure class="social-intro-featured-item">
                                <div class="social-intro-inner">
                                    <div class="social-intro-avatar">
                                        <img src="{{ asset('images/testimonial-gina.jpg') }}" alt="@gina_schweppe">
                                    </div>
                                    <figcaption>
                                        <span class="social-intro-user">@gina_schweppe</span>
                                        <p> "I received in the mail today. So in love with these products that are so kind to
                                            our world."</p>
                                    </figcaption>
                                </div>
                            </figure>
                        </aside>

                        <aside class="social-intro-featured">
                            <figure class="social-intro-featured-item">
                                <div class="social-intro-inner">
                                    <div class="social-intro-avatar">
                                        <img src="{{ asset('images/testimonial-happy.jpg') }}" alt="@HappyHumbleHomelife">
                                    </div>
                                    <figcaption>
                                        <span class="social-intro-user">@HappyHumbleHomelife</span>
                                        <p>"You guys are the best! Keep up the great work!"</p>
                                    </figcaption>
                                </div>
                            </figure>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial-bg">
        <img src="{{ asset('images/1pixel.png') }}">
    </div>
</section>
@endsection
 
@section('footer')
    @include('frontend.layouts.footer-subscription')
@endsection