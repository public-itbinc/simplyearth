@extends('themes.frontend') 
@section('header')
<div class="top-bar top-bar-wizard">
    <div class="container">
        <div class="col-xs-4 step-status">
            Step 1 of 2
        </div>
        <div class="col-xs-4 main-logo">
            <a href="/"><img src="{{ asset('images/logor.png') }}" alt="Simply Earth"></a>
        </div>
        {{--<div class="col-xs-4 chatbox  text-right">
            <img src="{{ asset('images/chat.png') }}" alt="chat" />
        </div>--}}
    </div>
</div>
<div class="top-bar-after"></div>
@endsection
 
@section('content')

<section class="pd20">
    <div class="container">
        <h1 class="text-center"><strong>Pick Your Plan</strong></h1>
        <div class="plan-box-area pdtb40">
            <a @click="gaAddToCart({{ $monthly_subscription_commitment }})" href="{{ route('subscribe').'?plan=REC-MONTHLY-3MONTHS'.(request()->has('code') ? '&code=' . request()->code : '') }}" class="plan-box">
                <span class="plan-badge">{{ $is_free_box_option ? "FIRST BOX FREE": "BEST VALUE" }}</span>
                <div class="plan-box-img">
                    <img src="{{ asset('/images/subscription.jpg') }}" alt="6 Month Commitment" />
                </div>
                <div class="plan-details">
                    <h3>3 Month Commitment</h3>
                    <p>$39/Recipe Box @if($is_free_box_option) + First Box Free @endif<br /><span class="text-blue">+Free Big Box Bonus</span></p>
                    <div class="plan-footer">BILLED MONTHLY</div>
                </div>
            </a>
            <a @click="gaAddToCart({{ $monthly_subscription }})" href="{{ route('subscribe').'?plan=REC-MONTHLY2019'.(request()->has('code') ? '&code=' . request()->code : '') }}" class="plan-box">
                <div class="plan-box-img">
                    <img src="{{ asset('/images/subscription2.jpg') }}" alt="Monthly Commitment" />
                </div>
                <div class="plan-details">
                    <h3>Monthly Commitment</h3>
                    <p>$45/Recipe Box<br /><span class="text-blue">+Free Big Box Bonus</span></p>
                    <div class="plan-footer">BILLED MONTHLY</div>
                </div>
            </a>
        </div>

        <div class="text-center">
            <img src="{{ asset('images/simply-earth-guarantee.svg') }}" width="80" alt="Simply Earth Guarantee">
            <h3>Simply Earth Guarantee</h3>
            <p>IF YOU DON'T LOVE YOUR OILS, WE'LL REPLACE THEM.<br /><small>Call us at 866-330-8165 or email us at help@simplyearth.com and we'll take care of you.</small>
            </p>
        </div>
    </div>
    </div>
</section>
@endsection
 
@section('footer')
    @include('frontend.layouts.footer-subscription')
@endsection