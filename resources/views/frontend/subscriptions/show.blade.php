@extends('themes.frontend')


@section('header')
    @include('frontend.layouts.header-subscription')
@endsection

@push('meta')
<meta property="og:image"              content="{{ $this_month_box->cover }}" />
@endpush
 
@section('content')

@if($is_free_box_option)
<div class="cta-section cta-section-xs text-center bg-secondary white-text" style="">
    <div class="container-fluid">
        <h3>Get Your First Box For Free <a href="{{ route('subscription-choose').((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '') }}" class="btn btn-white hidden-xs">Activate Offer</a></h3>
    </div>
</div>
@endif

<section class="pdtb40 bg-white">
    <div class="container-fluid">
        <div class="row image-with-text image-with-text-normal">

            <div class="col-sm-6">
                <div class="section-image">
                    <img src="{{$this_month_box->cover}}" alt="recipe box" width="100%" height="100%">
                </div>
            </div>

            @if($is_free_box_option)
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title"><strong>Get Your First Box for FREE</strong></h1>
                    <p>That's FOUR 100% Pure Essential Oils, Natural Ingredients, + Fun Extras! $150 value for only $39/month with a 3 month commitment. Just cover shipping.</p>
                        <a href="{{ route('subscription-choose').((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '') }}"
                        class="btn btn-wide-mobile btn-primary btn-lg m-b-5">
                                CLAIM YOUR FREE BOX
                        </a>
                </div>
            </div>
            @else
            
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Make your home natural</h1>
                    <p>with our essential recipe box. It includes 👇</p>
                    <p>4 Essential Oils | Natural Ingredients | + Fun Extras</p>
                    <p><span class="text-red">$150 value</span> for only $39/month</p>
                            <a href="{{ route('subscription-choose') }}"
                                    class="btn btn-wide-mobile btn-primary btn-lg m-b-5">
                                            {{ ($this_month_available ? 'GET YOUR ':'RESERVE YOUR ') .strtoupper($this_month->format('F')). ' BOX TODAY' }} 
                            </a>
                    @if(!$this_month_available)
                    <p><em>We are SOLD OUT of {{ $last_month->format('F') }}'s box. Reserve {{$this_month->format('F')}}'s box now to make sure you get your box! Mailed out on {{$this_month->format('F')}} 1st.</em></p>
                    @else
                    <p><em>Don't wait to make it yours. It WILL sell out!</em></p>
                    @endif
                </div>
            </div>

            @endif

        </div>
    </div>
</section>

    
<section class="pd20-0 bg-default">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12">
                
        <div class="in-subscribe-promise">
                <div class="promise-container text-center">
                    <ul class="list-inline">
                        <li>
                            <p class="promise-text"><i class="fa fa-lock"></i> Secure Transactions</p>
                        </li>
                        <li>
                            <p class="promise-text"><i class="fa fa-paper-plane-o "></i> Free Shipping</p>
                        </li>
                        <li>
                            <p class="promise-text"><i class="fa fa-shield"></i> Simply Earth Guarantee</p>
                        </li>
                        <li>
                            <p class="promise-text"><i class="fa fa-at"></i> Help@simplyearth.com</p>
                        </li>
                    </ul>
                </div>
        </div>
            </div>
        </div>

    </div>
</section>

<section class="how-it-works bg-white pd20-0">
    <div class="container-fluid">
        <h2 class="section-title">How it Works</h2>
        <div class="row work-row">
            <div class="col-sm-4 box--h">
                <div class="work-box">
                    <div class="image"><img src="{{ asset('images/Design.jpg') }}"></div>
                    <div class="box--content">
                        <h3>Design</h3>
                        <p>Our aromatherapy team designs the perfect recipes.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 box--h">
                <div class="work-box">
                    <div class="image"><img src="{{ asset('images/Deliver.jpg') }}"></div>
                    <div class="box--content">
                        <h3>Deliver</h3>
                        <p>Pure Oils. Honest Prices. Delivered Right To Your Door.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 box--h">
                <div class="work-box">
                    <div class="image"><img src="{{ asset('images/Discover.jpg') }}"></div>
                    <div class="box--content">
                        <h3>Discover</h3>
                        <p>Discover how fun and easy it is to make your home all natural.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pd0">
    <div class="cta-section text-center bg-secondary white-text">
        <div class="container-fluid">
            <h2>Free $40 Big Bonus Box When You Start Today!</h2>
            <p>Get it while supplies last</p>
            <a href="" class="btn btn-underlined white-text pop-video" data-video-id="_ZNtHiTXedM">WATCH VIDEO</a>
        </div>
    </div>
</section>

<section class="pdtb40 bg-white">
    <div class="container-fluid">
        <div class="row eq-height">
            <div class="col-md-6 col-md-push-6">
                <div class="video-player-box">
                    <div class="">
                        <img src="{{ $this_month_box->box_video_cover }}" class="preview-img" alt="video cover simplyearth" width="100%" height="100%">
                        <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                        <div class="videoPlayer" data-video-id="{{ $this_month_box->box_video_id  }}" data-video-type="youtube"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="section-copy">
                    <h2 class="blue wide-title">{{ ucfirst($this_month->format('F')) }}'s Subscription Box</h2>
                    <p>Each month you get all the ingredients, containers and extras you need to make 6 natural recipes... delivered
                        right to your doorstep. See what is included below!</p>
                    <div class="row small-gutter">
                        @foreach ($this_month_box->products as $product)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="subcription-ingredients">
                                <img src="{{ asset($product->cover) }}" class="pull-left">
                                <p>{{$product->name}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @if(!$this_month_available)
                    <p><em>{{$last_month->format('F')}}'s box is officially sold out! <br>Join today to reserve {{$this_month->format('F')}}'s box before it is gone!</em></p>
                    @else
                    <a href="{{ route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')}}"
                        class="btn btn-wide-mobile btn-primary btn-lg m-b-5">
                             {{ ($this_month_available ? 'GET YOUR ':'RESERVE YOUR ') .strtoupper($this_month->format('F')). ' BOX' }}
                        </a>
                    <p><em>You don't want to miss out! Last month's sold out.</em></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

<section class="subscription-boxes bg-default">
    <h2 class="section-title blue-text">Peek at the Recipes Inside the Box</h2>
    <ul class="nav nav-tabs box-tab" id="month-tab">
        <li><a data-toggle="tab" href="#last-month">{{strtoupper($last_month->format('F'))}}'S BOX</a></li>
        <li class="active"><a data-toggle="tab" href="#this-month">{{strtoupper($this_month->format('F'))}}'S
                    BOX</a></li>
        <li><a data-toggle="tab" href="#next-month">{{strtoupper($next_month->format('F'))}}'S BOX</a></li>
    </ul>

    <div class="tab-content box-content">
        <div id="last-month" class="tab-pane fade">
            <div id="slider-box-1" class="slick-shopping-box slider box-list-slider">
    @include ('frontend.layouts._month-slide', ['box' => $last_month_box, 'cta_link' => route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')])
            </div>
        </div>
        <div id="this-month" class="tab-pane fade in active">
            <div id="slider-box-2" class="slick-shopping-box slider box-list-slider">
    @include ('frontend.layouts._month-slide', ['box' => $this_month_box, 'cta_link' => route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')])
            </div>
        </div>
        <div id="next-month" class="tab-pane fade">
            <div id="slider-box-3" class="slick-shopping-box slider box-list-slider">
    @include ('frontend.layouts._month-slide', ['box' => $next_month_box, 'cta_link' =>  route('subscription-choose') . ((isset($discount_date['discount'])) ? '?code=' . $discount_data['discount']->code : '')])
            </div>
        </div>
    </div>
</section>

<section class="pd0">
    <div class="cta-section text-center bg-secondary white-text">
        <div class="container-fluid">
            <div class="col-md-4">
                <h2>$150 value for only <b>$39</b></h2>
            </div>
            <div class="col-md-4">
                <a href="{{ route('subscription-choose') . ((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '')}}"
                    class="btn btn-wide-mobile btn-primary btn-lg m-b-5">
                       {{ ($this_month_available ? 'GET YOUR ':'RESERVE YOUR ') .strtoupper($this_month->format('F')). ' BOX' }}
                    </a>
            </div>
            <div class="col-md-4">
                <h2>FREE SHIPPING</h2>
            </div>
        </div>
    </div>
</section>
<section class="section-video-testimonials bg-white">
    <div class="container-fluid">
        <div class="row testimonial-block">
            <h2 class="section-title blue-text">See What Others Are Saying</h2>

            <div id="testimonial-slider">
                <div class="">
                    <div class="video-player-box">
                        <div class="video-container">
                            <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                            <img class="preview-img" src="{{ asset('images/VideoCover_Jesica.jpg') }}">
                            <div class="videoPlayer" data-video-id="qqgWlZ6yIC0" data-video-type="youtube"></div>
                            <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left'
                                                                                      aria-hidden='true'></i></button>
                            <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right'
                                                                                       aria-hidden='true'></i></button>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="video-player-box">
                        <div class="video-container">
                            <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                            <img class="preview-img" src="{{ asset('images/VideoCover_Mary.jpg') }}">
                            <div class="videoPlayer" data-video-id="SmISkBbd-wc" data-video-type="youtube"></div>
                            <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left'
                                                                                      aria-hidden='true'></i></button>
                            <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right'
                                                                                       aria-hidden='true'></i></button>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="video-player-box">
                        <div class="video-container">
                            <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                            <img class="preview-img" src="{{ asset('images/VideoCover_brianLisa.jpg') }}">
                            <div class="videoPlayer" data-video-id="MqVPRc9lZNE" data-video-type="youtube"></div>
                            <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left'
                                                                                      aria-hidden='true'></i></button>
                            <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right'
                                                                                       aria-hidden='true'></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<section class="bg-default">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title blue-text">Frequently Asked Questions</h2>
                <div class="se-accordion panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                            <h4 class="panel-title">
                                <a href="#collapseOne">
                                    <p>What will I get in my essential oil Recipe Box? </p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Every month, we’ll send 4 full size 100% pure essential oils + extras to make 6 recipes.
                                    You will have everything you need to make your home natural. Each month is a new theme
                                    fun theme. Plus, you’ll get tips and tricks on other ways to use your oils.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
                            <h4 class="panel-title">
                                <a href="#collapseTwo">
                                    <p>Is it really only for $39 for 4 essential oils? </p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Yes! A monthly Simply Earth box, with no long-term commitment, is only $39 a month. If you
                                    bought everything included in a box from the big essential oil companies, it would cost
                                    over $150. We keep prices low by selling directly to you and not taking crazy margins.
                                    We keep quality pure by testing every oil for purity and visiting the farms our oils
                                    come from ourselves.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFour">
                            <h4 class="panel-title">
                                <a href="#collapseFour">
                                    <p>When will my next box ship?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse " role="tabpanel ">
                            <div class="panel-body ">
                                <p>Expect your new favorite piece of mail---your box!--- to ship the next business day with
                                    2-3 day shipping. We always have free & fast shipping on our boxes. Future boxes will
                                    ship out by the 20th each month and you can change your date. In some cases, the delivery
                                    date varies depends on the Country or State.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="half-half-container half-half-container-left bg-white">
    <div class="image-container" style="background-color: transparent">
        <div class="image-half" style="background-image: url({{ asset( 'images/natural-home-made-easy.jpg') }});"></div>
    </div>
    <div class="text-half">
        <div class="section-copy">
            <h2 class="blue wide-title ">A natural home made easy</h2>
            <p>Join {{ isset($discount_data['$name']) ? $discount_data['name'] : 'Simply Earth' }} to get a monthly delivery
                of 4 pure essential oils plus extras for the same price of 1 oil from the big oil companies. Just 39 a month. Pause at any time. </p>
            <a href="{{ route('subscription-choose') . ((isset($discount_data['discount'])) ? '?code=' . $discount_data['discount']->code : '') }}"
                 class="btn btn-wide-mobile btn-primary btn-lg m-b-5">
                    {{ ($this_month_available ? 'GET YOUR ':'RESERVE YOUR ') .strtoupper($this_month->format('F')). ' BOX' }}
                </a>
            <p><em>Get your box today before it sells out!</em></p>
        </div>
    </div>
</section>
@if(request()->has('ab-reviews'))
<!-- Reviews -->
<section class="pd0">
    <div class="cta-section cta-section-reviews text-center bg-secondary white-text">
        <h2 class="section-title">Real Reviews. Real People. <img src="{{ asset('images/real-reviews.png') }}" alt="real-reviews" /></h2>
    </div>
</section>
<section class="pd20-0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div id="stamped-main-widget" data-product-id="{{ $monthly_subscription->review_id }}" data-name="{{ $monthly_subscription->name }}"
                    data-url="{{ $monthly_subscription->link }}" data-image-url="{{ $monthly_subscription->cover }}" data-description="{{ $monthly_subscription->short_description }}"
                    data-product-sku="{{ $monthly_subscription->sku }}">
                </div>
            </div>
        </div>
    </div>
</section>

@else
<section class="bg-default section-testimonial">
    <div class="container-fluid">
        <div class="row eq-height testimonial-overview">
            <div class="col-sm-6 flex-center">
                <h2 class="section-title blue-text">See What Others Are Saying</h2>
                <p class="hidden-xs">Our fans love sharing how they are using Simply Earth to make their homes natural. Join the community using
                    #simplyearth.</p>
            </div>
            <div class="col-sm-6 testimonial-box">
                <div class="testimonial-image">

                    <div class="testimonial-inner">
                        <aside class="social-intro-featured active">
                            <figure class="social-intro-featured-item">
                                <div class="social-intro-inner">
                                    <div class="social-intro-avatar">
                                        <img src="{{ asset('images/testimonial-megan.jpg') }}" alt="@meganeacuna">
                                    </div>
                                    <figcaption>
                                        <span class="social-intro-user">@meganeacuna</span>
                                        <p>"I love it! Thanks @fromsimplyearth"</p>
                                    </figcaption>
                                </div>
                            </figure>
                        </aside>

                        <aside class="social-intro-featured">
                            <figure class="social-intro-featured-item">
                                <div class="social-intro-inner">
                                    <div class="social-intro-avatar">
                                        <img src="{{ asset('images/testimonial-gina.jpg') }}" alt="@gina_schweppe">
                                    </div>
                                    <figcaption>
                                        <span class="social-intro-user">@gina_schweppe</span>
                                        <p> "I received in the mail today. So in love with these products that are so kind to
                                            our world."</p>
                                    </figcaption>
                                </div>
                            </figure>
                        </aside>

                        <aside class="social-intro-featured">
                            <figure class="social-intro-featured-item">
                                <div class="social-intro-inner">
                                    <div class="social-intro-avatar">
                                        <img src="{{ asset('images/testimonial-happy.jpg') }}" alt="@HappyHumbleHomelife">
                                    </div>
                                    <figcaption>
                                        <span class="social-intro-user">@HappyHumbleHomelife</span>
                                        <p>"You guys are the best! Keep up the great work!"</p>
                                    </figcaption>
                                </div>
                            </figure>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial-bg">
        <img src="{{ asset('images/1pixel.png') }}">
    </div>
</section>

@endif
@endsection
 @push('footer_scripts')
<script>
    if ((window.location.pathname) == '/pages/subscription-box') {
            var browser = "<?= $current_browser; ?>";
            if (browser == 'Safari') {
                document.getElementById("month-tab").style.display = "block";
            }
        }

</script>







@endpush

@section('footer')
@include('frontend.layouts.footer-subscription')
@endsection