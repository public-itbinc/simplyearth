@extends('themes.frontend')

@section('content')
<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue">Join The #Squad</h1>
                    <p>Come and join us on our mission to help make homes natural in a fun, easy and affordable way! We are looking for people who love essential oils & are motivated to make the world a better place. The ideal ambassador is over 18 years old and actively creates content on either Facebook, Instagram, Youtube, or blog posts. If this sounds like you, then click the button below to sign up! </p>
                    <a class="typeform-share button" href="https://simplyearth.typeform.com/to/hTPSNS" data-mode="popup" class="btn btn-lg btn-primary" style="display:inline-block;text-decoration:none;background-color:#5acb89;color:white;cursor:pointer;font-family:Helvetica,Arial,sans-serif;font-size:18px;line-height:45px;text-align:center;margin:0;height:45px;padding:0px 30px;border-radius:22px;max-width:100%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;font-weight:bold;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale; data-submit-close-delay='10' target='_blank'">JOIN TODAY!</a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
				</div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <img src="{{ asset('images/LisaRiggs.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<section style="background-color: #fff;">
    <div class="container-fluid">
        <h1 class="section-title">How does it work?</h1>
        <div class="row ambassador-row">
            <div class="col-md-3 col-sm-6">
                <div class="ambassador-box">
                    <div class="image"><img src="{{ asset('images/ambassador-signup.jpg') }}" alt=""></div>
                    <p><span>1</span> Sign up for your custom coupon code.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="ambassador-box">
                    <div class="image"><img src="{{ asset('images/ambassador-share.jpg') }}" alt=""></div>
                    <p><span>2</span> Share your coupon code with friends.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="ambassador-box">
                    <div class="image"><img src="{{ asset('images/ambassador-discount.jpg') }}" alt=""></div>
                    <p><span>3</span> Your friends get discounts and you earn rewards.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="ambassador-box">
                    <div class="image"><img src="{{ asset('images/ambassador-paypal.jpg') }}" alt=""></div>
                    <p><span>4</span> Get paid through paypal!</p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('modals')
  <div class="modal fade" id="get-involve" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="get-involve-title">Form</h4>
        </div>
        <div class="modal-body">
          <p>Content</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
@endsection
