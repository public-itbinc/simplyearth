@extends('themes.frontend-no-header') 
@section('content')
<section class="pd0 bg-default sales-header">
    <div class="container-fluid">
        <div class="row pd20-0 top-bar top-bar-header-plain">
            <div class="header-left contact-info hidden-xs hidden-sm">M-F 9am-3pm: 866-330-8165</div>
            <div class="main-logo inline-block">
                <a href="/"><img src="{{ asset('images/logor.png') }}"></a>
            </div>
            @include('frontend.layouts._header-right')
        </div>
    </div>
</section>
<section class="pd20-0 sales-body bg-white">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
            <h1 class="text-blue text-center sales-body-headline">
                <strong>
                    Rebecca added over $8,000 in sales to her small business her first year!
                </strong>
            </h1>
            <h2 class="text-black text-center">
                <strong>Listen to her story and how you can get started today.</strong>
            </h2>
            <br>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="//www.youtube.com/embed/YzJghifkFXk"></iframe>
                {{--<iframe width="1198" height="674" src="https://www.youtube.com/embed/YzJghifkFXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
            </div>
        </div>
    </div>
</section>
<section class="how-it-works bg-white">
    <div class="container-fluid">
        <h1 class="section-title">How it Works</h1>
        <div class="row work-row">
            <div class="col-sm-4 box--h">
                <div class="work-box">
                    <div class="image"><img src="{{ asset('images/wholesaler-how-it-works-1.jpg') }}"></div>
                    <div class="box--content">
                        <br>
                        <p>Order our starter kit. Only $199 today and 8 payments of $89.</p>
                        <a href="" class="btn btn-underlined text-blue pop-video" data-video-id="iWucWVDSqPs">WATCH VIDEO</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 box--h">
                <div class="work-box">
                    <div class="image"><img src="{{ asset('images/wholesaler-how-it-works-2.jpg') }}"></div>
                    <div class="box--content">
                        <br>
                        <p>Get set up in under 5 minutes and start selling.</p>
                        <a href="" class="btn btn-underlined text-blue white-text pop-video" data-video-id="x3_Dq-M7uts">WATCH VIDEO</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 box--h">
                <div class="work-box">
                    <div class="image"><img src="{{ asset('images/wholesaler-how-it-works-3.jpg') }}"></div>
                    <div class="box--content">
                        <br>
                        <p>Easily reorder online as you run out of products.</p>
                        <a href="" class="btn btn-underlined text-blue white-text pop-video" data-video-id="EeMLK6XkFrs">WATCH VIDEO</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<a name="plans"></a>
@include('frontend.layouts._product-content')

    <div class="bg-white"><br><br><br><br></div>

<footer class="pd0 sales-double-bands">
    <div class="container-fluid">
        <div class="row pd10-0">
            <div class="col-xs-12 text-center">
                <ul class="list-inline">
                    <li>
                        <div class="circle-100-day inline-block bg-secondary mg0">
                            <p class="text-white mg0"><strong>100</strong></p>
                            <p class="text-white mg0"><strong>DAY</strong></p>
                        </div>
                    </li>
                    <li>
                        <h1 class="inline-block text-blue mg0">
                            <strong> MONEY BACK GUARANTEE </strong>
                        </h1>
                    </li>
                    <li>
                        <a href="#plans" class="btn btn-primary btn-lg mg0">Start Selling Pure Oils Today!</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @if(session()->has('wholesaler_registration'))
    <swal title="We have sent an email to {{ session()->get('wholesaler_registration')->email }}, please click the link included to verify your email address, set your password, and create your wholesale account." type="success"></swal>
    @endif
</footer>
@endsection
