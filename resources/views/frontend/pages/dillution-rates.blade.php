@extends('themes.frontend')

@section('content')

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy text-left">
                    <h1 class="section-title blue-text text-left wide-title">Essential Oil <br>Dilution Rate Guide</h1>
                    <p class="text-centerleft">We are trying to make it fun and easy to have a natural home at Simply Earth so we made this easy dilution guide to help you get started. It includes everything you need to know to use essential oils in proper amounts.</p>
                    <a href="#" class="btn btn-primary btn-lg cta-14115-trigger">DOWNLOAD NOW</a>
                </div>
            </div>
            <div class="col-sm-6">  
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/dillution.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection