@extends('themes.frontend')

@section('content')

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy text-left">
                    <h1 class="section-title blue-text text-left wide-title">Free Essential Oils <br>for Beginners eBook</h1>
                    <p class="text-centerleft">We love essential oils so much, we have developed a FREE essential oil ebook. It answers your questions like "What exactly is an essential oil?" and "How do I use this in my daily life?" This little book has everything you need to get started!</p>
                    <a href="#" class="btn btn-primary btn-lg cta-6743-trigger">DOWNLOAD NOW</a>
                </div>
            </div>
            <div class="col-sm-6">  
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/ebook.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection