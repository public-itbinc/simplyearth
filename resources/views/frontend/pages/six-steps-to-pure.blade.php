@extends('themes.frontend')

@section('content')

<section class="banner-page pd0">
    <img class="banner-img" src="{{ asset('images/6-steps/6-steps-banner.png') }}" alt="6 steps">
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Step 1: Right Plant in the Right Location</h1>
                    <p>The best oils are derived from the best plants grown in the best possible locations. One misconception is that the oils must be indigenously grown in order to be considered high quality. This belief is false. The plants must be grown where they will best thrive. For example, some plants in China do not thrive there and the distilled oil from those plants are of lesser quality than if they were to be grown in a better environment. However, other plants will thrive in China’s climate, causing the plant to produce a true, therapeutic-grade essential oil.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/6-steps/step1.png') }}">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Step 2: Farmed Responsibly</h1>
                    <p>Our oils are farmed responsibly. That means workers are treated fairly and the land and the oil both flourish. If the plants from which the essential oils are extracted have been treated with pesticides, those pesticides can become concentrated in the oil.That's why Simply Earth tests to make sure every batch of oil we produce is 100% pure, therapeutic-grade!</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/6-steps/step2.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Step 3: Oils are Extracted</h1>
                    <p><b>Steam</b>: Steam passes through the plant material (leaves, grass, roots, etc.), causing them to release their aromatic components. The steam is then condensed and the essential oil is extracted.</p>
                    <p><b>Water</b>(hydro): The plant materials (normally bark, wood, or harder compounds) are submerged in water to make a “soup.” As they are heated, they release their aromatic components into the steam which is then condensed and the essential oil is extracted.</p>
                    <p><b>Steam/Water</b>: Indirect steam is applied to the plant material (normally leaves) while water sits underneath the plant material.</p>
                    <p><b>Expression</b>: Plant materials (generally citrus rinds) are pressed by machines to squeeze out oils. Also known as “cold-pressed.”</p>    
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/6-steps/step3.png') }}">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pd0">
    <div class="cta-section text-center bg-secondary white-text">
        <div class="container-fluid">
            <p>Make Your Home Natural Today</p>
            <a href="#" class="btn btn-underlined white-text">LET’S GET STARTED</a>
        </div>
    </div>
</section>


<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Step 4: Packaged and Stored</h1>
                    <p>All our oils are stored in dark bottles as essential oils are light sensitive. Two common options are aluminum containers or amber glass bottles.  Our trained professionals then store our essential oils in temperature-controlled environments for optimal preservation.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/6-steps/step4.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Step 5: Tested for Purity</h1>
                    <p>I like to say, “I’ll believe it when I see it,” and I frequently apply this philosophy when testing the purity of essential oils. The type of testing oils undergo to examine purity is called gas chromatography and mass spectrometry- GC/MS for short. GC/MS testing breaks down the essential oil to reveal each of its unique chemical components. A trained aromatherapist then compares those amounts to the components in a standardized chemical composition for that oil to test if it truly is 100% pure. GC/MS testing will reveal if anything has been added or removed from the oil.</p>
                    <p><b>That is how we at Simply Earth make sure our oils truly are 100% pure and untarnished.</b></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/6-steps/step5.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Step 6: Enjoyed by You!</h1>
                    <p>We put in all the hard work to make high quality oils and natural home easy and affordable for you. So enjoy it!</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/6-steps/step6.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection