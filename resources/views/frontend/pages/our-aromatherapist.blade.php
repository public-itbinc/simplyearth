@extends('themes.frontend')

@section('content')
<section class="pdtb40">
    <div class="container-fluid">
        <div class="row eq-height video-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Tell us what you think! </h1>
                    <p>Comment below with ideas for recipes you want us to design and make. Or if there is a whole essential oil subscription box theme you really want, let us know!</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-video video-player-box">
                    <div class="video-container" style="background-image: url('{{ asset('images/meet-aromatherapist-thumbnail.jpg') }}');">
                        <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                        <div class="videoPlayer" data-video-id="y6v-8Z-IXCU" data-video-type="youtube"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pdtb40">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div id="disqus_thread"></div>
            </div>
        </div>
    </div>
</section>

<script>
    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
        var d = document, s = d.createElement('script');
        
        // IMPORTANT: Replace EXAMPLE with your forum shortname!
        s.src = 'https://simplyearth.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
@endsection