@extends('themes.frontend') 
@section('content')
<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Creating New Standards</h1>
                    <p>From displaying quality testing for our oils in the form of GC/MS reports for all our oils right on the
                        product page to visiting our vetiver farm in Haiti, we are working on creating the standards for
                        what it means to have high quality oils. All this is backed with our 365 day Simply Earth Guarantee.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <img src="{{ asset('images/haiti-farm.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<section class="pd0 bg-white">
    <div class="container-fluid">
        <img class="img-responsive mauto" src="{{ asset('images/map.jpg') }}">
    </div>
</section>


<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text">
            <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Commitment to Quality</h1>
                    <p>With the safety of your home in mind, we’re meticulous about every step of our process. To make sure
                        each plant reaches its potential, we carefully and ethically choose a region where it will thrive.
                        Then we work with our farmers. When our plants have reached maturity, they are harvested safely and
                        fairly. From there we collect their natural essential oils through either cold-pressing or steam-distillation.
                        Every batch is then tested to ensure 100% purity.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <img src="{{ asset('images/commitment.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>



<section class="bg-white">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Making Your Home Natural</h1>
                    <p>One of the beautiful things about essential oils is that each bottle is slightly different, because every
                        plant and harvest is different. We preserve these unique qualities by professionally packaging our
                        oils in amber glass bottles. This not only guards our oils freshness and natural benefits but also
                        protects against damaging UV rays. We’re so confident in our oils abilities to safeguard your home
                        we offer a 365 day satisfaction guarantee.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <img src="{{ asset('images/bubble-glasses.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pd0">
    <div class="cta-section text-center bg-secondary white-text">
        <div class="container-fluid">
            <h2>Try us for yourself—get everything you need to make a natural home with our essential oil recipe box</h2>
            <a href="{{ route('subscription') }}" class="btn btn-underlined white-text">GET STARTED TODAY</a>
        </div>
    </div>
</section>
@endsection