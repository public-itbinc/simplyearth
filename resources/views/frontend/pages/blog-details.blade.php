@extends('themes.frontend') 
@section('body_class', 'blog-details') 
@section('content')
@section('title', !empty($post->seo_title) ? $post->seo_title : $post->title )
<?php
if (isset($post->author) and $post->author->avatar != 'null') {
  $author = "storage/{$post->author->avatar}";
} else {
  $author = 'https://loremflickr.com/320/240';
    $post->author = (object) [
        'name' => 'Unknown',
        'avatar' => 'https://loremflickr.com/320/240',
        'about' => 'No information available'
    ];
}

?>
        <div class="container-fluid" style="display:flex;justify-content:  center;">

            <div class="blog-detail">  
                <div class="header">
                    <h1 class="blue-text">{{ ucwords($post->title) }}</h1>

                    <div class="author">
                        <div class="image" style="background-image: url('{{ asset($post->author->avatar) }}')"></div>
                        <div class="content">
                            <label>By {{ ucwords($post->author->name) }}</label>
                            <span>Posted on {{ date('F d, Y', strtotime($post->created_at)) }}</span><br>
                            <?php if (Auth::check()): ?>
                            <a href="{{ route('admin.blogs.edit', $post->id) }}">Edit</a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="social-links">
                        <div class="links">
                            <div class="ssk-group">
                                <a href="" class="ssk ssk-facebook"></a>
                                <a href="" class="ssk ssk-twitter"></a>
                                <a href="" class="ssk ssk-google-plus"></a>
                                <a href="" class="ssk ssk-pinterest"></a>
                                <a href="" class="ssk ssk-tumblr"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="body">
                     {!! $post->content !!} 
                </div>
      
                <div class="footer">
                    <div class="author">
                        <div class="image" style="background-image: url('{{ asset($post->author->avatar) }}')"></div>
                        <div class="content">
                            <label>Written by <a href="#">{{ ucwords($post->author->name) }}</a></label>
                            <p> {{ $post->author->about }} </p>
                        </div>
                    </div>

                    <div class="social-links">
                        <div class="links">
                            <div class="ssk-xs ssk-group">
                                <a href="" class="ssk ssk-facebook"></a>
                                <a href="" class="ssk ssk-twitter"></a>
                                <a href="" class="ssk ssk-google-plus"></a>
                                <a href="" class="ssk ssk-pinterest"></a>
                                <a href="" class="ssk ssk-tumblr"></a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>


                <div class="disqus-box">
                    <div id="disqus_thread"></div>
                </div>
            </div>

        </div>
@endsection
 @push('footer_scripts')

    <script>
        (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
            var d = document, s = d.createElement('script');
    
            // IMPORTANT: Replace EXAMPLE with your forum shortname!
            s.src = 'https://simplyearth.disqus.com/embed.js';
    
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
    
        })();
    </script>

    
@endpush