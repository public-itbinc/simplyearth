@extends('themes.frontend')

@section('content')
<section>
    <div class="container">
        <h1 class="blue wide-title">Terms of Use</h1>
        <p>We value you at Simply Earth. We are here to serve you. We have all our legal statements below, but because
            we value you, if you have any questions, please email us at <a href="mailto:help@simplyearth.com"
                class="green-text">help@simplyearth.com</a>, and we will get back to
            you within 1 business day.</p>
        <div class="section-video video-player-box">
            <div class="video-container"
                style="padding-bottom:0; margin:20px auto; background-repeat:no-repeat;background-size:cover;background-image:url({{ asset('images/justine-terms-of-use.jpg') }});height:480px; width:854px;">
                <img class="preview-img" src="{{ asset('images/justine-terms-of-use.jpg') }}"
                    style="visibility: hidden;">
                <a href="#" class="play-video"><span class="fa fa-play fa-2x"></p></a>
                <div class="videoPlayer" data-video-id="qEpa3xd3-lw" data-video-type="youtube"></div>
            </div>
        </div>
        <p>The Simply Earth website and services are provided by Simply Earth, Inc. (“Simply Earth”, “we” “us” or
            “our”). These terms and conditions (these “Terms and Conditions”) govern your (“you” or “your”) access to
            and use of the simplyearth.com web site (the “Website”) and all services provided by Simply Earth via the
            Website including without limitation our monthly product and gift service (collectively, the “Services”).
        </p>
        <p>Simply Earth provides an online store and subscription service for monthly delivery of natural-home-related
            products for you or as a gift to others. Access to the Website, use of the Services, and purchase of the
            Products is subject to these Terms and Conditions and the Simply Earth Privacy Policy available on the
            Website.</p>

        <h2 class="blue-text">Acceptance of Terms</h2>
        <p>By using our Website or subscribing to our Services, you indicate your unconditional acceptance of the
            following Terms and Conditions. Please read them carefully, as they may have changed since your last visit.
            The most recent version of these Terms and Conditions may be viewed at
            simplyearth.com/pages/terms-of-service</p>

        <h2 class="blue-text">Scope of Service</h2>
        <p>Simply Earth maintains this Website as a service to the user community that visits the Website subject to
            these Terms and Conditions. You are responsible for obtaining any equipment and internet service necessary
            to access our Website and for paying any fees for the equipment and service you select. We may alter,
            suspend, or discontinue this Website or the Services in whole or in part, at any time and for any reason,
            without notice. The Website may also periodically become unavailable due to maintenance, malfunction of
            computer equipment, or for other reasons.</p>
        <h2 class="blue-text">Website Content</h2>
        <p>Users have a personal, non-transferable, non-exclusive right to access and use the Content of this Website
            subject to these Terms and Conditions. The term “Content” means all information, text, images, data, links,
            software, or other material accessible through the Website or Services, whether created by us or provided by
            another person for display on the Website or through the Services.</p>
        <p>The Content may contain typographical errors, other inadvertent errors, or inaccuracies. We reserve the right
            to make changes to document names and content, descriptions or specifications of products or services, or
            other information without obligation to issue any notice of such changes.</p>
        <p>You may view, copy, download, and print Content that is available on this website or through the Services,
            subject to the following conditions:</p>
        <p>
            <ul style="list-style:disc; padding-left:40px;">
                <li>The Content may be used solely for internal informational purposes. No part of this website or its
                    Content may be reproduced or transmitted in any form, by any means, electronic or mechanical,
                    including photocopying and recording for any other purpose.</i>
                <li>The Content may not be modified.</i>
                <li>Copyright, trademark, and other proprietary notices may not be removed.</i>

            </ul>
        </p>
        <p>Nothing contained on this Website should be construed as granting, by implication, estoppel, or otherwise,
            any license or right to use this Website or any Content displayed on this Website, through the use of
            framing or otherwise, except: (a) as expressly permitted by these terms of use; or (b) with our prior
            written permission or the permission of such third party that may own the trademark or copyright of material
            displayed on this Website.</p>
        <h2 class="blue-text">Registration and Membership; Product Sales</h2>
        <p>In order to start your ongoing Simply Earth monthly subscription, you must register as a member on our
            Website. To register, you can simply provide information about yourself, your shipping information including
            your address and billing information, and your valid email address and password to create your login
            profile. All information that you provide to Simply Earth, including your credit card information, is
            subject to Simply Earth's Privacy Policy. You are responsible for keeping your registration information up
            to date through the account page on the Website.</p>
        <p>As a registered user of Simply Earth, you agree to receive emails promoting any special offer(s), including
            third party offers. You may opt-out from receiving special promotions by emailing <a
                href="mailto:help@simplyearth.com" class="green-text">help@simplyearth.com</a> or selecting to
            unsubscribe as may be provided in the applicable e-mail correspondence.</p>
        <h2 class="blue-text">Monthly Delivery</h2>
        <p>As a subscribed Simply Earth member, each month we will ship you a package with a different selection of
            items that may consist of a selection from oils, powders, waxes, stickers, containers, or other
            natural-home-related items (“Products”). Each month of your subscription, the selection of Products may
            change. Accordingly, Simply Earth cannot guarantee that a selection available in a particular timeframe will
            be available in any subsequent timeframe.</p>
        <p>BY SUBSCRIBING YOU AGREE TO PAY THE MONTHLY SUBSCRIPTION FEE FOR THE PRODUCTS THAT ARE SUPPLIED EACH MONTH.
        </p>
        <h2 class="blue-text">Products</h2>
        <p>Certain products or services may be available exclusively online through the website. We have made every
            effort to display as accurately as possible the colors and images of our products that appear at the store.
            We cannot guarantee that your computer monitor's display of any color will be accurate.</p>
        <p>We reserve the right, but are not obligated, to limit the sales of our products or Services to any person,
            geographic region, or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right
            to limit the quantities of any products or services that we offer. All descriptions of products or product
            pricing are subject to change at anytime without notice, at the sole discretion of us. We reserve the right
            to discontinue any product at any time. Any offer for any product or service made on this site is void where
            prohibited.</p>
        <p>We do not warrant that the quality of any products, services, information, or other material purchased or
            obtained by you will meet your expectations, or that any errors in the Service will be corrected.</p>

        <h2 class="blue-text">Billing and Payments</h2>
        <p>Prices for our products are subject to change without notice. We shall not be liable to you or to any
            third-party for any modification, price change, suspension or discontinuance of the Service.</p>

        <p>The price of the Services and/or goods is payable in full before delivery. We accept the following credit
            cards at this time: Visa, MasterCard, American Express, or Discover. You will automatically be charged each month for
            your ongoing subscription. If you have committed to a subscription period lasting longer than one month
            (e.g., a six month plan), you will automatically be charged each month during that subscription period, even
            if you have cancelled your subscription or membership prior to the end of that subscription period. Further,
            unless you cancel your subscription or membership prior to the end of your then-current subscription period,
            at the end of that period, your subscription will automatically be renewed for an additional subscription
            period of the same amount of time.</p>

        <p>We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel
            quantities purchased per person, per household or per order. These restrictions may include orders placed by
            or under the same customer account, the same credit card, and/or orders that use the same billing and/or
            shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by
            contacting the e-mail and/or billing address/phone number provided at the time the order was made. We
            reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers,
            resellers, or distributors.</p>

        <p>For your convenience and continuous subscription benefits as a member, if your payment method reaches its
            expiration date, you do not edit your credit card information, and you have an ongoing subscription, you
            authorize us to continue billing that credit card on file including extending the expiration date until we
            are notified by you or the credit card company that the account is no longer valid. We encourage you to
            constantly update your payment method information or cancel your membership should you wish to discontinue
            your monthly purchase of Products.</p>

        <p>We use a third party payment service in lieu of directly processing your credit card information. By
            submitting your credit card information, you grant Simply Earth the right to store and process your
            information with the third party payment service, which it may change from time to time; you agree that
            Simply Earth will not be responsible for any failures of the third party to adequately protect such
            information. All financial matters regarding your information are subject to the conditions of the third
            party payment service provider's terms of service; the current version of which is attached as a link at
            https://www.braintreepayments.com/legal. You acknowledge that we may change the third party payment service
            and move your information to other service providers that encrypt your information using secure socket layer
            technology (SSL) or other comparable security technology.</p>
        <h2 class="blue-text">Shipping and Risk of Loss</h2>
        <p>Unless otherwise indicated at the time of your purchase, shipping and handling fees are included with your
            order. Shipping dates and/or arrival times are only estimates. For loss/damage claims, you must notify
            Simply Earth within 365 days of the date of your purchase if you believe all or part of your order is
            missing or damaged.</p>

        <p>Replacement of Products and credits to your account for shipped merchandise claimed as not received are
            subject to our investigation, which may include postal-service notification. We will adjust your account at
            our discretion. Repeated claims of undelivered merchandise may result in the cancellation of your
            membership.</p>
        <h2 class="blue-text">Returns and Exchanges</h2>
        <p>If a Product is defective, you may return it within 365 days of receipt, and we will send you a new item or
            credit your account. To request a refund, please contact us at <a href="mailto:help@simplyearth.com"
                class="green-text">help@simplyearth.com</a>. When returning Products, it is your responsibility to take
            reasonable care to see that the Products are not damaged in transit and are received by us at our address as
            displayed on the Website. Please note credits resulting from the monthly charge are only available up to 30
            days past the date of the charge. Refunds are at the sole discretion of SimplyEarth.com.</p>
        <h2 class="blue-text">Local Taxes</h2>
        <p>You may be charged local sales tax or VAT, if applicable.</p>
        <h2 class="blue-text">International Access</h2>
        <p>This Website may be accessed from countries other than the United States. This Website and the Services may
            contain products or references to products that are only available within the United States and U.S.
            territories. Any such references do not imply that such products will be made available outside the United
            States. If you access and use this Website outside the United States you are responsible for complying with
            your local laws and regulations.</p>
        <h2 class="blue-text">Membership Cancellations</h2>
        <p>We work hard to make your membership satisfying; however, you may cancel your ongoing membership through the
            account page on the Website. You must update your account on the Website by the first day of the month
            following the end of your then-current subscription period.</p>

        <p>YOUR SUBSCRIPTION IS CONTINUOUS UNTIL YOU CANCEL AND, IF YOU CANCEL YOUR MEMBERSHIP BEFORE THE END OF AN
            EXISTING SUBSCRIPTION PERIOD, YOU WILL BE CHARGED FOR THE REMAINDER OF THAT SUBSCRIPTION PERIOD.</p>

        <p>YOUR SUBSCRIPTION WILL CONTINUE UNTIL YOU CANCEL AND, IF YOU DO NOT CANCEL YOUR MEMBERSHIP PRIOR TO THE FIRST
            DAY OF THE MONTH FOLLOWING THE END OF A SUBSCRIPTION PERIOD, YOUR SUBSCRIPTION WILL AUTOMATICALLY RENEW FOR
            A NEW SUBSCRIPTION PERIOD OF THE SAME AMOUNT OF TIME.</p>

        <p>All cancellation requests received after the first day of the calendar month following a Subscription Period
            will apply to the following Subscription Period.</p>

        <p>We may terminate your membership, without notice, for conduct we believe violates these Terms and Conditions
            or our policies, is harmful to our business interests, or for an inactive account.</p>
        <h2 class="blue-text">Limitation of Liability</h2>
        <p>IN NO EVENT SHALL Simply Earth OR ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AFFILIATES BE LIABLE TO ANY USER OF
            THIS WEBSITE OR ANY OTHER PERSON OR ENTITY FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR
            EXEMPLARY DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, LOSS OF DATA, LOSS OF USE, OR
            COSTS OF OBTAINING SUBSTITUTE GOODS OR SERVICES) ARISING OUT OF THE USE, INABILITY TO USE, UNAUTHORIZED
            ACCESS TO OR USE OR MISUSE OF THE PRODUCTS OR THE WEBSITE OR ANY INFORMATION CONTAINED THEREON, WHETHER
            BASED UPON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), OR OTHERWISE, EVEN IF HAS BEEN ADVISED OF THE
            POSSIBILITY OF SUCH DAMAGES OR LOSSES.</p>

        <p>YOU AGREE Simply Earth'S TOTAL CUMULATIVE LIABILITY IN CONNECTION WITH THESE TERMS AND CONDITIONS, THE
            WEBSITE, THE CONTENT, OR ANY PRODUCT OR SERVICES WHETHER IN CONTRACT, TORT, OR OTHERWISE, SHALL NOT EXCEED
            THE AMOUNT YOU PAID TO Simply Earth IN THE THEN-PRIOR CALENDAR MONTH.</p>
        <h2 class="blue-text">Indemnification</h2>
        <p>By using this Website, our Services, or supplied Products, you agree to indemnify, hold harmless and defend
            Simply Earth from any claims, damages, losses, liabilities, and all costs and expenses of defense, including
            but not limited to, attorneys' fees, resulting directly or indirectly from a claim by a third party that
            arises in connection with use of this Website, Services, or Products by you or any other person accessing
            the Website using your member login account.</p>
        <h2 class="blue-text">Content Submitted by Users</h2>
        <p>Accuracy: You may use this Website (but not subscribe to our Services) without volunteering personally
            identifiable information. Please refer to our Privacy Policy for additional information on our practices for
            handling personally identifiable information. However, if you choose to provide information to register for
            or participate in a service, event, or promotion on this Website or to use our Services, you agree that you
            will provide accurate, complete, and up to date information as requested on the screens that collect
            information from you.</p>

        <p>Liability: We are not responsible or liable for the conduct of users or for any views, opinions, and
            statements expressed in Content submitted for public display through our Website, such as through an online
            discussion forum or chat room. We do not prescreen information posted to online discussion forums or chat
            rooms, if any. With respect to such forums and chat rooms, we are acting as a passive conduit for such
            distribution and are not responsible for Content. Any opinions, advice, statements, services, offers, or
            other information in Content expressed or made available by users of an online discussion forum or chat room
            are those of the respective author(s) or distributor(s) and not of Simply Earth. We neither endorse nor
            guarantee the accuracy, completeness, or usefulness of any such Content. You are responsible for ensuring
            that Content submitted to this Website is not provided in violation of any copyright, trade secret, or other
            intellectual property rights of another person or entity. You shall be solely liable for any damages
            resulting from any infringement of copyrights, trade secret, or other intellectual property rights, or any
            other harm resulting from your uploading, posting, or submission of Content to this Website.</p>

        <p>Monitoring: We have the right, but not the obligation, to monitor Content submitted to our Website through an
            online discussion forum or chat room, to determine compliance with these Terms and Conditions and any other
            applicable rules that we may establish. We have the right in our sole discretion to edit or remove any
            material submitted to or posted in any online discussion forum or chat room provided through this Website.
            Without limiting the foregoing, we have the right to remove any material that Simply Earth, in its sole
            discretion, finds to be in violation of these Terms and Conditions or otherwise objectionable, and you are
            solely responsible for the Content that you post to this Website.</p>

        <p>Lobbying: Federal law restricts lobbying activities by tax-exempt organizations. “Lobbying” includes certain
            activities intended to influence legislation. Content posted by users does not constitute lobbying by Simply
            Earth, but may constitute lobbying by you or an organization that you represent. You are responsible for
            complying with any applicable lobbying restrictions.</p>
        <h2 class="blue-text">Prohibited Content</h2>
        <p>By accessing our Website or any chat room, online discussion forum, or other service provided through our
            Website, you agree to abide by the following standards of conduct. You agree that you will not, and will not
            authorize or facilitate any attempt by another person to use our Website or any related chat room or online
            discussion forum to:</p>
        <ul style="list-style:disc; padding-left:40px;">
            <li>
                <p>Transmit any Content that is unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar,
                    offensive, obscene, pornographic, lewd, lascivious, or otherwise objectionable, as determined by
                    Simply Earth.</p>
            </li>
            <li>
                <p>Use a name or language that Simply Earth, in its sole discretion, deems offensive.</p>
            </li>
            <li>
                <p>Post defamatory statements.</p>
            </li>
            <li>
                <p>Post hateful or racially or ethnically objectionable Content.</p>
            </li>
            <li>
                <p>Post Content which infringes another's copyright, trademark or trade secret.</p>
            </li>
            <li>
                <p>Post unsolicited advertising or unlawfully promote products or services.</p>
            </li>
            <li>
                <p>Harass, threaten or intentionally embarrass or cause distress to another person or entity.</p>
            </li>
            <li>
                <p>Impersonate another person.</p>
            </li>
            <li>
                <p>Promote, solicit, or participate in any multi-level marketing or pyramid schemes.</p>
            </li>
            <li>
                <p>Exploit children under 18 years of age.</p>
            </li>
            <li>
                <p>Engage in disruptive activity such as sending multiple messages in an effort to monopolize the forum.
                </p>
            </li>
            <li>
                <p>Introduce viruses, worms, Trojan horses and/or harmful code to the Website.</p>
            </li>
            <li>
                <p>Obtain unauthorized access to any computer system through the Website.</p>
            </li>
            <li>
                <p>Invade the privacy of any person, including but not limited to posting personally identifying or
                    otherwise private information about a person without their consent (or their parent's consent in the
                    case of a child under 13 years of age).</p>
            </li>
            <li>
                <p>Solicit personal information from children under 13 years of age.</p>
            </li>
            <li>
                <p>Violate any federal, state, local, or international law or regulation.</p>
            </li>
            <li>
                <p>Encourage conduct that would constitute a criminal or civil offense.</p>
            </li>
        </ul>
        <h2 class="blue-text">Submitted Content</h2>
        <p>Simply Earth does not claim ownership of any materials you make available through the Website. With respect
            to any materials you submit or make available for inclusion on the Website, you grant Simply Earth a
            perpetual, irrevocable, non-terminable, worldwide, royalty-free and non-exclusive license to use, copy,
            distribute, publicly display, modify, create derivative works, and sublicense such materials or any part of
            such materials. You hereby represent, warrant and covenant that any materials you provide do not include
            anything (including, but not limited to, text, images, music or video) to which you do not have the full
            right to grant Simply Earth the license specified above. You further represent, warrant and covenant that
            any materials you provide will not contain libelous or otherwise unlawful, abusive, or obscene material.
            Simply Earth will be entitled to use any content submitted by you without incurring obligations of
            confidentiality, attribution, or compensation to you.</p>
        <h2 class="blue-text">Minors</h2>
        <p>Simply Earth services are available only to, and may only be used by, individuals who are 18 years and older
            who can form legally binding contracts under applicable law. Individuals under the age of 18 can use this
            service only in conjunction with and under the supervision of a parent or legal guardian. In this case, the
            adult is the user and is responsible for any and all activities, subscribers and purchasers.</p>
        <h2 class="blue-text">Intellectual Property Rights</h2>
        <p>Unless otherwise noted, all Content contained on this Website is the property of Simply Earth and/or its
            affiliates or licensors, and is protected from unauthorized copying and dissemination by United States
            copyright law, trademark law, international conventions and other intellectual property laws. Product names
            are trademarks or registered trademarks of their respective owners.</p>

        <p>We do not claim ownership of Content submitted by users without compensation by Simply Earth and with the
            expectation that such Content will be made publicly accessible through our Website. By submitting such
            Content, however, you agree to grant us a world-wide, royalty-free, perpetual, irrevocable, non-exclusive
            license to use, distribute, reproduce, modify, adapt, create derivative works from, and publicly perform or
            display such Content. This license shall remain in effect until we delete the Content from our systems.</p>
        <h2 class="blue-text">Copyright Infringement; Notice and Take Down Procedures</h2>
        <p>If you believe that any materials on this Website infringe your copyright, you may request that they be
            removed. This request must bear a signature (or electronic equivalent) of the copyright holder or an
            authorized representative and must include the following information: (1) identification of the copyrighted
            work that you believe to be infringed, including a description of the work and, where possible, a copy or
            the location of an authorized version of the work; (2) identification of the material that you believe to be
            infringing and its location, including a description of the material, its Website location or other
            pertinent information that will help us to locate the material; (3) your name, address, telephone number,
            and email address; (4) a statement that you have a good faith belief that the complained of use of the
            materials is not authorized by the copyright owner, its agent, or the law; (5) a statement that the
            information in your claim is accurate; and (6) a statement that “under penalty of perjury,” you declare that
            you are the lawful copyright owner or are authorized to act on the owner's behalf. </p>

        <p>In an effort to protect the rights of copyright owners, we maintain a policy for the termination, in
            appropriate circumstances, of users of this Website who are repeat infringers.</p>
        <h2 class="blue-text">Security</h2>
        <p>When you register to participate in Simply Earth services on this Website, you may be required to establish a
            login identifier and a password. You are responsible for protecting your login and password from
            unauthorized use, and you are responsible for all activity that occurs on your account (including without
            limitation financial obligations). You agree to notify us immediately if you believe that your login or
            password has been or may be used without your permission so that appropriate action can be taken. We are not
            responsible for losses or damage caused by your failure to safeguard your login and password.</p>
        <h2 class="blue-text">Disclaimer of Warranty</h2>
        <p>YOU ARE SOLELY RESPONSIBLE FOR DETERMINING IF THE PRODUCTS ARE SUITABLE FOR USE. WE ARE NOT ABLE TO PROVIDE
            ANY ASSURANCES REGARDING ALLERGIES.</p>

        <p>Under no circumstances will we be liable for any loss or damage caused by your use of the Products or your
            reliance on information in any Content on this Website. YOU AGREE THAT YOUR SOLE AND EXCLUSIVE REMEDY
            ARISING FROM OR RELATING IN ANY WAY TO ANY PRODUCT SHALL BE ITS REPLACEMENT OR A CREDIT TOWARDS ANOTHER
            MONTHLY SHIPMENT, IN Simply Earth'S DISCRETION.</p>

        <p>YOU MAY RETURN DEFECTIVE PRODUCTS ONLY WITHIN THIRTY (30) DAYS OF DELIVERY. EXCEPT AS EXPRESSLY PROVIDED
            HEREIN, ALL PRODUCTS SUPPLIED, AND ALL TEXT, IMAGES, AND OTHER INFORMATION ON OR ACCESSIBLE FROM THIS
            WEBSITE ARE PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
            LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
            NON-INFRINGEMENT. SPECIFICALLY, BUT WITHOUT LIMITATION, Simply Earth DOES NOT WARRANT THAT: (i) THE
            INFORMATION AVAILABLE ON THIS WEBSITE IS FREE OF ERRORS; (ii) THE PRODUCTS OR SERVICES ARE NOT DEFECTIVE;
            (iii) THE FUNCTIONS OR SERVICES (INCLUDING BUT NOT LIMITED TO MECHANISMS FOR THE DOWNLOADING AND UPLOADING
            OF CONTENT) PROVIDED BY THIS WEBSITE WILL BE UNINTERRUPTED, SECURE, OR FREE OF ERRORS; (iv) DEFECTS WILL BE
            CORRECTED, OR (v) THIS WEBSITE OR THE SERVER(S) THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL
            COMPONENTS.</p>
        <h2 class="blue-text">Exclusions</h2>
        <p>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF
            LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY
            TO YOU.</p>
        <h2 class="blue-text">Links to Third-Party Websites</h2>
        <p>This Website may contain links to third party Websites that are provided to you as a convenience. Any outside
            Website accessed from our Website is independent from Simply Earth, and we have no control over the content
            of such Websites. We are not responsible for the content of any linked Website or for any loss or damage
            incurred in connection with your use such links or dealings with the operators of such third party Websites.
        </p>
        <h2 class="blue-text">No Implied Endorsements</h2>
        <p>In no event shall any reference to any third party or third party product or service be construed as an
            approval or endorsement by Simply Earth of that third party or of any product or service provided by a third
            party. Likewise, a link to any third party Website does not imply that we endorse or accept any
            responsibility for the content or use of such a Website. Simply Earth does not endorse, warrant or guarantee
            any product or service offered by any third party through an online discussion forum or chat room accessible
            through this Website and will not be a party to or in any way monitor any transaction involving any third
            party providers of products or services. As with the purchase of a product or service through any medium or
            in any environment, you are responsible for exercising appropriate judgment and caution.</p>
        <h2 class="blue-text">Jurisdictional Issues</h2>
        <p>We make no representation that information on this Website, or the Products or Services we provide, are
            appropriate or available for use outside the United States. Those who choose to access this Website from
            outside the United States do so on their own initiative and at their own risk and are responsible for
            compliance with applicable local laws.</p>
        <h2 class="blue-text">Termination</h2>
        <p>We may terminate any user's monthly subscription or access to our Website or Services, including access to
            any online discussion forum or chat room, in our sole discretion, for any reason and at any time, with or
            without prior notice. It is our policy to terminate users who violate these terms and conditions, as deemed
            appropriate in our sole discretion. You agree that we are not liable to you or any third party for any
            termination of your access to our Website or Services.</p>
        <h2 class="blue-text">Enforcement</h2>
        <p>These Terms and Conditions shall be governed and interpreted pursuant to the laws of W4228 Church Road,
            Waldo, Wisconsin, United States of America 53093, notwithstanding any principles of conflicts of law.</p>

        <p>All disputes arising out of or relating to these Terms and Conditions shall be finally resolved by
            arbitration conducted in the English language in the U.S.A. under the commercial arbitration rules of the
            American Arbitration Association. The parties shall bear equally the cost of the arbitration (except that
            the prevailing party shall be entitled to an award of reasonable attorneys' fees incurred in connection with
            the arbitration in such an amount as may be determined by the arbitrator). All decisions of the arbitrator
            shall be final and binding on both parties and enforceable in any court of competent jurisdiction.
            Notwithstanding the foregoing, we shall be entitled to seek injunctive relief, security, or other equitable
            remedies from federal and state courts or any other court of competent jurisdiction. Under no circumstances
            shall the arbitrator be authorized to award punitive damages, including but not limited to federal or state
            statutes permitting multiple or punitive damage awards. Any purported award of punitive or multiple damages
            shall be beyond the arbitrator's authority, void, and unenforceable. BY AGREEING TO THESE TERMS OF SERVICE,
            YOU ARE WAIVING YOUR RIGHT TO A JURY TRIAL AND YOUR RIGHT TO HAVE A COURT HEAR CLAIM ARISING IN CONNECTION
            WITH THIS AGREEMENT, AMONG OTHER WAIVERS OF RIGHTS SET FORTH IN THIS AGREEMENT.</p>
        <h2 class="blue-text">Severability</h2>
        <p>To the extent you are located in the United States, if any provisions of this Agreement are not permitted by
            applicable law or regulation, those provisions shall be of no force or effect as between you and Simply
            Earth. If any part of these Terms and Conditions is held to be unlawful, void, or unenforceable, that part
            will be deemed severable and shall not affect the validity and enforceability of the remaining provisions as
            between you and Simply Earth.</p>
        <h2 class="blue-text">Entire Agreement</h2>
        <p>The failure of us to exercise or enforce any right or provision of these Terms of Service shall not
            constitute a waiver of such right or provision. </p>

        <p>These Terms of Service and any policies or operating rules posted by us on this site or in respect to The
            Service constitutes the entire agreement and understanding between you and us and govern your use of the
            Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or
            written, between you and us (including, but not limited to, any prior versions of the Terms of Service).</p>

        <p>Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting
            party.</p>
        <h2 class="blue-text">For Additional Information</h2>
        <p>If you have any questions about these Terms and Conditions, please contact <a href="mailto:help@simplyearth.com" class="green-text">help@simplyearth.com</a>.</p>
    </div>
</section>
@endsection