@extends('themes.frontend')

@section('content')
<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Free Shipping</h1>
                    <p>Shipping is free on orders over $29 to anywhere in the USA!  *Includes 50 states and U.S. territories.</p>
                    <p>$5.95 shipping on orders under $29</p>
                    <p>All orders will arrive within 2-5 business days.</p>

                    <a href="{{ route('products') }}" class="btn btn-primary">SHOP SIMPLY EARTH</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/503x397.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pdtb40 bg-default darker">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Free Returns</h1>
                    <p>If you don’t love what you purchased from Simply Earth, feel free to contact us within 365 days for a full refund in the form of store credit. To request a store credit, just contact us with your order info.<p>

                    <p>Contact Simply Earth Customer Service by phone or email at (866)-330-8165 or <a class="green-text" href="mailto:hello@simplyearth.com">hello@simplyearth.com.</a></p>

                    <p>*When you contact us, please include:
                        <ul class="bullets">
                            <li>Your first and last name.</li>
                            <li>Your order number or a copy of your digital receipt</li>
                            <li>The name of the product you intend to return or replace</li>
                            <li>Why you aren’t entirely happy with your purchase</li>
                        </ul>
                    </p>
                    <p>If you choose to email us, please include a photo of the item. If the item is damaged, please make sure the damage is visible in the photo.</p>

                    <a href="{{ route('products') }}" class="btn btn-primary">SHOP SIMPLY EARTH</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/481x424.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection