@extends('themes.frontend')

@section('content')
<section>
	<div class="container-fluid">
		<div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue wide-title">Give a $20 Gift Card,<br>Earn a Free Box</h1>
					<p>For every three friends you refer, you earn a free essential oil recipe box. No limits on rewards.</p>
					<a href="#" class="btn btn-primary">START EARNING YOUR FREE BOX</a></center>
				</div>
			</div>
			<div class="col-md-6">
                <div class="section-video video-player-box">
                    <div class="video-container">
                        <img src="{{ asset('images/ambassadress.jpg') }}" class="preview-img">
                        <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                        <div class="videoPlayer" data-video-id="vf3YYN8hJc" data-video-type="youtube"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>

<section class="how-it-works">
			<div class="container-fluid">

				<h1 class="section-title">How it Works</h1>
				<div class="row work-row">
					<div class="col-sm-3 box--h">
						<div class="work-box">
							<div class="image"><img src="{{ asset('images/Design.jpg') }}"></div>
							<div class="box--content">
								<h3>SIGN UP</h3>
								<p>Your custom coupon code awaits.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3 box--h">
						<div class="work-box">
							<div class="image"><img src="{{ asset('images/Deliver.jpg') }}"></div>
							<div class="box--content">
								<h3>SHARE</h3>
								<p>You get a unique coupon code.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3 box--h">
						<div class="work-box">
							<div class="image"><img src="{{ asset('images/Discover.jpg') }}"></div>
							<div class="box--content">
								<h3>GIVE</h3>
								<p>Your friend gets a Big Bonus Box and a $20 gift card.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3 box--h">
						<div class="work-box">
							<div class="image"><img src="{{ asset('images/Discover.jpg') }}"></div>
							<div class="box--content">
								<h3>ENJOY!</h3>
								<p>Enjoy a free box for every three friends that subscribe.</p>
							</div>
						</div>
					</div>
				</div>
			
			</div>
</section>

@endsection