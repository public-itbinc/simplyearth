@extends('themes.frontend')

@section('content')

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy">
                    <p>We are on a mission to make having a natural home fun and easy. We started Simply Earth knowing that it is our mission to give employees a chance to really use their gifts and flourish.<br><br>So, if joining a high-performing, incredibly fun, and creative team sounds like something you'd love, email us at <a href="mailto:hr@simplyearth.com" class="green-text">hr@simplyearth.com.</a> We're always looking for amazing talent and we'd love to chat.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/our-team.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection