@extends('themes.frontend')

@section('content')
<section class="bg-default">
	<div class="container-fluid">
		<div class="row eq-height image-with-text"> <!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue wide-title">Get Rewarded While Making Your Home Natural</h1>
					<p>Simply shop & share to earn drops for deep loyalty discounts, free gifts, and more!</p>
					<a href="#" class="btn btn-primary">JOIN NOW</a></center>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="section-image">
					<div class="loader"></div>
					<img class="preview-img" src="{{ asset('images/Shop_SingleOils.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid">
		<div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue wide-title">Give a $20 Gift Card and Get a Free Box</h1>
					<p>For every three friends that you Give the gift of a natural home! Send a coupon code for a FREE $20 to a friend when they subscribed to our essential oil recipe box. When three of your friends subscribe, you get a FREE essential oil recipe box. </p>
					<a href="#" class="btn btn-primary">LEARN MORE</a></center>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="section-image">
					<div class="loader"></div>
					<img class="preview-img" src="{{ asset('images/natural-oil-home.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection