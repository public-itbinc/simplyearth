@extends('themes.frontend')

@section('content')
<section class="pdtb40 bg-secondary">
	<div class="container-fluid">
		<div class="cta-section text-center bg-secondary white-text our-story-header-text">
			<h1>We created Simply Earth to make having a natural home fun and easy at honest prices.</h1>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid">
		<div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue wide-title">It all started with katies stinky feet</h1>
					<p>Every idea starts with a problem. For us, it was katie’s stinky feet. The only solutions we could find were chemical laden products. We did the research and found a blend of oils that would naturally eliminate the odor. When sourcing quality and pure oils in bulk, we found out they didn’t have to be so expensive.</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="section-image">
					<div class="loader"></div>
					<img class="preview-img" src="{{ asset('images/katie-smiling.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pdtb40 bg-default darker">
	<div class="container-fluid">
		<div class="row eq-height image-with-text"> <!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue wide-title">An Idea Was Born</h1>
					<p>We were tired of overpaying for essential oils and having to go to parties to actually buy them. We were frustrated by using natural products that didn’t always work. When we asked around, we learned lots of women were upset about the situation too, so we decided to do something about it. We started Simply Earth to help make your home natural.</p>
				</div>
			</div>
			<div class="col-md-6">
                <div class="section-video video-player-box">
                    <div class="video-container">
                        <img class="preview-img" src="{{ asset('images/story_video_overlay.jpg') }}">
                        <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                        <div class="videoPlayer" data-video-id="v3t8rAsvY6U" data-video-type="youtube"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>

<section class="pdtb40">
	<div class="container-fluid">
		<div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue wide-title">Make Your Home Natural</h1>
					<p>We loved making our own natural products and knowing what was in them, so we created . With the essential Oil recipe box, you get everything you need to make your home natural; easy and fun recipes to follow, honest prices, and products that work. It puts you in charge and you can enjoy time together making natural recipes with friends and family.</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="section-image">
					<div class="loader"></div>
					<img class="preview-img" src="{{ asset('images/enjoy-a-natural-home.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pdtb40 bg-secondary">
	<div class="container-fluid">
		<div class="cta-section text-center bg-secondary white-text">
			<h2>Try us for yourself</h2>
			<h4>Get everything you need to make a natural home with our essential oil recipe box. </h4>
			<a href="{{ route('subscription') }}" class="btn btn-underlined white-text">GET STARTED TODAY</a>
		</div>
	</div>
</section>

@endsection