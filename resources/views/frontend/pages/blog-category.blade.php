@extends('themes.frontend')

@section('body_class', 'blog-category')

@section('content')
  <section class="">
      <div class="container-fluid">
          <div class="convert-flow">
            <div class="cf-1515-area-9755"></div>
          </div>
          <div class="blog-title">
              <h3 class="green-text">SEARCH THE BLOG</h3>
          </div>
          <div class="blog-filter clearfix">
              <div class="input">
                <form class="" action="{{ route('admin.blogcategorygroup.store') }}" method="post">
                  {{ csrf_field() }}
                  <div class="btn-group">
                    <select class="form-control" name="parent_category">
                      <option value="">Filter by category</option>
                      @foreach ($parentCategory as $key => $cat)
                        <option value="{{ $cat->parent }}">{{ ucfirst($cat->name) }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="btn-group">
                    <select class="form-control" name="first_sub_category">
                      <option value="">Filter by Sub-category</option>
                      @foreach ($firstSubCategory as $key => $cat)
                        <option value="{{ $cat->firstSubCategory->id }}">{{ ucfirst($cat->firstSubCategory->name) }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="btn-group">
                    <select class="form-control" name="second_sub_category">
                      <option value="">Filter by Sub-category</option>
                      @foreach ($secondSubCategory as $key => $cat)
                        <option value="{{ $cat->secondSubCategory->id }}">{{ ucfirst($cat->secondSubCategory->name) }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" id="filter">Filter</button>
                  </div>
                </form>
              </div>
          </div>

          <div class="masonry" id="masonry">
            @foreach ($blogs as $key => $blog)
              <div class="grid-item">
                <div class="inner">
                  <a href="{{ route('single.post', $blog->blogPost->slug) }}">
                    <img src="{{ asset("storage/{$blog->blogPost->image}") }}">
                  </a>
                  <div class="details">
                    <label>{{ ucfirst($blog->blogCategory->name) }}</label>
                    <a href="{{ route('single.post', $blog->blogPost->slug) }}">
                      <h1 style="text-transform:  uppercase;">{{ $blog->blogPost->title }}</h1>
                    </a>

                    <div class="tags">
                      @foreach ($blog->blogPost->groups as $key => $blog)
                        <a href="{{ route('admin.blogtagsgroups.show', $blog->blogTag->id) }}" style="text-transform: uppercase;">{{ $blog->blogTag->name }}</a>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
      </div>
  </section>
@endsection
