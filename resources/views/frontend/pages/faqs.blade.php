@extends('themes.frontend') 
@section('content')

<section class="banner-page pd0">
    <img class="banner-img" src="{{ asset('images/compare-chart-banner.jpg') }}" alt="6 steps">
</section>


<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Simply Earth Questions</h2>
                <div class="se-accordion panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                            <h4 class="panel-title blue-text">
                                <a href="#collapseOne">
                                    <p>Who is Simply Earth? </p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                We are a small, family-run essential oils company based in Hingham, Wisconsin. There is lots more to our story, though: you
                                can read more on our <a class="green-text" href="{{ route('our-story') }}">“Our Story” page!</a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#partnership">
                            <h4 class="panel-title blue-text">
                                <a href="#collapseOne">
                                    <p>Have a partnership opportunity? </p>
                                </a>
                            </h4>
                        </div>
                        <div id="partnership" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                If you have a partnership request, please email our team at <a class="text-primary" href="mailto:hello@simplyearth.com">hello@simplyearth.com</a>. A person on the team will review your
                                request within the next few weeks and, if they feel that this would be a good fit, they’ll
                                shoot you an email to discuss further.
                            </div>
                        </div>
                    </div>

                    <h2 class="section-title">General Product Questions</h2>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
                            <h4 class="panel-title blue-text">
                                <a href="#collapseTwo">
                                    <p>Do you dilute your essential oils?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                No! We do not dilute our oils - every oil we sell is 100% pure, guaranteed. You can see quality process on our Simply Pure
                                Promise page!
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-oils-produced">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-oils-produced">
                                    <p>Where are your oils produced from?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-oils-produced" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Our oils come from farms all over the world, are quality tested, and bottled right in our own commercial kitchen. You can
                                find their exact location on our <a class="green-text" href="{{ route('pure-natural-products') }}"> Simply Pure Promise page</a>.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-how-to-dilute">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-how-to-dilute">
                                    <p>How to dilute the essential oils?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-how-to-dilute" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <table class="table table-bordered" style="table-layout: fixed;max-width: 500px;">
                                    <tr>
                                        <th>% Dilution</th>
                                        <th>Use it for</th>
                                    </tr>
                                    <tr>
                                        <td>1%</td>
                                        <td>Children, those who are pregnant, elderly and those with sensitive skin</td>
                                    </tr>
                                    <tr>
                                        <td>2%</td>
                                        <td>General Blends<br />(oil massage and daily use)</td>
                                    </tr>
                                    <tr>
                                        <td>3%</td>
                                        <td>Blend specific</td>
                                    </tr>
                                </table>
                                <p>(massage oil and daily use)</p>

                                <p> 1% = 1 EO drop + 1 tsp carrier oil<br /> 2% = 2 EO drops +1 tsp carrier oil<br /> 3% = 3
                                    EO drops + 1 tsp carrier oil<br /> We recommend to start at lower dilution and add as
                                    needed.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
        <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Subscription</h2>
                <div class="se-accordion panel-group" id="eo-accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#fun-monthly-themes">
                            <h4 class="panel-title green-text">
                                <a href="#fun-monthly-themes">
                                    <p>Fun monthly themes? Wow! Now what do I get in my box?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="fun-monthly-themes" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Every month, we’ll send 4 full size 100% pure essential oils + 1 -2 extra + bottles to make 5-6 recipes. So you’ll have everything
                                you need. Each month is a new theme. Plus, you’ll get more ideas and recipes on how to use
                                the oils inside your box outside of the recipes included.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#is-it-really">
                            <h4 class="panel-title green-text">
                                <a href="#is-it-really">
                                    <p>IS IT REALLY ONLY $39 FOR 4 ESSENTIAL OILS + EXTRAS?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="is-it-really" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Yes! A monthly Simply Earth box, with no long-term commitment, is only $39 a month. If you bought everything included in
                                a box from the big essential oil companies, it would cost over $150. We keep prices low by
                                selling directly to you and not taking crazy margins. We keep quality pure by testing every
                                oil for purity and visiting the farms our oils come from ourselves. <a href="https://simplyearth.com/pages/subscription-box" class="text-primary">https://simplyearth.com/pages/subscription-box</a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#firstsub-collapseFive">
                            <h4 class="panel-title green-text">
                                <a href="#firstsub-collapseFive">
                                    <p>I saw something about a Big Bonus Box. What is it?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="firstsub-collapseFive" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>When you subscribe, you get our BIG BONUS BOX whith your first purchase. It contains worth
                                    $50 of supplies: hard oil, spray bottles, roller bottles, and more!</p>

                                <p>Plus, you automatically get this Big Bonus Box shipped to you with every 6 box.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#firstsub-collapseEight">
                            <h4 class="panel-title green-text">
                                <a href="#firstsub-collapseEight">
                                    <p>I just placed my subscription! When will my box be shipped?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="firstsub-collapseEight" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Expect your favorite piece of mail---your box!--- to ship the next day you ordered it and arrive 2-3 business days after
                                that. We always have free & fast shipping on our boxes.
                            </div>
                        </div>
                    </div>



                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#firstsub-collapseNine">
                            <h4 class="panel-title green-text">
                                <a href="#firstsub-collapseNine">
                                    <p>I just received my first box, when will my next box be shipped?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="firstsub-collapseNine" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                We’re glad to hear that you received your first box! Your next box will be billed and shipped on the same day you placed
                                your first order in.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-add-other-products">
                            <h4 class="panel-title green-text">
                                <a href="#accordion-add-other-products">
                                    <p>Can I add other products to be shipped along with my box?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-add-other-products" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Yes you can! Just log into your account and when you add an item to your cart, you’ll be given the option of shipping it
                                right away, or adding it to your subscription to get free shipping on the item.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-applying-subscription">
                            <h4 class="panel-title green-text">
                                <a href="#accordion-applying-subscription">
                                    <p>I'm thinking about applying for a subscription, what subscription options do you offer?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-applying-subscription" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>We have 2 subscription options, a <strong>monthly</strong> and a <strong>quarterly</strong> subscription.</p>

                                <p>In <strong>MONTHLY</strong> subscription, you will receive <strong>ONE BOX EVERY MONTH</strong>. This costs only $39 a month.</p>

                                <p>In <strong>QUARTERLY</strong> subscription, you will receive <strong>ONE BOX, EVERY 3 MONTHS</strong>. This subscription costs
                                    $45 every 3 months.</p>

                                <p>There is no membership fee for subscribing!</p>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-active-quarterly">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-active-quarterly">
                                    <p>I have an active quarterly subscription and I want to have a monthly subscription, how
                                        do I change that?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-active-quarterly" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>The first thing you will have to do is log in to your account, after logging in click the
                                    “Manage Subscriptions” on the left side of your account panel. Next is click the “Edit”
                                    button in the subscription and then “Change Delivery Schedule”.</p>

                                <p>If your having problems, you can reach us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> and our representatives
                                    will be happy to assist you!</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#mansub-collapseThree">
                            <h4 class="panel-title blue-text">
                                <a href="#mansub-collapseThree">
                                    <p>I will be out of town for a while, can I skip my boxes?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="mansub-collapseThree" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Sure thing! You can do that by logging in to your account then click “Manage Subscriptions”
                                    on the left side of your account panel. Then click on the “Edit” button and click “Change
                                    Next Charge Date” and you’re good to go!</p>

                                <p>If your having problems, you can reach us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> and our representatives
                                    will be happy to assist you!</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-change-shipping-date">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-change-shipping-date">
                                    <p>Can I change the date my box is shipped?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-change-shipping-date" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Yes you can. Just login to your account and click manage subscription. Next, click change
                                    date.
                                </p>

                                <p>If your having problems, you can reach us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> and our representatives
                                    will be happy to assist you!</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-cancel-subscription">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-cancel-subscription">
                                    <p>Unfortunately, I will need to cancel my subscription. How can I do that?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-cancel-subscription" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>We’re sad to say goodbye to your subscription, but we’ll be happy to assist you on this.
                                    Just log in to your account and click “Manage Subscriptions” after that you will see
                                    a “Cancel” button and you’ll have to enter your reason as to why you will cancel your
                                    subscription. After providing the needed details, just click the green button to proceed
                                    with the cancellation of your subscription.</p>

                                <p>If your having problems, you can reach us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> and our representatives
                                    will be happy to assist you!</p>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-reactivate">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-reactivate">
                                    <p>I'd like to reactivate my subscription please!</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-reactivate" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Just login to your account and click, reactivate my subscription.</p>

                                <p>If your having problems, you can reach us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> and our representatives
                                    will be happy to assist you!</p>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

        <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Manage Your Account</h2>
                <div class="se-accordion panel-group" id="sg-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-moved-address">
                            <h4 class="panel-title green-text">
                                <a href="#accordion-moved-address">
                                    <p>I’ve moved to a different address, how do I change my address?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-moved-address" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>The first thing you’ll have to do is log in to your account, afterwards you will see your
                                    Primary Addresses in the account panel on the left side. Click “View Addresses” so you
                                    can see the current/default address in your account. Click “Edit” and fill up the information
                                    needed.
                                </p>

                                <p>If your having problems, you can reach us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> and our representatives
                                    will be happy to assist you!</p>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-cant-login">
                            <h4 class="panel-title green-text">
                                <a href="#accordion-cant-login">
                                    <p>Help! I can’t log in to my account!</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-cant-login" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                No need to worry! Just click forgot password when you are login in and we will send you an email to reset your password.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-update-billing-information">
                            <h4 class="panel-title green-text">
                                <a href="#accordion-update-billing-information">
                                    <p>How can I update my billing information?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-update-billing-information" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                No need to worry! Just login to your account and click, Payment Method to update your payment method.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

       <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Orders</h2>
                <div class="se-accordion panel-group" id="sg-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-accepted-payment-methods">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-accepted-payment-methods">
                                    <p>What payment methods do you accept?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-accepted-payment-methods" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                We accept all major credit cards and PayPal through our secure checkout process.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-track-orders">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-track-orders">
                                    <p>How do I track my order I just placed</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-track-orders" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>You’re able to track you orders in your Purchase History while logged into your account.
                                    Click into the order you’d like to track — then, click the "Tracking info" link at the
                                    top of the page. Your tracking link will be available once your order status is "Complete."</p>

                                <p>You'll also receive your tracking information for each order via email.</p>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-accidentally-placed-order">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-accidentally-placed-order">
                                    <p>I accidentally placed an order, how do I cancel it?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-accidentally-placed-order" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                No worries, message us at <a class="text-primary" href="mailto:help@simplyearth.com">help@simplyearth.com</a> ASAP so we can check your order information and we’ll cancel it from our end.
                                Orders are processed and shipped out within 24 business hours of your purchase.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-not-satisfied">
                            <h4 class="panel-title blue-text">
                                <a href="#accordion-not-satisfied">
                                    <p>I just received an item I’m not satisfied with, what do I do?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-not-satisfied" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>If you don’t love what you purchased from Simply Earth, feel free to contact us within 365
                                    days for a full refund in the form of store credit or a fix for your unsatisfactory purchase.
                                    To request a refund or replacement just contact us with your order info.</p>

                                <p>Contact Simply Earth Customer Service by phone or email at (866)-330-8165 or <a class="text-primary" href="mailto:hello@simplyearth.com">hello@simplyearth.com</a>.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-free-shipping-offer">
                            <h4 class="panel-title green-text">
                                <a href="#accordion-free-shipping-offer">
                                    <p>Do you offer free shipping?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="accordion-free-shipping-offer" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Shipping is free on orders over $29 to anywhere in the USA!<br /> *Includes 50 states and
                                    U.S. territories.</p>

                                <p>$5.95 shipping on orders under $29</p>

                                <p>All orders will arrive within 3-5 business days.</p>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#ord-collapseTwo">
                            <h4 class="panel-title green-text">
                                <a href="#ord-collapseTwo">
                                    <p>I live outside US, what shipping options do I have?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="ord-collapseTwo" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>If you live in Canada, here are our shipping rates:</p>

                                <p>Shipment fee for Canada:</p>

                                <p>$0 - $29.99 = $15.99<br /> Orders Over $30 = $9.99<br /> Free shipping for orders over $100</p>

                                <p>We do not ship overseas as this time. Email wholesale@simplyearth.com for large orders.</p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

         <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="section-title">Gifting</h1>
                <div class="se-accordion panel-group" id="sg-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#gft-collapseOne">
                            <h4 class="panel-title blue-text">
                                <a href="#gft-collapseOne">
                                    <p>Do you have gift cards?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="gft-collapseOne" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Yes
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#gft-collapseTwo">
                            <h4 class="panel-title blue-text">
                                <a href="#gft-collapseTwo">
                                    <p>How do I gift my subscriptions boxes to my friends & Family?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="gft-collapseTwo" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Just login to your account and go to simplyearth.com/future-orders. Then click the Gift This Month button and enter in the
                                information and that box will be shipped to your friends or family!
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#gft-update-friends">
                            <h4 class="panel-title blue-text">
                                <a href="#gft-update-friends">
                                    <p>How do I update a gifted subscriptions boxes to my friends & Family?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="gft-update-friends" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Just login to your account and go to <a href="{{ route('future-orders') }}">simplyearth.com/future-orders</a>.
                                Then click the update on the month you have gifted.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#gft-ongoing-box">
                            <h4 class="panel-title blue-text">
                                <a href="#gft-ongoing-box">
                                    <p>How do I gift an ongoing subscription box?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="gft-ongoing-box" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Just sign up for the box and put in their name and shipping address.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#gft-collapseThree">
                            <h4 class="panel-title blue-text">
                                <a href="#gft-collapseThree">
                                    <p>Do you have any options for corporate gifting?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="gft-collapseThree" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Please email <a class="text-primary" href="mailto:hello@simplyearth.com">hello@simplyearth.com</a> with your corporate gifting needs and we will work with you for the perfect gift.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="section-title">Coupon Codes, Discount Codes, and Referral Program</h1>
                <div class="se-accordion panel-group" id="sg-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#cdr-collapseOne">
                            <h4 class="panel-title green-text">
                                <a href="#cdr-collapseOne">
                                    <p>I have a code that I want to use on my purchase, how do I use it?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="cdr-collapseOne" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                After checking out, the total amount of your cart will be shown in the Order Summary. Click the “Have a discount code? Enter
                                it here” button so a field will appear for you to enter the discount code.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#cdr-collapseTwo">
                            <h4 class="panel-title green-text">
                                <a href="#cdr-collapseTwo">
                                    <p>I have 2 codes, how do I apply the second one?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="cdr-collapseTwo" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                We’re truly sorry, but only one code is allowed per order.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#cdr-forgot">
                            <h4 class="panel-title green-text">
                                <a href="#cdr-forgot">
                                    <p>I have a coupon code that I forgot to use when I placed my order, can you help?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="cdr-forgot" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                Please contact us with your order number and promo code ASAP and we’ll do our best to take care of it!
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#cdr-collapseFive">
                            <h4 class="panel-title green-text">
                                <a href="#cdr-collapseFive">
                                    <p>A friend of mine wants to subscribe to your boxes. How can I have credit for the purchase?</p>
                                </a>
                            </h4>
                        </div>
                        <div id="cdr-collapseFive" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <p>Thank you for sharing the word of Simply Earth. You can create your own referral code in
                                    this link: <a href="http://simplyearth.referralcandy.com/">http://simplyearth.referralcandy.com/</p>

                                    <p>Make sure that the purchase is made through your link so that you will be credited for the purchase.</p>                                    
                            </div>
                        </div>
                    </div>                   

                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="bg-default">

</section>

<section>

</section>

<section>
 
</section>

<section>
   
</section>

<section>
   
</section> -->
@endsection