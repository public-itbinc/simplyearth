@extends('themes.frontend')

@section('content')

<section>
    <div class="container-fluid">
        <div class="row eq-height video-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Our Cause: Fighting Human Trafficking</h1>
                    <p>As a company who supplies products to enhance the beauty of our customers, we at Simply Earth believe it to be our responsibility to help people whose beauty has been abused. The average age of an individual forced into human trafficking is 13. That is why when you shop Simply Earth, 13% of our profits go to help fight human trafficking.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-video video-player-box">
                    <div class="video-container">
                        <img class="preview-img" src="{{ asset('images/Daintry.jpg') }}">
                        <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                        <div class="videoPlayer" data-video-id="JsfSnGEaknU" data-video-type="youtube"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">What Can You Do?</h1>
                    <p>After further research into this issue it is easy to feel overwhelmed, but together we can help end human trafficking. While tackling this issue on a national level is always a good idea, you need to start right where you live. That is where you can have an impact. Check out the organizations we support.  These organizations are tirelessly working to fight and ultimately end human trafficking.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/Cause.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pd0">
    <div class="cta-section text-center bg-secondary white-text">
        <div class="container-fluid">
            <p>13% of our profits go to help end human trafficking.</p>
            <h2>Check Out The Organizations We Support</h2>
            <p>Each month we highlight one organization so that you can get involved.</p>
        </div>
    </div>
</section>


<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Nightlight</h1>
                    <p>Nightlight is a non-profit organization that helps victims of commercial sexual exploitation. They rescue, give hope and provide intervention. They offer alternative vocational 
opportunities, life-skills training, as well as spiritual, physical and emotional development. They have support networks all over the world to help women, men and children who were once victims of the sex industry.</p>
                    <a href="http://www.nightlightinternational.com/get-involved/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/night-light.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">A21</h1>
                    <p>A21 fights to fully eradicate slavery and stop the demand for it. THey have 12 offices around the world that tirelessly rescue, restore and rebuild the lives of those who were victims of modern day slavery. They aim to provide education and awareness to the next generation to stop it from happening.</p>
                    <a href="http://www.a21.org/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/a-21.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Destiny Rescue</h1>
                    <p>Destiny Rescue’s mission is to save children who are trafficked or trapped in sexual exploitation. They provide safe shelter, education, vocational training, and counseling. They also make sure the children have proper healthcare and spiritual guidance. Most of all, they give love.</p>
                    <a href="https://www.destinyrescue.org/us/get-involved/partnerships/ambassadors/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/destiny-rescue.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">My Refuge House</h1>
                    <p>What started in a small church in South California, My Refuge House is now the home of more than 40 young women who were once victims of human trafficking. Each one is given therapeutic care and education to help rebuild their lives.</p>
                    <a href="http://www.myrefugehouse.org/give/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/refuge-house.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">5 Stones</h1>
                    <p>5-Stones is an all-volunteer, nonprofit organization that fights human trafficking through education, awareness, and prevention. They reach out to local communities by teaching them the dangers of human trafficking as well as the facts about it. They raise awareness through helping the people be involved in this fight.</p>
                    <a href="http://5-stones.org/donate/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/5-stones.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Freedom Cry</h1>
                    <p>Freedom Cry is a faith-based organization based in Wisconsin. They help survivors of human trafficking by providing “Hope Bags” that contain clothes and basic toiletries. They spread awareness by having presentations in churches, schools and the community. They are strong advocates of giving the proper sentences to the traffickers.</p>
                    <a href="http://freedomcryinc.weebly.com/donate.html" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/freedom-cry.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pd0">
    <div class="cta-section text-center bg-secondary white-text">
        <div class="container-fluid">
            <h2>13% of profits from your purchase go to help end human trafficking</h2>
            <a href="{{ route('products') }}" class="btn btn-underlined white-text">Shop Simply Earth and Make a Difference</a>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Children of the Night</h1>
                    <p>Prostitution is quite common even to this day. Children of the Night is a privately funded organization that rescues victims of prostitution all over the United States. In their facility, they have an on-site school and a college placement program. They give access to education by providing free GED assessment, tutoring assessment and even transportation during the examination day. This program is for those who do not have access to traditional education. They have a 24-hour hotline to help anyone who needs help.</p>
                    <a href="https://www.childrenofthenight.org/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/children-of-the-night.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>



<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Shared Hope International</h1>
                    <p>Shared Hope International is dedicated to bringing sex trafficking into a halt through their three-pronged approach - prevent, restore and bring justice. They teach communities about warning signs of trafficking and employ intervention. Shared Hope has programs and partners that help with the intervention of the survivors.</p>
                    <a href="https://sharedhope.org/donate/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/shared-hope.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Anti-Slavery International</h1>
                    <p>Anti- Slavery International’s goal is to end slavery throughout the world, once and for all. They have projects from America to Africa but campaigns all over. They work with local organizations to make sure the freedom of the victims of slavery are secure. They are strong advocates of implementing proper laws against slavery.</p>
                    <a href="https://www.antislavery.org/donate/" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/anti-slavery.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">International Justice Mission</h1>
                    <p>International Justice Mission is a global organization that helps protect the less fortunate from violence. They have numerous lawyers, social workers and other professionals that work for their 17 field offices. They work with the local government and communities as they think it helps make the change last. IJM makes sure that slave-owners, traffickers, rapists, and criminals are held accountable in court. They help restore the lives of the victims by creating customized care plans for each.</p>
                    <a href="https://www.ijm.org/make-gift" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/ijm.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Faith Alliance Against Slavery and Trafficking</h1>
                    <p>Promotion description appears here.FAAST is an alliance of Christian organizations that work together to fight slavery and human trafficking. Their mission is to help communities and mobilize them in fighting this cause as well as restore the lives of the survivors. They want to see a world free from human trafficking one day.</p>
                    <a href="http://faastinternational.org/donate/donate" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/fast.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text"> <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">ECPAT International</h1>
                    <p>ECPAT International is an organization based in Bangkok that envisions a world free from sexual exploitation of children. They have 98 partners in over 88 countries. They coordinate evidence-based advocacies to strengthen national justice and protection of systems. They want to increase the investment in the fight against sexual exploitation.</p>
                    <a href="http://www.ecpat.org/donate" class="btn btn-lg btn-primary">GET INVOLVED HERE</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/ecpat.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection