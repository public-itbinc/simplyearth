@extends('themes.frontend')

@section('content')

<section class="pd0 banner-page">
    <img class="banner-img"  src="{{ asset('images/compare-chart-banner.jpg') }}" alt="">
</section>


<section>
    <div class="container-fluid">
        <h1 class="section-title blue-text">Compare Our Blends</h1>
        <center><p>Our Proprietary Blends were developed in house to make it easy for you to start your essential oil journey.</p></center>

        <br>
        <table class="table table-responsive comparison-table text-center">
            <thead>
                <tr>
                    <th>
                        <img src="{{ asset('/images/logor.png') }}" />
                    </th>
                    <th>
                        <img src="{{ asset('/images/icons/logo-doterra.png') }}" />
                    </th>
                    <th>
                        <img src="{{ asset('/images/icons/young-living.png') }}" />
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        Beauty Within
                    </td>
                    <td>
                        Imortelle
                    </td>
                    <td>
                        Art Renewal Serum
                    </td>
                </tr>
                <tr>
                    <td>
                        Bo Be Gone
                    </td>
                    <td>
                        Purify
                    </td>
                    <td>
                        Purification
                    </td>
                </tr>
                <tr>
                    <td>
                        Breathe Easy
                    </td>
                    <td>
                        Breathe
                    </td>
                    <td>
                        Breathe Again
                    </td>
                </tr>
                <tr>
                    <td>
                        Bug Fighter
                    </td>
                    <td>
                        TerraShield
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Bumbs &amp; Boo-boos
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        Owie
                    </td>
                </tr>
                <tr>
                    <td>
                        Citrus Burst
                    </td>
                    <td>
                        Citrus Bliss
                    </td>
                    <td>
                        Citrus Fresh
                    </td>
                </tr>
                <tr>
                    <td>
                        Clean & Fresh
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Clear Skin
                    </td>
                    <td>
                        HD Clear
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Defender
                    </td>
                    <td>
                        On Guard
                    </td>
                    <td>
                        Thieves
                    </td>
                </tr>
                <tr>
                    <td>
                        Digest Aid
                    </td>
                    <td>
                        Digest Zen
                    </td>
                    <td>
                        Digize
                    </td>
                </tr>
                <tr>
                    <td>
                        Energy
                    </td>
                    <td>
                        Elevation
                    </td>
                    <td>
                        En-R-Gee
                    </td>
                </tr>
                <tr>
                    <td>
                        Farewell Scars
                    </td>
                    <td>
                        Imortelle
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Fit
                    </td>
                    <td>
                        Slim &amp; Sassy
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Focus
                    </td>
                    <td>
                        InTune
                    </td>
                    <td>
                        Brain Power
                    </td>
                </tr>

                <tr>
                    <td>
                        Happy Joy
                    </td>
                    <td>
                        Cheer
                    </td>
                    <td>
                        Joy
                    </td>
                </tr>
                <tr>
                    <td>
                        Head Soothing
                    </td>
                    <td>
                        PastTense
                    </td>
                    <td>
                        M-Grain
                    </td>
                </tr>
                <tr>
                    <td>
                        Ladies Choice
                    </td>
                    <td>
                        Clary Calm
                    </td>
                    <td>
                        Dragon Time
                    </td>
                </tr>
                <tr>
                    <td>
                        Love
                    </td>
                    <td>
                        Passion
                    </td>
                    <td>
                        Joy
                    </td>
                </tr>
                <tr>
                    <td>
                        Peace &amp; Quiet
                    </td>
                    <td>
                        Balance
                    </td>
                    <td>
                        Peace &amp; Calming
                    </td>
                </tr>
                <tr>
                    <td>
                        Relief
                    </td>
                    <td>
                        Deep Blue
                    </td>
                    <td>
                        PanAway
                    </td>
                </tr>

                <tr>
                    <td>
                        Sleepy
                    </td>
                    <td>
                        Serenity
                    </td>
                    <td>
                        Sleepylze
                    </td>
                </tr>
                <tr>
                    <td>
                        Tranquility
                    </td>
                    <td>
                        Peace
                    </td>
                    <td>
                        Stress Away
                    </td>
                </tr>
                <tr>
                    <td>
                        Veins
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </tbody>
        </table>
        <center>
        <p>* The names Young Living, Thieves and Valor are owned by Young Living Essential Oils, LLC and have no relation to Simply Earth®.</p>
        <p>•• The names doTERRA, On Guard, Deep Blue, Slim and Sassy, AromaTouch and Digest-Zen are owned by Doterra and have no relation to SimpleEarth®.</p></center>
    </div>
</section>
@endsection