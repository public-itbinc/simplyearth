@extends('themes.frontend')

@section('body_class','blog')

@section('content')
  <section class="">
      <div class="container-fluid">
          <div class="convert-flow">
           <!-- <div class="cf-4040-area-12732"></div> -->
           <div class="cf-1515-area-9755"></div>

          </div>
          <div class="topnav">
              <div class="search-container pull-left">
                  {!! Form::open(['method' => 'GET','url' => route('all.blog') ]) !!}
                      <input type="text" placeholder="Search.." name="search">
                    {!! Form::button("<i class=\"fa fa-search\"></i>", ['type' => 'submit', 'onclick' => 'this.form.submit();']) !!}
                  {!! Form::close() !!}
              </div>
          </div>
          <div class="blog-title">
              <h3 class="green-text">SEARCH THE BLOG</h3>
          </div>
          <div class="blog-filter clearfix">
            <div class="input">
                <input type="hidden" value="{{route('blog.cat.filter', '')}}" id="cat_url">
                <div class="btn-group">
                    {!! Form::select('category_id', $parentCategory, $defaultValue, ['class' => 'form-control', 'id' => 'category_id','style' => 'text-transform: capitalize', 'onChange' => 'document.getElementById("filter").href = document.getElementById("cat_url").value + "/" + document.getElementById("category_id").value']) !!}
                </div>
                <div class="btn-group">
                    <a href="{{route('blog.cat.filter', '')}}" id="filter" class="btn btn-primary">Filter</a>
                </div>
            </div>
          </div>
          
          <div class="masonry infinite-scroll">
             <div class="grid__col-sizer"></div>
              @foreach ($blogs as $key => $blog)
                <div class="grid-item">
                  <div class="card inner ">
                    <a href="{{ route('single.post', $blog->slug) }}">
                      <img class="card-img-top d-block img-fluid" src="{{ asset("{$blog->image}") }}">
                    </a>
                    <div class="card-body details ">
                      <a href="{{ route('single.post', $blog->slug) }}">
                          <h3 style="">{{ $blog->title }}</h3>
                      </a>

                        @if ($blog->category->count() > 0)
                            <label>{{ ucfirst($blog->category->first()->blogCategory->name) }}</label>
                        @else
                            <label for="">&nbsp;</label>
                        @endif

 <!--                    <div class="tags">
                      @foreach ($blog->groups as $key => $tag)
                        <a href="{{ route('blog.tag.filter', str_replace(" ", "-", $tag->blogTag->name)) }}" style="text-transform: uppercase;">{{ $tag->blogTag->name }}</a>
                      @endforeach
                    </div> -->
                    </div>
                  </div>
                </div>
                @endforeach
                {{$blogs->links()}}
          </div>   
      </div>
  </section>
<!--   <script>
if (window.convertflow == undefined) {
  var script = document.createElement('script'); 
  script.async = true;
  script.src = "https://js.convertflow.co/production/websites/1515.js"; 
  document.body.appendChild(script); 
};
</script> -->
   <script async src="https://js.convertflow.co/production/websites/1515.js" ></script>
  <!-- <script async src="https://js.convertflow.co/production/websites/4040.js"></script> -->
@endsection
