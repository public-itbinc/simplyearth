@extends('themes.frontend')

@section('body_class', 'blog-tags')

@section('content')
  <section class="">
      <div class="container-fluid">
          <div class="convert-flow">

          </div>
          <div class="blog-title">
              <h3 class="green-text">SEARCH THE BLOG</h3>
          </div>
          <div class="blog-filter clearfix">
              <div class="input">
                  <!-- Single button -->
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Filter by Category <span class="icon ti-plus"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                    </ul>
                  </div>

                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Filter by Sub-category  <span class="icon ti-plus"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                    </ul>
                  </div>

                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Filter by Sub-category  <span class="icon ti-plus"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                    </ul>
                  </div>
              </div>
          </div>
          <div class="masonry" id="masonry">
            @foreach ($blogs as $key => $blog)
              <div class="grid-item">
                <div class="inner">
                  <a href="{{ route('single.post', $blog->blogPost->slug) }}"><img src="{{ asset("images/{$blog->blogPost->image}") }}"></a>
                  <div class="details">
                    <label>HOW TO</label>
                    <a href="{{ route('single.post', $blog->blogPost->slug) }}"><h1 style="text-transform:  uppercase;">{{ $blog->blogPost->title }}</h1></a>

                    <div class="tags">
                      <a href="{{ route('admin.blogtagsgroups.show', $blog->blogTag->id) }}" style="text-transform: uppercase;">{{ $blog->blogTag->name }}</a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
      </div>
  </section>
@endsection
