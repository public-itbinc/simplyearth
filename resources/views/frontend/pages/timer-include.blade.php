@if(isset($discount_data['discount']))
<timer 
end_date="{{ $discount_data['discount']->end_date->format('Y-m-d H:i:s.u') }}" 
offer="{{ $discount_data['offer'] }}"
name="{{ $discount_data['name'] }}"
text="{{ $discount_data['text'] }}"></timer>
@endif
