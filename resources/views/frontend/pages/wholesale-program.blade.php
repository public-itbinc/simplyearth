@extends('themes.frontend')
@section('content')

<section>
    <div class="container-fluid">
        <div class="row eq-height video-with-text copy-on-right">
            <div class="col-md-6">
                <div class="">
                    <h1 class="blue">Increase Your Store’s Sales</h1>
                    <p>Our oils have proven success to help you increase your store or private practices revenue. When you join Simply Earth, we support you every step of the way with ready made kits, a custom promotion plan, and a dedicated wholesale success manager. It all works because customers love getting 100% pure oils at honest prices locally!</p>

                    <type-form-button url="{{ $typeform_url }}">JOIN TODAY!</type-form-button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-video video-player-box">
                    <div class="video-container" style="background-image:url({{ asset('images/shilah.jpg') }});background-size: cover;">
                        <img class="preview-img" src="{{ asset('images/shilah.jpg') }}" width="100%" height="100%">
                        <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                        <div class="videoPlayer" data-video-id="hY6tPBwFIaI" data-video-type="youtube"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="bg-default darker">
    <div class="container-fluid">
        <div class="row eq-height image-with-text">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Who is a Good Fit?</h1>
                    <p>We accept anyone who has a business. See some examples below.</p>
                    <ul class="feature-list li-p0">
                        <li>Local Boutique &amp; Gift Shops</li>
                        <li>Salons</li>
                        <li>Massage Therapists</li>
                        <li>Chiropractors</li>
                        <li>Yoga Studios</li>
                        <li>Spas</li>
                        <li>People with their own small natural products brand</li>
                    </ul>

                    <type-form-button url="{{ $typeform_url }}">JOIN TODAY!</type-form-button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/good-fit-banner.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-video-testimonials bg-white">
    <div class="container-fluid">
        <div class="row testimonial-block">
            <h1 class="section-title blue-text">Join the Hundreds of Others Serving Their Customers</h1>

            <div id="testimonial-slider">
                <div class="">
                    <div class="video-player-box">
                        <div class="video-container">
                            <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                            <img class="preview-img" src="{{ asset('images/VideoCover_OsthoffResort.jpg') }}">
                            <div class="videoPlayer" data-video-id="sPCYVwy0n1A" data-video-type="youtube"></div>
                            <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
                            <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="video-player-box">
                        <div class="video-container">
                            <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                            <img class="preview-img" src="{{ asset('images/VideoCover_IntuneChirocare.jpg') }}">
                            <div class="videoPlayer" data-video-id="EmhIxBIVSqk" data-video-type="youtube"></div>
                            <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
                            <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="video-player-box">
                        <div class="video-container">
                            <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                            <img class="preview-img" src="{{ asset('images/VideoCover_kateDeriso.jpg') }}">
                            <div class="videoPlayer" data-video-id="mnrzCgZZCRQ" data-video-type="youtube"></div>
                            <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
                            <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div align="center">
                <type-form-button url="{{ $typeform_url }}">JOIN TODAY!</type-form-button>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="">
                    <h1 class="blue wide-title">Have More Happy Customers</h1>

                    <p>Our favorite part of selling our oils at fairs is watching the smiles light up people’s faces as they smell our oils. You can bring joy to your clients and customers with our pure essential oils sold at honest prices. As a bonus to you, when they need more oils, they will come back to your place to buy more!</p>
                    <type-form-button url="{{ $typeform_url }}">JOIN TODAY!</type-form-button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/feature-img.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
