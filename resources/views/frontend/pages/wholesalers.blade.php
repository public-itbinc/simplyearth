@extends('themes.frontend') 
@section('content')

<section class="pdt40 account-boxes products-category">
    <div class="container-fluid">
        <div class="row products-category-container box-row same-height">
            <div class="col-md-6">
                <div class="box">
                    <h2>YOUR NEXT BOX SHIPS</h2>

                    <div class="object"><span class="se-icon icon-quick-order-form"></span></div>

                    <a href="#" class="keep-bot green-text">Place my wholesale order <span class="fa fa-angle-right"></span></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <h2>MONTHS TILL NEXT REWARD</h2>
                    <div class="object"><img src="{{ asset('images/essential-oil.jpg') }}"></div>
                    <a href="#" class="keep-bot green-text">Place my wholesale order <span class="fa fa-angle-right"></span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row box-row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <a href="#">
                                <h2>SHIPPING ADDRESS</h2>
                                <p>202 Western Ave Sheboygan Fal, WI 5305</p>
                                <span class="icon-anchor ti-angle-right"></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <a href="#">
                                <h2>PAYMENT METHOD</h2>
                                <p><span class="se-icon icon-paypal inline-block"></span> +7414 05/20</p>
                                <span class="icon-anchor ti-angle-right"></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <a href="#">
                                <h2>EMAIL</h2>
                                <p>lee.veldkamp@simplyearth</p>
                                <span class="icon-anchor ti-angle-right"></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="box text-left box-anchor">
                            <a href="#">
                                <h2>PASSWORD</h2>
                                <p>*********</p>
                                <span class="icon-anchor ti-angle-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="product-header clearfix">
                    <h3 class="pull-left">Toss these in.</h3>
                    <a href="#" class="browse-all pull-right">Browse All</a>
                </div>

                <div id="products" class="row list-group product-list">
                    <div class="item col-xs-6 col-sm-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="thumb-box">
                                <a href="product-detail.html">
                                    <img class="group list-group-image" src="{{ asset('images/products/item-1.jpg') }}" alt="" />
                                <img class="group list-group-image" src="{{ asset('images/products/item-1-3.jpg') }}" alt="" />
                                </a>
                                <div class="caption-top">
                                    <a href="product-detail.html">
                                        <h4 class="group inner list-group-item-heading">Product title Product title Product title</h4>
                                    </a>
                                    <div class="rating">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="caption">
                                <div class="row more-detail category--v sr-only">
                                    <div class="col-xs-8">
                                        <p class="lead price">$21.000</p>|
                                        <p class="lead oz">12oz</p>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="pull-right btn btn-primary btn-cart-add">ADD</button>
                                    </div>
                                </div>

                                <div class="row more-detail category--v basket-count">
                                    <div class="col-xs-4">
                                        <button class="btn btn-secondary cart-minus" data-action="minus"><span class="ti-minus"></span></button>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="counter-box">
                                            <input type="hidden" class="form-control" value="1" min="0" max="10">
                                            <!--add min and max attribute for validation-->
                                            <span class="count">1</span>
                                            <p>in basket</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="btn btn-secondary cart-plus" data-action="plus"><span class="ti-plus"></span></button>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="item  col-xs-6 col-sm-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="thumb-box">
                                <a href="product-detail.html">
                                    <img class="group list-group-image" src="{{ asset('images/products/item-1.jpg') }}" alt="" />
                                <img class="group list-group-image" src="{{ asset('images/products/item-1-3.jpg') }}" alt="" />
                                </a>
                                <div class="caption-top">
                                    <a href="product-detail.html">
                                        <h4 class="group inner list-group-item-heading">Product title Product title Product title</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="caption">
                                <div class="row more-detail category--v">
                                    <div class="col-xs-8">
                                        <p class="lead price">$21.000</p>|
                                        <p class="lead oz">12oz</p>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="pull-right btn btn-primary btn-cart-add">ADD</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-xs-6 col-sm-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="thumb-box">
                                <a href="product-detail.html">
                                    <img class="group list-group-image" src="{{ asset('images/products/item-1.jpg') }}" alt="" />
                                <img class="group list-group-image" src="{{ asset('images/products/item-1-3.jpg') }}" alt="" />
                                </a>
                                <div class="caption-top">
                                    <a href="product-detail.html">
                                        <h4 class="group inner list-group-item-heading">Product title Product title Product title</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="caption">
                                <div class="row more-detail category--v">
                                    <div class="col-xs-8">
                                        <p class="lead price">$21.000</p>|
                                        <p class="lead oz">12oz</p>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="pull-right btn btn-primary btn-cart-add">ADD</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-xs-6 col-sm-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="thumb-box">
                                <a href="product-detail.html">
                                    <img class="group list-group-image" src="{{ asset('images/products/item-1.jpg') }}" alt="" />
                                <img class="group list-group-image" src="{{ asset('images/products/item-1-3.jpg') }}" alt="" />
                                </a>
                                <div class="caption-top">
                                    <a href="product-detail.html">
                                        <h4 class="group inner list-group-item-heading">Product title Product title Product title</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="caption">
                                <div class="row more-detail category--v">
                                    <div class="col-xs-8">
                                        <p class="lead price">$21.000</p>|
                                        <p class="lead oz">12oz</p>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="pull-right btn btn-primary btn-cart-add">ADD</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-xs-6 col-sm-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="thumb-box">
                                <a href="product-detail.html">
                                    <img class="group list-group-image" src="{{ asset('images/products/item-1.jpg') }}" alt="" />
                                <img class="group list-group-image" src="{{ asset('images/products/item-1-3.jpg') }}" alt="" />
                                </a>
                                <div class="caption-top">
                                    <a href="product-detail.html">
                                        <h4 class="group inner list-group-item-heading">Product title</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="caption">
                                <div class="row more-detail category--v">
                                    <div class="col-xs-8">
                                        <p class="lead price">$21.000</p>|
                                        <p class="lead oz">12oz</p>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="pull-right btn btn-primary btn-cart-add">ADD</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-xs-6 col-sm-4 col-lg-4">
                        <div class="thumbnail">
                            <div class="thumb-box">
                                <a href="product-detail.html">
                                    <img class="group list-group-image" src="{{ asset('images/products/item-1.jpg') }}" alt="" />
                                <img class="group list-group-image" src="{{ asset('images/products/item-1-3.jpg') }}" alt="" />
                                </a>
                                <div class="caption-top">
                                    <a href="product-detail.html">
                                        <h4 class="group inner list-group-item-heading">Product title Product title Product title</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="caption">
                                <div class="row more-detail category--v">
                                    <div class="col-xs-8">
                                        <p class="lead price">$21.000</p>|
                                        <p class="lead oz">12oz</p>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="pull-right btn btn-primary btn-cart-add">ADD</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="pd0 m-b20 store-orders recent-orders">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row-block">
                    <div class="order-header clearfix">
                        <h3 class="inner pull-left">Your Recent Orders</h3>
                        <a href="#" class="pull-right">View All</a>
                    </div>
                </div>

                <div class="row-block order-items">
                    <div class="item clearfix same-height">
                        <div class="col-md-4">
                            <p class="data">NOV 08, 2017</p>
                            <span>Subscription Purchase</span>
                        </div>
                        <div class="col-md-4">
                            <div class="product-detail">
                                <img src="{{ asset('images/products/thumb/item-1-thumb.jpg') }}" alt="">
                                <div class="order-no">
                                    <h4>Order No. 232402491</h4>
                                    <span>1 (item)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="order-detail-right">
                                <p>$12.00</p>
                                <span class="status complete">ORDER COMPLETE</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row-block order-items">
                    <div class="item clearfix same-height">
                        <div class="col-md-4">
                            <p class="data">NOV 08, 2017</p>
                            <span>Subscription Purchase</span>
                        </div>
                        <div class="col-md-4">
                            <div class="product-detail">
                                <img src="{{ asset('images/products/thumb/item-1-thumb.jpg') }}" alt="">
                                <div class="order-no">
                                    <h4>Order No. 232402491</h4>
                                    <span>1 (item)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="order-detail-right">
                                <p>$12.00</p>
                                <span class="status complete">ORDER COMPLETE</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="bg-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="section-title blue-text">WHOLESALERS GUIDE QUESTIONS</h1>
                <div class="se-accordion panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <p>What will I get in my essential oil Recpe Box? </p> <span class="ti-angle-down"></span></a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                                3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                                labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <p>Is it really only for $39 for 4 essential oils? </p> <span class="ti-angle-down"></span></a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                                3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                                labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    <p>Do I have to make a purchase to use a reward? </p> <span class="ti-angle-down"></span></a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                There are several different rewards up for grabs, some are for a free gift card (you’d only need to pay the postage), if
                                you don’t fancy adding other snacks) or for $5 off your next shop order.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection