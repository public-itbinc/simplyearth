@if(isset($meta_title) && !empty($meta_title))
<meta name="title" content="{{ $meta_title }}" /> 
@endif

@if(isset($meta_description) && !empty($meta_description))
<meta name="description" content="{{ $meta_description }}" /> 
@endif

@if(isset($meta_keywords) && !empty($meta_keywords))
<meta name="keywords" content="{{ $meta_keywords }}" /> 
@endif