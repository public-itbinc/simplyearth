<div class="modal animated fadeIn" id="forgot-password" tabindex="-1" role="dialog">
    <div class="modal-dialog small-dialog" role="document">
        <div class="modal-content" :class="{'sk-loading':forgot_submit}">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h1>FORGOT?</h1>
                <p class="header-sub-title">Enter your eamil below and we'll send you reset instructions.</p>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" v-model="forgotForm.email" placeholder="Email" @keydown="forgotForm.errors.clear('email')">
                            <div class="text-danger m-t-xs" v-if="forgotForm.errors.has('email')" v-text="forgotForm.errors.get('email')"></div>

                        </div>
                    </div>

                    <div class="">
                        <div class="row">
                            <div class="col-xs-6">
                                <button type="button" data-dismiss="modal" class="btn btn-block btn-gray">
                                            CANCEL
                                        </button>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" @click="forgotSubmit" class="btn btn-block btn-primary">
                                            SEND
                                        </button>
                            </div>

                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>