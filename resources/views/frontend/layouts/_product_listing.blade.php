<div class="product-listing-inner">
    <div class="product-filter clearfix">
        <?= Form::open(['method' => 'GET','url' => request()->path(), 'class' => 'product-filter-inner' ]) ?>
        <div class="visible-xs text-left">
            <span class="category-buttonlink" id="category-sidebar-opener"><span><i class="fa fa-list"></i></span> Categories</span>
        </div>
        <div class="search-form">
            <div class="input-group hidden-xs">
                <?= Form::search('q','',['class' => 'form-control','placeholder' => 'Search for products']) ?>
                <span class="input-group-addon" id="basic-addon2"><button type="submit" class="btn btn-xs btn-link" style="padding:0;"><i class="fa fa-search"></i></button></span>
            </div>
        </div>
        <div class="product-sorter-dropdown">
            <?= Form::select('sortby',[
                'total_sales.desc' => 'Most Popular',
                'created_at.desc' => 'Newest First',
                'price.asc' => 'Price Low-High',
                'price.desc' => 'Price High-Low'
            ],null,['class' => 'form-control', 'onchange' => 'this.form.submit();']) ?>
        </div>
        
        <?= Form::close() ?>
    </div>
    <div id="products">

        <ProductListing :collection="{{ $products->toJson() }}"></ProductListing>


    </div>

    
</div>