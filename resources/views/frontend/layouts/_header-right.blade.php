<div class="se-nav-right {{Route::is('subscribe') ? 'hidden-xs' : ''}}">
    <ul>

        @if(!customer() && !Route::is('subscribe'))
        <li class="hidden-xs"><a href="#" class="account" @click="EventHub.$emit('show_login')"><span class="ti-user"></span>Login</a></li>
        @elseif(!customer() && Route::is('subscribe'))
        <li><a href="#" class="account" @click="EventHub.$emit('show_login')"><span class="ti-user"></span>Login</a></li>
        @else
        <li class="hidden-xs">
            <a href="#" class="account"><span class="ti-user"></span>{{ customer()->name }}</a>

            <ul class="sub-nav">

                <li>
                    <a href="{{ route('profile') }}"><span class="ti-user"></span> Your Details</a>
                </li>
                <li>
                    <a href="{{ route('future-orders') }}"><span class="ti-gift"></span> Gift A Box</a>
                </li>
                {{--
                <li>
                    <a href="#"><span class="ti-share"></span> Invite Friends</a>
                </li>
                <li>
                    <a href="#"><span class="ti-star"></span> Loyalty Program</a>
                </li>--}}
                <li>
                    <a href="{{ route('past-orders') }}"><span class="ti-shopping-cart-full"></span>
                                        Purchase History</a>
                </li>
                
            <li>
                    <a href="{{ route('referral-invites') }}"><span class="ti-heart"></span> Earn a Free Box</a>
            </li>
                <li>
                    <a href="{{ route('customer.logout') }}" onclick="event.preventDefault();
                                                                                         document.getElementById('logout-form').submit();">
                                        <span class="ti-mouse"></span> Logout
                                    </a>

                    <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>

        @endif @if(!Route::is('subscribe'))
        <CartWidget></CartWidget>
        @endif
    </ul>
</div>