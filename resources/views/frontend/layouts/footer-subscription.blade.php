<footer>
    <div class="text-center">
        <p class="footer-copyright">© 2019, Made with ❤️ from &nbsp;<a href="/" class="{{Route::is('subscribe') ? 'main-logo-in-subscribe':''}}"><img src="{{ asset('images/logor.png') }}" class="footer-logo"></a></p>
        <p class="footer-policy">
            <a href="{{ route('privacy-policy') }}">Privacy Policy</a> &nbsp;
            <a href="{{ route('terms-of-service') }}">Terms of Use</a>
        </p>
    </div>
</footer>