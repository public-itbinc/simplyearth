@if(customer())    
    <a href="{{ route('past-orders') }}" class="btn btn-primary btn-lg">CHECK YOUR ORDER UPDATES</a>
@elseif($customer->account)
    <a href="{{ route('past-orders') }}" @click.prevent="EventHub.$emit('show_login', false, {{
        json_encode(['redirect_to' => route('past-orders'), 'email' => $customer->account->email])
    }});" class="btn btn-primary btn-lg">CHECK YOUR ORDER UPDATES</a>
@else
    <a href="{{ route('past-orders') }}" @click.prevent="EventHub.$emit('show_register', {{
        json_encode([
        'first_name' => $customer->first_name,
        'last_name' => $customer->last_name,                         
        'email' => $customer->email])
    }})" class="btn btn-primary btn-lg">CHECK YOUR ORDER UPDATES</a>
@endif