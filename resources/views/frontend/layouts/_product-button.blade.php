<div class="price pull-left" v-if="global.cart_select.wholesale_pricing"><span v-text="global.cart_select.wholesalePriceString"></span> <span class="strike"><span v-text="global.cart_select.priceString"></span></span></div>

					<div class="price pull-left" v-else><span v-text="global.cart_select.priceString"></span></div>


					<div class="oz pull-right">{{ collect($product->extra_attributes)->firstWhere('label','Volume')['value'] }}</div>
					<div class="clearfix"></div>

					@if($product->setup=='variable') @foreach($product->getVariantGroups() as $groupName => $group)

					<div class="form-group">
						<label>{{ $groupName }}: 
							@foreach($group as $variant_attribute)
						<span v-if="global.cart_select.variant_attributes=='{{ $groupName.':'.$variant_attribute['attributes'] }}'" v-text="'{{ $variant_attribute['attributes'] }}'"></span>
						@endforeach
					</label>
						<div class="variant-images">
							@foreach($group as $variant_attribute)
							<div data-attributes='{{ $variant_attribute['attributes'] }}' class="variant-attribute" @click="global.switchVariant({{ collect($variant_attribute['product']) }})"><img src="{{ $variant_attribute['product']['variant_cover'] }}" /></div>
							@endforeach
						</div>
					</div>

					@endforeach @endif

                    <button class="btn add-to-cart btn-primary btn-lg btn-wide" @click="global.addVariant()">ADD TO CART</button>