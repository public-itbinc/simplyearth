<section class="pdtb40 product-single">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7">
				<div class="product-gallery invisible">
					<div class="slider-thumb slider-nav">

						@foreach($product->images('thumb') as $image)
						<div class="slide-item">
							<div class="image" style="background-image: url({{ $image['url'] }});"></div>
						</div>

						@endforeach

					</div>
					<div class="slider-main slider-for">

						@foreach($product->images() as $image)

						<div><img alt="{{ $product->name }}" src="{{ $image['url'] }}"></div>


						@endforeach @if($product->setup=='variable') @foreach($product->variants as $variant)

						<div><img alt="{{ $variant->name }}" src="{{ $variant->cover }}"></div>


						@endforeach @endif

					</div>

				</div>

			</div>
			<div class="col-sm-5 product-description">
				<h1>{{ $product->name }}</h1>
				<div class="opt-desc">
					{{--
					<p class="rating">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</p>

					<p><span>29 reviews</span> | <i class="ti-comments"></i> 1 answered question</p>--}}
					<p><span class="stamped-product-reviews-badge stamped-main-badge" data-id="{{ $product->review_id }}"></span></p>
				</div>

				<div class="description">
					{{ $product->short_description }}
					<div><a @click="showProductDescription" class="text-pointer more-button">more details</a></div>
				</div>

				<div class="description-footer">

                    @if($product->hasPlans())
                    
                        @include('frontend.layouts._product-button-with-plans')

                    @else

                        @include('frontend.layouts._product-button')
                    
                    @endif

					@if(!Route::is('wholesaler.sales'))
					<div class="thumbnail">
						<div class="img-box">
							<img alt="Free Shipping" class="img-responsive" src="{{ asset('images/free-shipping.svg') }}">
						</div>
						<div>Free shipping for orders over $29.</div>
					</div>
					@endif

					<div class="thumbnail">
						<div class="img-box">
							<img alt="Simply Earth Guarantee" class="img-responsive" src="{{ asset('images/simply-earth-guarantee.svg') }}">
						</div>
						<div>Simply Earth Guarantee. <span class="tooltip-grayish" data-toggle="tooltip" data-placement="top" title="If you don't love it, return it within 365 days for account credit. Just contact us and we'll take care of you."><i class="fa fa-question-circle"></i></span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@if(!empty($product->videos))
<section class="bg-default">
	<div class="container-fluid">
		<div class="row testimonial-block">
			<h1 class="section-title">SEE WHAT OTHERS ARE SAYING</h1>

			@foreach(collect($product->videos) as $video)

			<div class="col-sm-4">
				<div class="video-player-box">
					<div class="video-container">
						<a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
						<img class="preview-img" src="{{ $video->cover?$video->cover:'https://img.youtube.com/vi/'.$video->id.'/mqdefault.jpg' }}">
						<div class="videoPlayer" data-video-id="{{ $video->id }}" data-video-type="youtube"></div>
					</div>
				</div>
			</div>

			@endforeach

		</div>
	</div>
</section>

@endif

<section class="half-half-container bg-default">

		<div class="image-container" style="background-color: transparent">
				<div class="image-half" style="background-image: url({{ $product->quality_certification_image }});"></div>
			</div>
	<div class="text-half">
		<div class="section-copy">
			<h1 class="blue">{{ $product->quality_certification_title }}</h1>
			{!! $product->quality_certification !!}
			<div class="">
					@if($product->categories->whereIn('slug', ['blends', 'single-oils'])->count())
					<gcms :product="{{ $product->toJson() }}" ></gcms>
					@endif
			</div>
		</div>
	</div>
	
</section>

<section class="pdtb40 bg-white">
	<div class="container-fluid">
		<ul class="product-detail-tab nav nav-tabs">
			<li class="active"><a href="#reviews" data-toggle="tab">REVIEWS</a></li>
			<li><a href="#details" data-toggle="tab">DETAILS</a></li>
			@if($product->show_recipe)
			<li><a href="#recipes" data-toggle="tab">{{ $product->recipe_title ?? 'RECIPES' }}</a></li>
			@endif
		</ul>

		<div class="product-detail-content  tab-content">
			<div id="reviews" class="tab-pane fade in active">
				<h3 class="blue">Reviews</h3>
				<div id="stamped-main-widget" data-product-id="{{ $product->review_id }}" data-name="{{ $product->name }}" data-url="{{ $product->link }}"
				 data-image-url="{{ $product->cover }}" data-description="{{ $product->short_description }}" data-product-sku="{{ $product->sku }}">
				</div>
			</div>
			<div id="details" class="tab-pane fade">
				<h3 class="blue">Product Details</h3>

				<div class="product-description">
					{!! $product->description !!}
				</div>
			</div>
			@if($product->show_recipe)
			<div id="recipes" class="tab-pane fade">
			<h3 class="blue">{{ $product->recipe_title ?? 'Recipes' }}</h3>
				{!! $product->recipe !!}
			</div>
			@endif
		</div>
	</div>
</section>
