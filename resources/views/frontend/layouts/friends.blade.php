<section class="bg-default">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1 text-center">
                <center><img style="width: 100%;" src="{{ asset('images/279x278.jpg') }}"></center>
                <br>
                <button class="btn btn-primary">CHECK YOUR REFFERAL STATUS</button>
            </div>
            <div class="col-sm-6">
                <div class="panel-tab">
                    <div class="tab clearfix">
                        <ul>
                            <li class="active" data-content-target="#email">
                                EMAIL
                            </li>
                            <li content-target="#facebook">
                                FACEBOOK
                            </li>
                        </ul>
                    </div>
                    <div class="content">
                        <div class="tab-content active text-center" id="email">
                            <center><span class="se-icon icon-green-check"></span></center>

                            <h3>THANK YOU FOR SHARING !<br><span class="light-text">YOUR FRIENDS WILL RECEIVE AN INVITATION FROM YOU</span></h3>
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="facebook">
                                    <div class="fb-like" data-href="https://www.facebook.com/fromsimplyearth/" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                </div>
                                <div class="twitter">
                                    <a href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="true">Follow @TwitterDev</a>
                                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-content" id="facebook">
                            facebook
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="text-center">
                            <p>Share your coupon code on the web and get a free box for every three friends that join Simply Earth</p>
                            <p>Paste it anywhere:</p>
                        </div>
                        <input type="text" name="coupon" class="coupon-box form-control input-lg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>