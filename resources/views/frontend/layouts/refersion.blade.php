<script src="//simplyearth.refersion.com/tracker/v3/{{ config('conversion.refersion.api_key') }}.js"></script>
<script>
@if(Route::currentRouteName()=='checkout.thankyou')
_refersion(function(){

	_rfsn._addTrans({
		'order_id': "{{ $purchased_data['transaction_id'] }}",
		'shipping': "{{ $purchased_data['total_shipping'] }}",
		'tax': "{{ $purchased_data['total_tax'] }}",
		'discount': "{{ $purchased_data['total_discounts'] }}",
		'discount_code': "{{ $purchased_data['discount_code'] }}",
		'currency_code': 'USD'
	});

	_rfsn._addCustomer({
		'first_name': '{{ $customer->first_name }}',
		'last_name': '{{ $customer->last_name }}',
		'email': '{{ $customer->email }}',
		'ip_address': '{{ request()->ip() }}'
    });
    
    @if(count($purchased_data['products']))

    @foreach($purchased_data['products'] as $order_item)

	_rfsn._addItem({
		'sku': "{{ $order_item->sku }}",
		'quantity': "{{ $order_item->qty }}",
		'price': "{{ $order_item->price }}",
    });

    @endforeach
    
    @endif

	_rfsn._sendConversion();

});

@else
_refersion();
@endif
</script>
<!-- REFERSION TRACKING: END -->

