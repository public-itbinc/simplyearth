@if(customer() && Auth::check())
<div id="login-as"><div class="login-as-inner">You are logged as customer <strong>{{ customer()->name }}</strong></div></div>
@endif