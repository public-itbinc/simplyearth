@if($box->products) @foreach(collect($box->recipes) as $recipe)
<div class="shopping-box-recipe">
    <div class="box-inner">
        <div class="recipe-image" style="background-repeat:no-repeat;background-size:cover;background-image:url({{ $recipe->cover }});height:auto">
            <img src="{{ $recipe->cover }}" style="visibility: hidden;" />
            <div class="video-container">
                <a href="#" class="play-video"><span class="fa fa-play fa-2x"></span></a>
                <div class="videoPlayer" data-video-id="{{ $recipe->id }}" data-video-type="youtube"></div>
                <button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
                <button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
            </div>
        </div>
    </div>
    <h2>{{ $recipe->header }}</h2>
    <p>{{ $recipe->text }}</p>
                <center><a href="{{ $cta_link }}" 
            class="btn btn-primary btn-lg">GET STARTED TODAY!</a></center>
</div>
@endforeach @endif