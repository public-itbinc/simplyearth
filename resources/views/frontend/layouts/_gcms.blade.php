<div class="row b-info">
    @foreach($gcms->groupBy('componentFamily') as $family => $components)
    <ul>
        <label>{{ $family }}</label>
        @foreach($components as $component)
        <li>{{ $component->component }}: <span>{{ $component->percentage }}%</span></li>
        @endforeach
    </ul>
    @endforeach
</div>