<script>
  var _learnq = _learnq || [];
    @if(customer())
    _learnq.push(['identify', {
      $email: '{{ customer()->email }}',
      $first_name: '{{ customer()->first_name }}',
      $last_name: '{{ customer()->last_name }}',
      'Has Account':true,
      'Tags':"{{ customer()->tags->count() ? customer()->tags->implode('name', ' ') : ' '}}"
    }]);
    @endif

</script>