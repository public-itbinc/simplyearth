<header class="clearfix header-top">
    @include('frontend.layouts.loginas')

    @if(isset($_GET['t']) && $_GET['t'] === 'true')
        @include('frontend.pages.timer-include')
    @endif

    <div class="top-bar">

        @yield('top-bar-header')

    </div>
    

    <div class="top-bar-footer">
        
        <div class="main-logo">
            <a href="/" class="{{Route::is('subscribe') ? 'main-logo-in-subscribe':''}}"><img src="{{ asset('images/logor.png') }}"></a>
        </div>
        
        
        <div class="se-nav-right-continer">
    
        <span class="se-nav-right {{Route::is('subscribe') ? 'hidden-xs' : ''}}">
            <ul>
                <li><a href="#" class="account" @click="EventHub.$emit('show_login')"><span class="ti-user"></span>Login</a></li>
            </ul>
        </span>
        
        <a href="#plans"
                @click="gaAddToCart({{ $monthly_subscription }})" class="btn btn-primary pull-right btn-xs btn-lg subscribe-today">GET STARTED TODAY!</a>
        </div>
    </div>

</header>
