<div class="sidebar-inner">

    @if(count($categories)) @foreach($categories as $category)
    <h2 class="sidebar-title"><a href="{{ $category->link() }}">{{ $category->name }}</a></h2>

    @if(count($category->children))

    <ul class="list">
        @foreach($category->children as $child)
        <li><a href="{{ $child->link() }}">{{ $child->name }}</a></li>
        @endforeach
    </ul>

    @endif @endforeach @endif
    <span id="category-sidebar-closer"><i class="fa fa-angle-left"></i></span>
</div>