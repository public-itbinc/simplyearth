<!-- Google Tag Manager -->
@if(Route::currentRouteName()=='products.show' && isset($product)) @php echo App\Shop\Misc\JSONLD::product($product)->generate()->toScript()

@endphp

<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    
    window.dataLayer.push({
    ecommerce: {
        'detail': {
        'products': [{
            'name': '{{ $product->name }}',
            'id': '{{ $product->sku }}',
            'price': '{{ $product->price }}',
        }]
    }
    },
    'product':{
            'name': '{{ $product->name }}',
            'id': '{{ $product->sku }}',
            'price': '{{ $product->price }}',
        }
    });

</script>
{{--@elseif(Route::currentRouteName()=='subscription' && isset($this_month))
<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    
    window.dataLayer.push({
        ecommerce: {
            'detail': {
                'products': [{
                    'name': 'Simply Earth Box',
                    'id': 'REC-MONTHLY',
                    'price': '{{ $monthly_price }}',
                }]
            }
        }
    });

</script>--}}

@elseif((Route::currentRouteName()=='subscription') && isset($this_month))
<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    
    window.dataLayer.push({
        ecommerce: {
            'detail': {
            'products': [
                {
                    'name': 'Monthly Subscription Box',
                    'id': 'REC-MONTHLY',
                    'price': '{{ $monthly_subscription->price }}',
                    },
                ]
            }
        },
    });

</script>

@elseif(Route::currentRouteName()=='checkout.thankyou')	

 <script type="text/javascript">	
    window.dataLayer = window.dataLayer || [];	
    	
    window.dataLayer.push(@php echo json_encode($conversion->purchased() ) @endphp);	
 </script>

@elseif(Route::currentRouteName()=='checkout' && isset($add_to_cart))	

 <script type="text/javascript">	
    window.dataLayer = window.dataLayer || [];	
    	
    window.dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'add': { 
                'products': [{ 
                    'name': "{{ $add_to_cart->name }}",
                    'id': "{{ $add_to_cart->sku }}",
                    'price': "{{ $add_to_cart->price }}",
                    'quantity': 1
                }]
            }
        },
        'product': { 
            'name': "{{ $add_to_cart->name }}",
            'id': "{{ $add_to_cart->sku }}",
            'price': "{{ $add_to_cart->price }}",
            'quantity': 1
        }
    });
 </script>

@endif

<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KSBFZ42');

</script>
<!-- End Google Tag Manager -->