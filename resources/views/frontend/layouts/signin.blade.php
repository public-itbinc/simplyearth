<div class="modal animated fadeIn" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog small-dialog" role="document">
		<div class="modal-content" :class="{'sk-loading':signup_submit}">
			<div class="sk-spinner sk-spinner-wave">
				<div class="sk-rect1"></div>
				<div class="sk-rect2"></div>
				<div class="sk-rect3"></div>
				<div class="sk-rect4"></div>
				<div class="sk-rect5"></div>
			</div>
			<div class="modal-header">
					
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h1>REGISTER</h1>
			</div>
			<div class="modal-body">
				<form class="" method="POST">

					<div class="form-group">
						<input type="text" class="form-control" v-model="signupForm.first_name" placeholder="First Name" @keydown="signupForm.errors.clear('first_name')">
						<div class="text-danger m-t-xs" v-if="signupForm.errors.has('first_name')" v-text="signupForm.errors.get('first_name')"></div>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" v-model="signupForm.last_name" placeholder="Last Name" @keydown="signupForm.errors.clear('last_name')">
						<div class="text-danger m-t-xs" v-if="signupForm.errors.has('last_name')" v-text="signupForm.errors.get('last_name')"></div>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" v-model="signupForm.email" placeholder="Email" @keydown="signupForm.errors.clear('email')">
						<div class="text-danger m-t-xs" v-if="signupForm.errors.has('email')" v-text="signupForm.errors.get('email')"></div>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" v-model="signupForm.password" placeholder="Password (Minimum 6 characters)" @keydown="signupForm.errors.clear('password')">
						<div class="text-danger m-t-xs" v-if="signupForm.errors.has('password')" v-text="signupForm.errors.get('password')"></div>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" v-model="signupForm.password_confirmation" placeholder="Confirm Password" @keydown="signupForm.errors.clear('password_confirmation')">
						<div class="text-danger m-t-xs" v-if="signupForm.errors.has('password_confirmation')" v-text="signupForm.errors.get('password_confirmation')"></div>
					</div>
					<div class="form-group">
							<div class="row">
									<div class="col-xs-6">
										<button type="button" data-dismiss="modal" class="btn btn-block btn-gray">
													CANCEL
												</button>
									</div>
									<div class="col-xs-6">
										<button type="button" @click="signupSubmit" class="btn btn-block btn-primary">
													DONE
												</button>
									</div>
		
								</div>
					</div>
					{{--
					<div class="or">
						<span>OR</span>
					</div>
					<div class="form-group btn-social-actn">
						<button type="submit" class="btn btn-fb btn-wide btn-lg"><span class="fa fa-facebook-square"></span> LOGIN WITH FACEBOOK</button>
					</div>
					<div class="form-group btn-social-actn">
						<button type="submit" class="btn btn-google btn-wide btn-lg"><span class="google-icon"></span> LOGIN WITH GOOGLE</button>
					</div>--}}
				</form>

			</div>
		</div>
	</div>
</div>