<div class="modal animated fadeIn" id="login" tabindex="-1" role="dialog">
	<div class="modal-dialog small-dialog" role="document">
		<div class="modal-content" :class="{'sk-loading':logging_in}">
			<div class="sk-spinner sk-spinner-wave">
				<div class="sk-rect1"></div>
				<div class="sk-rect2"></div>
				<div class="sk-rect3"></div>
				<div class="sk-rect4"></div>
				<div class="sk-rect5"></div>
			</div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				<h1>Welcome Back</h1>
				<p class="header-sub-title">Please login to continue</p>
			</div>
			<div class="modal-body">
				<form method="POST">

					{{ csrf_field() }}
					<div class="form-group">
						<input type="text" class="form-control" v-model="login.email" placeholder="Email" @keydown="login.errors.clear('email')">
						<div class="text-danger m-t-xs" v-if="login.errors.has('email')" v-text="login.errors.get('email')"></div>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" v-model="login.password" placeholder="Password" @keydown="login.errors.clear('password')">
						<div class="text-danger m-t-xs" v-if="login.errors.has('password')" v-text="login.errors.get('password')"></div>
					</div>
					<div class="form-group">
						<button type="button" @click="customerLogin" class="btn btn-primary btn-wide btn-lg">LOGIN</button>
					</div>
					{{--
					<div class="or">
						<span>OR</span>
					</div>
					<div class="form-group btn-social-actn">
						<button type="submit" class="btn btn-fb btn-wide btn-lg"><span class="fa fa-facebook-square"></span> LOGIN WITH FACEBOOK</button>
					</div>
					<div class="form-group btn-social-actn">
						<button type="submit" class="btn btn-google btn-wide btn-lg">
								<span class="google-icon"></span> LOGIN WITH GOOGLE</button>
					</div>--}}
				</form>

				<div class="other-actn">
					<p>Not a member? <a href="#" @click.prevent="showSignup">Join Us</a></p>
					<p><a href="#" @click.prevent="showForgot">Forgot Password?</a></p>
					<p><a href="#">Forgot email?</a></p>
				</div>

			</div>
		</div>
	</div>
</div>