@if (Route::is('wholesaler.sales') && (!customer() || !customer()->isWholesaler()))
<a @click.prevent="warnNonWholesaler" href="#" class="btn btn-lg btn-white">BUY IT NOW: {{$product->wholesale_pricing ? $product->wholesalePriceString : $product->priceString }}</h5>
<a @click.prevent="warnNonWholesaler" href="#" class="btn add-to-cart btn-primary btn-lg btn-wide">PLAN:${{ $product->first_plan['deposit'] }} DOWN + {{ $product->first_plan['cycles'] }} PAYMENTS OF ${{ $product->first_plan['amount'] }}/M </a>@else
<a href="{{ route('cart.add', ['product' => $product]) }}" class="btn btn-lg btn-white">BUY IT NOW: {{$product->wholesale_pricing ? $product->wholesalePriceString : $product->priceString }}</h5>
<a href="{{ route('cart.addplan', ['product' => $product]) }}" class="btn add-to-cart btn-primary btn-lg btn-wide">PLAN:${{ $product->first_plan['deposit'] }} DOWN + {{ $product->first_plan['cycles'] }} PAYMENTS OF ${{ $product->first_plan['amount'] }}/M </a>@endif
