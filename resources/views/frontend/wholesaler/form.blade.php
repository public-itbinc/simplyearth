@extends('themes.frontend') 
@section('content') 
@section('body_class','bg-white')

<section class="pdt40 m-b20 store-orders upcoming-orders">
    <div class="container-fluid">
        <div class="row">
            <div class="row-block wholesaler-order-form">
                <div class="col-lg-12">
                    <div class="order-header clearfix">
                        <h3 class="inner pull-left">Quick Order</h3>
                    </div>

                    {{ Form::open(['route' => 'wholesaler.form.checkout', 'method' => 'POST']) }} {{ csrf_field() }}
                    <div class="form-group">
                        <div class="list-header">
                            <div class="header-product">Product</div>
                            <div class="header-sku">SKU</div>
                            <div class="header-price">Price</div>
                            <div class="header-quantity">Qty</div>
                        </div>
                        @if(count($products)) @foreach($products as $product)


                        <div class="product-row  ">
                            <div class="product-item ">

                                <div class="product-details ">
                                    <div class="img-holder ">
                                        <img src="{{ $product->cover }}" alt="">
                                    </div>
                                    <div class="product-name">
                                        {{ $product->name }}
                                    </div>
                                </div>

                                <div class="product-sku ">
                                    {{ $product->sku }}
                                </div>


                                <div class="product-price">
                                    {{ $product->priceString }}
                                </div>


                            </div>
                            <div class="product-quantity">
                                {{ Form::number('products['.$product->id.']',$previous->firstWhere('id', $product->id)['qty'], ['class' => 'form-control', 'min' => 0, 'placeholder' => 0, 'step' => 1 ])}}
                            </div>
                        </div>



                        @endforeach @endif

                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary btn-lg pull-right">CHECKOUT</button>
                    </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer_base_scripts')
<script src="{{ mix('js/profile.js') }}"></script>
@endsection