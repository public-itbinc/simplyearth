@extends('themes.frontend') 
@section('content')


<div class="text-center gift-card">
    <h1>Your gift card</h1>

    <div class="code">{{ strtoupper($gift_card->code) }}</div>

    <p>Use this code at checkout to redeem your {{ $gift_card->orderItem->product->priceString }} gift card</p>
</div>
@endsection