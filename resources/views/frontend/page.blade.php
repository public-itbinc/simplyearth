@extends('themes.frontend')

@section('title')
    {{ $page->meta_title->value ?? config('app.name', 'Simply Earth Laravel') }}
@endsection

@push('meta')
    @if($page->meta_description && $page->meta_description->value)
        <meta name="description" content="{{$page->meta_description->value}}">
    @endif
@endpush


@if($page->header != 'main')
    @section('header')
        @include('frontend.layouts.header-'.$page->header)
    @endsection
@endif

@section('content')

    <div id="page">
    </div>
    <input type="hidden" id="page-id" value="{{ $page->id }}">
@endsection

@section('js')
    <script>
        window.pageId = document.getElementById('page-id').value;
    </script>
    {{--<script src=https://cdn.jsdelivr.net/npm/jquery@3.3></script>--}}
    <script src=https://cdn.jsdelivr.net/npm/trumbowyg@2></script>
    <link href=https://cdn.jsdelivr.net/npm/trumbowyg@2/dist/ui/trumbowyg.min.css rel=stylesheet>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/upload/trumbowyg.upload.min.js></script>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/pasteembed/trumbowyg.pasteembed.min.js></script>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/colors/trumbowyg.colors.min.js></script>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/base64/trumbowyg.base64.min.js></script>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/fontfamily/trumbowyg.fontfamily.min.js></script>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/fontsize/trumbowyg.fontsize.min.js></script>
    <script src=https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.18.0/plugins/noembed/trumbowyg.noembed.min.js></script>
    <script src=https://cdn.jsdelivr.net/npm/vue-trumbowyg@3></script>
    {{--    <script src="{{ asset('builder/js/chunk-vendors.js') }}" charset="utf-8"></script>--}}
    {{--    <script src="{{ asset('builder/js/app.js') }}" charset="utf-8"></script>--}}
    <script src="{{ asset('builder/js/main2.js') }}" charset="utf-8"></script>

    <link rel="stylesheet" href="{{ asset('builder/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('builder/css/chunk-vendors.css') }}">
    {{--    <script src="{{ mix('js/admin-blog.js') }}" charset="utf-8"></script>--}}

    <style>
        .border-none {
            display: none!important;
        }

        .text-and-blue .img-avatar {
            top: -10px;
        }
    </style>
@endsection

@if($page->header != 'main')
    @section('footer')
        @include('frontend.layouts.footer-'.$page->header)
    @endsection
@endif



@section('css')

@endsection