@extends('themes.frontend') 
@section('body_class','home-page') 
@section('content')
<div class="banner">
	<div class="banner-inner">
		<div class="banner-content">
			<h1>A Natural Home, Made Easy</h1>
			<p>With Our Essential Oil Recipe Box</p>

			<a href="{{ route('subscription') }}" class="btn btn-primary btn-lg">YES PLEASE</a>
		</div>
	</div>
</div>

<!-- Need to update the assets -->
{{--<section class="pd0 bg-default">--}}
    {{--<div class="full-section image-list home-page-banner">--}}
        {{--<div><img src="{{ asset('images/img-4-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-5-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-1-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-2-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-3-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-4-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-5-322x318.jpg') }}" alt=""></div>--}}
        {{--<div><img src="{{ asset('images/img-2-322x318.jpg') }}" alt=""></div>--}}
    {{--</div>--}}
{{--</section>--}}

<section class="company-image-list-section">
	<div class="container">
		<div><img class="max-width-100" src="{{ asset('images/companies.png') }}"></div>
	</div>
</section>

<section class="how-it-works">
	<div class="container-fluid">
		<h1 class="section-title index-section-title">Why Simply Earth</h1>
		<p class="panel-group">With Simply Earth, you can enjoy making incredible handmade products with your friends and family. You can be in control of your natural home.</p>
		<br><br>
		<div class="row">
			<div class="col-md-4 image-container">
				<div class="thumbnail text-center">
					<img src="{{ asset('images/Essential OIls.jpg') }}" alt="" class="img-responsive">
					<div class="caption">
						<h1><strong>Pure Essential Oils</strong></h1>
						<a href="/categories/single-oils" class="btn btn-primary btn-lg">SHOP NOW</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 image-container">
				<div class="thumbnail text-center">
					<img src="{{ asset('images/Design.jpg') }}" alt="" class="img-responsive" width="300px">
					<div class="caption">
						<h1><strong>Essential Oil Recipe Box</strong></h1>
						<a href="/pages/subscription-box" class="btn btn-primary btn-lg">SHOP NOW</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 image-container">
				<div class="thumbnail text-center">
					<img src="{{ asset('images/Diffusers.jpg') }}" alt="" class="img-responsive">
					<div class="caption">
						<h1><strong>Diffusers</strong></h1>
						<a href="/categories/accessories" class="btn btn-primary btn-lg">SHOP NOW</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="half-half-container bg-default">

		<div class="image-container" style="background-color: transparent">
				<div class="image-half" style="background-image: url({{ asset('images/Simply-Pure-Promise.jpg') }});"></div>
			</div>

	<div class="text-half">
		<div class="section-copy">
			<h1 class="blue">Simply Pure Promise</h1>
			<ul class="feature-list">
				<li>All of the good stuff.</li>
				<li>None of the bad stuff.</li>
				<li>Tested 100% Pure.</li>
			</ul>
			<a href="{{ route('pure-natural-products') }}" class="btn btn-primary btn-lg">LEARN MORE</a>
		</div>
	</div>
	
</section>

<section class="half-half-container half-half-container-left">
	
		<div class="image-container" style="background-color: transparent">
				<div class="image-half" style="background-image: url({{ asset('images/theirs-vs-ours2.png') }});"></div>
			</div>
	<div class="text-half">
		<div class="section-copy">
			<h1 class="blue">Quality oils for under half the price</h1>
			<p>We sell our pure oils to you directly. No middlemen, no upcharges. Sourced from the best farms around the world, GC/MS
				tested, and backed by our Simply Pure Promise.</p>
			<a href="{{ route('products') }}" class="btn btn-primary btn-lg">VISIT OUR SHOP</a>
		</div>
	</div>

</section>

<section class="bg-default">
	<div class="container-fluid">
		<div class="row eq-height image-with-text">
			<!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy section-text-left">
					<h1 class="blue">Join a Movement</h1>
					
					<div class="section-image visible-xs visible-sm">
						<img src="{{ asset('images/join-movement.jpg') }}" alt="">
					</div>
					<p>Human trafficking is a worlwide phenomenon and tackling it in a big scale is great. However it can be overwhelming. In our own little way, we try to help out as much as we can. Simply Earth shares 13% of our profits to organizations who helps end this atrocity.</p>
					<a href="{{ route('products') }}" class="btn btn-primary btn-lg">GET INVOLVED</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="section-image hidden-xs hidden-sm">
					<img src="{{ asset('images/join-movement.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- <section>
	<div class="container-fluid">
		<div class="row eq-height image-with-text">
	
			<div class="col-sm-6 col-xs-12 col-sm-push-6">
				<div class="section-copy">
					<h1 class="blue">Quality oils for under half the price</h1>
					<p>We sell our pure oils to you directly. No middlemen, no upcharges. Sourced from the best farms around the world, GC/MS
						tested, and backed by our Simply Pure Promise.</p>
					<a href="{{ route('products') }}" class="btn btn-primary btn-lg">VISIT OUR SHOP</a>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12 col-sm-pull-6">
				<div class="section-image">
					<img src="{{ asset('images/theirs-vs-ours2.png') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section> -->

{{--
<section class="bg-default darker">
	<div class="container-fluid">
		<div class="row eq-height image-with-text">
			<!--options: copy-on-right-->
			<div class="col-sm-6">
				<div class="section-copy">
					<h1 class="blue">Join a Movement</h1>
					<p>Human trafficking is a worlwide phenomenon and tackling it in a big scale is great. However it can be overwhelming. In our own little way, we try to help out as much as we can. Simply Earth shares 13% of our profits to organizations who helps end this atrocity.</p>
					<a href="/pages/our-cause" class="btn btn-primary btn-lg">GET INVOLVED</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="section-image">
					<img src="{{ asset('images/Cause.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
@include('frontend.layouts.tiles')--}}
@endsection
