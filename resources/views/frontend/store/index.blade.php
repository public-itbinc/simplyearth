@extends('themes.frontend')

@section('content')
  <section class="bg-default">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 store-header">
            <h1 class="bh-sl-title">Find Our Stores</h1>
          </div>
        </div>
        <div class="row store">
          <form class="" action="index.html" method="post">
            <label for="">Current Location</label>
            <div class="form-group">
              <div class="col-md-10">
                <input type="text" class="form-control" id="addressInput" placeholder="Enter a location">
                {{-- <input type="text" class="form-control" id="addressInput" onFocus="geolocate()" placeholder="Enter a location"> --}}
              </div>
              <div class="col-md-2" id="current-location">
                <button type="button" class="btn btn-success" id="findMyLocation">FIND MY LOCATION</button>
              </div>
            </div>

            <label for="">Search Radius</label>
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" id="radiusSelect" label="Radius">
                  <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option value="40">40</option>
                  <option value="50">50</option>
                  <option value="no-limit">No Limit</option>
                </select>
              </div>
            </div>
            <br>
            <hr>
            <button type="button" class="btn btn-success btn-search-location" id="searchButton">Search</button>
          </form>
        </div>

        <br>

        <div class="row">
          <div class="col-md-3 location-list">
            <div>
              <ul id="locationSelect">&nbsp;</ul>
            </div>
          </div>
          <div class="col-md-9 map">
            <div class="convert-flow">
              <div id="map" style="width: 100%; height: 90%"></div>
            </div>
          </div>
        </div>
      </div>
  </section>
@endsection

@section('js')
  <script type="text/javascript">
    var map;
    var markers = [];
    var infoWindow;
    var locationSelect;
    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    /**
     * Initialize the map
     * @return [type] [description]
     */
    function initMap() {
      initAutocomplete();

      var sydney = {lat: 33.3053199, lng: -85.48358869999998};

      map = new google.maps.Map(document.getElementById('map'), {
        center: sydney,
        zoom: 4,
        mapTypeId: 'roadmap',
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
      });
      infoWindow = new google.maps.InfoWindow();

      getXml();

      searchButton = document.getElementById("searchButton").onclick = searchLocations;
      findMyLocation = document.getElementById("findMyLocation").onclick = searchLocations;

      locationSelect = document.getElementById("locationSelect");

      // mao ni nga code gamitun pag select location kilid sa mapa sa left
      locationSelect.onchange = function() {
        var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
        if (markerNum != "none") {
          google.maps.event.trigger(markers[markerNum], 'click');
        }
      };
    }

    function initAutocomplete() {
      // Create the autocomplete object, restricting the search to geographical
      // location types.
      autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */
        (document.getElementById('addressInput')), {
          types: ['geocode']
      });

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
      // autocomplete.addListener('place_changed', fillInAddress);
    }

    function searchLocations() {
      var address  = document.getElementById("addressInput").value;
      var geocoder = new google.maps.Geocoder();

      geocoder.geocode({address: address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          searchLocationsNear(results[0].geometry.location);
        } else {
          alert(address + ' not found');
        }
      });
    }

    function getXml(searchUrl = '')
    {
      var searchUrl = '/store/get/xml';

      downloadUrl(searchUrl, function(data) {
        var xml         = parseXml(data);
        var markerNodes = xml.documentElement.getElementsByTagName("marker");
        var bounds      = new google.maps.LatLngBounds();

        for (let i = 0; i < markerNodes.length; i++) {
          var id       = markerNodes[i].getAttribute("id");
          var name     = markerNodes[i].getAttribute("name");
          var address  = markerNodes[i].getAttribute("address");
          var distance = parseFloat(markerNodes[i].getAttribute("distance"));
          var latlng   = new google.maps.LatLng(
            parseFloat(markerNodes[i].getAttribute("latitude")),
            parseFloat(markerNodes[i].getAttribute("longitude"))
          );

          createOption(markerNodes[i], distance, i);
          createMarker(latlng, name, address);
          bounds.extend(latlng);
        }

        map.fitBounds(bounds);
        locationSelect.style.visibility = "visible";
        locationSelect.onchange = function() {
          var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
          google.maps.event.trigger(markers[markerNum], 'click');
        };
      });
    }

    function searchLocationsNear(center) {
      clearLocations();

      var radius    = document.getElementById('radiusSelect').value;
      var searchUrl = '/store/search?lat=' + center.lat() + '&lng=' + center.lng() + '&radius=' + radius;

      downloadUrl(searchUrl, function(data) {
        var xml         = parseXml(data);
        var markerNodes = xml.documentElement.getElementsByTagName("marker");
        var bounds      = new google.maps.LatLngBounds();

        for (let i = 0; i < markerNodes.length; i++) {
          var id       = markerNodes[i].getAttribute("id");
          var name     = markerNodes[i].getAttribute("name");
          var address  = markerNodes[i].getAttribute("address");
          var distance = parseFloat(markerNodes[i].getAttribute("distance"));
          var latlng   = new google.maps.LatLng(
            parseFloat(markerNodes[i].getAttribute("latitude")),
            parseFloat(markerNodes[i].getAttribute("longitude"))
          );

          createOption(markerNodes[i], distance, i);
          createMarker(latlng, name, address);
          bounds.extend(latlng);
        }

        map.fitBounds(bounds);

        let locSel = document.getElementById('locationSelect').getElementsByClassName('location-select');

        for(let i=0; i < locSel.length; i++) {
            locSel[i].onclick = function() {
              let markerNum = locSel[i].dataset.num;
              google.maps.event.trigger(markers[markerNum], 'click');
            };
        }
      });
    }

    function clearLocations() {
      infoWindow.close();
      for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
      }
      markers.length = 0;

      locationSelect.innerHTML = "";
      var option = document.createElement("li");
      // option.value = "none";
      option.innerHTML = "";
      locationSelect.appendChild(option);
    }

    function createMarker(latlng, name, address) {
      var html = "<b>" + name + "</b> <br/>" + address;
      var marker = new google.maps.Marker({
        map: map,
        position: latlng
      });
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
      markers.push(marker);
    }

    function createOption(name, distance, num) {
      let html =
      	'<li class="location-select" data-num="'+ num +'">' +
    			'<div class="">' +
    				'<div id="title" class="">'+ name.getAttribute("name") +'</div>' +
    				'<div id="address" class="">'+ name.getAttribute('address') +'</div>' +
    				'<div>' +
    					'<span id="" class="">'+ name.getAttribute('city') +' ,</span> ' +
    					'<span id="" class="">'+ name.getAttribute('state') +' </span>' +
    					'<span id="" class="">'+ name.getAttribute('zipcode') +'</span>, ' +
    					'<span id="" class="">'+ name.getAttribute('country') +'</span>' +
    				'</div>' +
    				'<div id="" class="">'+ name.getAttribute('phone') +'</div>' +
    				'<div id="" class=""></div>' +
    				'<div id="" class="">' +
    					'<a href="" target="_blank"></a>' +
    				'</div>' +
    			'</div>' +
      	'</li>';

      locationSelect.insertAdjacentHTML('afterbegin', html);
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request.responseText, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function parseXml(str) {
      if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.loadXML(str);
        return doc;
      } else if (window.DOMParser) {
        return (new DOMParser).parseFromString(str, 'text/xml');
      }
    }

    function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();

      for (let component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (let i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById(addressType).value = val;
        }
      }
    }

    /**
     * Use in autocompleting the address in search bar
     * @return void
     */
    function geolocate() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          let geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          let circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
          });
          autocomplete.setBounds(circle.getBounds());
        });
      }
    }

    function doNothing() {}
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAC_aNhPd3VhqYlu2Egc1I0A5-vqTnYaB0&callback=initMap&libraries=places"></script>
@endsection
