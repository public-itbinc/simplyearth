@extends('themes.frontend') 
@section('body_class','') @push('meta')

<meta name="title" content="{{ $product->meta_title }}" />
<meta name="description" content="{{ $product->meta_description }}" />
<meta name="keywords" content="{{ $product->meta_keywords }}" /> 
@endpush 
@section('title',$product->name) 
@section('content')

<section class="pdtb40 product-single">
	<div class="container-fluid">
		<div class="alert alert-warning text-center">
			<h3>Sorry, this product is not available for your account.</h3>
			<p>
				<a href="{{ route('products') }}" class="btn btn-primary">Continue shopping</a>
			</p>
		</div>
	</div>
</section>
@endsection