@extends('themes.frontend') 
@section('body_class','') @push('meta')

<meta name="title" content="{{ $product->meta_title }}" />
<meta name="description" content="{{ $product->meta_description }}" />
<meta name="keywords" content="{{ $product->meta_keywords }}" /> 
@endpush 
@section('title',$product->name) 
@section('content')

@include('frontend.layouts._product-content');

<section>
	<div class="container-fluid related-products-view">
		<ProductListingHorizontal :collection="{{ $related_products->toJson() }}"></ProductListingHorizontal>
	</div>
</section>

@endsection