@extends('themes.frontend') 
@section('body_class','bg-default checkout-page') 
@section('content')

<section class="pdtb40 bg-white">
    <div class="container-fluid">
        <div class="row eq-height eq-smaller-height video-with-text">
            <div class="col-md-6">
                <div class="section-copy">
                    <h1 class="blue wide-title">Welcome To Simply Earth</h1>
                    <p>Hey {{ $customer->first_name }}! You’ve made a great choice in choosing natural products with
                        a purpose. We’ve sent order confirmation to {{ $customer->email }} and your order should arrive
                        in 3-5 business days. Enjoy making your home more natural with our products!</p>

                    @include('frontend.layouts._button_check_order');
                </div>
            </div>
            <div class="col-md-6">
                <iframe width="560" height="420" src="https://www.youtube.com/embed/3djfSd2c8RQ?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

{{--
    @include('frontend.layouts.friends') --}}

<section class="bg-default">
    <div class="container-fluid">
        <div class="row eq-height image-with-text">
            <!--options: copy-on-right-->
            <div class="col-sm-6">
                <div class="section-image">
                    <img src="{{ asset('images/enjoy-family.jpg') }}" alt="">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="section-copy section-text-left">
                    <h1 class="blue">Enjoy Your Home</h1>
                    <p>We are always coming up with fun and easy ways to help you make your home natural on our blog. So check
                        out some of our DIY recipes and make them while enjoying time together with friends and family.
                    </p>
                    <a href="{{ route('all.blog') }}" class="btn btn-primary btn-lg">Check out the blog</a>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection