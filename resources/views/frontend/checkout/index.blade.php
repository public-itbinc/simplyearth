@extends('themes.plain') 
@section('body_class','bg-default checkout-page') 
@if($is_monthly2019)
@section('header')
<div class="top-bar top-bar-wizard">
    <div class="container">
        <div class="col-xs-4 step-status">
            Step 2 of 2
        </div>
        <div class="col-xs-4 main-logo">
            <a href="/"><img src="{{ asset('images/logor.png') }}" alt="Simply Earth"></a>
        </div>
        {{--<div class="col-xs-4 chatbox  text-right">
            <img src="{{ asset('images/chat.png') }}" alt="chat" />
        </div>--}}
    </div>
</div>
<div class="top-bar-after top-bar-after-final"></div>
@endsection
@endif

@section('content')
<div class="site checkout-page">

    <div class="container-fluid">
        <div class="row top-checkout">
            <div class="col-sm-6">
                <div class="main-logo">
                    <a href="/"><img src="{{ asset('images/logor.png') }}"></a>
                </div>
            </div>
            <div class="col-sm-6 lock-label">
                <div class="lock-content">
                    <span class="se-icon icon-lock"></span>
                    <div class="lbl-info">
                        <label>SAFE AND SECURE CHECKOUT</label>
                        <span>128-bit SSL encryption</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="pd0">
        
                    <checkout-form :checkout_settings="{{ collect($settings) }}"
                        :customer="{{ collect($customer)->toJson() }}"></checkout-form>
        
    </section>
    <div class="modal animated fadeIn" id="modal-how-to-cancel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                    HOW TO CANCEL
                </div>
                <div class="modal-body">
                    You may cancel your subscription at anytime by following the cancellation procedures from your account, or by sending us
                    a message at <a href="mailto:help@simplyearth.com">help@simplyearth.com</a>.
                </div>
            </div>
        </div>
    </div>

    <div class="modal animated fadeIn" id="modal-auto-billed-monthly" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                    AUTOMATICALLY REBILLED MONTHLY
                </div>
                <div class="modal-body">
                    Subscriptions will be automatically renewed for successive monthly periods and your payment method will be automatically
                    be charged until you cancel.
                </div>
            </div>
        </div>
    </div>

    <div class="modal animated fadeIn" id="modal-6month-commitment" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                    BILLED MONTHLY
                    </div>
                    <div class="modal-body">
                            You are committing to 3 months of our essential oil recipe box. You are welcome to cancel your renewal at any time so that we know to stop sending you our essential oil recipe box after that initial 3 months 
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- .wrapper -->
@endsection
 
@section('footer_base_scripts')
<script src="https://www.paypalobjects.com/api/checkout.min.js" data-version-4></script>
<script src="{{ mix('js/checkout.js') }}"></script>
@endsection