
<table class="centered" align="center" width="600" cellpadding="0" cellspacing="0">
    <tr>
        <td class="" align="center">
            {{ Illuminate\Mail\Markdown::parse($slot) }}
        </td>
    </tr>
</table>