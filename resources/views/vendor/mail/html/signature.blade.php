
<table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="600">
    <tbody>
        <tr>
            <td>
                <p>Till next time!</p>
                <P><img src="{{ asset('mail/logo.jpg') }}" alt="logo" /></p>
                <p><em>The Simply Earth Team - Making your home natural in a fun and easy way.&nbsp;</em></p>
            </td>
        </tr>
    </tbody>
</table>