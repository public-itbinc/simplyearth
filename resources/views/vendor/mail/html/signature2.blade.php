
<table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="600">
    <tbody>
        <tr>
            <td>
                <p>Till next time!</p>
                <P><img src="{{ asset('mail/katie.jpg') }}" alt="logo" /></p>
                <p><em>Katie & Glo - Chief Recipe Makers, Simply Earth</em></p>

            </td>
        </tr>
    </tbody>
</table>