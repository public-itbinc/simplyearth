<table class="order-table-wrapper" align="left" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <td>
                <table class="order-table" width="100%">
                    <thead>
                        <tr>
                            <th class="header-product-info">ITEM</th>
                            <th class="text-center">QTY</th>
                            <th class="header-product-subtotal">SUB TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                @foreach($order->order_items as $order_item)
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="product-image"><img src="{{ asset($order_item->cover) }}" /></td>
                                            <td class="product-info">{{ $order_item->name }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="text-center">{{ $order_item->quantity }}</td>
                            <td class="product-price">
                                @if($order_item->sale_price)
                                <small class="strike">{{ SEPriceFormat($order_item->quantity * $order_item->price) }}</small> {{ SEPriceFormat($order_item->quantity * $order_item->sale_price) }} 
                                @else
                                {{ SEPriceFormat($order_item->quantity * $order_item->price) }}
                                @endif
                            </td>
                        </tr>
                @endforeach
                    </tbody>
                </table>
                <table class="order-table order-table-footer" width="100%">
                    <tbody>
                        <tr>
                            <td align="right">
                                SHIPPING
                            </td>
                            <td class="product-price" width="30%">{{ SEPriceFormat($order->total_shipping) }}</td>
                        </tr>

                        <tr>
                            <td align="right">
                                DISCOUNT
                            </td>
                            <td class="product-price" width="30%">&#8211;{{ SEPriceFormat($order->total_discounts) }}</td>
                        </tr>

                        <tr>
                            <td align="right">
                                TAX
                            </td>
                            <td class="product-price" width="30%">{{ SEPriceFormat($order->total_tax) }}</td>
                        </tr>

                        <tr>
                            <td align="right">
                                    <strong>TOTAL (USD)</strong>
                            </td>
                            <td class="product-price" width="30%"><strong>{{ SEPriceFormat($order->total_price) }}</strong></td>
                        </tr>

                    </tbody>
                </table>
                <table class="order-table" width="100%">
                    <thead>
                        <tr>
                            <th align="left" class="">SHIPPING</th>
                            <th align="left" class="">BILLING</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                @if($order->shipping_address)
                                <div>{{ $order->shipping_address->name }}</div>
                                <div>{{ $order->shipping_address->email }}</div>
                                <div>{{ $order->shipping_address->company }}</div>
                                <div>{{ $order->shipping_address->address1 }}</div>
                                <div>{{ $order->shipping_address->address2 }}</div>                                
                                <div><span>{{ $order->shipping_address->city }}</span> <span>{{ $order->shipping_address->region }}</span> <span>{{ $order->shipping_address->country }}</span> <span>{{ $order->shipping_address->zip }}</span></div>
                                @endif
                            </td>
                            <td>
                                {{ $order->payment_method['maskedNumber'] }}

                                @if($order->billing_address)
                                <div>{{ $order->billing_address->name }}</div>
                                <div>{{ $order->billing_address->email }}</div>
                                <div>{{ $order->billing_address->company }}</div>
                                <div>{{ $order->billing_address->address1 }}</div>
                                <div>{{ $order->billing_address->address2 }}</div>                                
                                <div><span>{{ $order->billing_address->city }}</span> <span>{{ $order->billing_address->region }}</span> <span>{{ $order->billing_address->country }}</span> <span>{{ $order->billing_address->zip }}</span></div>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>