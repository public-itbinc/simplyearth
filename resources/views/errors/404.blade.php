@extends('themes.frontend')

@section('content')

<section class="bg-default">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="content-404 text-center">
                    <h1><img src="{{ asset('images/oops.png') }}"></h1>

                    <p class="caption blue-text">That page can't be found.</p>

                    <div class="row">
                        <div class="col-xs-4 col-xs-offset-4">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="ti-search"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-xs-offset-3">
                            <p class="caption">It looks like nothing was found at this location	and we would like to make it up to you! The Essential Oils Beginner guide is yours for <span class="blue-text"><strong>FREE</strong></span>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-white">
    <div class="container-fluid">
        <div class="row eq-height image-with-text copy-on-right">
            <div class="col-md-6">
                <div class="section-copy text-left">
                    <h1 class="section-title blue-text text-left wide-title">Free Essential Oils <br>for Beginners eBook</h1>
                    <p class="text-centerleft">New to Essential Oils?  Not sure where to begin?  We are here to help. We love essential oils so much that we have developed a FREE essential oils ebook. It will help you get started with essential oils. It includes everything from what essential oils actually are to ideas on how to incorporate essential oils into your daily life. </p>
                    <a href="#" class="btn btn-primary btn-lg cta-6743-trigger">DOWNLOAD NOW</a>
                </div>
            </div>
            <div class="col-sm-6">	
                <div class="section-image">
                    <div class="loader"></div>
                    <img class="preview-img" src="{{ asset('images/ebook.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection