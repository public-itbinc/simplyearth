import Vue from 'vue'
import App from './App.vue'
import Page from './page.vue'
import VueSweetalert2 from 'vue-sweetalert2';
window.$ = window.jQuery = jQuery = $ = require('jquery');

import 'trumbowyg/dist/plugins/upload/trumbowyg.upload'
import 'trumbowyg/dist/plugins/base64/trumbowyg.base64'


const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674'
}
Vue.component('Trumbowyg', VueTrumbowyg.default);

Vue.use(VueSweetalert2, options)

Vue.config.productionTip = false

new Vue({
    render: h => h(Page),
}).$mount('#page')

new Vue({
  render: h => h(App),
}).$mount('#app')

window.FastClone = {

    factory: function (source, isDeep) {
        if (typeof source != 'object' || Array.isArray(source)) {
            throw new Error('Source is not an object');
        }
        var deep = isDeep === undefined ? true : isDeep;

        return new Function('src', FastClone._getKeyMap(source, deep));
    },

    cloneArray: function(source, isDeep) {
        if (!Array.isArray(source)) {
            throw new Error('Source should be an array');
        }
        var deep = isDeep === undefined ? true : isDeep;

        var clonedArray = [];
        if (source.length) {
            var Clone = FastClone.factory(source[0], deep);
            for (var i = 0; i < source.length; i++) {
                clonedArray.push(new Clone(source[i]));
            }
        }
        return clonedArray;
    },

    _getKeyMap: function (source, deep, baseKey, arrIndex) {
        var base = baseKey || '';
        var index = (arrIndex || 0) + 1;

        var keysMap = base ? 'this' + base : '';

        if (Array.isArray(source)) {
            var iterVal = 'i' + index;
            var iterPath = base + '[' + iterVal + ']';

            if (typeof source[0] == 'object') {
                keysMap += base ? ' = [];' : '';
                keysMap += 'for (var ' + iterVal + ' = 0; ' + iterVal + ' < src' + base + '.length; ' + iterVal + '++) {';
                keysMap += FastClone._getKeyMap(source[0], deep, iterPath, index);
                keysMap += '}';
            } else {
                keysMap += ' = src' + base + '.slice();';
            }
        } else {
            keysMap += base ? ' = {};' : '';

            for (var key in source) {
                if (!source.hasOwnProperty(key)) {
                    continue;
                }

                var value = source[key];
                var path = base + '.' + key;

                if (deep && typeof value == 'object' && value !== null) {
                    keysMap += FastClone._getKeyMap(value, deep, path, index);
                } else {
                    keysMap += 'this' + path + ' = src' + path + ';';
                }
            }
        }

        return keysMap;
    }
};

if (typeof module == 'object' && typeof module.exports == 'object') {
    module.exports = FastClone;
}