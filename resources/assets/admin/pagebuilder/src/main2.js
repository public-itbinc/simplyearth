import Vue from 'vue'
import Page from './page.vue'
// window.$ = window.jQuery = jQuery = $ = require('jquery');
import 'trumbowyg/dist/plugins/upload/trumbowyg.upload'
import 'trumbowyg/dist/plugins/base64/trumbowyg.base64'


const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674'
}
Vue.component('Trumbowyg', VueTrumbowyg.default);


Vue.config.productionTip = false


new Vue({
    render: h => h(Page),
}).$mount('#page')
