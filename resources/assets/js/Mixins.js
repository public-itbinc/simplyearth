import Popups from "./components/Popups.vue";
import Cartwidget from "./components/Cartwidget.vue";
import Swal from "./components/Swal.vue";
import Productlisting from "./components/ProductListing.vue"
import Subscribe from "./components/Subscribe.vue";
import Productlistinghorizontal from "./components/products/ProductListingHorizontal";
import IngredientListing from "./components/recipes/IngredientListing";
import {
    Global,
    EventHub
} from "./components/Global";

export const FrontendMixin = {
    components: {
        Popups,
        Cartwidget,
        Productlisting,
        Subscribe,
        Productlistinghorizontal,
        IngredientListing,
        Swal,
    },
    methods: {
        warnNonWholesaler() {
            swal({
                title: '',
                text: 'You need to log-in to your wholesale account to order our starter kit. If you don\'t have an account yet, check your email for an account activation email or call us at (866) 330-8165',
                buttons: {
                    ok: {
                        text: 'OK',
                        className: "btn btn-primary"
                    }
                }
            })
        },
        productLink(slug) {
            window.location.href = '/products/' + slug
            return false;
        },
        globalQueryUpdate(value) {
            this.show_global_search_results = value.length > 0 ? true : false
        },
        gaAddToCart(product) {

            if (!window.dataLayer) {
                return;
            }

            window.dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'add': { // 'add' actionFieldObject measures.
                        'products': [{ //  adding a product to a shopping cart.
                            'name': product.name,
                            'id': product.sku,
                            'price': product.price,
                            'quantity': 1
                        }]
                    }
                },
                'product': { //  adding a product to a shopping cart.
                    'name': product.name,
                    'id': product.sku,
                    'price': product.price,
                    'quantity': 1
                }
            });
        }
    },
    data() {
        return {
            global: Global,
            show_global_search_results: false,
            EventHub: EventHub,
            global_search_tags: window.SimplyEarth.user_tags
        };
    },
}