class OrderClass
{    
    set(data)
    {
        this.data = data

        for(let field in data)
        {
            this[field] = data[field]
        }
    }
}

export const _OrdersCollection = new OrderClass