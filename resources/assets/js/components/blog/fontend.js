$(function() {
  window.SocialShareKit.init()

  $('.blog-detail .body blockquote > p').prepend(
    "<i class='fa fa-quote-left fa-2x blue-text'></i>"
  )
  $('.blog-detail .body blockquote > p').append(
    '<i class="fa fa-quote-right fa-2x blue-text"></i>'
  )

  let iframe = document.querySelectorAll('.blog-detail .body p > iframe');

  if(iframe.length > 0){
    let iframe_wrapper = document.createElement('div');
    let iframe_parent = iframe[0].parentNode;
    iframe_wrapper.className = 'iframe-wrapper';
    iframe_wrapper.appendChild(iframe[0]);
    iframe_parent.appendChild(iframe_wrapper);
  }

  /* Share on tweeter ---------------------------------------------------------------------------*/
  /*let div = $(".blog-detail > .body > div");

    for (var i = 0; i < div.length; i++) {
      let idiv = $(div[i]);

      if (div[i].innerText.length > 1) {
        let appendHtml =
        '<p class="actn">' +
        '<a href="#" class="tweet" data-text="'+ div[i].innerText +'">CLICK TO TWEET <span class="ti-twitter-alt"></span></a>' +
        '</p>';

        idiv.append(appendHtml);
      } else {
        idiv.remove();
      }
    }*/

  let shareURL = 'http://twitter.com/share?'

  $(document).on('click', '.tweet', function() {
    let forTweeting = $(this).data('text')

    let params = {
      url: window.location.origin,
      text: forTweeting,
      hashtags: 'simplyearth'
    }

    for (let prop in params)
      shareURL += '&' + prop + '=' + encodeURIComponent(params[prop])
    window.open(
      shareURL,
      '',
      'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0'
    )
  })
  /*-------------------------------------------------------------------------------------------------- */
})
