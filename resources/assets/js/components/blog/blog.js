$(function() {
  /**
   * Preview the blog article
   * @var
   */
  $(document).on('click', '#blog-preview', function() {
    $('#draft').html('');
    $('#preview').html('<input type="hidden" name="preview" value="true">');
    $('#draft').html('<input type="hidden" name="status" value="draft">');

    appendCategories();
    $('#blog-form').submit();
  });

  /**
   * Publish the blog article
   * @var
   */
  $(document).on('click', '#blog-publish', function() {
    $('#draft').html('');
    $('#publish').html('<input type="hidden" name="status" value="publish">');

    appendCategories();
    $('#blog-form').submit();
  });

  /**
   * submit edited article
   * @return void
   */
  $(document).on('click', '#blog-publish-edit', function() {
    $('#draft').html('');
    $('#publish').html('<input type="hidden" name="status" value="publish">');

    appendCategories();
    $('#blog-form').submit();
  });

  /**
   * publish draft
   * @return void
   */
  $(document).on('click', '#blog-publish-draft', function() {
    $('#draft').html('');
    $('#publish').html('<input type="hidden" name="status" value="publish">');

    appendCategories();
    $('#blog-form').submit();
  });

  /**
   * Decided to preview the composed blog
   * @var
   */
  $(document).on('click', '#btn-preview', function() {
    $('#preview').html();

    appendCategories();
    $('#blog-form').submit();
  });

  /**
   * submit draft
   * @return void
   */
  $(document).on('click', '#btn-draft', function() {
    $('#draft').html('<input type="hidden" name="status" value="draft">');

    appendCategories();
    $('#blog-form').submit();
  });

  /**
   * Generate slug
   * @return {[type]} [description]
   */
  $(document).on('change', '#title', function() {
    let str = $(this).val();
    str = str.replace(/\s+/g, '-').toLowerCase();
    $('#slug').val(str);
  })
});

$("input").val();

/**
 * Appending categories
 *
 * @return
 */
function appendCategories()
{
  $('#categories input:checked').each(function() {
    let val = $(this).attr('value');
    $('#blog-form').append('<input type="checkbox" name="category[]" value="'+ val +'" checked> category');
  });
}
