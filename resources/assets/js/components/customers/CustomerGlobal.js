class CustomerClass
{
    set(data)
    {
        this.data = data

        for(let field in data)
        {
            this[field] = data[field]
        }
    }
}

export const _CustomersCollection = new CustomerClass