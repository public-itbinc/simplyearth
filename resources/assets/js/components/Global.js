import Vue from "vue";
import numeral from "numeral";

class GlobalClass {
  constructor() {
    this.Cart = [];
    this.cart_initialized = false
    this.cart_select = window.SimplyEarth ? window.SimplyEarth.cart_select : null
  }

  initCart() {
    axios.get('/cart').then(response => {
      this.Cart = response.data
      this.cart_initialized = true
      EventHub.$emit('cart_initialized');
    })
  }

  setCart(cart) {
    this.Cart = cart    
    EventHub.$emit("cart_initialized")
  }

  getTotal() {
    if (!this.Cart.currency) return "";
    return this.Cart.currency.symbol + this.Cart.grand_total;
  }

  getSubTotal() {
    if (!this.Cart.currency) return "";
    return this.Cart.currency.symbol + this.Cart.subtotal;
  }

  getWholesaleDiscountTotal() {
    if (!this.Cart.currency) return "";
    return this.Cart.currency.symbol + this.Cart.wholesale_discount_total;
  }

  getTax() {
    if (!this.Cart.currency) return "";
    return this.Cart.currency.symbol + this.Cart.tax;
  }

  getShipping() {

    if (this.Cart.shipping_total == 0) {
      return 'FREE';
    } else if (this.Cart.shipping_total > 0) {
      return this.Cart.currency.symbol + this.Cart.shipping_total;
    } else if (this.Cart.shipping) {
      return this.Cart.currency.symbol + this.Cart.shipping.rate;
    } else {
      return '-';
    }
  }

  getDiscount() {
    if (!this.Cart.currency) return "";
    return this.Cart.currency.symbol + this.Cart.discount_total;
  }

  itemCount(id) {
    return this.Cart.count;
  }

  productItemCount(id) {
    let count = 0;
    for (let i in this.Cart.items) {
      if (this.Cart.items[i].id == id) {
        count = this.Cart.items[i].qty;
        break;
      }
    }

    return count;
  }

  getProductByRowID(id) {
    for (let i in this.Cart.items) {
      if (this.Cart.items[i].id == id) {
        return this.Cart.items[i];
      }
    }

    return;
  }

  decrement(id) {
    let row = false;

    for (let i in this.Cart.items) {
      if (this.Cart.items[i].id == id) {
        row = this.Cart.items[i];
        break;
      }
    }

    if (row) {
      if (window.dataLayer) {
        window.dataLayer.push({
          'event': 'removeFromCart',
          'ecommerce': {
            'remove': { // 'add' actionFieldObject measures.
              'products': [{ //  adding a product to a shopping cart.
                'name': row.type=='subscription' ? row.name.split(':')[0] : row.name,
                'id': row.sku,
                'price': row.wholesale_pricing ? row.wholesale_price : row.price,
                'quantity': 1
              }]
            }
          },
          'product': { //  adding a product to a shopping cart.
            'name': row.name,
            'id': row.sku,
            'price': row.wholesale_pricing ? row.wholesale_price : row.price,
            'quantity': 1
          }
        });
      }

      this.updateCartItem(row.id, row.qty - 1);
    }
  }

  increment(id) {
    let row = false;

    for (let i in this.Cart.items) {
      if (this.Cart.items[i].id == id) {
        row = this.Cart.items[i];
        break;
      }
    }

    if (row) {
      if (window.dataLayer) {
        window.dataLayer.push({
          'event': 'addToCart',
          'ecommerce': {
            'add': { // 'add' actionFieldObject measures.
              'products': [{ //  adding a product to a shopping cart.
                'name': row.type=='subscription' ? row.name.split(':')[0] : row.name,
                'id': row.sku,
                'price': row.wholesale_pricing ? row.wholesale_price : row.price,
                'quantity': 1
              }]
            }
          },
          'product': { //  adding a product to a shopping cart.
            'name': row.name,
            'id': row.sku,
            'price': row.wholesale_pricing ? row.wholesale_price : row.price,
            'quantity': 1
          }
        });
      }
      this.updateCartItem(row.id, row.qty + 1);
    }
  }

  updateCartItem(rowId, qty) {

    if (qty < 1) {
      swal({
        title: 'Do you want to remove "' + this.getProductByRowID(rowId).name + '" from your cart?',
        className: 'buttons-vertical',
        buttons: {
          catch: {
            text: "Yes Please",
            value: true,
            className: 'btn btn-primary btn-lg btn-wide'
          },

          nevermind: {
            text: "Nevermind",
            value: false,
            className: 'btn btn-bare btn-lg'
          },
        },
      }).then((value) => {
        if (value) {
          this.updateCartItemFinal(rowId, qty);
        }
      });

      return;
    }

    this.updateCartItemFinal(rowId, qty);
  }

  updateCartItemFinal(rowId, qty) {
    axios
      .patch("/cart/" + rowId + "/edit", {
        qty: qty,
        ship_now: 1
      })
      .then(response => {
        this.Cart = response.data.data;
      });
  }

  updateShippingMethod(checkout) {
    axios
      .patch("/cart/shipping", {
        checkout: checkout,
      })
      .then(response => {
        this.Cart = response.data
        EventHub.$emit('cart_initialized');
      });
  }

  updateAddon(monthKey, rowId, qty, callback) {
    axios
      .patch("/box/" + monthKey + '/addon/' + rowId, {
        qty: qty,
      })
      .then(response => {
        callback(response.data)
      });
  }

  switchVariant(variant) {
    this.cart_select = variant
  }

  addVariant() {
    this.addToCart(this.cart_select);
  }

  addBulkToCart(product){
    for (var i = product.length - 1; i >= 0; i--) {
      axios
        .patch("/cart/" + product[i].id + "/edit", {
          qty: 1,
        })
        .then(response => {
          if (response.data.has_se_message) {

            this.seMessage(response.data)

          } else if (response.data.when_to_ship) {
            EventHub.$emit("when_to_ship", response.data);
          } else if (response.data.shipped_nextbox) {
            EventHub.$emit("shipped_nextbox");
          } else {
            EventHub.$emit("cart_item_added");
            this.Cart = response.data.data;
            if (window.dataLayer) {
             
              window.dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                  'add': { // 'add' actionFieldObject measures.
                    'products': [{ //  adding a product to a shopping cart.
                      'name': product[i].type=='subscription' ? product[i].name.split(':')[0] : product[i].name,
                      'id': product[i].sku,
                      'price': product[i].wholesale_pricing ? product[i].wholesale_price : product[i].price,
                      'quantity': qty
                    }]
                  }
                },
                'product': { //  adding a product to a shopping cart.
                  'name': product[i].name,
                  'id': product[i].sku,
                  'price': product[i].wholesale_pricing ? product[i].wholesale_price : product[i].price,
                  'quantity': qty
                }
              });
            }
          }
        })
        .catch(error => {
          swal({
            text: "Something is wrong",
            icon: "error"
          });
        });

    }

  }

  addToCart(product, qty, ship_now) {

    if (!qty) EventHub.$emit("howmuch", product);
    else {
      axios
        .patch("/cart/" + product.id + "/edit", {
          qty: qty,
          ship_now: ship_now
        })
        .then(response => {
          if (response.data.has_se_message) {
            this.seMessage(response.data)

          } else if (response.data.when_to_ship) {
            EventHub.$emit("when_to_ship", response.data);
          } else if (response.data.shipped_nextbox) {
            EventHub.$emit("shipped_nextbox");
          } else {
            EventHub.$emit("cart_item_added");
            this.Cart = response.data.data;

            if (window.dataLayer) {
              window.dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                  'add': { // 'add' actionFieldObject measures.
                    'products': [{ //  adding a product to a shopping cart.
                      'name': product.type=='subscription' ? product.name.split(':')[0] : product.name,
                      'id': product.sku,
                      'price': product.wholesale_pricing ? product.wholesale_price : product.price,
                      'quantity': qty
                    }]
                  }
                },
                'product': { //  adding a product to a shopping cart.
                  'name': product.name,
                  'id': product.sku,
                  'price': product.wholesale_pricing ? product.wholesale_price : product.price,
                  'quantity': qty
                }
              });
            }
          }
        })
        .catch(error => {
          swal({
            text: "Something is wrong",
            icon: "error"
          });
        });
    }
  }

  showOrderDetails(order_id) {
    EventHub.$emit('popup_order_details', order_id);
  }

  priceFormat(price) {
    return '$' + numeral(price).format("0,0.00");
  }

  seMessage(data) {
    swal({
      text: data.message,
      buttons: {
        link: {
          text: data.link_text,
          value: 'link',
          className: 'btn btn-primary btn-lg'
        }
      }
    }).then(value => {
      switch (value) {
        case 'link':
          window.location = data.link
          break;
      }
    })
  }
}

export const Global = new GlobalClass();

/**
 * Form Class
 */

export class ErrorsClass {
  constructor() {
    this.errors = {};
  }

  has(field) {
    return this.errors.hasOwnProperty(field);
  }

  set(errors) {
    this.errors = errors;
  }

  get(field) {
    if (this.errors[field]) return this.errors[field][0];
  }

  clear(field) {
    if (this.errors[field]) delete this.errors[field];
  }

  getMessages() {
    let messages = [];
    for (let field in this.errors) {
      messages.push(this.errors[field][0])
    }

    return messages;
  }

  reset() {
    this.errors = {};
  }

}

export class FormClass {
  constructor(data) {
    this.set(data);
    this.errors = new ErrorsClass();
  }

  set(data) {
    for (let field in data) {
      this[field] = data[field];
    }
  }

  get() {
    let data = Object.assign({}, this);

    delete data["errors"];

    return data;
  }

  reset() {
    this.errors.reset();

    let keys = Object.keys(this);

    for (let field in keys) {
      if (keys[field] === "errors") continue;

      delete this[keys[field]];
    }
  }
}

export const EventHub = new Vue();