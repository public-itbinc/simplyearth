import BaseCollectionClass from "../../base/BaseCollectionClass";

class ShippingZoneClass extends BaseCollectionClass {}

export const _ShippingZoneCollection = new ShippingZoneClass();
