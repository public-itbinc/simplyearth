require("./bootstrap-admin");

window.Vue = require("vue");

import VueRouter from "vue-router";
import { EventHub } from "./components/Global";
import { _OrdersCollection } from "./components/orders/OrderGlobal";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      component: require("./components/orders/OrderList.vue")
    },
    {
      path: "/create",
      name: "create",
      component: require("./components/orders/OrderForm.vue")
    },
    {
      path: "/:id",
      name: "order",
      component: require("./components/orders/OrderDetail.vue")
    },
    {
      path: "/:id/edit",
      name: "edit",
      component: require("./components/orders/OrderEdit.vue")
    }
  ]
});

new Vue({
  el: "#app-orders",
  router,
  methods: {
    openCreateForm() {
      router.push({ name: "create" });
    }
  },
  created() {
    EventHub.$on("order_created", function(data) {
      _OrdersCollection.data = null;
      router.push({ name: "order", params: { id: data.id } });
    });

    EventHub.$on("order_updated", function(data) {
      _OrdersCollection.data = null;
    });
  }
});