/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const Vue = require("vue");

require("./bootstrap");
require("./base/dependencies");
require("./master");

//Moment
import moment from "moment";

Vue.prototype.moment = moment;

import swal from "sweetalert";
import slick from "slick-carousel";
import {FormClass} from "./components/Global";
import {
  FrontendMixin
} from "./Mixins"

import CustomerAddress from "./components/profile/CustomerAddress.vue";
import CustomerPaymentMethod from "./components/profile/CustomerPaymentMethod.vue";
import CustomerEmail from "./components/profile/CustomerEmail.vue";
import CustomerPassword from "./components/profile/CustomerPassword.vue";

Vue.component("futurebox", require("./components/subscription/FutureBox.vue"));
Vue.component("resumebox", require("./components/subscription/ResumeBox.vue"));
Vue.component("v-button", require("./components/misc/VButton.vue"));
Vue.component(
  "managesubscription",
  require("./components/subscription/ManageSubscription.vue")
);

import InstantSearch from 'vue-instantsearch';
Vue.use(InstantSearch);

const app = new Vue({
  el: "#app",
  mixins: [FrontendMixin],
  methods: {},
  components: {
    customeraddress: CustomerAddress,
    customerpaymentmethod: CustomerPaymentMethod,
    customeremail: CustomerEmail,
    customerpassword: CustomerPassword,
  }
});