/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const Vue = require("vue");

require("./bootstrap");
require("./base/dependencies");
require("./master");
require('./components/blog/fontend');

//Moment
import moment from "moment";

Vue.prototype.moment = moment;
Vue.prototype.swal = swal;

import swal from "sweetalert";
import slick from "slick-carousel";
import Timer from "./components/Timer.vue";
import SocialShareKit from "./components/blog/social-share-kit";
import IngredientListing from "./components/recipes/IngredientListing";

import {
  Global,
  FormClass,
  EventHub
} from "./components/Global";


import {
  FrontendMixin
} from "./Mixins"

import InstantSearch from 'vue-instantsearch';
import TypeFormButton from "./components/TypeFormButton.vue";
import gcms from "./components/GCMS.vue";
Vue.use(InstantSearch);

const app = new Vue({
  el: "#app",
  mixins: [FrontendMixin],
  components:{
    Timer,
    IngredientListing,
    TypeFormButton,
    gcms
  },
  methods: {
    showProductDescription() {
      $('.product-detail-tab a[href="#details"]').tab('show');
      setTimeout(() => {
        $('html, body').animate({
          scrollTop: $('#details').offset().top - 100
        })
      }, 250)
    },
  },
});