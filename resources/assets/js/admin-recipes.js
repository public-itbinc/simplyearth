require("./bootstrap-admin");
require("./form");

var dt = require("datatables.net");

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("ingredientListing", require("./components/recipes/IngredientListing"));
Vue.component("recipeboxmini", require("./components/recipes/RecipeBoxMini"));

const app = new Vue({
 el: "#ingredientListing",
   components: {
  
  }
});

