require("./bootstrap-admin");
require('./components/blog/bootstrap-tagsinput');
require('./components/blog/blog');
require("./form");


window.Vue = require("vue");

Vue.component("medialibrary", require("./components/MediaLibrary.vue"));


const app = new Vue({
  el: "#app",
});
