require('./bootstrap-admin');

var dt = require('datatables.net');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('droppable', require('./components/Droppable.vue'));
Vue.component('medialibrary', require('./components/MediaLibrary.vue'));
Vue.component('repeatable', require('./components/Repeatable.vue'));
Vue.component("searchlister", require("./components/SearchLister.vue"));
Vue.component("input-tag", require("./components/InputTag.vue"));

import PageDiscount from "./pages/Discount.vue";
import PageRecipe from "./pages/Recipe";
import VueRouter from "vue-router";
//Moment
import moment from "moment-timezone";

Vue.prototype.moment = moment;
Vue.use(VueRouter);

const router = new VueRouter({
    routes: [{
            path: "/",
            component: require("./components/Dashboard.vue"),
            name: "dashboard"
        },
        {
            path: "/gift-cards",
            component: require("./components/giftcards/GiftCardsList.vue"),
            name: "gift-cards"
        },
        {
            path: "/administrators",
            component: require("./components/staff/Administrators.vue"),
            name: "administrators",
        },
        {
            path:'/admin/administrators/:id/edit',
            component: require("./components/staff/StaffEdit.vue"),
            name: "staff-edit",
        },
        {
            path: "/reports",
            component: require("./components/reports/ReportList.vue"),
            name: "reports"
        },
    ],
    //mode: 'history',
});


new Vue({
    el: '#app',
    router,
    components: {
        pagediscount: PageDiscount,
        pagerecipe: PageRecipe
    },
    methods: {
        deleteStore(id) {
            swal({
                title: 'You are about to delete store location',
                className: 'buttons-vertical',
                buttons: {
                    catch: {
                        text: "Continue",
                        value: true,
                        className: 'btn btn-primary btn-lg btn-wide'
                    },

                    nevermind: {
                        text: "Cancel",
                        value: false,
                        className: 'btn btn-default btn-lg'
                    },
                },
            }).then((value) => {
                if (value) {
                    axios.delete('/admin/storelocator/' + id).then(response => {
                        window.location.reload();
                    })
                }
            });
        }
    }
});
