import numeral from "numeral";

class SEFunctions {
    formatNumber(number) {
        return numeral(number).format("0,0,.00");
    }
}

export default SE = new SEFunctions;