// Core
import tinymce from 'tinymce/tinymce'
import 'tinymce/themes/modern/theme'
// Plugins
import 'tinymce/plugins/paste/plugin'
import 'tinymce/plugins/link/plugin'
import 'tinymce/plugins/autoresize/plugin'
import 'tinymce/plugins/lists/plugin'
import 'tinymce/plugins/image/plugin'
import 'tinymce/plugins/media/plugin'
import 'tinymce/plugins/code/plugin.min'
import 'tinymce/plugins/image/plugin.min'
import 'tinymce/plugins/imagetools/plugin.min'

tinymce.init({
  selector: '.post-editor',
  toolbar:
    'formatselect | bold italic strikethrough forecolor backcolor | link | code | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | medialibrary media image | undo redo',
  plugins: [
    'paste',
    'link',
    'code',
    'autoresize',
    'lists',
    'media',
    'image',
    'imagetools'
  ],
  valid_elements : "*[*]",
  extended_valid_elements : "*[*],script[charset|defer|language|src|type],style",
  custom_elements: "*[*],script[charset|defer|language|src|type],style",
  valid_children : "+body[style],+body[script]",
  verify_html : false,
  media_strict: false,
  menubar: ['insert', 'tools'],
  image_title: true,
  automatic_uploads: true,
  relative_urls: false,
  remove_script_host: false,
  file_picker_types: 'image',
  images_upload_url: '/admin/media',
  content_style: 'img {max-width: 100%;}',
  image_advtab: true,
  image_caption: true,
  style_formats: [
    {
      title: 'Image Left',
      selector: 'img',
      styles: {
        float: 'left',
        margin: '0 10px 0 10px'
      }
    },
    {
      title: 'Image Right',
      selector: 'img',
      styles: {
        float: 'right',
        margin: '0 10px 0 10px'
      }
    }
  ],
  setup: function(editor) {
    editor.addButton('medialibrary', {
      text: 'Add Image',
      icon: false,
      onclick: function() {
        var modal = $(`
                <div class="modal" id="media-library-uploader"> 
                    <div class="modal-dialog modal-lg"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                            </div> 
                            <div class="modal-body"> 
                                <div class="form-group form-inline">
                                    <input type="file" name="cover" class="form-control" id="new-media" /> 
                                    <button type="button" class="btn btn-primary" id="uploadNewCover">Upload new image</button> 
                                </div> 
                                <div class="medialibrary"> 
                                    <div class="row media-collection"></div> 
                                </div>
                            </div> 
                        </div> 
                    </div> 
                </div>
                `)

        modal.modal('show')

        let modal_body = modal.find(
          '.modal-body .medialibrary .media-collection:first'
        )

        modal.on('click', 'img', function() {
          editor.insertContent('<img src="' + this.src + '" />')
          modal.modal('hide')
        })

        modal.on('click', '#uploadNewCover', function() {
          var formData = new FormData()
          let imagefile = $('#new-media', modal)
          formData.append('file', imagefile.get(0).files[0])
          axios
            .post('/admin/media/upload', formData, {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            })
            .then(response => {
              modal_body.prepend(
                '<div class="col-sm-2 image-holder"><img src="' +
                  response.data.url +
                  '" width="150" height="auto" class="text-pointer img-thumbnail" /></div>'
              )
            })
        })

        axios
          .get('/admin/media', {
            params: {
              source: 'media',
              page: 1 // page ? page : 1
            }
          })
          .then(response => {
              let data = response.data;

              data.data.sort((x,y) => {
                return y.timestamp - x.timestamp;
              });

            for (let i in data.data) {
              modal_body.append(
                '<div class="col-sm-2 image-holder"><img src="' +
                  data.data[i].url +
                  '" width="150" height="auto" class="text-pointer img-thumbnail" /></div>'
              )
            }
          })

        //editor.insertContent('&nbsp;<b>It\'s my button!</b>&nbsp;');
      }
    })
  },
  images_upload_handler(blobInfo, success, failure) {
    var formData = new FormData()
    formData.append('file', blobInfo.blob(), blobInfo.filename())
    axios
      .post('/admin/media/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(response => {
        success(response.data.url)
      })
  }
})
