/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const Vue = require("vue");

require("./bootstrap");
require("./base/dependencies");
require("./master");

import numeral from "numeral";
import swal from "sweetalert";
import slick from "slick-carousel";
import CheckoutForm from "./components/checkout/CheckoutForm.vue";
import Popups from "./components/Popups.vue";
import {
  EventHub,
  Global,
  FormClass
} from "./components/Global";
Vue.component("popups", require("./components/Popups.vue"));


const app = new Vue({
  el: "#app",
  components: {
    CheckoutForm,
    Popups
  },
  mounted() {
    Global.initCart()
  },
  methods:{},
  data: {
    global: Global
  }
});