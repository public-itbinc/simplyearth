/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const Vue = require("vue");

require("./bootstrap");
require("./base/dependencies");
require("./master");

//Moment
import moment from "moment";

Vue.prototype.moment = moment;

import swal from "sweetalert";
import slick from "slick-carousel";
import {FormClass} from "./components/Global";
import {
  FrontendMixin
} from "./Mixins"

import Invites from "./components/invites/Invites.vue";

import InstantSearch from 'vue-instantsearch';
Vue.use(InstantSearch);

const app = new Vue({
  el: "#app",
  mixins: [FrontendMixin],
  data:{
    invite_emails:""
  },
  methods: {
    sendInvitation() {

      axios
        .post("/pages/referral/send-invitation", {'email': this.invite_emails})
        .then(response => {
          swal("Invite sent!", "", "success");
        })
        .catch(error => {
          swal("Something went wrong", "", "error");
        });
    },
    saveShares(customer_id, source) {
      axios
        .post("/pages/save-shares", {'customer_id': customer_id, 'source': source})
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          swal("Something went wrong", "", "error");
        });
    }
  },
  components: {
    invites: Invites
  }
});