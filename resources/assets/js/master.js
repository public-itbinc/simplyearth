/*!
 * [Project Name]
 * Simply Earth
 */

import hoverintent from "hoverintent";

var infiniteScroll = require('infinite-scroll');
var imagesLoaded = require('imagesloaded');

infiniteScroll.imagesLoaded = imagesLoaded;
var simplyEarth = (function () {
  'use strict';

  function init() {
    var $searchForm = $('.search-form');
    var sidebar = $('#main-sidebar');
    // Your code here
    // $('.main-carousel').slick({
    //           slide: 'article',
    //           speed: 500,
    //           slidesToShow: 1,
    //           slidesToScroll: 1,
    //           autoplay: true,
    //           arrows: true
    //       });

    // masonry
    $(document).ready(function () {

      $('.testimonial-image > .testimonial-inner').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        arrows: false,
      });

      var $grid = $('.masonry').masonry({
        itemSelector: '.grid-item'
      });

      var loading_options = {
        finishedMsg: "<div class='end-msg'>No more blogs to load!</div>",
        msgText: "<div class='center'>Loading new Blogs...</div>",
        img: "/../images/icons/loader.gif"
      };

      var msnry = $grid.data('masonry');

      $grid.infiniteScroll({
        // options
        loading: loading_options,
        path: '.pagination li.active + li a',
        append: '.grid-item',
        outlayer: msnry,
        history: false,
        scrollThreshold: 500,
        loadOnScroll: true,
        hideNav: '.pagination',
        debug: false
      });
    });
    //Accordion auto open
    if (window.location.hash) {
      var $target = $('.se-accordion').find(window.location.hash);
      if ($target.length > 0) {
        $target.collapse('show').siblings().addClass('collapsed')
        $('html, body').animate({
          scrollTop: ($target.offset().top - 100)
        }, 1000);
      }
    }

    $('#category-sidebar-opener').click(function () {
      sidebar.addClass('open');
    })

    $('#category-sidebar-closer').click(function () {
      sidebar.removeClass('open');
    })

    $('#burger-menu').click(function () {
      $(this).toggleClass('open');

      var $menu = $('.main-menu');
      if ($(this).hasClass('open')) {
        $menu.addClass('show');
      } else {
        $menu.removeClass('show');
      }
    });

    function footerBoxToggle() {
      var $footerBox = $('.footer-box');
      var $boxToggle = $('.box-toggle');
      $('.footer-box .footer-title').click(function () {
        $(this).closest($footerBox).toggleClass('open');
      });
    }
    footerBoxToggle();

    //$(".videoPlayers").fitVids();


    function initializeVideo($videoId, $videoType) {
      var $player = '<iframe src="https://www.youtube.com/embed/' + $videoId + '?autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
      return $player;
    }

    function initializeVideo640($videoId, $videoType) {
      var $player = '<iframe width="640" height="360" src="https://www.youtube.com/embed/' + $videoId + '?autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
      return $player;
    }

    $(document).on('click', '.play-video', function (e) {
      e.preventDefault();
      clear_video_player();
      var $player = $(this).parent().find('.videoPlayer');
      var $vidId = $player.data('video-id');
      var $videoType = $player.data('video-type');

      var result = initializeVideo($vidId, $videoType);

      $player.append(result);
    });

    $('.pop-video').click(function (e) {
      e.preventDefault();
      let $this = $(this);
      let video_id = $(this).data('video-id')

      if (video_id == '') {
        return;
      }

      if ($('#player' + video_id).length == 0) {
        $('body').append(`<div id="player` + video_id + `" class="modal video-modal">
        <div class="modal-dialog">
          <div class="modal-body"></div>
        </div>
        </div>`)
      }

      let player = initializeVideo640(video_id, 'youtube');
      let video_container = $('<div class="videoPlayers" />').append(player)



      let modal = $('#player' + video_id).modal().on('hidden.bs.modal', function (e) {
        modal.find('.modal-body').html('');
      });

      modal.find('.modal-body').append(video_container);



    })

    //subscription box
    var box_slick = $('.slick-shopping-box').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 1,
      centerMode: true,
      infinite: false,
      variableWidth: true,
      draggable: false,
      arrows: false,
      cssEase: 'linear',
    })

    box_slick.find('.slick-prev').click(function () {
      box_slick.slick('slickPrev');
    })
    box_slick.find('.slick-next').click(function () {
      box_slick.slick('slickNext');
    })

    // START: subscription Page Slider for testimonial 
    var testiMonialSlider = $('#testimonial-slider');


    testiMonialSlider.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });

    $('.slick-prev', testiMonialSlider).click(function () {
      testiMonialSlider.slick('slickPrev');
    })
    $('.slick-next', testiMonialSlider).click(function () {
      testiMonialSlider.slick('slickNext');
    })


    let variants = $('.variant-attribute', '.variant-images')

    $('.slider-for', '.product-gallery').on('init', function () {

      //Variant


      if (variants.length > 0) {
        let first_slide = $('.slick-current', '.slider-main');

        variants.click(function () {
          first_slide.find('img').attr('src', $(this).addClass('active').find('img').attr('src'))
          variants.not(this).removeClass('active');
          $('.slick-current .image', '.slider-thumb').css({
            'background-image': 'url(' + $(this).addClass('active').find('img').attr('src') + ')'
          })
          $('.slider-for').slick('slickGoTo', 0);
        })

        let $first_variant = variants.first();
        let image_src = $first_variant.addClass('active').find('img').attr('src');

        first_slide.find('img').attr('src', image_src)
      }

      // end variant

      $('.product-gallery').removeClass('invisible')
    }).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: false,
      adaptiveHeight: true,
      infinite: false,
      useTransform: true,
      speed: 400,
      cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
    });


    $('.slider-for', '.recipe-gallery').on('init', function () {
      // end variant

      $('.recipe-gallery').removeClass('invisible')
    }).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav'
    }).on('afterChange', function (event, slick, currentSlide) {
      clear_video_player();
    })
    $('.slider-nav', '.recipe-gallery').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    });



    $('.slider-nav').on('init', function (event, slick) {
        $('.slider-nav .slick-slide.slick-current').addClass('is-active');

        if (variants.length > 0) {

          let $first_variant = variants.first();
          let image_src = $first_variant.addClass('active').find('img').attr('src');
          $('.slick-current .image', '.slider-thumb').css({
            'background-image': 'url(' + image_src + ')'
          })
        }


      })
      .slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        focusOnSelect: false,
        infinite: false,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
          }
        }, {
          breakpoint: 640,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        }, {
          breakpoint: 420,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        }]
      });

    $('.slider-for').on('afterChange', function (event, slick, currentSlide) {
      $('.slider-nav').slick('slickGoTo', currentSlide);
      var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
      $('.slider-nav .slick-slide.is-active').removeClass('is-active');
      $(currrentNavSlideElem).addClass('is-active');
    });

    $('.slider-nav').on('click', '.slick-slide', function (event) {
      event.preventDefault();
      var goToSingleSlide = $(this).data('slick-index');

      $('.slider-for').slick('slickGoTo', goToSingleSlide);
    });

    $('.nav-tabs a').click(function () {
      clear_video_player();
      $(this).tab('show');
    });


    $('.box-tab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      box_slick.slick('slickGoTo', 1)
    })

    var productBox = $('#products .thumb-box');
    productBox.hover(function (e) {
      if (e.type == "mouseenter") {
        $(this).addClass('hovered');
      } else {
        $(this).removeClass('hovered');
      }
    });


    // var $panelTab = $('.panel-tab .tab li');
    // var $tabContent = $('.panel-tab .tab-content');
    // $panelTab.click(function() {
    //  $panelTab.removeClass('active');
    //  $tabContent.removeClass('active');

    //  $(this).addClass('active');

    //  var $target = $(this).data('content-target');
    //  $($target).addClass('active');


    // });

    function clear_video_player() {
      $('.videoPlayer').html("");
    }

    var $pmLabel = $('.payment-method-selection label');
    $pmLabel.on('click', function (e) {
      e.preventDefault();
      $pmLabel.parent().removeClass('checked');
      if ($('input[name="payment-method"]:checked')) {
        $(this).parent().addClass('checked');
      }
      var $content = $(this).parent().find('input').val();
      var $paypalContainer = $('.paypal-content');
      var $ccContainer = $('.cc-content');

      var $contentMethod = $('.content-method');
      $contentMethod.removeClass('active');
      if ($content == "paypal") {
        $paypalContainer.addClass('active');
      } else {
        $ccContainer.addClass('active');
      }
    });

    $('.se-accordion .panel-title a').click(function (e) {
      e.preventDefault();
    });

    $('.checkout-wizard ul li').click(function (e) {
      e.preventDefault();
      var target = $(this).data('target');
      var content = $('.wizard-content');
      content.removeClass('active');

      $(this).addClass('active');
      $('#' + target).addClass('active');
    });

    $('.main-menu .has-sub').each(function () {
      hoverintent(this, function () {
        $(this).addClass('open')
      }, function () {
        $(this).removeClass('open')
      }).options({
        timeout: 300
      });
    })


    var text_max = $('#textarea').data('max');
    $('#wordcounter').html(text_max + ' characters remaining');

    $('#textarea').keyup(function () {
      var text_length = $('#textarea').val().length;
      var text_remaining = text_max - text_length;

      $('#wordcounter').html(text_remaining + ' characters remaining');
    });


    $('.submit-search').click(function () {
      $('body').toggleClass('show-search-form');
      $searchForm.find('input[type=search]').focus();
    });


    $('#forgotbox').on('show.bs.modal', function () {
      $('#login').modal('hide');
    });

    $('[data-toggle="tooltip"]').tooltip()
  }

  return {
    init: init
  };
}());

jQuery(document).ready(function ($) {
  simplyEarth.init();
});



function setModalMaxHeight(element) {
  this.$element = $(element);
  this.$content = this.$element.find('.modal-content');
  var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
    'overflow': 'hidden'
  });

  this.$element
    .find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
    });
}

$('.modal').on('show.bs.modal', function () {
  $(this).show();
  //setModalMaxHeight(this);
});

$(window).resize(function () {
  if ($('.modal.in').length != 0) {
    //setModalMaxHeight($('.modal.in'));
  }
});


// END: Subscription Page Slider for testimonial 



// Footer Arrangement in Mobile
function arrangeFooter() {
  var row1 = $('.footer-bottom .col-sm-4:eq(0)');
  var row2 = $('.footer-bottom .col-sm-4:eq(1)');
  var row3 = $('.footer-bottom .col-sm-4:eq(2)');
  var guaranteeIcons = $('.guarantee-icons');

  var socialIcons = $('footer .social-icons');

  if ($(window).width() < 769) {
    socialIcons.insertAfter('.guarantee-box');
    guaranteeIcons.insertBefore('.top-box p');
  }
  if ($(window).width() > 768) {
    guaranteeIcons.insertBefore('.guarantee-box p:eq(0)');
    socialIcons.insertAfter('.newsletter-form');
  }
}
arrangeFooter();
$(window).resize(function () {
  arrangeFooter();
});