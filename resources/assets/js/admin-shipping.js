require("./bootstrap-admin");

window.Vue = require("vue");

import VueRouter from "vue-router";
import { EventHub } from "./components/Global";
import { _ShippingZoneCollection } from "./components/shipping/ShippingZoneGlobal";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      component: require("./components/shipping/Shipping.vue")
    },
    {
      path: "/create",
      name: "create",
      component: require("./components/shipping/ShippingZoneForm.vue")
    },
    {
      path: "/:id",
      name: "shipping-zone",
      component: require("./components/shipping/ShippingZoneForm.vue")
    }
  ]
});

new Vue({
  el: "#app-shipping-zones",
  router,
  methods: {
    openCreateForm() {
      router.push({ name: "create" });
    },
    refreshList() {
      _ShippingZoneCollection.data = null;
      router.push("/");
    }
  },
  created() {
    EventHub.$on("shipping_zone_created", () => this.refreshList());

    EventHub.$on("shipping_zone_updated", () => this.refreshList());

    EventHub.$on("shipping_zone_deleted", () => this.refreshList());
  }
});
