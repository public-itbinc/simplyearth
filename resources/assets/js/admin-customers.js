require("./bootstrap-admin");

window.Vue = require("vue");

//Moment
import moment from "moment";

Vue.prototype.moment = moment;
import swal from 'sweetalert';
import VueRouter from "vue-router";
import {EventHub} from './components/Global';
import { _CustomersCollection } from "./components/customers/CustomerGlobal";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      component: require("./components/customers/CustomerList.vue")
    },
    {
      path: "/create",
      name: "create",
      component: require("./components/customers/CustomerForm.vue")
    },
    {
      path: "/:id",
      name: "customer",
      component: require("./components/customers/CustomerDetail.vue")
    }
  ]
});

new Vue({
  el: "#app-customers",
  router,
  methods:{
    openCreateForm(){
      router.push({name:"create"});
    }
  },
  created(){

    EventHub.$on('customer_created',function(data){
      _CustomersCollection.data= null;
      router.push({name:'customer',params:{id:data.id}})
    })

    EventHub.$on("customer_updated", function(data) {
      _CustomersCollection.data = null;
    });
  }
});
