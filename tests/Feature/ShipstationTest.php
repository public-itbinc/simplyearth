<?php

namespace Tests\Feature;

use App\Shop\Customers\Address;
use App\Shop\Customers\Customer;
use App\Shop\Orders\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;

class ShipstationTest extends TestCase
{
    use RefreshDatabase;

    public function test_shipstation_get()
    {

        Queue::fake();

        $order = factory(Order::class)->create(
            [
                'id' => 123456,
                'processed_at' => '2012-03-23 22:00:00'
            ]
        );

        $customer = factory(Customer::class)->create();

        $order->customer()->associate($customer);
        $order->order_items()->createMany(
            [
                [
                    'quantity' => 1,
                    'price' => 20,
                    'product_id' => 1234,
                    'sku' => 'ABCDE',
                    'name' => 'Product 1',
                    'extra_attributes' => [
                        [
                            'label' => 'Volume',
                            'value' => '20ML',
                        ],
                    ],
                ],
            ]
        );

        $address = factory(Address::class)->create(['primary' => 1]);
        $order->shipping_address()->create($address->toArray());
        $order->billing_address()->create($address->toArray());

        $order->save();

        $this->getJson('shipstation')->assertStatus(401);

        $response = $this->getJson(
            'shipstation?SS-UserName=' . config('shipstation.store.username')
            . '&SS-Password=' . config('shipstation.store.password')
            . '&action=export'
            . '&start_date=03/23/2012 21:09'
            . '&end_date=03/23/2014 21:09'
        )
            ->assertStatus(200);

        $xml_shipstation = new \DOMDocument();
        $xml_shipstation->loadXML($response->content());

        $xml_order = new \DOMDocument();
        $xml_order->load(dirname(__FILE__) . '/../xml/order.xml');

        $xml = simplexml_load_file(dirname(__FILE__) . '/../xml/shipnotify.xml');

        $content = $xml->asXML();

        $this->call(
            'POST',
            'shipstation?SS-UserName=' . config('shipstation.store.username')
            . '&SS-Password=' . config('shipstation.store.password')
            . '&action=shipnotify'
            . '&carrier=USPS'
            . '&service=Priority Mail'
            . '&tracking_number=1Z909084330298430820', [], [], [], $this->transformHeadersToServerVars(
                [
                    'CONTENT_LENGTH' => mb_strlen($content, '8bit'),
                    'CONTENT_TYPE' => 'application/xml',
                ]
            ),
            $content
        )->assertStatus(200);

        /*(new OrderShipNotify(
        $xml,
        [
        'carrier' => 'USPS',
        'service' => 'Priority Mail',
        'tracking_number' => '1Z909084330298430820',
        ]
        ))->process();*/

        $order->refresh();

        $this->assertEquals(
            [
                $xml->Carrier,
                $xml->Service,
                $xml->TrackingNumber,
                Order::ORDER_COMPLETED,
            ],
            [
                $order->shipping_carrier,
                $order->shipping_service,
                $order->tracking_number,
                $order->status,
            ]
        );

    }

}
