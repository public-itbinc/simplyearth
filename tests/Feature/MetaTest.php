<?php

namespace Tests\Feature;

use App\BlogPost;
use Tests\TestCase;

class MetaTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_shows_blog_meta()
    {
        $post = factory(BlogPost::class)->create();
        $this->getJson('/blogs/stories/' . $post->slug)->assertStatus(200)
            ->assertViewHas([
                'meta_title' => $post->seo_title,
                'meta_description' => $post->seo_description,
                'meta_keywords' => $post->seo_Keywords,
            ]);

    }
}
