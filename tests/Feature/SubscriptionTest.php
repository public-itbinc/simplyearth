<?php

namespace Tests\Feature;

use App\Events\OrderCreated;
use App\Events\Subscribed;
use App\Notifications\AccountManagement;
use App\Shop\Customers\Account;
use App\Shop\Customers\Customer;
use App\Shop\Orders\Order;
use App\Shop\Products\Product;
use App\Shop\Shipping\ShippingZone;
use App\Shop\Subscriptions\Subscription;
use Facades\App\Shop\Cart\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use App\Shop\ShoppingBoxes\ShoppingBoxBuilder;
use App\Shop\ShoppingBoxes\ShoppingBox;

class SubscriptionTest extends TestCase
{

    use RefreshDatabase;

    protected $non_subscription;

    protected $subscription_monthly;

    protected $subscription_monthly_new;

    protected $subscription_3_months;

    protected $shipping_zone;

    public function setUp()
    {
        parent::setUp();

        $this->non_subscription = factory(Product::class)->create(
            ['type' => 'default', 'sku' => 'non-subscription', 'price' => 5]
        );
        $this->subscription_monthly = factory(Product::class)->create(
            ['type' => 'subscription', 'sku' => env('SUBSCRIPTION_MONTHLY_SKU', 'subscription-monthly'), 'price' => 5]
        );

        $this->subscription_monthly_new = factory(Product::class)->create(
            ['type' => 'subscription', 'sku' => env('SUBSCRIPTION_MONTHLY2019_SKU', 'REC-MONTHLY2019'), 'price' => 5]
        );

        $this->subscription_3_months = factory(Product::class)->create(
            ['type' => 'subscription', 'sku' => 'subscription-3-months', 'price' => 5]
        );

        factory(Product::class)->create(
            ['type' => 'default', 'sku' => config('subscription.bonus_box') , 'price' => 20]
        );

        $this->shipping_zone = ShippingZone::create(
            [
                'name' => 'Test 2',
                'countries' => 'US']
        );

        $this->shipping_zone->shipping_rate_prices()->create([
            'name' => 'Regular',
            'min' => 0,
            'is_free' => 0,
            'rate' => 15,
        ]);

        $shop_date = Carbon::parse('first day of last month');

        for ($i = 0; $i < 13; $i++) {
            factory(ShoppingBox::class)->create([
                'name' => $shop_date->format('F Y'),
                'stock' => 999,
                'key' => str_slug($shop_date->format('F Y')),
            ]);

            $shop_date->addMonth(1);
        }
    }

    public function test_subscription_can_be_created()
    {

        Mail::fake();
        Notification::fake();

        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        $account = customer()->account;

        $this->assertTrue($account->subscribed());
        $this->assertEquals($this->subscription_monthly->sku, $account->subscription->plan);
        $this->assertTrue($account->subscription->active());
        $this->assertFalse($account->subscription->stopped());
        // $this->assertEquals(min(28, Carbon::now()->format('d')), $account->subscription->schedule);

        $subscription = $account->subscription;
        // Cancel Subscription
        $subscription->stop()->save();
        $this->assertFalse($subscription->active());
        $this->assertTrue($subscription->stopped());

        $subscription->fill(['ends_at' => Carbon::now()->subDays(5)])->save();
        $this->assertFalse($subscription->active());
        $this->assertTrue($subscription->stopped());

        // Resume Subscription
        $subscription->resume();

        $this->assertTrue($subscription->active());
        $this->assertFalse($subscription->stopped());

        //Test subscribing will resume the subscription
        $account->subscribe($this->subscription_monthly->sku)
            ->setSchedule(15)
            ->create();

        $account->refresh();

        $this->assertTrue($account->subscribed());
        $this->assertEquals($this->subscription_monthly->sku, $account->subscription->plan);
        $this->assertTrue($account->subscription->active());
        $this->assertFalse($account->subscription->stopped());
        $this->assertEquals(15, $account->subscription->schedule);
    }

    public function test_subscription_frontend()
    {

        Mail::fake();
        Notification::fake();

        $this->getJson('/pages/subscription-box')->assertStatus(200);

        $this->getJson('/subscribe')->assertStatus(422);

        $this->getJson('/subscribe', ['plan' => 'random'])->assertStatus(422);
        //$this->getJson('/subscribe?plan=' . $this->non_subscription->sku)->assertStatus(404);
        $this->getJson('/subscribe?plan=' . $this->subscription_monthly->sku)->assertRedirect(route('checkout'));

        $this->assertCount(1, Cart::getProducts());

        $this->assertEquals($this->subscription_monthly->id, Cart::getProducts()->first()->id);

        $this->expectsEvents([Subscribed::class, OrderCreated::class]);
        //Checkout

        $box_key = Cart::getSubscriptionBoxKey();

        $this->postJson('checkout', [
            'nonce' => 'fake-valid-nonce',
            'checkout' => [
                'email' => 'mharkrollen@gmail.com',
                'shipping_address' => [
                    'first_name' => 'John Doe',
                    'address1' => 'Kamagong RD Uptown',
                    'city' => 'Tagbilaran',
                    'zip' => '6300',
                    'country' => 'PH',
                    'region' => 'Bohol',
                ],
                'same_as_shipping_address' => true,
                'shipping_method' => $this->shipping_zone->shipping_rate_prices->first(),
            ],
        ])->assertStatus(200);

        $customer = Customer::where('email', 'mharkrollen@gmail.com')->first();

        $this->assertEquals([
            'John',
            'Doe',
        ], [
            $customer->first_name,
            $customer->last_name,
        ]);

        $this->assertNotNull($customer->account);
        $this->assertNotNull($customer->account->braintree_id);
        $this->assertNotNull($customer->account->subscription);

        $this->assertDatabaseHas('orders', [
            'email' => 'mharkrollen@gmail.com',
            'box_key' => $box_key,
        ]);

        //Check if Monthly box item name has the box key
        $this->assertDatabaseHas('order_items', [
            'name' => $this->subscription_monthly->name.":".$box_key 
        ]);

        $customer->refresh();

        $order = Order::first();

        $this->assertNotNull($customer->account->subscription);
        $this->assertTrue($customer->account->subscribed());
        $this->assertTrue($order->isSubscriptionPurchase());
        $this->assertCount(2, $order->order_items);

        $shopping_box = ShoppingBox::getByKey($box_key);

        //Test the shoppingbox stock is deducted
        $this->assertEquals(998, $shopping_box->stock);
        Notification::assertSentTo($customer->account, AccountManagement::class);

        //Order again
        $order = Cart::add($this->subscription_monthly)->build();
        $this->assertEquals(Carbon::now()->format('F Y'), $order->box_key);
        $this->assertEquals(997, $shopping_box->refresh()->stock);

        //Out of stock box should move to the next month;
        $shopping_box->update(['stock' => 0]);

        $order = Cart::add($this->subscription_monthly)->build();
        $shopping_box = ShoppingBox::getByKey($order->box_key);
        $this->assertEquals(Carbon::parse('first day of next month')->format('F Y'), $order->box_key);
        $this->assertEquals(998, $shopping_box->refresh()->stock);
        $this->assertEquals(1, $customer->refresh()->account->subscription->schedule);    
    }

    public function test_customer_can_skip_or_resume_or_gift_a_month()
    {
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->setSchedule(15)
            ->create();

        //Skipping
        $nextBox = customer()->account->nextBox();

        $this->assertFalse($nextBox->skipped());

        $this->postJson('/subscription/' . $nextBox->monthKey . '/skip')->assertStatus(200);

        $box = customer()->account->subscription->refresh()->futureOrders()->getMonth($nextBox->monthKey);

        $this->assertTrue($box->skipped());
        $this->assertFalse($box->gifted());

        //Resume

        $this->postJson('/subscription/' . $nextBox->monthKey . '/resume')->assertStatus(200);
        $box = customer()->account->subscription->refresh()->futureOrders()->getMonth($nextBox->monthKey);

        $this->assertFalse($box->skipped());
        $this->assertFalse($box->gifted());

        //Gift
        $this->postJson('/subscription/' . $nextBox->monthKey . '/gift', [
            'address_address1' => "dsaf",
            'address_address2' => "dsaf",
            'address_city' => "dsfdsa",
            'address_company' => "fdsf",
            'address_country' => "US",
            'address_first_name' => "fdasf",
            'address_last_name' => "sdafds",
            'address_phone' => null,
            'address_region' => "AR",
            'address_zip' => "fdsaf",
            'contact_by_email' => false,
            'email' => "gfdsg@gmail.com",
            'email_date' => "2018-04-06T16:00:00.000Z",
            'first_name' => "gfsd",
            'gift_date' => "2018-06-01T12:00:00.000Z",
            'last_name' => "gfds",
            'message' => "rwrewrewgfdgfdgfdgfd",
            'step' => 3,
        ])->assertStatus(200);

        $box = customer()->account->subscription->futureOrders()->refresh()->getMonth($nextBox->monthKey);

        $this->assertDatabaseHas('subscription_gifts', [
            'address_address1' => "dsaf",
            'address_address2' => "dsaf",
            'address_city' => "dsfdsa",
            'address_company' => "fdsf",
            'address_country' => "US",
            'address_first_name' => "fdasf",
            'address_last_name' => "sdafds",
            'address_phone' => null,
            'address_region' => "AR",
            'address_zip' => "fdsaf",
            'contact_by_email' => false,
            'email' => "gfdsg@gmail.com",
            'first_name' => "gfsd",
            'last_name' => "gfds",
            'message' => "rwrewrewgfdgfdgfdgfd",
        ]);

        $this->assertFalse($box->skipped());
        $this->assertTrue($box->gifted());

        //Cancel Gift
        $this->deleteJson('/subscription/' . $nextBox->monthKey . '/gift')->assertStatus(200);
        $box = customer()->account->subscription->futureOrders()->refresh()->getMonth($nextBox->monthKey);
        $this->assertFalse($box->gifted());
    }

    public function test_it_manage_subscriptions()
    {
        Carbon::setTestNow(Carbon::create(2018, 4, 26, 12));
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        //Stop

        $this->postJson('/subscription/stop')->assertStatus(200);

        $this->assertTrue(customer()->account->refresh()->paused());

        //resume
        $this->postJson('/subscription/resume')->assertStatus(200);

        customer()->account->subscription->refresh();

        $this->assertFalse(customer()->account->paused());
        $this->assertEquals(20, customer()->account->subscription->schedule);

        //Schedule
        $this->patchJson('/subscription/schedule', ['schedule' => 3])->assertStatus(200);
        $this->assertEquals(3, customer()->account->subscription->schedule);

        //change Plan
        //$this->patchJson('/subscription/plan', ['plan' => $this->subscription_3_months->sku])->assertStatus(200);
        //$this->assertEquals($this->subscription_3_months->sku, customer()->account->subscription->plan);
    }

    public function test_it_sets_consolidate_schedule_1()
    {
        Carbon::setTestNow(Carbon::create(2018, 4, 26, 12));
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        $this->assertEquals(20, customer()->account->subscription->schedule);
    }

    public function test_it_sets_consolidate_schedule_other_dates()
    {
        Carbon::setTestNow(Carbon::create(2018, 4, 14, 12));
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        $this->assertEquals(14, customer()->account->subscription->schedule);
    }

    public function test_it_sets_consolidate_schedule_16()
    {
        Carbon::setTestNow(Carbon::create(2018, 4, 27, 12));
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        $this->assertEquals(20, customer()->account->subscription->schedule);
    }

    public function test_skip_continue()
    {
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        $months = customer()->account->futureOrders()->getMonths();

        $this->postJson('/subscription/nextbox-continue', [
            'key' => $months[3]->monthKey,
        ])->assertStatus(200);

        $months = customer()->account->futureOrders()->refresh()->getMonths();

        $this->assertTrue($months[0]->skipped());
        $this->assertTrue($months[1]->skipped());
        $this->assertTrue($months[2]->skipped());
        $this->assertFalse($months[3]->skipped());
    }

    public function test_it_redirects_to_profile_if_active_or_paused()
    {
        $this->signInAsCustomer();

        customer()->account->subscribe($this->subscription_monthly->sku)
            ->create();

        $this->get('/pages/subscription-box')->assertRedirect(route('profile'));
        customer()->account->subscription->stop()->save();
        $this->get('/pages/subscription-box')->assertRedirect(route('profile'));
    }

    public function test_can_get_all_subscribed_users()
    {

        $account1 = factory(Account::class)->create();

        $account2 = factory(Account::class)->create();

        $account3 = factory(Account::class)->create();

        $account4 = factory(Account::class)->create();

        $account1->subscribe($this->subscription_monthly->sku)
            ->create();

        $account2->subscribe($this->subscription_monthly->sku)
            ->create();

        $account2->subscription->stop();

        $account3->subscribe($this->subscription_monthly->sku)
            ->create();

        $this->assertCount(2, Account::subscribedUsers()->get());
    }

    function test_user_box_count_by_orders()
    {
        $this->signInAsCustomer();

        account()->subscribe($this->subscription_monthly->sku)->create();

        $this->assertNotNull(account()->nextBox());

        $order = (new ShoppingBoxBuilder(account()->nextBox()))->setData(['status' => Order::ORDER_PROCESSING])->build();

        $this->assertNotNull($order->order_name);

        $this->assertEquals(1, account()->customer->subscriptionOrders()->count());
    }

    function test_wholesaler_tags_will_not_be_removed_after_subscription_build()
    {
        $this->signInAsCustomer();

        customer()->makeWholesaler();

        $this->assertTrue(customer()->isWholesaler());

        account()->subscribe($this->subscription_monthly->sku)->create();

        //added addons   
        $addon = factory(Product::class)->create(['price' => 50, 'shipping' => 1]);
        $addon2 = factory(Product::class)->create(['price' => 50, 'shipping' => 1]);

        account()->nextBox()->updateAddons($addon, 1);
        account()->nextBox()->updateAddons($addon2, 1);
   

        $order = (new ShoppingBoxBuilder(account()->nextBox()))->setData(['status' => Order::ORDER_PROCESSING])->build();

        $this->assertTrue(customer()->refresh()->isWholesaler());
    }
}
