<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Shop\Customers\Customer;
use App\Shop\Customers\CustomerReferral;
use App\Shop\Discounts\Discount;
use Illuminate\Support\Facades\Queue;
use App\Jobs\SendReferralInvite;
use Illuminate\Support\Facades\Auth;
use Facades\App\Shop\Cart\Cart;
use App\Shop\Products\Product;
use App\Shop\ShoppingBoxes\ShoppingBox;
use Illuminate\Support\Carbon;
use App\Shop\Orders\Order;
use App\Jobs\KlaviyoTrack;
use App\Shop\Customers\Invitation;
use App\Shop\ShoppingBoxes\ShoppingBoxBuilder;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReferralInvitationReferrer;

class ReferralTest extends TestCase
{
    use RefreshDatabase;

    protected $subscription_monthly;
    protected $subscription_commitment;
    protected $gift_card;


    public function setUp()
    {
        parent::setUp();

        $this->subscription_monthly = factory(Product::class)->create(
            ['type' => 'subscription', 'sku' => config('subscription.monthly'), 'price' => 5]
        );

        $this->subscription_commitment = factory(Product::class)->create(
            ['type' => 'subscription', 'sku' => config('subscription.commitment_box'), 'price' => 5]
        );

        $this->gift_card = factory(Product::class)->create(
            ['type' => 'gift_card', 'sku' => env('REFERRAL_GIFT_PRODUCT', 'sGIFT-CARD-20'), 'price' => 20]
        );

        $shop_date = Carbon::parse('first day of this month');

        for ($i = 0; $i < 12; $i++) {
            factory(ShoppingBox::class)->create([
                'name' => $shop_date->format('F Y'),
                'key' => str_slug($shop_date->format('F Y')),
            ]);

            $shop_date->addMonth(1);
        }
    }

    /** @test */
    public function it_can_generate_share_code()
    {

        $customer1 = factory(Customer::class)->create(['first_name' => 'John Doe', 'last_name' => 'Thurman']);

        $customer1->prepareShareCode();

        $this->assertEquals('JOHNDOETFREE', $customer1->share_code);

        //it should auto create referral discount;
        $this->assertNotNull($customer1->referral_discount);

        //remaining referral should be equal to referral factor]
        $this->assertEquals(config('app.referral_factor'), $customer1->remaining_referrals);

        //It should append numerica value if same names
        $customer2 = factory(Customer::class)->create(['first_name' => 'John Doe',  'last_name' => 'Thurman']);

        $customer2->prepareShareCode();

        $this->assertEquals('JOHNDOETFREE1', $customer2->share_code);

        //It should also append a numeric value if theres an existing discount code
        $customer3 = factory(Customer::class)->create(['first_name' => 'Jacinth Faith',  'last_name' => 'William']);

        $discount = Discount::create(['code' => 'JACINTHFAITHWFREE', 'type' => 'referral']);

        $customer3->prepareShareCode();

        $this->assertEquals('JACINTHFAITHWFREE1', $customer3->share_code);

        //Test if giftcard applies
        $this->signInAsCustomer();

        $regular_subscription_customer = customer();

        Cart::add($this->subscription_monthly);

        Cart::applyDiscount('JACINTHFAITHWFREE1');

        $this->assertEquals('referral', Cart::getDiscount()->type);

        $order = Cart::setData(['status' => 'processed'])->build();

        $this->assertEquals('JACINTHFAITHWFREE1', $order->refresh()->invitation->code);
        $this->assertEquals('waiting', $order->invitation->status);

        //It should not reward immediately but wait for the order's status to be completed before giving points to the referer
        $this->assertEquals(3, $customer3->refresh()->remaining_referrals);

        //Canceled order should be counted 
        $order->status = Order::ORDER_CANCELLED;
        $order->save();

        $this->assertEquals(3, $customer3->refresh()->remaining_referrals);

        //IT should not add points immediatetely for non commitment subscription plan
        $order->complete();
        $customer3->refresh();
        $this->assertEquals(0, $customer3->points);

        //Here we are testing subscription commitment plan, it should add points immedately after the order has been completed
        Auth::logout();
        $this->signInAsCustomer();

        Cart::setCustomer(customer());

        Cart::add($this->subscription_commitment);

        Cart::applyDiscount('JACINTHFAITHWFREE1');

        $order = Cart::setData(['status' => 'processed'])->build();

        $this->assertEquals('processing', $order->refresh()->invitation->status);

        Queue::fake(KlaviyoTrack::class);

        $order->complete();
        $customer3->refresh();

        $this->assertEquals(0, $customer3->points);
        $this->assertEquals(3,  $customer3->remaining_referrals);

        Queue::assertPushed(KlaviyoTrack::class, function ($job) {
            return $job->getEvent() == 'reward';
        });

        //Rewards will be awarded until referral factor value is reached
        $this->assertDatabaseMissing('rewards', [
            'customer_id' => $customer3->id,
            'type' => 'free_box',
        ]);


        //referral factor points is reached equals reward
        $customer3->addPoints(4);
        $customer3->refresh();

        $this->assertEquals(1, $customer3->points);
        $this->assertDatabaseHas('rewards', [
            'customer_id' => $customer3->id,
            'type' => 'freebox',
        ]);

        $this->assertCount(2, $customer3->rewards);

        $order = (new ShoppingBoxBuilder($regular_subscription_customer->refresh()->account->nextBox()))->setData(['status' => Order::ORDER_PROCESSING])->build();
        $order->complete();
        $this->assertCount(2, $customer3->refresh()->rewards);

        $order = (new ShoppingBoxBuilder($regular_subscription_customer->refresh()->account->nextBox()))->setData(['status' => Order::ORDER_PROCESSING])->build();
        $order->refresh();
        $order->complete();

        $this->assertCount(3, $customer3->refresh()->rewards);
    }

    /** @test */
    public function it_can_send_invitation()
    {

        $this->signInAsCustomer();

        Mail::fake();

        $emails = 'test1@gmail.com, test2@gmail.com';

        $this->postJson('pages/referral/send-invitation', [
            'email' => $emails
        ])->assertStatus(200);

        Mail::assertQueued(ReferralInvitationReferrer::class, 2);

        $this->assertDatabaseHas('invitations', [
            'email' => 'test1@gmail.com',
            'status' => 'pending'
        ]);

        $this->assertDatabaseHas('invitations', [
            'email' => 'test2@gmail.com',
            'status' => 'pending'
        ]);

        //it should work space separated as well
        $emails = 'test3@gmail.com test4@gmail.com test3@gmail.com';

        $this->postJson('pages/referral/send-invitation', [
            'email' => $emails
        ])->assertStatus(200);

        $this->assertDatabaseHas('invitations', [
            'email' => 'test3@gmail.com',
            'status' => 'pending'
        ]);

        $this->assertDatabaseHas('invitations', [
            'email' => 'test4@gmail.com',
            'status' => 'pending'
        ]);
    }
}
