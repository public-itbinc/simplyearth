# simplyearth
Simply Earth Online Shop

To get started:

1. composer install
2. configure db in .env file
2. php artisan migrate // db migrations 


laravel mix

1. npm install
2. npm run dev

create admin user

1. php artisan admin:create {name} {email} {password}
