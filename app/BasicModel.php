<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QueryFilters;
use App\Traits\Filterable;

class BasicModel extends Model
{
    use Filterable;

    function scopeSearch($query,$search, $column = 'name', $pattern = null)
    {
        $pattern = ($pattern) ? $pattern : "%" . $search . '%';

        return $query->where($column, 'LIKE', $pattern);
    }

}