<?php

namespace App\Providers;

use App\Shop\Tax\TaxCalculator;
use Illuminate\Support\ServiceProvider;

class TaxProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TaxCalculator::class, function ($app) {

            $tax = new TaxCalculator();

            return $tax;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [TaxCalculator::class];
    }
}
