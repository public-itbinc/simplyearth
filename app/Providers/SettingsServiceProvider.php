<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Shop\Settings\Setting;

class SettingsServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('settings', function ($app) {
            return Setting::pluck('value', 'option')->toArray();
            /*return $app['cache']->remember('site.settings', 60, function () {
                return Setting::pluck('value', 'option')->toArray();
            });*/
        });
    }
}
