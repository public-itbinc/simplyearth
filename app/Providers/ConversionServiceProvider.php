<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Shop\Conversion\Klaviyo;
use App\Shop\Conversion\ReferralCandy;

class ConversionServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Klaviyo::class, function ($app) {
            return new Klaviyo(config('conversion.klaviyo.api_key'), config('conversion.klaviyo.private_api_key'));
        });

        $this->app->singleton(ReferralCandy::class, function ($app) {
            return new ReferralCandy(config('conversion.referral_candy.access_id'), config('conversion.referral_candy.api_secret'));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Klaviyo::class, ReferralCandy::class];
    }
}
