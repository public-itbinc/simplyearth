<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ShipStation\ShipStation;

class ShipStationServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ShipStation::class, function ($app) {
            $shipstation = new ShipStation();
            $shipstation
                ->setSsApiKey(config('services.shipstation.api_key'))
                ->SetSsApiSecret(config('services.shipstation.api_secret'));

            return $shipstation;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ShipStation::class];
    }
}
