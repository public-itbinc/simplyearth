<?php

namespace App\Providers;

use App\Shop\Products\Product;
use Illuminate\Support\ServiceProvider;
use Gornymedia\Shortcodes\Facades\Shortcode;
use App\Shop\Recipes\Recipe;
class ShortcodeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        Shortcode::add('row', function($atts, $content, $name)
        {
            $content = Shortcode::compile($content);
            return "<div class='row'>$content</div>";
        });

        Shortcode::add('onehalf', function($atts, $content, $name)
        {
            $content = Shortcode::compile($content);
            return "<div class='col-xs-6'>$content</div>";
        });

        Shortcode::add('onefourth', function($atts, $content, $name)
        {
            $content = Shortcode::compile($content);
            return "<div class='col-xs-3'>$content</div>";
        });

        Shortcode::add('products', function($atts, $content, $name)
        {
            $a = Shortcode::atts(array(
                'related' => $name
            ), $atts);

            $product = Product::where('slug', $a['related'])
            ->active()
            ->orWhere('id',$a['related'])
            ->first();

            $related_products = $product ? $product->getRelatedProducts() : collect([]);

            return view('frontend.blogs.partials.blog-related-products', ['related_products' => $related_products]);
        });
        Shortcode::add('recipe', function($atts, $content, $name)
        {
            $a = Shortcode::atts(array(
                'related' => $name
            ), $atts);

            $ingredients = Recipe::getRelatedProducts($a['related']);

            return view('frontend.blogs.partials.blog-recipe-ingredients', ['ingredients' => $ingredients]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
