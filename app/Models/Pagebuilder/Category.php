<?php

namespace App\Models\Pagebuilder;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'pagebuilder_categories';

    protected $fillable = [
        'name',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function templates()
    {
        return $this->hasMany(Template::class, 'section', 'slug');
    }
}
