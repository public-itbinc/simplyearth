<?php

namespace App\Models\Pagebuilder;

use App\Traits\MetaData;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Page extends Model
{
    use MetaData;

    protected $table = 'pagebuilder_pages';

    protected $fillable = [
        'title',
        'slug',
        'header',
        'footer',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function contents()
    {
        return $this->belongsToMany(Template::class, 'pagebuilder_contents', 'page_id', 'template_id')
            ->withPivot(['content', 'rang'])
            ->orderBy('pagebuilder_contents.rang');
    }

    public function getCacheKey()
    {
        return sprintf(
            "%s/%s-%s",
            $this->getTable(),
            $this->getKey(),
            $this->updated_at->timestamp
        );
    }

    public function clearCache()
    {
        return Cache::forget($this->getCacheKey());
    }
}
