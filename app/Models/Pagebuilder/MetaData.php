<?php

namespace App\Models\Pagebuilder;

use Illuminate\Database\Eloquent\Model;

class MetaData extends Model
{
    protected $table = 'meta_data';

    protected $fillable = [
        'name',
        'value'
    ];

    public function metadatatable()
    {
        return $this->morphTo();
    }
}
