<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

/**
 * Gallery Model class
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 * @package Simply Earth
 * @date May 30, 2018
 * @date May 30, 2018 -- updated
 */
class BlogImageGallery extends Model
{
    protected $fillable = [
      'name', 'image'
    ];
}
