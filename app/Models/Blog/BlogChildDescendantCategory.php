<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class BlogChildDescendantCategory extends Model
{
    protected $fillable = [
      'child', 'descendant'
    ];

    /**
     * Return the child descendant category
     *
     * @param  int $id Category ID
     * @return array
     */
    public static function getDescendant($id)
    {
      return static::with(['descendant'])
        ->whereChild($id)
        ->get()
        ->toArray();
    }

    /**
     * Check the child category if has descendant
     *
     * @param  int  $id Category ID
     * @return boolean
     */
    public static function hasDescendant($id)
    {
      return static::whereChild($id)
        ->exists();
    }

    /**
     * Return the child
     * @return Illuminate\Response
     */
    public function child()
    {
      return $this->belongsTo('App\BlogCategory', 'child');
    }

    /**
     * Return the child
     * @return [type] [description]
     */
    public function descendant()
    {
      return $this->belongsTo('App\BlogCategory', 'descendant');
    }

    /**
     * Return true if the given ID exists
     *
     * Useful checking if the category is a parent
     *
     * @param  int  $id category ID
     * @return boolean
     */
    public static function isParent($id)
    {
      return static::whereParent($id)->exists();
    }
}
