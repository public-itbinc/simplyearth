<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for blog sub categories
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 */
class BlogSubCategories extends Model
{
    protected $fillable = [
      'parent', 'first_sub_category', 'second_sub_category'
    ];

    /**
     * Return the the blog parent categories
     *
     * @return Illuminate\Response
     */
    public static function getParentCategory()
    {
      return static::with('parent')
        ->join('blog_categories', 'blog_categories.id', '!=', 'blog_sub_categories.parent')
        ->get();
    }

    /**
     * Return the parent sub-category
     *
     * @return Illuminate\Response
     */
    public static function getFirstSubCategory()
    {
      return static::with('firstSubCategory')
        ->where('first_sub_category', '!=', 'null')
        ->where('parent', '!=', 'null')
        ->get();
    }

    /**
     * Return sub category of the first sub category
     *
     * @return Illuminate\Response
     */
    public static function getSecondSubCategory()
    {
      return static::with('secondSubCategory')
        ->where('second_sub_category', '!=', 'null')
        ->where('first_sub_category', '!=', 'null')
        ->get();
    }

    /**
     * Return the parent names of the category
     *
     * @return
     */
    public function parent()
    {
      return $this->belongsTo('App\BlogCategory', 'parent');
    }

    /**
     * return the first sub category
     *
     * @return
     */
    public function firstSubCategory()
    {
      return $this->belongsTo('App\BlogCategory', 'first_sub_category');
    }

    /**
     * return the second sub_category
     *
     * @return
     */
    public function secondSubCategory()
    {
      return $this->belongsTo('App\BlogCategory', 'second_sub_category');
    }
}
