<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class BlogParentChildCategory extends Model
{
    protected $fillable = [
      'parent', 'child'
    ];

    /**
     * Return parent and child given the parent ID
     *
     * @param  int $id category ID
     * @return array
     */
    public static function getChild($id)
    {
      return static::with(['child'])
        ->whereParent($id)
        ->get()
        ->toArray();
    }

    /**
     * Check if the given ID is a parent category
     *
     * @param  int  $id Category ID
     * @return boolean
     */
    public static function hasChild($id)
    {
      return static::whereParent($id)
        ->exists();
    }

    /**
     * Return the parent categories
     *
     * @return Illuminate\Response
     */
    public function parent()
    {
      return $this->belongsTo('App\BlogCategory', 'parent');
    }

    /**
     * Return the categories
     *
     * @return Illuminate\Response
     */
    public function child()
    {
      return $this->belongsTo('App\BlogCategory', 'child');
    }

    /**
     * Return true if the given ID exists
     *
     * Useful checking if the category is a child
     *
     * @param  int  $id category ID
     * @return boolean
     */
    public static function hasDescendant($id)
    {
      return static::whereChild($id)->exists();
    }
}
