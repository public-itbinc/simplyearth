<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Builder;
use App\QueryFilters;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Filterable;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'about', 'avatar'
    ];

    protected $appends = ['role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function scopeNoneStaff($query)
    {
        return $query->whereDoesntHave('roles',function($query){
            $query->whereIn('name',['super-admin','admin']);
        });
    }

    function scopeStaff($query)
    {
        return $query->whereHas('roles', function ($query) {
            $query->whereIn('name', ['super-admin', 'admin']);
        });
    }

    public function getRoleAttribute()
    {
        if(!count($this->roles))
            return '';

        return $this->roles->first()->name;
    }
}
