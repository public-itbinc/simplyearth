<?php

namespace App\Rules;

use Facades\App\Shop\Cart\Cart;
use App\Shop\Discounts\Discount;
use Illuminate\Contracts\Validation\Rule;

class CouponCode implements Rule
{

    protected $message = "Invalid code";
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $discount = Discount::where('code', $value)->first();

        if (!$discount) {
            return false;
        }

        if ($discount->setOrder(Cart::getInstance())->orderPassed()) {
            return true;
        } else {
            $this->message = $discount->getDiscountErrorMessage();
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
