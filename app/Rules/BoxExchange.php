<?php

namespace App\Rules;

use App\Shop\Products\Product;
use Illuminate\Contracts\Validation\Rule;

class BoxExchange implements Rule
{
    protected $message = '';
    protected $from;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Product $from)
    {
        $this->from = $from;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $product = Product::find($value);

        if (!$product) {
            $this->message = 'The product is not available for exchange';
            return false;
        }

        //Check if the product is a subscription type

        if ($product->type !== 'default') {
            $this->message = 'The product is not available for exchange';
            return false;
        }

        //Check if the product price is over 5 dollars

        if (($product->price - $this->from->price) > 5) {
            $this->message = 'The product\'s value is $2 more than the oil you are exchanging with.';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
