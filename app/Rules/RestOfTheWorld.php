<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Shop\Shipping\ShippingZone;
use Illuminate\Support\Facades\DB;

class RestOfTheWorld implements Rule
{
    protected $ignore;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($ignore = null)
    {
        $this->ignore = $ignore;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $where = [];

        $countries = is_array($value) ?$value:[$value];

        foreach ($countries as $country) {
            $where[] = ['countries', 'LIKE', '%' . $country . '%'];
        }

        if($this->ignore)
        {
            $where[] = ['id','!=',$this->ignore];
        }

        if (empty($where)) return true;

        return !ShippingZone::where($where)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Country already assigned';
    }
}
