<?php

namespace App\Rules;

use App\Shop\Orders\Order;
use Illuminate\Contracts\Validation\Rule;

class OrderStatusUpdate implements Rule
{
    protected $order;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        switch ($value) {
            case Order::ORDER_PROCESSING:
                if ($this->order->status == Order::ORDER_PENDING) {
                    return true;
                }

                break;

            case Order::ORDER_COMPLETED:
                if ($this->order->status == Order::ORDER_PROCESSING) {
                    return true;
                }

            case Order::ORDER_CANCELLED:

                if($this->order->status != Order::ORDER_CANCELLED) {
                    return true;
                }

                break;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid order status';
    }
}
