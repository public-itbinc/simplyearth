<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Filterable;

class BlogPost extends Model
{
    use Filterable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "title",
        "slug",
        "content",
        "excerpt",
        "seo_title",
        "seo_description",
        "seo_Keywords",
        "image",
        "status",
        "created_at",
        "updated_at",
        "published_at"
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($post) {

            $post->slug = $post->generateSlug();

        });

        static::updating(function ($post) {

            $post->slug = $post->generateSlug(true);

        });

    }

    /**
     * Get filtered or unfiltered blogpost
     * @param array $arr
     * @return mixed
     */
    public static function getBlogPosts($arr = [])
    {
        $category = isset($arr['category']) ? $arr['category'] : '';
        $tag = isset($arr['tag']) ? $arr['tag'] : '';
        $search = isset($arr['search']) ? $arr['search'] : '';

        if ($category !== "") {
            return static::whereHas('category.blogCategory', function ($query) use ($category) {
                $query->where('name', '=', str_replace("-", " ", $category));
            })->whereStatus('publish')->orderBy('created_at', 'DESC')->with('category')->paginate(8);
        } else if ($tag !== "") {
            return static::whereHas('tags.blogTag', function ($query) use ($tag) {
                $query->where('name', '=', str_replace("-", " ", $tag));
            })->whereStatus('publish')->orderBy('created_at', 'DESC')->with('category')->paginate(8);
        } else if ($search) {
            return static::with(self::relationship())
                ->where('title', 'LIKE', '%' . $search . '%')
                ->orWhere('content', 'LIKE', '%' . $search . '%')
                ->whereStatus('publish')
                ->orderBy('created_at', 'DESC')
                ->with('category')
                ->paginate(8);
        } else {
            return static::with(self::relationship())->whereStatus('publish')->orderBy('created_at', 'DESC')->with('category')->paginate(8);
        }
    }

    /**
     * Return the post relationship to tags and author
     *
     * @return array
     */
    private static function relationship()
    {
        return [
            'groups' => function ($query) {
                $query
                    ->with('blogTag')
                    ->get();
            },
            'category' => function ($query) {
                $query
                    ->with('blogCategory')
                    ->get();
            },
            'author',
        ];
    }

    /**
     * Return all blog post
     * @return object
     */
    public static function getAllBlogPosts()
    {
        return static::with(self::relationship())
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
    }

    /**
     * return a single post using slug
     *
     * @param string $slug
     * @return object
     */
    public static function getSinglePost($slug)
    {
        return static::with(self::relationship())
            ->whereSlug($slug)
            ->get()
            ->first();
    }

    /**
     * return search results
     *
     * @param  string $term
     * @return object
     */
    public static function search($term)
    {

        return static::with(self::relationship())
            ->where('title', 'LIKE', '%' . $term . '%')
            ->orWhere('content', 'LIKE', '%' . $term . '%')
            ->paginate(10);
    }

    /**
     * Return a single post to be edited
     * using ID
     *
     * @return object
     */
    public static function getEditablePost($id)
    {
        return static::with(self::relationship())
            ->where('id', $id)
            ->get()
            ->first();
    }

    /**
     * Check if the given slug already in used
     *
     * @param  array $data
     * @return boolean
     */
    public static function checkSlug($slug)
    {
        return BlogPost::whereSlug($slug)->exists();
    }

    public function generateSlug($exclude = false)
    {
        $slug = str_slug(!empty($this->slug) ? $this->slug : $this->title);

        // check to see if any other slugs exist that are the same & count them
        $builder = static::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'");

        if ($exclude) {
            $builder->where('id', '<>', $this->id);
        }

        $count = $builder->count();

        // if other slugs exist that are the same, append the count to the slug
        return $count ? "{$slug}-{$count}" : $slug;
    }

    /**
     * For every post can have many tag, define in table
     * blog_groups
     *
     * @return object
     */
    public function groups()
    {
        return $this->hasMany('App\BlogTagsGroups', 'blog_post_id');
    }

    /**
     * Every post has an author
     *
     * @return object
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Every post is part of a category
     * @return [type] [description]
     */
    public function category()
    {
        return $this->hasMany('App\BlogCategoryGroup', 'blog_post_id');
    }

    public function tags()
    {
        return $this->hasMany('App\BlogTagsGroups', 'blog_post_id');
    }
}
