<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Customer;
use App\Shop\Discounts\GiftCard as GiftCardDiscount;

class GiftCard extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $gift_card;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(GiftCardDiscount $gift_card)
    {
        $this->tries = 3;
        
        $this->gift_card = $gift_card;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.giftcard')->subject('Your Simply Earth gift card is ready!');
    }
}
