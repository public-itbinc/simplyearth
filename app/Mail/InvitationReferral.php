<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Customer;

class InvitationReferral extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $referral_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($referral_data)
    {
        $this->tries = 3;
        
        $this->referral_data = $referral_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        if (isset($this->referral_data['customer_email'])) {
            
            return $this->from($this->referral_data['customer_email'])->markdown('emails.referral_invite')->subject('Your Simply Earth Referral Invitation!');
            
        } else {
            return $this->markdown('emails.referral_invite')->subject('Your Simply Earth Referral Invitation!');
        }
        
    }
}
