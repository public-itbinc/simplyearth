<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Customer;

class SubscribeFirstMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subscriber_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscriber_data)
    {
        $this->tries = 3;
        $this->subscriber_data = $subscriber_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.subscriptions.subscribe_first_mail')->subject('🎉[New Customer Exclusive] Free Essential Oil Recipe Box at Your Fingertips');
    }
}
