<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSubscriptionGifted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subscription;
    public $gift;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscription, $gift)
    {
        $this->tries = 3;
        $this->subscription = $subscription;
        $this->gift = $gift;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.subscriptions.gifted')->subject(
            $this->subscription->owner->customer->first_name.", You just gifted ".$this->gift->first_name." your monthly box! 💘🎁"
        );
    }
}
