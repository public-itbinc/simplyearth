<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Shop\Orders\Order;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->tries = 3;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->order->isSubscriptionPurchase()) {
            return $this
            ->subject('Your Natural Home Is On Its Way 😍')
            ->markdown('emails.orders.shipped_subscription');
        } else {
            return $this
            ->subject('Your Natural Home Is On Its Way 😍')
            ->markdown('emails.orders.shipped');
        }
    }
}
