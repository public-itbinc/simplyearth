<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Activation extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    public $activation;

    public $options;

    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $activation, $options = [])
    {
        $this->customer = $customer;
        $this->activation = $activation;
        $this->options = $options;
        $this->message = isset($options['message']) ? $options['message'] : sprintf("Hi %s, you've created a new customer account at %s. All you have to do is activate
it and choose a password.", $customer->first_name, config('app.name'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.customers.activation')
            ->subject(isset($this->options['subject']) ? $this->options['subject'] : "Activate Your Account");
    }
}
