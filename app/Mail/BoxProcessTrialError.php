<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BoxProcessTrialError extends Mailable
{
    use Queueable, SerializesModels;

    public $error_message;
    public $schedule;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($schedule, $message)
    {
        $this->error_message = $message;
        $this->schedule = $schedule;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.box_trial_error')->subject('Failed Box Trial for schedule '.$this->schedule);
    }
}
