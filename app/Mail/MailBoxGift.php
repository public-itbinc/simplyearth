<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Subscriptions\SubscriptionGift;

class MailBoxGift extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $gift;
    public $subscription;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SubscriptionGift $gift)
    {
        $this->tries = 3;
        $this->gift = $gift;
        $this->subscription = $this->gift->subscription;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.gift')->subject($this->subscription->owner->customer->first_name." Gifted You An Essential Oil Recipe Box 💌");
    }
}
