<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\WholesalerRegistration;

class WholesalerVerify extends Mailable
{
    use Queueable, SerializesModels;

    public $registration;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(WholesalerRegistration $registration)
    {
        $this->registration = $registration;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.customers.wholesaler-verify')
        ->subject('Your Wholesale Account is Pre-Aproved at Simply Earth!')
        ->from('wholesale@simplyearth.com', 'Simply Earth Wholesale');
    }
}
