<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSubscriptionSkipped extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subscription;

    public $old_box;

    public $new_box;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscription, $old_box, $new_box)
    {
        $this->tries = 3;
        $this->subscription = $subscription;
        $this->old_box = $old_box;
        $this->new_box = $new_box;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.subscriptions.skipped')->subject('You Have Successfully Skipped Your Next Shipment 📦');
    }
}
