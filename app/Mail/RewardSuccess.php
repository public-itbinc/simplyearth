<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Customer;

class RewardSuccess extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $reward_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reward_data)
    {
        $this->tries = 3;
        $this->reward_data = $reward_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.reward_successful')->subject('Simply Earth - Reward Successful');
    }
}
