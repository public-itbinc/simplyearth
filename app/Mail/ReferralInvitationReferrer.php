<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Customer;

class ReferralInvitationReferrer extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->tries = 3;
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.referrals.referrer')
        ->from('hello@simplyearth.com', trim(ucwords($this->customer->first_name . ' ' . $this->customer->last_name)))
        ->subject('Congratulations!  '.ucwords($this->customer->first_name).' has invited you to Simply Earth');
    }
}
