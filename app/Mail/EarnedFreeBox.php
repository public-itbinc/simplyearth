<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Customer;

class EarnedFreeBox extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $earn_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($earn_data)
    {
        $this->tries = 3;
        $this->earn_data = $earn_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.referral_invite')->subject('Simply Earth - Earned Free Box Email!');
    }
}
