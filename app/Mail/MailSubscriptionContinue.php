<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class MailSubscriptionContinue extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subscription;

    public $stopped_date;

    public $continue_date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscription, $stopped_date, $continue_date)
    {
        $this->tries = 3;
        $this->subscription = $subscription;
        $this->stopped_date = $stopped_date;
        $this->continue_date = $continue_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.subscriptions.continue')->subject('You have successfully stopped your monthly box 🚫');
    }
}
