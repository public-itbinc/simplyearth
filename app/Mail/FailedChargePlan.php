<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Shop\Customers\Account;

class FailedChargePlan extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $day;

    public $account;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Account $account, $day = 1)
    {
        $this->tries = 3;
        $this->account = $account;
        $this->day = min(5, $day);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.failed_charge_plan_'.$this->day);
    }
}
