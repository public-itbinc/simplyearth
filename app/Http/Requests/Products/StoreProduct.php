<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Shop\Products\Product;

class StoreProduct extends FormRequest {

    protected $rules = [
        'name' => 'required',
        'price' => 'numeric',
        'sku' => 'required|unique:products,sku',
    ];
    protected $messages = [
        'name.required' => 'Product name is required',
        'sku.required' => 'SKU is required',
        'sku.unique' => 'SKU already exists',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $this->rules['status'] = Rule::in(array_keys(Product::statuses()));

        return $this->rules;
    }

    public function messages() {
        return $this->messages;
    }

}
