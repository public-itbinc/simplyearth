<?php

namespace App\Http\Requests\Products;

use App\Http\Requests\Products\StoreProduct;
use Illuminate\Validation\Rule;
use App\Shop\Products\Product;

class UpdateProduct extends StoreProduct
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $this->rules['sku'] = 'required|unique:products,sku,' . $this->product->id;   
        $this->rules['status'] = Rule::in(array_keys(Product::statuses()));
        
        return $this->rules;
    }

}
