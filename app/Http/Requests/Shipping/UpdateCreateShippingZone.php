<?php

namespace App\Http\Requests\Shipping;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\RestOfTheWorld;
use Illuminate\Support\Facades\Validator;

class UpdateCreateShippingZone extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'countries' => ['required', new RestOfTheWorld($this->shipping_zone ? $this->shipping_zone->id : null)],
        ];
    }

    public function validate()
    {
        parent::validate();

        if ($this->has('shipping_rate_prices')) {
            foreach ($this->shipping_rate_prices as $rate_price) {

                $rules = [
                    'name' => 'required',
                    'min' => 'required|numeric',
                ];

                if (isset($rate_price['max'])) {
                    $rules['max'] = 'numeric|min:' . $rate_price['min'];
                }

                $validator = Validator::make($rate_price, $rules, [
                    'min.max' => 'Min price should be lesser or equal to Max price'
                ]);

                $validator->sometimes('rate', 'required|numeric', function ($input) {
                    return $input->is_free != 1;
                });

                $validator->validate();
            }
        }

        if ($this->has('shipping_rate_weights')) {
            foreach ($this->shipping_rate_weights as $rate_price) {

                $rules = [
                    'name' => 'required',
                    'min' => 'required|numeric',
                ];

                if (isset($rate_price['max'])) {
                    $rules['max'] = 'numeric|min:' . $rate_price['min'];
                }

                $validator = Validator::make($rate_price, $rules, [
                    'min.max' => 'Min price should be lesser or equal to Max price'
                ]);

                $validator->sometimes('rate', 'required|numeric', function ($input) {
                    return $input->is_free != 1;
                });

                $validator->validate();
            }
        }
    }
}
