<?php

namespace App\Http\Requests;

use App\Rules\CouponCode;
use Illuminate\Foundation\Http\FormRequest;

class PromoCodeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'code' => [
                'required',
                'alpha_dash',
                'exists:discounts,code',
                new CouponCode,
            ]
        ];
    }

    public function messages()
    {
        return [
            'code.required' => __('The coupon code field is required.'),
            'code.exists' => __("This coupon code doesn't exist."),
            'code.alpha_dash' => __("Please take out any spaces in your coupon code."),
        ];
    }
}
