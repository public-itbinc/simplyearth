<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DisableCheckoutAsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('app.env') == 'production' && Auth::guard('web')->check()) {
            flash('Sorry, admins are not allowed to checkout', 'danger');
            return redirect(route('products'));
        }

        return $next($request);
    }
}
