<?php

namespace App\Http\Middleware;

use App\Shop\Discounts\Discount;
use Closure;
use Illuminate\Support\Carbon;

class CheckDiscount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->isMethod('get') && $request->has('coupon')) {

            $discount = Discount::where('code', $request->coupon)->first();

            if ($discount && !$discount->expired()) {

                $discount_data = ['discount' => $discount];

                if ($request->get('t') == 'true') {
                    $discount_data = array_merge($discount_data, [
                        'timer' => $discount->end_date->diff(Carbon::now()),
                        'offer' => $request->get('offer'),
                        'name' => $request->has('name') ? strtoupper($request->get('name')) . "'S GIFT TO YOU" : '',
                        'text' => $request->get('text'),
                    ]);
                }

                view()->share('discount_data', $discount_data);
                app()->discount_data = $discount_data;
            }
        }

        return $next($request);
    }
}
