<?php

namespace App\Http\Middleware;

use Closure;

class WholesalerOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $customer = customer();

        if (!$customer) {
            return redirect(route('customer.login'));
        } elseif (!$customer->isWholesaler()) {
            flash('Sorry, wholesalers only', 'warning');
            return redirect(route('profile'));
        }

        return $next($request);
    }
}
