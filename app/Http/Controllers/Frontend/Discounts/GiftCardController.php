<?php

namespace App\Http\Controllers\Frontend\Discounts;

use App\Http\Controllers\Controller;
use App\Shop\Discounts\GiftCard;
use Illuminate\Http\Request;

class GiftCardController extends Controller
{
    public function show(GiftCard $gift_card, Request $token)
    {
        return view('frontend.discounts.gift_card', ['gift_card' => $gift_card->load('orderItem.product')]);
    }
}