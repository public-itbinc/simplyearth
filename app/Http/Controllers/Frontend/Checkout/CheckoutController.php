<?php

namespace App\Http\Controllers\Frontend\Checkout;

use App\Http\Controllers\Controller;
use App\Shop\Checkout\ConversionTracker;
use App\Shop\Orders\Order;
use Braintree\ClientToken as BraintreeClientToken;
use Facades\App\Shop\Cart\Cart;
use Facades\App\Shop\Checkout\Checkout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Shop\Discounts\Discount;
use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\Mail;
use App\Shop\Conversion\Klaviyo;
use Illuminate\Support\Facades\DB;
use App\Jobs\ProcessOrder;
use App\Shop\Checkout\GATracker;
use App\Shop\Cart\SessionCart;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('checkout_admin');
    }

    public function index(Request $request)
    {
        $data = [
            'is_monthly2019' => Cart::hasCommitmentSubscription() || Cart::hasNewSubscriptionProduct(),
            'settings' => [
                'client_token' => BraintreeClientToken::generate(),
                'env' => config('services.braintree.environment')
            ],
        ];

        if ($customer = customer()) {
            $data['customer'] = $customer->append('payment_method');
        } else {
            $data['customer'] = [];
        }

        if ($request->session()->exists('add_to_cart')) {
            $data['add_to_cart'] = $request->session()->get('add_to_cart');
        }

        //Maybe add a Bonus box

        if (!Cart::getShipping()) {
            Cart::autoSetShipping();
        }

        return view('frontend.checkout.index', $data);
    }

    public function settings()
    {
        return [
            'countries' => countries_list(),
            'customer' => customer(),
        ];
    }

    public function store(Request $request)
    {

        $this->commonCheck($request);

        if (Cart::requiresShipping() && !Cart::isFreeShipping()) {
            $this->validate($request, [

                'checkout.shipping_method_key' => 'required',

            ], [
                'checkout.shipping_method_key.required' => 'Shipping method is required',
            ]);

            if (Cart::availableShippingMethods()->filter(function ($method) use ($request) {
                return $method->shipping_method_key == $request->checkout['shipping_method_key'];
            })->count() == 0) {
                SEThrowvalidationException('shipping_method', 'Invalid shipping method');
            }

            Cart::setShippingMethod(Checkout::parseShippingMethod($request->checkout['shipping_method_key']));
        }

        if (is_wholesaler() && !Cart::metWholesaleMinimum() && !Cart::hasPlanProduct()) {
            SEThrowvalidationException('checkout', wholesaleErrorMessage());
        }

        $request_data = $request->all();

        //Separate full name to first name and last names
        $shipping_fullname = explode(' ', $request->checkout['shipping_address']['first_name']);

        $request_data['checkout']['shipping_address']['first_name'] = $shipping_fullname[0];
        $request_data['checkout']['shipping_address']['last_name'] = $shipping_fullname[1] ?? '';

        if ($request->checkout['same_as_shipping_address'] !== true) {
            $billing_fullname = explode(' ', $request->checkout['billing_address']['first_name']);
            $request_data['checkout']['billing_address']['first_name'] = $billing_fullname[0];
            $request_data['checkout']['billing_address']['last_name'] = $billing_fullname[1] ?? '';
        }

        $customer = Checkout::setData($request_data)->getOrCreateCustomer();

        if (!customer() || !$customer->account->hasBraintreeId()) {
            $this->validate($request, [
                'nonce' => 'required',
            ], [
                'Missing payment session',
            ]);
        }

        Cart::setCustomer($customer);

        //Check if the customer has an unpaid plan
        if ($customer->hasEitherTags(['installment-failed-charge', 'installment-incomplete'])) {
            SEThrowvalidationException('has_failed_charge_plan', 'Has failed charge plan');
        }

        if (Cart::hasSubscriptionProduct() && $customer->hasSubscription()) {
            SEThrowvalidationException('subscription', 'Subscription already exists');
        }
        //Check if the order has a subscription product and create a new account if customer doesnt have an account
        if (Cart::hasSubscriptionProduct() && !$customer->account) {
            $customer->sendAccountManagement();
        }

        $common = [
            'ip_address' => $request->ip(),
            'buyer_accepts_marketing' => $request->checkout['buyer_accepts_marketing'] ?? true,
            'status' => Order::ORDER_PROCESSING,
        ];


        $cart_total = Cart::getTotal();
        $order_name = $customer->id.time();

        //If the total is zero we will process the order right away
        if ($cart_total <= 0) {

            ProcessOrder::dispatch(array_merge($common, [
                'payment_details' => [],
                'order_name' => $order_name
            ]), $customer, Cart::getDetail());

            return $this->checkoutSuccess($customer, $order_name);
        }

        //Charge 
        try {
            $result = $customer->charge($cart_total, $request->nonce);
        } catch (\Exception $e) {
            Log::error($customer->email . ':' . $e->getMessage() . ':' . Cart::getTotal());
            SEThrowvalidationException('server', $e->getMessage());
        }

        //IF Successful charge
        if ($result->success) {

            ProcessOrder::dispatch(array_merge($common, [
                'payment_details' => $result->transaction,
                'order_name' => $order_name
            ]), $customer, Cart::getDetail());

            return $this->checkoutSuccess($customer, $order_name);
        }

        SEThrowvalidationException('server', 'Checkout error');
    }

    public function check(Request $request)
    {
        $errors = collect([]);

        $validator = Validator::make($request->all(), ['checkout.email' => 'required|email']);

        if ($validator->fails()) {
            $errors->put('email', $validator->errors());
        }

        $validator->setRules([
            'checkout.shipping_address.first_name' => 'required',
            'checkout.shipping_address.address1' => 'required',
            'checkout.shipping_address.city' => 'required',
            'checkout.shipping_address.zip' => 'required',
            'checkout.shipping_address.country' => 'required',
        ]);

        if ($validator->fails()) {
            $errors->put('shipping_address', $validator->errors());
        }

        if ($request->checkout['same_as_shipping_address'] !== true) {
            $validator->setRules([
                'checkout.billing_address.first_name' => 'required',
                'checkout.billing_address.address1' => 'required',
                'checkout.billing_address.city' => 'required',
                'checkout.billing_address.zip' => 'required',
                'checkout.billing_address.country' => 'required',
            ]);

            if ($validator->fails()) {
                $errors->put('billing_address', $validator->errors());
            }
        }

        if ($request->has('checkout.shipping_address')) {
            Cart::setShippingLocation($request->checkout['shipping_address']);
        }

        if ($errors->count()) {
            return response()->json(['data' => ['errors' => $errors]], 422);
        }

        return response('');
    }

    public function thankyou()
    {
        if (!session()->exists('purchased_data') || !($purchased_data = session()->get('purchased_data'))) {
            return redirect(route('products'));
        }

        session()->forget('purchased_data');

        $conversion = new GATracker($purchased_data);

        if (isset($purchased_data['subscription'])) {
            return view('frontend.checkout.thankyou-subscribers', ['conversion' => $conversion, 'customer' => $purchased_data['customer'], 'purchased_data' => $purchased_data]);
        }

        return view('frontend.checkout.thankyou', ['conversion' => $conversion, 'customer' => $purchased_data['customer'], 'purchased_data' => $purchased_data]);
    }

    private function commonCheck(Request $request)
    {
        $this->validate($request, [
            'checkout.email' => 'required|email',
        ], [
            'checkout.email.required' => 'Email address is required',
            'checkout.email.email' => 'Invalid email address',
        ]);

        $shipping_fullname = explode(' ', $request->checkout['shipping_address']['first_name']);

        if (empty($shipping_fullname[1])) {
            SEThrowvalidationException('checkout.shipping_address.first_name', 'Your Last name is Required for your shipping address.');
        }

        $this->validate($request, [

            'checkout.shipping_address.first_name' => 'required',
            'checkout.shipping_address.address1' => 'required',
            'checkout.shipping_address.city' => 'required',
            'checkout.shipping_address.zip' => 'required',
            'checkout.shipping_address.country' => 'required',

        ], [
            'checkout.shipping_address.first_name.required' => 'Shipping full name is required',
            'checkout.shipping_address.address1.required' => 'Shipping address1 is required',
            'checkout.shipping_address.city.required' => 'Shipping city is required',
            'checkout.shipping_address.zip.required' => 'Shipping zip is required',
            'checkout.shipping_address.country.required' => 'Shipping country is required',
        ]);

        if ($request->checkout['same_as_shipping_address'] !== true) {
            $this->validate($request, [

                'checkout.billing_address.first_name' => 'required',
                'checkout.billing_address.address1' => 'required',
                'checkout.billing_address.city' => 'required',
                'checkout.billing_address.zip' => 'required',
                'checkout.billing_address.country' => 'required',

            ], [
                'checkout.billing_address.last_name.required' => 'Billing full name is required',
                'checkout.billing_address.address1.required' => 'Billing address1 is required',
                'checkout.billing_address.city.required' => 'Billing city is required',
                'checkout.billing_address.zip.required' => 'Billing zip is required',
                'checkout.billing_address.country.required' => 'Billing country is required',
            ]);

            $billing_fullname = explode(' ', $request->checkout['billing_address']['first_name']);

            if (empty($billing_fullname[1])) {
                SEThrowvalidationException('checkout.billing_address.first_name', 'Your Last name is Required for your billing address. ');
            }
        }
    }

    private function checkoutSuccess($customer, $order_name)
    {
        session()->put('purchased_data', [
            'transaction_id' => $order_name,
            'total' => Cart::getTotal(),
            'total_tax' => Cart::getTaxTotal(),
            'total_shipping' => Cart::getTotalShipping(),
            'total_discounts' => Cart::getOverallDiscounts(),
            'discount_code' => Cart::getDiscount() ? Cart::getDiscount()->code : null,
            'products' => Cart::getProducts(),
            'subscription' => Cart::getSubscriptionProduct(),
            'customer' => $customer,
        ]);

        Cart::clear();

        return ['redirect' => route('checkout.thankyou')];
    }
}
