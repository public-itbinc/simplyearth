<?php

namespace App\Http\Controllers\Frontend\Subscriptions;

use App\Events\CommitmentStopAutoRenew;
use App\Events\SubscriptionCancelled;
use App\Events\SubscriptionContinue;
use App\Events\SubscriptionRestarted;
use App\Events\SubscriptionSkipped;
use App\Events\SubscriptionUnskipped;
use App\Http\Controllers\Controller;
use App\Http\Resources\BoxDetail;
use App\Rules\CouponCode;
use App\Rules\GiftCardRule;
use App\Shop\Conversion\Klaviyo;
use App\Shop\Customers\Customer;
use App\Shop\Discounts\Discount;
use App\Shop\Discounts\GiftCard;
use App\Shop\Products\Product;
use App\Shop\ShoppingBoxes\FutureBoxDetail;
use App\Shop\ShoppingBoxes\ShoppingBox;
use App\Shop\Subscriptions\Subscription;
use Facades\App\Shop\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class SubscriptionController extends Controller
{

    public function show(Request $request)
    {
        //Redirect if the user is has an active or paused subscription;
        if (customer() && (customer()->account->subscribed() || customer()->account->paused())) {
            return redirect('/profile');
        }

        $this_month = Carbon::parse('first day of this month');
        $next_month = $this_month->copy()->addMonth(1);

        $last_six_months = [];

        //get past last 6 months
        for ($i = 1; $i <= 6; $i++) {
            $last_six_months[] = $this_month->copy()->subMonth($i);
        }

        $monthly_subscription = Product::subscriptions()->where('sku', config('subscription.monthly'))->first();

        if (!$monthly_subscription) {
            flash('Sorry, subscription is not available', 'danger');
            return redirect('/');
        }

        $shopping_boxes = ShoppingBox::whereIn(
            'key',
            array_merge(
                [$this_month->format('F-Y'), $next_month->format('F-Y'), $next_month->copy()->addMonth(1)->format('F-Y')],
                collect($last_six_months)->map(function ($month) {
                    return $month->format('F-Y');
                })->toArray()
            )
        )->with(['products', 'products.metas'])->get();

        //This Month
        $this_month_box = $shopping_boxes->filter(function ($month) use ($this_month) {
            return $month->key == strtolower($this_month->format('F-Y'));
        })->first();

        //Last_month

        $last_month = $this_month->copy()->subMonth(1);
        $last_month_box = $shopping_boxes->filter(function ($month) use ($last_month) {
            return $month->key == strtolower($last_month->format('F-Y'));
        })->first();

        //Last 2 months        
        $last_last_month_box = $shopping_boxes->filter(function ($month) use ($last_month) {
            return $month->key == strtolower($last_month->copy()->subMonth(1)->format('F-Y'));
        })->first();

        $next_month_box = $shopping_boxes->filter(function ($month) use ($next_month) {
            return $month->key == strtolower($next_month->format('F-Y'));
        })->first();

        $next_next_month_box = $shopping_boxes->filter(function ($month) use ($next_month) {
            return $month->key == strtolower($next_month->copy()->addMonth(1)->format('F-Y'));
        })->first();

        $last_six_months_keys = collect($last_six_months)->map(function ($month) {
            return strtolower($month->format('F-Y'));
        })->toArray();

        $last_six_months_boxes = $shopping_boxes->filter(function ($month) use ($last_six_months_keys) {
            return in_array($month->key, $last_six_months_keys);
        });

        $this_month_available = $this_month_box->available();
        $current_browser = $this->getBrowser();

        $template = 'frontend.subscriptions.old';

        return view($template, [
            'monthly_price' => $monthly_subscription->price,
            'this_month' => $this_month_available ? $this_month : $next_month,
            'last_month' => $this_month_available ? $last_month : $this_month,
            'next_month' => $this_month_available ? $next_month : $next_month->copy()->addMonth(1),
            'this_month_box' => $this_month_available ? $this_month_box : $next_month_box,
            'last_month_box' => $this_month_available ? $last_month_box : $this_month_box,
            'last_last_month_box' => $this_month_available ? $last_last_month_box : $last_month_box,
            'next_month_box' => $this_month_available ? $next_month_box : $next_next_month_box,
            'recipes' => $last_six_months_boxes->filter(function ($month_box) {
                return !empty($month_box->recipes->count() > 0);
            })->map(function ($month_box) {
                return $month_box->recipes->first();
            }),
            //'monthly_subscription_commitment' => Product::subscriptions()->where('sku', 'REC-MONTHLY-3MONTHS')->first(),
            'monthly_subscription' => $monthly_subscription,
            'this_month_available' => $this_month_available,
            'current_browser' => $current_browser,
            'is_free_box_option' => app()->bound('discount_data') && app()->discount_data['discount']->isFreeFirstMonth(),
            'meta_title' => 'The Best Essential Oil Recipe Box',            
        ]);
    }

    public function box($monthKey)
    {
        if ($monthKey == 'nextbox') {
            $box = customer()->account->nextBox();
        } else {
            $box = customer()->account->futureOrders()->getMonth($monthKey);
        }

        return new BoxDetail($box);
    }

    public function boxFuture($monthKey)
    {
        if ($monthKey == 'nextbox') {
            $box = customer()->account->nextBox();
        } else {
            $box = customer()->account->futureOrders()->getMonth($monthKey);
        }

        return (new FutureBoxDetail($box))->toJson();
    }

    public function subscribe(Request $request)
    {
        $this->validate(
            $request,
            ['plan' => 'required|exists:products,sku']
        );

        if (customer() && customer()->account->subscribed()) {
            flash("You already have an active subscription", 'warning');
            return redirect(route('profile'));
        }

        $product = Product::subscriptions()->where('sku', 'REC-MONTHLY')->firstOrFail();

        Cart::add($product);

        if ($request->exists('code')) {
            Cart::applyDiscount($request->code)->autoSetShipping();
        }

        $request->session()->flash('add_to_cart', $product);

        return redirect(route('checkout'));
    }

    public function skipMonth($monthKey)
    {

        //Check if the subscription is a commitment and there are more than 1 paused within the commitment bracket
        if (customer()->account->subscription->isCommitment() && $box = customer()->account->futureOrders()->getMonth($monthKey)) {

            if (customer()->account->futureOrders()->getMonths()->filter(function ($item) use ($box) {
                return $item->getCommitment() && $item->getCommitment()->id == $box->getCommitment()->id && $item->skipped();
            })->count() > 1) {
                SEThrowvalidationException('skipbox', 'Cant skip more than 2 months');
            }
        }

        customer()->account->subscription->skipMonth($monthKey);

        customer()->account->futureOrders()->refresh();

        history('box_skipped', customer()->id, [
            'month_key' => $monthKey,
        ]);

        event(new SubscriptionSkipped(customer()->account->subscription, $monthKey, customer()->account->nextBox()));

        return response('', 200);
    }

    public function giftMonth($monthKey, Request $request)
    {
        if ($request->step == 1) {
            $this->validateRecipientInfo($request);
            return response('');
        } elseif ($request->step == 2) {
            $this->validateDelivery($request);
            return response('');
        } elseif ($request->step == 3) {
            $this->validateTiming($request);
            customer()->account->subscription->giftMonth($monthKey, array_merge(
                $request->all(),
                [
                    'email_date' => Carbon::parse($request->email_date),
                    'gift_date' => Carbon::parse($request->gift_date),
                ]
            ));
            return response('');
        }
        return redirect('profile');
    }

    public function giftDelete($monthKey, Request $request)
    {
        customer()->account->subscription->SubscriptionGifts()->where('schedule', $monthKey)->delete();

        history('box_deleted_gift', customer()->id, [
            'month_key' => $monthKey,
        ]);
    }

    public function resumeMonth($monthKey)
    {
        customer()->account->subscription->resumeMonth($monthKey);

        customer()->account->futureOrders()->refresh();

        history('box_resumed', customer()->id, [
            'month_key' => $monthKey,
        ]);

        event(new SubscriptionUnskipped(customer()->account->subscription));

        return response('', 200);
    }

    public function nextboxContinue(Request $request)
    {
        if (!Carbon::parse($request->key)->isFuture()) {
            SEThrowvalidationException('month_key', 'Month key is not a future date');
        }

        $subscription = customer()->account->subscription;

        $key = $request->key;

        if ($subscription->isCommitment()) {

            if ($box = $subscription->commitment_allowed_skips->filter(function ($item) use ($key) {
                return $item->month_key == $key;
            })->first()) {

                $subscription->commitmentPause($box->month_key);

                return response('After your current 6-month committment, your box will resume on ' . $box->getDate()->format('F dS'), 200);
            }

            SEThrowvalidationException('month_key', 'Invalid pause date');
        }

        list($stopped, $continue) = $subscription->nextboxContinue($request->key);

        event(new SubscriptionContinue($subscription, $stopped->getDate(), $continue->getDate()));

        return response("Your Box Has Been Stopped Till " . $continue->getDate()->format('F dS'), 200);
    }

    public function stop(Request $request)
    {
        $subscription = customer()->account->subscription;

        if ($subscription->isCommitment()) {

            $subscription->commitmentStop();

            event(new CommitmentStopAutoRenew($subscription));

            return response("You have successfully stopped your auto-renew. Your last box will ship on " . $subscription->commitment_last_box->getDate()->format('F dS') . ". If you would like to resume it at any time, please let us know!", 200);
        }

        $subscription = customer()->account->refresh()->subscription->stop($request->reason);

        event(new SubscriptionCancelled($subscription));

        return response("You Have Cancelled Your Subscription", 200);
    }

    public function unPause()
    {
        $subscription = customer()->account->subscription;

        if (!$subscription->isCommitment()) {

            SEThrowvalidationException('subscription', 'Invalid subscription Type');
        }

        $subscription->commitmentUnpause();

        return redirect(route('future-orders'));
    }

    public function resume()
    {
        $subscription = customer()->account->subscription->resume();

        event(new SubscriptionRestarted($subscription));

        return response('', 200);
    }

    public function updateSchedule(Request $request)
    {
        customer()->account->subscription->setSchedule($request->schedule)->save();

        history('subscription_updated_schedule', customer()->id, [
            'schedule' => customer()->account->subscription->schedule,
        ]);

        return response('', 200);
    }

    public function updatePlan(Request $request)
    {
        $subscription = Product::subscriptions()->where('sku', $request->plan)->firstOrFail();

        customer()->account->subscription->setPlan($subscription->sku)->save();

        history('subscription_updated_plan', customer()->id, [
            'plan' => $subscription->sku,
        ]);

        return response('', 200);
    }

    private function validateRecipientInfo($request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
        ]);
    }

    private function validateDelivery($request)
    {
        $this->validate($request, [
            'address_country' => 'required',
            'address_last_name' => 'required',
            'address_address1' => 'required',
            'address_city' => 'required',
            'address_zip' => 'required',
        ]);
    }

    private function validateTiming($request)
    {
        $this->validate($request, [
            'gift_date' => 'required',
        ]);
    }

    public function emailSubscribe(Request $request, Klaviyo $klaviyo)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $klaviyo->subscribe(config('conversion.klaviyo.list'), $request->email);

        return response('', 200);
    }

    public function applyGiftCard(Request $request)
    {
        $this->validate($request, [
            'code' => ['required', new GiftCardRule],
        ]);

        $gift_card = GiftCard::where('code', $request->code)->first();

        $box = customer()->account->futureOrders()->getMonth($request->month_key);

        if (!$box) {
            SEThrowvalidationException('box', 'Box schedule does not exists');
        }

        $box->applyGiftCard($gift_card);

        history('box_applied_giftcard', customer()->id, [
            'month_key' => $request->month_key,
            'code' => $request->code,
        ]);

        return response('', 200);
    }

    public function applyDiscount(Request $request)
    {
        $this->validate($request, [
            'code' => ['required', new CouponCode],
        ]);

        customer()->account->nextBox()->applyDiscount($request->code);

        history('box_applied_discount', customer()->id, [
            'code' => $request->code,
        ]);

        return response('', 200);
    }

    public function removeGiftCard($subscription_gift_card)
    {
        $subscription_gift_card = customer()->account->subscription->subscriptionGiftcards()->where('id', $subscription_gift_card)->firstOrFail();

        history('box_removed_giftcard', customer()->id, [
            'code' => $subscription_gift_card->code,
        ]);

        $subscription_gift_card->restore();

        return response('', 200);
    }

    public function removeDiscount()
    {
        customer()->account->nextBox()->deleteDiscount();

        history('box_removed_discount', customer()->id);

        return response('', 200);
    }

    public function getBrowser()
    {
        $arr_browsers = [
            "Firefox", "Chrome", "Safari", "Opera",
            "MSIE", "Trident", "Edge"
        ];

        $agent = @$_SERVER['HTTP_USER_AGENT'];

        $user_browser = '';
        foreach ($arr_browsers as $browser) {
            if (strpos($agent, $browser) !== false) {
                $user_browser = $browser;
                break;
            }
        }

        switch ($user_browser) {
            case 'MSIE':
                $user_browser = 'Internet Explorer';
                break;

            case 'Trident':
                $user_browser = 'Internet Explorer';
                break;

            case 'Edge':
                $user_browser = 'Internet Explorer';
                break;
        }

        return $user_browser;
    }

    public function plans(Request $request)
    {
        $is_free_box_option = false;

        if ($request->has('code') && $discount = Discount::where('code', $request->code)->first()) {

            if (!$discount->expired() && $discount->isFreeFirstMonth()) {

                $is_free_box_option = true;
            }
        }

        $this_month = Carbon::parse('first day of this month');
        $next_month = $this_month->copy()->addMonth(1);

        $this_month_box = ShoppingBox::getByKey($this_month->format('F-Y'));

        return view('frontend.subscriptions.plans', [
            'this_month' => $this_month_box->available() ? $this_month : $next_month,
            'is_free_box_option' => $is_free_box_option,
            'monthly_subscription_commitment' => Product::subscriptions()->where('sku', 'REC-MONTHLY-3MONTHS')->first(),
            'monthly_subscription' => Product::subscriptions()->where('sku', 'REC-MONTHLY2019')->first()
            ]);
    }
}
