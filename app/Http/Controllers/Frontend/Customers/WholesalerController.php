<?php

namespace App\Http\Controllers\Frontend\Customers;

use App\Http\Controllers\Controller;
use App\Mail\WholesalerVerify;
use App\Shop\Customers\WholesalerRegistration;
use App\Shop\Products\Product;
use Facades\App\Shop\Cart\WholesaleCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Shop\Customers\WholesalerBuilder;
use Illuminate\Support\Facades\Auth;

class WholesalerController extends Controller
{
    public function __construct()
    {
        $this->middleware('wholesaler')->except(['typeform', 'verify']);
    }

    public function form()
    {
        return view('frontend.wholesaler.form', [
            'products' => Product::active()->limitByTags()->get(),
            'previous' => collect(customer()->wholesalers_cart->content ?? []),
        ]);
    }

    public function checkout(Request $request)
    {
        $this->validate($request, [
            'products' => 'required',
        ]);

        $products = collect(array_filter($request->products))->map(function ($quantity, $product_id) {
            return ['id' => $product_id, 'qty' => $quantity];
        });

        $wholesalers_cart = customer()->wholesalers_cart()->firstOrNew([]);
        $wholesalers_cart->content = $products;
        $wholesalers_cart->save();

        WholesaleCart::addBulkProducts($products->toArray());

        return redirect(route('checkout'));
    }

    public function typeform(Request $request)
    {
        if ($request->has('key') && $request->has('email') && env('TYPEFORM_KEY', '') == $request->key) {
            $registration = WholesalerRegistration::createAndSendVerification($request->all());

            Mail::to($registration->email)->send(new WholesalerVerify($registration));

            session()
            ->flash('wholesaler_registration', $registration);
        }

        return redirect(route('wholesaler.sales'));
    }

    public function verify($email, $token)
    {
        $registration = WholesalerRegistration::where('email', $email)->where('token', $token)->first();

        if (!$registration) {
            flash('Invalid Wholesaler Registration Token', 'error');

            return redirect(route('home'));
        }

        $customer = (new WholesalerBuilder)
            ->setEmail($registration->email)
            ->setData($registration->data)
            ->build();

        $registration->delete();

        //If the build returns a string - which means its a token, redirect the user to reset the password
        if (is_string($customer)) {
            return redirect(route('customer.password.reset', ['token' => $customer, 'email' => $email, 'wholesaler_redirect' => 1]));
        }

        Auth::guard('customer')->login($customer->account);

        return redirect(route('wholesaler.sales'));
    }
}
