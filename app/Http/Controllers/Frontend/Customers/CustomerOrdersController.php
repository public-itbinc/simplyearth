<?php

namespace App\Http\Controllers\Frontend\Customers;

use App\Http\Controllers\Controller;
use App\Shop\Orders\Order;
use Illuminate\Http\Request;
use App\Http\Resources\OrderDetail;
use Illuminate\Support\Carbon;

class CustomerOrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer')->except(['show']);
    }

    public function index()
    {
        return view('frontend.customers.orders', [
            'orders' => customer()->orders()->with(['order_items'])->orderBy('processed_at', 'DESC')->paginate(10),
        ]);
    }

    public function future()
    {
        if (!customer()->account->subscribed()) {
            return redirect('profile');
        }

        $box_groups = customer()->account->futureOrders()->getMonths()->groupBy(function($box, $key) {
            return $box->getCommitment() ? 'commitment-group-'.$box->getCommitment()->status.' commitment-group-'.$box->getCommitment()->id : null;
        });

        return view('frontend.customers.future_orders', [
            'box_groups' => $box_groups,
        ]);
    }

    public function order(Request $request, $order)
    {
        $order = customer()->orders()->findOrFail($order);
        
        return new OrderDetail($order->load(['order_items', 'shipping_address', 'installment_plans']));
    }

    public function show(Request $request)
    {

        $order = Order::where('token', $request->token)->first();

        if (!$order) {
            return redirect(route('products'));
        }

        return view('frontend.orders.show', ['order' => $order]);
    }
}
