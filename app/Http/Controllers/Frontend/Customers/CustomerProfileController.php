<?php

namespace App\Http\Controllers\Frontend\Customers;

use App\Http\Controllers\Controller;
use App\Http\Resources\AddressResource;
use App\Rules\CurrentPassword;
use App\Shop\Customers\CustomerRepository;
use App\Shop\Products\Product;
use App\Shop\Referrals\Reward;
use App\Shop\Rewards\ProcessReward;
use Braintree\ClientToken as BraintreeClientToken;
use Illuminate\Http\Request;

class CustomerProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer');
    }

    public function show()
    {
        return view('frontend.customers.profile', [
            'available_subscriptions' => Product::subscriptions()->get()->map(function ($item) {
                return (object) [
                    'sku' => $item->sku,
                    'name' => $item->name,
                    'price' => $item->priceStringSimple,
                    'priceString' => $item->priceString,
                    'months' => (int) ($item->subscription_months ?? 1),
                ];
            }),
            'account' => customer()->account,
            'client_token' => BraintreeClientToken::generate(),
            'env' => config('services.braintree.environment'),
            'default_address' => customer()->default_address ? (new AddressResource(customer()->default_address))->toArray([]) : collect([]),
            'products' => Product::active()->excludeSpecials()->whereIn('type', ['default'])->popular()->paginate(6),
            'recent_orders' => customer()->orders()->with(['order_items'])->orderBy('processed_at', 'DESC')->limit(3)->get(),
            'boxes' => customer()->account->getBoxes(),
        ]);
    }

    public function rewards()
    {
        return [
            'rewards' => customer()->unclaimed_rewards,
        ];
    }

    public function claimReward(Reward $reward, Request $request)
    {
        if (!customer()->owns($reward)) {
            SEThrowvalidationException('reward', 'Reward not found');
        }

        if (!is_null($reward->claimed_at)) {
            SEThrowvalidationException('reward', 'Reward already claimed');
        }

        ProcessReward::claim($reward, $request);

        return response('', 200);
    }

    public function claimFreebox(Request $request)
    {

        if (!customer()->account->subscribed()) {
            SEThrowvalidationException('subscription', 'You need to be subscribed to claim your freebox');
        }

        ProcessReward::claimFreebox(customer(), $request->month_key ?? customer()->account->nextBox()->monthKey);

        return response('', 200);
    }

    public function unclaimFreebox(Request $request)
    {

        if (!customer()->account->subscribed()) {
            SEThrowvalidationException('subscription', 'You need to be subscribed to use this feature');
        }

        ProcessReward::unclaimFreebox(customer(), $request->month_key ?? customer()->account->nextBox()->monthKey);

        return response('', 200);
    }

    public function updateAddress(Request $request, CustomerRepository $customerRepo)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'country' => 'required',
        ]);

        $customerRepo->updateOrCreateAddress(customer(), array_merge($request->all(), ['primary' => 1]));

        history('updated_shipping_address', customer()->id, [
            'shipping_address' => customer()->default_address->toArray(),
        ]);

        return response('', 200);
    }

    public function updateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:accounts,email,' . customer()->account->id,
        ]);

        customer()->account->update(['email' => $request->email]);
        customer()->update(['email' => $request->email]);

        history('updated_email', customer()->id, [
            'email' => $request->email,
        ]);

        return response('', 200);
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => ['required', new CurrentPassword()],
            'password' => 'required|confirmed',
        ]);

        customer()->account->forceFill(['password' => bcrypt($request->password)])->save();

        history('updated_password', customer()->id);

        return response('', 200);
    }

    public function updatePayment(Request $request)
    {
        $this->validate($request, [
            'nonce' => 'required',
        ]);

        customer()->account->getBraintreeCustomer($request->nonce);

        history('updated_payment_method', customer()->id, [
            'payment_method' => customer()->payment_method,
        ]);

        return ['payment_method' => customer()->payment_method];
    }
}
