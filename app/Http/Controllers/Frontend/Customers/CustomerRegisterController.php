<?php

namespace App\Http\Controllers\Frontend\Customers;

use App\Events\Registered as RegisteredCustomer;
use App\Http\Controllers\Controller;
use App\Mail\Activation as ActivationMail;
use App\Shop\Customers\Account;
use App\Shop\Customers\Activation;
use App\Shop\Customers\Customer;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Admin\Discounts\DiscountsController;
use App\Shop\Discounts\Discount;

class CustomerRegisterController extends Controller
{

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/products';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('frontend.customers.register');
    }

    public function activation(Request $request, $token)
    {
        if ($activation = DB::table('customer_activations')->where('token', $token)->first()) {

            $customer = Customer::where(['email' => $activation->email])->first();

            if (!$customer) {
                flash('Customer information not found. Please register instead')->error();
                return redirect(route('customer.register'));
            }

            $reset_token = $customer->accountActivate($token);

            return redirect(route('customer.password.reset', ['token' => $reset_token, 'email' => $customer->email]));
        }

        return redirect('login');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        if ($customer = Customer::where('email', $request->email)->first()) {

            $activation = $customer->activation()->updateOrCreate(['email' => $customer->email], ['token' => str_random(30), 'created_at' => Carbon::now()]);

            Mail::to($customer)->send(new ActivationMail($customer, $activation));
            $message = sprintf(
                'We have sent an email to %s, please click the link included to verify your email address.',
                $customer->email
            );

            if ($request->ajax()) {
                return response()->json(['message' => $message, 'found_customer' => true]);
            }

            flash($message)->error();
            return redirect(route('customer.login'));
        }

        event(new RegisteredCustomer($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($request->ajax()) {
            return response('');
        }

        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:accounts',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $account = Account::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $customer = $account->customer()->create([
            'email' => $data['email'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
        ]);

        return $account;
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }
}
