<?php

namespace App\Http\Controllers\Frontend\Pages;

use App\Http\Controllers\Controller;
use App\Shop\Products\Product;
use JavaScript;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.index', [
            'meta_description' => 'Simply Earth aims to provide natural products for a safe environment and at the same time to give back to those in need. We live to be world changers!',
        ]);
    }

    /**
     *  Ambassador Page
     */

    public function ambassador()
    {
        return view('frontend.pages.ambassador-club');
    }
    /**
     *  Our Cause Page
     */
    public function our_cause()
    {
        return view('frontend.pages.our-cause');
    }
    /**
     *  Careers Page
     */
    public function careers()
    {
        return view('frontend.pages.careers');
    }

    /**
     *  Wholesalers Page
     */
    public function wholesalers()
    {
        return view('frontend.pages.wholesalers');
    }

    /**
     *  Dillution Page
     */
    public function dillution_rates()
    {
        return view('frontend.pages.dillution-rates');
    }
    /**
     *  E-book Page
     */
    public function e_book()
    {
        return view('frontend.pages.ebook');
    }

    /**
     *  Comparison Chart Page
     */
    public function comparison_chart()
    {
        return view('frontend.pages.comparison-chart');
    }

    /**
     *  6 Steps to Pure Page
     */
    public function six_steps_to_pure()
    {
        return view('frontend.pages.six-steps-to-pure');
    }

    /**
     *  Faq Page
     */
    public function faqs()
    {
        return view('frontend.pages.faqs');
    }

    /**
     *  Privacy Policy Page
     */
    public function privacy_policy()
    {
        return view('frontend.pages.privacy-policy');
    }

    /**
     *  Terms of user Page
     */
    public function terms_of_use()
    {
        return view('frontend.pages.terms-of-use');
    }

    /**
     *  Faq Page
     */
    public function blog()
    {
        return view('frontend.pages.blog');
    }
    /**
     *  Aroma Therapist Page
     */
    public function our_aromatherapist()
    {
        return view('frontend.pages.our-aromatherapist');
    }
    /**
     *  Simply Pure Promis Page
     */
    public function simply_pure_promise()
    {
        return view('frontend.pages.simply-pure-promise');
    }
    /**
     * Free Shipping Page
     */
    public function free_shipping()
    {
        return view('frontend.pages.free-shipping');
    }

    /**
     * Our Story Page
     */
    public function our_story()
    {
        return view('frontend.pages.our-story');
    }

    /**
     * Wholesale Program Page
     */
    public function wholesale_program(Request $request)
    {
        return view('frontend.pages.wholesale-program', ['typeform_url' => url('https://simplyearth.typeform.com/to/nFepSt', $request->all())]);
    }

    /**
     * Wholesalers sales page
     */
    public function wholesalerSalesPage()
    {
        $product = Product::where('sku', env('INSTALLMENT_PRODUCT', ''))
            ->active()
            ->firstOrFail();

        if ($product->isVariant()) {
            $product = $product->parent;
        }

        \JavaScript::put([
            'cart_select' => new \App\Http\Resources\ProductItem(($product->setup == 'variable') ? $product->variants->first() : $product),
        ]);

        return view('frontend.pages.wholesaler-sales', ['product' => $product]);
    }
}
