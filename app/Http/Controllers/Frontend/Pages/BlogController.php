<?php

namespace App\Http\Controllers\Frontend\Pages;

use App\BlogPost;
use App\BlogCategory;
use App\Models\Blog\BlogSubCategories;
use App\Http\Controllers\Controller;
use App\Traits\BlogTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Self_;

/**
 * Display all the blog post on the brower
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 */
class BlogController extends Controller
{
    use BlogTraits;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        if ($request->input('search') !== null || $request->input('search') != '') {
            $blogs = BlogPost::getBlogPosts(['search' => $request->input('search')]);
        } else {
            $blogs = BlogPost::getBlogPosts();
        }
        // Categories for the filter box

//        self::modifyTitle($blogs);

        return view('frontend/pages/blog')->with([
            'blogs' => $blogs,
            'parentCategory' => $this->getCategories(),
            'defaultValue' => '',
        ]);
    }

    /**
     * @return array
     */
    function getCategories()
    {
        $parentCategory = DB::table('blog_categories')->orderBy('id')->pluck('name', 'name')->prepend('Filter by category', '');
        $categories = [];
        foreach ($parentCategory as $key => $val) {
            $categories[str_replace(" ", "-", $val)] = $val;
        }

        return $categories;
    }

    /**
     * Show single post
     *
     * @param  string $slug
     * @return Illuminate\Response
     */
    public function show($slug)
    {
        $post = BlogPost::getSinglePost($slug);

        if (!$post || ($post->status != 'publish' && !Auth::check())) {
            abort(404);
        }

        return view('frontend/pages/blog-details')->compileShortcodes()->with([
            'post' => $post,
            'meta_title' => $post->seo_title,
            'meta_description' => $post->seo_description,
            'meta_keywords' => $post->seo_Keywords,
        ]);
    }

    /**
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function catFilter($category)
    {

        $blogs = BlogPost::getBlogPosts(['category' => $category]);

        self::modifyTitle($blogs);

        return view('frontend/pages/blog')->with([
            'blogs' => $blogs,
            'parentCategory' => $this->getCategories(),
            'defaultValue' => $category
        ]);
    }

    /**
     * @param $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tagFilter($tag)
    {

        $blogs = BlogPost::getBlogPosts(['tag' => $tag]);
        self::modifyTitle($blogs);

        return view('frontend/pages/blog')->with([
            'blogs' => $blogs,
            'parentCategory' => $this->getCategories(),
            'defaultValue' => ''
        ]);
    }
}
