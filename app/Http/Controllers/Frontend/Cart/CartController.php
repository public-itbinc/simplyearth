<?php

namespace App\Http\Controllers\Frontend\Cart;

use App\Http\Controllers\Controller;
use App\Http\Requests\PromoCodeRequest;
use App\Http\Resources\CartResource;
use App\Shop\Products\Product;
use Facades\App\Shop\Cart\Cart;
use Facades\App\Shop\Checkout\Checkout;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function cart(Request $request)
    {
        return getCart();
    }

    public function add(Product $product)
    {

        if (!$product->canAccessByTags()) {
            return view('frontend.products.noaccess', ['product' => $product]);
        }

        Cart::add($product);

        return redirect(route('checkout'));
    }

    public function addPlan(Product $product)
    {

        if (!$product->canAccessByTags()) {
            return view('frontend.products.noaccess', ['product' => $product]);
        }

        Cart::addPlan($product);

        return redirect(route('checkout'));
    }

    public function update(Product $product, Request $request)
    {

        if (customer()) {
            //Check if the customer has an unpaid plan
            if (customer()->hasEitherTags(['installment-failed-charge', 'installment-incomplete'])) {
                return response()->json([
                    'has_se_message' => true,
                    'message' => 'Sorry. We have a failed charge on your payment plan for the starter kit. Before you can order, you need to update your card and get current on payments.',
                    'link' => route('profile'),
                    'link_text' => 'Update My Payment Method'
                ]);
            }

            if ($request->qty !== 0 && customer()->account->subscribed()) {
                if (!$request->has('ship_now')) {
                    return response()->json([
                        'when_to_ship' => true,
                        'available_shipping' => Cart::availableShippingMethods()->first(),
                    ]);
                }

                if ($request->ship_now == 0) {
                    customer()->account->nextBox()->updateAddons($product->id, $request->qty);
                    return response()->json(['shipped_nextbox' => true]);
                }
            }
        }

        Cart::update($product, request()->qty)->autoSetShipping()->processBonus();

        return new CartResource(getCart());
    }

    public function promocode(PromoCodeRequest $request)
    {
        Cart::applyDiscount($request->get('code'))->autoSetShipping();

        \Session::put('set_discount', $request->get('code'));

        return getCart();
    }

    public function giftCard(Request $request)
    {

        $this->validate($request, [
            'code' => ['required', 'exists:gift_cards,code'],
        ]);

        Cart::applyGiftCard($request->code);

        return getCart();
    }

    public function promocodeDelete()
    {
        Cart::deleteDiscount()->autoSetShipping();

        return getCart();
    }

    public function giftCardDelete()
    {
        Cart::deleteGiftCard();

        return getCart();
    }

    public function updateShipping(Request $request)
    {
        Cart::setShippingLocation($request->checkout['shipping_address'])
            ->setShippingMethod(Checkout::parseShippingMethod($request->checkout['shipping_method_key']));

        return getCart();
    }

    public function shippingAddress(Request $request)
    {
        Cart::setShippingLocation($request->only([
            'first_name',
            'last_name',
            'address1',
            'city',
            'zip',
            'region',
            'country',
        ]));

        Cart::autoSetShipping();

        return getCart();
    }
}
