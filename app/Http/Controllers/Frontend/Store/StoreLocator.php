<?php

namespace App\Http\Controllers\Frontend\Store;

use App\Store;
use App\Http\Controllers\Controller;
use App\Traits\StoreTraits;
use Illuminate\Http\Request;

/**
 * Author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class StoreLocator extends Controller
{
    /**
     * Traits
     * @var traits
     */
    use StoreTraits;

    /**
     * Display the store locator.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('frontend/store/index');
    }

    /**
     * return the xml format of the map location
     * @return string
     */
    public function getXml()
    {
      echo self::StoreToXml();
    }
}
