<?php

namespace App\Http\Controllers\Frontend\Store;

use DB;
use App\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Store Locator controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class StoreSearchController extends Controller
{
  /**
   * Search store in the database that is nearest to the location of the
   * searcher
   *
   * @param  Request $request
   * @return
   */
  public function search(Request $request)
  {
    $center_lat = $request->input("lat");
    $center_lng = $request->input("lng");
    $radius     = $request->input("radius");

    $dom        = new \DOMDocument("1.0");
    $node       = $dom->createElement("markers");
    $parentNode = $dom->appendChild($node);

    $query = "SELECT *, ( 3959 * acos( cos( radians(:lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:lng) ) + sin( radians(:lat2) ) * sin( radians( latitude ) ) ) ) AS distance FROM stores HAVING distance < :radius ORDER BY distance LIMIT 0 , 20";

    $result = DB::select($query, ['lat' => $center_lat, 'lng' => $center_lng, 'radius' => $radius, 'lat2' => $center_lat]);

    foreach ($result as $key => $value) {
      $node    = $dom->createElement("marker");
      $newnode = $parentNode->appendChild($node);

      $newnode->setAttribute("id",        $value->id);
      $newnode->setAttribute("name",      $value->name);
      $newnode->setAttribute("address",   $value->address);
      $newnode->setAttribute("latitude",  $value->latitude);
      $newnode->setAttribute("longitude", $value->longitude);
      $newnode->setAttribute("distance",  $value->distance);
      $newnode->setAttribute("city",   $value->city);
      $newnode->setAttribute("zipcode",   $value->zipcode);
      $newnode->setAttribute("country",   $value->country);
      $newnode->setAttribute("state",   $value->state);
      $newnode->setAttribute("phone",   $value->phone);
    }

    echo $dom->saveXML();
  }
}
