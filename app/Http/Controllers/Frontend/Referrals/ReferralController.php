<?php

namespace App\Http\Controllers\Frontend\Referrals;

use App\DiscountClick;
use App\Http\Controllers\Controller;
use App\Shop\Customers\Invitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReferralInvitationReferrer;

class ReferralController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!customer()) {
                return redirect(route('referral'));
            }

            return $next($request);
        })->except(['show']);
    }
    public function show()
    {
        return view('frontend.referrals.show');
    }

    public function invites()
    {
        customer()->prepareShareCode();

        return view('frontend.referrals.invites', ['code' => customer()->share_code, 'remain_ref' => customer()->remaining_referrals, 'is_mobile' => isMobile()]);
    }

    public function sendInvitation(Request $request)
    {

        $this->validate($request, ['email' => 'required']);

        $code = customer()->prepareShareCode()->share_code;

        $emails_arr = array_unique(array_filter(array_map('trim', explode(' ', str_replace(',', ' ', $request->email)))));

        $to_be_invited = collect($emails_arr)->filter(function ($email) {
            return filter_var($email, FILTER_VALIDATE_EMAIL) !== false && !customer()->invitations->contains('email', $email);
        });

        if ($to_be_invited->count() > 0) {
            customer()->invitations()->createMany($to_be_invited->map(function ($email) use ($code) {
                return [
                    'email' => $email,
                    'code' => $code,
                ];
            })->toArray());
        }

        //SEND INVITATION 1 by 1
        collect($emails_arr)->each(function ($email) {
            Mail::to($email)->send(new ReferralInvitationReferrer(customer()));
        });

        return response('', 200);
    }

    public function getInvites(Request $request)
    {
        return Invitation::where('customer_id', customer()->id)->get();
    }

    public function getConversion(Request $request)
    {
        return [
            'lifetime_boxes_count' => customer()->unclaimed_free_boxes_count
        ];
    }

    public function getReferralClicks(Request $request)
    {
        return [];
    }

    public function storeShare(Request $request)
    {
        $data = [];
        $errors = [];
        $success = true;

        if (count($request->all())) {
            $data = $request->all();
            $share_data = new CustomerShare;

            $share_data->customer_id = $data['customer_id'];
            $share_data->is_subscribe = 1;
            $share_data->source = $data['source'];

            $result = $share_data->save();
        }

        return compact('data', 'success', 'errors');
    }
}
