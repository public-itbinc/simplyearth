<?php

namespace App\Http\Controllers\Frontend\Products;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductItem;
use App\Http\Resources\ProductResourceCollection;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\QueryFilters\ProductFilters;
use App\Shop\Recipes\Recipe;
use Illuminate\Http\Request;
use JavaScript;
use App\Shop\Products\GCMS;

class ProductsController extends Controller
{
    public function index(ProductFilters $product_filters)
    {
        $builder = Product::with(['metas', 'media'])
            ->limitByTags()
            ->active()
            ->whereIn('type', ['default'])
            ->filter($product_filters);

        if (!$product_filters->getRequest()->has('sortby')) {
            $builder->orderBy('total_sales', 'desc');
        }

        $products = new ProductResourceCollection($builder->paginate(9)->appends(request()->query()));

        if (request()->ajax()) {
            return $products->toJson();
        }

        $categories = Category::parents()->with('children')->get();

        return view('frontend.products.index', ['products' => $products, 'categories' => $categories]);
    }

    public function show($param)
    {
        $product = Product::where('slug', $param)
            ->active()
            ->orWhere('id', $param)
            ->firstOrFail();

        $related_products = $product->getRelatedProducts();

        $recipes = [];
        if (isset($product->recipe_id)) {
            $recipes = Recipe::find($product->recipe_id)->products;
        }

        if ($product->isSubscription()) {
            return redirect(route('subscription'));
        }

        if ($product->isVariant()) {
            $product = $product->parent;
        }

        if (!is_wholesaler()) {
            unset($product->wholesale_pricing);
            unset($product->wholesale_price);
        }

        JavaScript::put([
            'cart_select' => new ProductItem(($product->setup == 'variable') ? $product->variants->first() : $product),
        ]);

        return view($product->canAccessByTags() ? 'frontend.products.show' : 'frontend.products.noaccess', [
            'product' => $product,
            'related_products' => $related_products,
            'recipes' => $recipes,
        ]);
    }

    public function gcms(Product $product, Request $request)
    {
        return [
            'gcms' => (new GCMS)->getGCMS($product->sku, $request->lot)
        ];
    }

    public function feed()
    {
        $products = Product::active()
        ->whereNotIn('sku', ['REC-QUARTERLY', 'REC-2-MONTHS', 'REC-MONTHLY2019', 'REC-MONTHLY-ONETIME'])
        ->where('setup', '!=', 'variable')->with(['categories'])->get();

        return response()->view('xml.google-feed', ['products' => $products])->header('Content-Type', 'text/xml');
    }
}
