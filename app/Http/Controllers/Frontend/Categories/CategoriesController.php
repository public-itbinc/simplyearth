<?php

namespace App\Http\Controllers\Frontend\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Products\Product;
use App\Shop\Categories\Category;
use App\Http\Resources\ProductResourceCollection;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Shop\QueryFilters\ProductFilters;

class CategoriesController extends Controller
{
    function show(Category $category, ProductFilters $product_filters)
    {
        
        $builder = $category->products()
        ->limitByTags()
        ->filter($product_filters)
        ->active()
        ->whereIn('type', ['default']);

        if (!$product_filters->getRequest()->has('sortby')) {
            $builder->orderBy('total_sales', 'desc');
        }

        $products = new ProductResourceCollection($builder->paginate(9));

        if (request()->ajax()) {
            return $products->toJson();
        }

        $categories = Category::parents()->with('children')->get();
        
        return view('frontend.categories.show', ['products' => $products, 'category' => $category, 'categories' => $categories]);
    }
}
