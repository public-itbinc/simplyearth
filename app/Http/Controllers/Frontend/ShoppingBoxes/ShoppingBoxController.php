<?php

namespace App\Http\Controllers\Frontend\ShoppingBoxes;

use App\Http\Controllers\Controller;
use App\Http\Resources\BoxDetail;
use App\Http\Resources\ExchangeDetail;
use App\Rules\BoxExchange;
use App\Shop\Products\Product;
use Illuminate\Http\Request;

class ShoppingBoxController extends Controller
{
    public function exchange($monthKey, Product $from, Request $request)
    {
        $box = customer()->account->futureOrders()->getMonth($monthKey);

        return new ExchangeDetail($box, $from);
    }

    public function updateAddon($monthKey, Product $product, Request $request)
    {
        $box = customer()->account->futureOrders()->getMonth($monthKey);
        $box->updateAddons($product->id, $request->qty);

        return new BoxDetail($box);
    }

    public function updateExchange($monthKey, Product $from, Request $request)
    {
        $this->validate($request, [
            'product' => [new BoxExchange($from)],
        ]);

        $box = customer()->account->futureOrders()->getMonth($monthKey);
        $box->updateExchanges($from->id, $request->product);

        return new ExchangeDetail($box, $from);
    }
}
