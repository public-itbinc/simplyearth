<?php

namespace App\Http\Controllers\Api\Pagebuilder;

use App\Http\Resources\Pagebuilder\CategoryCollection;
use App\Models\Pagebuilder\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function __invoke()
    {
        return new CategoryCollection(Category::all());
    }
}
