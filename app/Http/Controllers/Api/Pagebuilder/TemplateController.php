<?php

namespace App\Http\Controllers\Api\Pagebuilder;

use App\Http\Resources\Pagebuilder\CategoryCollection;
use App\Http\Resources\PageResource;
use App\Models\Pagebuilder\Category;
use App\Models\Pagebuilder\Page;
use App\Models\Pagebuilder\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class TemplateController extends Controller
{
    public function show($page)
    {
        /** @var Page $page */
        $page = Page::query()->with('contents')->find($page);

        return Cache::rememberForever($page->getCacheKey(), function () use ($page) {
            return new PageResource($page);
        });
    }

    public function store(Page $page, Request $request)
    {
        $rang = 0;
//        $content = [];

        $page->clearCache();

        $page->fill($request->only(['header', 'footer']));
        $page->save();

        $page->contents()->detach();

        foreach ((array) $request->data as $data) {
            $block = $data['dataObject'];
            $template = Template::find($block['id']);

            $page->contents()->attach($template, [
                'content' => json_encode($block['structure']),
                'rang'    => $rang++,
            ]);
        }

        return response()->json(true);
    }

    public function loadImage(Request $request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $directory = 'images/';
            $file_name = md5($file->getFilename().time()).'.'.$file->getClientOriginalExtension();

            \Storage::disk('public')->put($directory.$file_name, \File::get($file));

            return config('app.url').\Storage::url($directory.$file_name);
        }
    }
}
