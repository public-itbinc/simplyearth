<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop\Products\Product;
use AlgoliaSearch\Client as Algolia;
use App\Http\Resources\ProductResourceCollection;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        /*$algolia = new Algolia(config('scout.algolia.id'), config('scout.algolia.secret'));
        $algolia = $algolia->initIndex('products_index');

        $result = $algolia->search($request->q);

        return collect($result['hits'])->pluck('id');*/
        return Product::limitByTags()->search($request->q)->paginate(15);
        return new ProductResourceCollection(Product::search($request->q)->paginate(12));
    }
}
