<?php

namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Subscriptions\Subscription;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class AdminController extends Controller
{
    function index()
    {
        return redirect('/admin/pagebuilder');
    }

    function stats()
    {
        return Cache::remember('dashboard.stats', 60, function(){
            return [
                'subscribers_count' => DB::table('subscriptions')->select(DB::raw('count(*) as subscribers_count'))->first()->subscribers_count,
                'subscribers_active_count' => DB::table('subscriptions')->select(DB::raw('count(*) as subscribers_active_count'))->whereNull('ends_at')->first()->subscribers_active_count,
                'orders_count' => DB::table('orders')->select(DB::raw('count(*) as orders_count'))->first()->orders_count,
            ];
        });
    }
}
