<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Admin\Categories\Requests\CreateCategoryRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Shop\Categories\Category;
use App\Shop\QueryFilters\CategoryFilters;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index', [
            'categories' => Category::with(['media'])->paginate(20),
        ]);
    }

    public function search(CategoryFilters $filters)
    {
        return new CategoryResource(Category::filter($filters)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create', [
            'categories' => Category::all(),
        ]);
    }

    public function store(CreateCategoryRequest $request)
    {
        Category::create($request->all());

        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        $category = Category::findOrFail($category);

        return view('admin.categories.edit', [
            'categories' => Category::where('id', '!=', $category->id)->get(),
            'category' => $category,
        ]);
    }

    /**
     * Category Update
     *
     * @param int $category
     * @param Request $request
     * @return void
     */
    public function update($category, Request $request)
    {
        $category = Category::findOrFail($category);

        $category->update($request->all());

        if ($request->has('images')) {

            $category->getMedia()->each(function ($image) use ($request) {

                if (!in_array($image['id'], $request->images)) {
                    $image->delete();
                }

                $image_order = array_search($image->id, $request->images);

                if ($image_order !== $image->order_column) {
                    $image->order_column = $image_order;
                    $image->save();
                }

            });

        }

        flash('Category updated.')->success();
        return redirect()->route('admin.categories.edit', $category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        $category = Category::findOrFail($category);
        $category->delete();

        return redirect(route('admin.categories.index'));
    }

    public function images(Request $request, $category)
    {
        $this->validate($request, [
            'file' => 'mimes:jpeg,bmp,png',
        ]);

        $category = Category::findOrFail($category);

        $newImage = $category->addMedia($request->file)->toMediaCollection();

        return ['url' => $newImage->getUrl('thumb'), 'id' => $newImage->id];
    }
}
