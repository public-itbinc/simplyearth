<?php

namespace App\Http\Controllers\Admin\Customers;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Jobs\KlaviyoCustomerTags;
use App\Shop\Customers\Customer;
use App\Shop\Customers\CustomerRepository;
use App\Shop\Customers\TaxDocument;
use App\Shop\QueryFilters\CustomerFilters;
use App\Shop\Tags\Tag;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller
{
    use SendsPasswordResetEmails;

    public function index()
    {

        return view('admin.customers.index');
    }

    public function search(CustomerFilters $customer_filter)
    {

        return CustomerResource::collection(Customer::with(['account', 'orders', 'addresses'])->filter($customer_filter)->latest()->paginate(10));
    }

    public function show($customer, CustomerRepository $customerRepo)
    {
        $customer = $customerRepo->getCustomerWithAddresses($customer)->load(['account.subscription', 'history', 'tax_documents']);

        return new CustomerResource($customer);
    }

    public function store(Request $request, CustomerRepository $customerRepo)
    {
        $this->validate($request, [
            'email' => 'email|required|unique:customers',
        ]);

        $customer = $customerRepo->getOrCreateCustomer($request->all());

        if ($request->has('tags')) {

            $tags = [];
            foreach ($request->tags as $tag) {

                if (!in_array("valid", $tag['tiClasses'])) {
                    continue;
                }

                $tags[] = Tag::firstOrCreate(['name' => str_slug($tag['text'])])->id;
            }

            $customer->tags()->sync($tags);
        }

        return new CustomerResource($customer);
    }

    public function update(Request $request, $customer, CustomerRepository $customerRepo)
    {
        $customer = $customerRepo->getCustomerWithAddresses($customer);

        $this->validate($request, [
            'email' => 'email|required|unique:customers,email,' . $customer->id,
        ]);

        $customer->update($request->all());

        if ($request->has('email') && $customer->account) {
            $customer->account->update(['email' => $customer->email]);
        }

        return new CustomerResource($customer);
    }

    public function updateTags(Customer $customer, Request $request)
    {
        $tags = [];
        $accepted_tags = [];
        foreach ($request->tags as $tag) {

            if (!in_array("valid", $tag['tiClasses'])) {
                continue;
            }

            $tag_slug = str_slug($tag['text']);
            $tags[] = Tag::firstOrCreate(['name' => $tag_slug])->id;
            $accepted_tags[] = $tag_slug;
        }

        $customer->tags()->sync($tags);

        history('updated_tags', $customer->id, ['tags' => implode(',', $accepted_tags)]);

        KlaviyoCustomerTags::dispatch($customer);

        return new CustomerResource($customer);
    }

    public function overrideSchedule(Customer $customer, Request $request)
    {
        $customer->account->subscription->overrideSchedule($request->schedule)->save();

        history('subscription_override_schedule', $customer->id, [
            'schedule' => $request->schedule,
        ]);

        return response('', 200);
    }
    

    public function cancelSubscription(Customer $customer, Request $request)
    {
        $customer->account->subscription->stop()->save();

        return response('', 200);
    }

    public function searchTags($tag)
    {
        return Tag::where('name', 'LIKE', '%' . $tag . '%')->get();
    }

    public function address_update(Request $request, Customer $customer, CustomerRepository $customerRepo)
    {
        $customer = $customerRepo->updateOrCreateAddress($customer, $request->all());

        history('updated_address', $customer->id);

        return new CustomerResource($customerRepo->getCustomerWithAddresses($customer->id));
    }

    public function address_delete(Request $request, Customer $customer, CustomerRepository $customerRepo)
    {

        $this->validate($request, ['id' => 'required']);

        $customer = $customerRepo->removeAddress($customer, $request->id);

        history('deleted_address', $customer->id);

        return new CustomerResource($customerRepo->getCustomerWithAddresses($customer->id));
    }

    public function send_invite(Request $request, $customer)
    {
        $customer = Customer::findOrFail($customer);

        history('sent_invitation', $customer->id);

        return new CustomerResource($customer->sendInvite($request->all()));
    }

    public function changePassword(Customer $customer, Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
        ]);

        if (!$customer->account) {
            SEThrowvalidationException('password', 'Customer has no account yet');
        }

        $customer->account->password = bcrypt($request->password);
        $customer->account->save();

        history('updated_password', $customer->id);

        return response('', 200);
    }

    public function createAccount(Customer $customer, Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
        ]);

        if (empty($customer->email)) {
            SEThrowvalidationException('password', 'Customer doesn\'t have an email');
        }

        if ($customer->account) {
            SEThrowvalidationException('password', 'Customer already has an account');
        }

        $customer->account()->create(['password' => bcrypt($request->password)]);

        $customer->setRelations(['account']);

        return new CustomerResource($customer);
    }

    public function disable_account(Customer $customer, CustomerRepository $customerRepo)
    {
        $customerRepo->disableAccount($customer);

        $customer = $customerRepo->getCustomerWithAddresses($customer->id);

        history('disabled_account', $customer->id);

        return new CustomerResource($customer);
    }

    public function settings()
    {
        return [
            'countries' => countries_list(),
        ];
    }

    public function loginAs(Customer $customer)
    {
        if (!$customer->account) {
            SEThrowvalidationException('accont', 'Customer doesnt have an account yet.');
        }

        Auth::guard('customer')->login($customer->account);

        return response('', 200);
    }

    public function exemptToggle(Customer $customer, Request $request)
    {
        $customer->tax_exempt = !$customer->tax_exempt;
        $customer->save();

        return response('', 200);
    }

    public function uploadTaxDocument(Customer $customer, Request $request)
    {
        $filename = $request->file('taxdocument')->store('taxdocument');

        if (is_string($filename)) {
            $customer->tax_documents()->create([
                'document_name' => $request->taxdocument->getClientOriginalName(),
                'document_file' => $filename,
            ]);
        }

        return $customer->tax_documents;
    }

    public function downloadTaxDocument($document_id)
    {
        $tax_document = TaxDocument::find($document_id);

        $path = storage_path('app') . '/' . $tax_document->document_file;

        return response()->download($path);
    }

    public function deleteTaxDocument($document_id)
    {
        $tax_document = TaxDocument::find($document_id);

        $path = storage_path('app') . '/' . $tax_document->document_file;

        \Storage::delete($path);

        $tax_document->delete();

        $customer = Customer::find($tax_document->customer_id);

        if ($customer && $customer->tax_documents->count() == 0 && $customer->tax_exempt) {
            $customer->tax_exempt = false;
            $customer->save();
        }

        return response('', 200);
    }
}
