<?php

namespace App\Http\Controllers\Admin\Discounts;

use App\Http\Controllers\Controller;
use App\Http\Resources\DiscountResource;
use App\Shop\Discounts\Discount;
use App\Shop\Discounts\DiscountGenerator;
use App\Shop\QueryFilters\DiscountFilters;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Shop\Misc\DateTimeUtility;

class DiscountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.discounts.index');
    }

    public function search(Request $request, DiscountFilters $discount_filters)
    {
        return Discount::where('type', '!=', 'referral')->filter($discount_filters)->orderBy('id', 'DESC')->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.discounts.create', [
            'types' => [
                'percentage' => 'Percentage',
                'fixed' => 'Fixed',
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = [
            'type' => 'required',
        ];

        $type = $request->get('creation_type', 'normal');

        if ($type != 'bulk') {
            $validate['code'] = 'required|unique:discounts';
        }

        $this->validate($request, $validate);

        $request_data = $request->all();

        $request_data['options']['customer_tags'] = collect($request->options['customer_tags'])->toArray();

        $data = array_merge($request_data, [
            'start_date' => isset($request->start_date) ? DateTimeUtility::tzStartDay($request->start_date, 'America/Chicago') : null,
            'end_date' => isset($request->end_date) ? DateTimeUtility::tzEndDay($request->end_date, 'America/Chicago') : null,
        ]);

        if ($type != 'bulk') {
            Discount::create($data);
            return response('', 200);
        } else {

            $count = max(1, min(200, $request->get('count', 1)));

            $filename = (new DiscountGenerator)->setCount($count)->setData($data)->generate();

            return response()->json(['csv' => $filename]);
        }
    }

    public function downloadDiscounts($filename)
    {
        return Storage::download("bulk-discounts/{$filename}");
    }

    public function show(Discount $discount)
    {
        return new DiscountResource($discount);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount $discount)
    {
        $this->validate($request, [
            'code' => 'required|unique:discounts,code,' . $discount->id,
            'type' => 'required',
        ]);

        $data = $request->all();

        $data['options']['customer_tags'] = collect($request->options['customer_tags'])->toArray();
        
        $discount->update(array_merge($data, [
            'start_date' => isset($request->start_date) ? DateTimeUtility::tzStartDay($request->start_date, 'America/Chicago') : null,
            'end_date' => isset($request->end_date) ? DateTimeUtility::tzEndDay($request->end_date, 'America/Chicago') : null,
        ]));

        return response('', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount)
    {
        $discount->delete();

        return response('');
    }
}
