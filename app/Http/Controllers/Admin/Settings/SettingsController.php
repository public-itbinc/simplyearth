<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Shop\Settings\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function wholesale()
    {
        return view('admin.settings.wholesale', ['settings' => get_option('settings_wholesale', [])]);
    }

    public function wholesaleUpdate(Request $request)
    {

        $this->validate($request, [
            'settings_wholesale' => 'required',
        ]);

        Setting::updateOrCreate([
            'option' => 'settings_wholesale',
            'value' => $request->settings_wholesale]);

        flash('Settings updated', 'success');
        return back();
    }
}
