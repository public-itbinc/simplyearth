<?php

namespace App\Http\Controllers\Admin\Tools;

use App\Http\Controllers\Controller;
use App\Shop\Exporter\CSVExporter;

class ToolsController extends Controller
{

    function export_subscribers()
    {

        $exporter = new CSVExporter();

        $exporter->getWriter()->insertOne([
            'Subscription ID',
            'Email',
            'Plan',
            'Schedule',
            'Override Schedule',
            'Subscription Date',
            'Subscription Cancelled Date',
            'Cancel Reason',
            'Referrer'
        ]);

        \App\Shop\Customers\Customer::whereHas('account', function ($query) {
            return $query->whereHas('subscription');
        })->orderBy('id', 'desc')
            ->with(['orders.invitation.referrer', 'account.subscription'])->chunk(200, function ($subscribers) use (&$exporter) {



                $data = $subscribers->map(function ($customer) {



                    if ($customer->account) {

                        $invitation = $customer->orders->filter(function ($order) {
                            return $order->invitation;
                        })->first();

                        return [
                            $customer->account->subscription->id,
                            $customer->account->email,
                            $customer->account->subscription->plan,
                            $customer->account->subscription->schedule,
                            $customer->account->subscription->override_schedule,
                            $customer->account->subscription->created_at,
                            $customer->account->subscription->ends_at,
                            $customer->account->subscription->cancel_reason,
                            $invitation ? $invitation->referrer->email : null
                        ];
                    } else {

                        return [
                            $customer->email,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                        ];

                    }

                })->toArray();

                $exporter->getWriter()->insertAll($data);
            });


        $exporter->generate();

        exit();
    }
}
