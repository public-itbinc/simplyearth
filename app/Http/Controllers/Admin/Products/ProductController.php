<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\StoreProduct;
use App\Http\Requests\Products\UpdateProduct;
use App\Http\Resources\ProductResource;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\QueryFilters\ProductFilters;
use App\Shop\Recipes\Recipe;
use App\Shop\Tags\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JavaScript;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductFilters $filters)
    {
        $products = Product::where('setup', '!=', 'variable')->filter($filters)->with(['metas']);

        $resouce = ProductResource::collection($products->paginate(10));

        if (request()->ajax()) {
            return $resouce;
        }

        return view('admin.products.index', ['products' => $resouce]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'admin.products.create',
            ['categories' => Category::all(), 'productCategories' => [], 'tags' => collect([])]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        $product = new Product(array_merge(
            [
                'wholesale_pricing' => 0,
                'wholesale_price' => 0,
                'shipping' => 0,
                'weight' => 0,
            ],
            $request->all()
        ));

        $product->setMeta(array_filter($request->all()));

        $product->save();

        //Categories
        if ($request->has('categories')) {
            $collection = collect($request->input('categories'));
            $product->categories()->sync($collection->all());

        }

        //Tags

        $tags = [];
        foreach ($request->get('tags', []) as $tag) {
            $tags[] = Tag::firstOrCreate(['name' => str_slug($tag)])->id;
        }

        $product->tags()->sync($tags);

        flash('Product created.')->success();

        return redirect("admin/products/{$product->id}/edit/");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if ($product->type == 'variant') {
            return redirect(route('admin.products.edit', ['product' => $product->parent_id]));
        }

        $product->load('variants');

        //dd($product->variants->toArray());

        $productCategories = $product->categories()->get();

        $ids = [];
        foreach ($productCategories as $category) {
            $ids[] = $category->id;
        }

        JavaScript::put(['product' => $product]);
        return view('admin.products.edit', [
            'product' => $product,
            'productCategories' => $ids,
            'categories' => Category::all(),
            'recipes' => Recipe::pluck('name', 'id'),
            'tags' => collect($product->tags ? $product->tags->map(
                function ($product) {
                    return ['text' => $product->name];
                }
            ) : []),
            'products' => Product::all('id', 'name', 'status')->where('status', '=', 'active')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request, Product $product)
    {
        if ($product->type == 'variant') {
            return redirect(route('admin.products.edit', ['product' => $product->parent_id]));
        }

        if ($request->has('videos')) {
            $videos = collect($request->videos)->map(function ($item) {

                if (Storage::exists($item['cover'])) {
                    $item['url'] = item['cover'];
                } else {
                    $item['url'] = '';
                }
                return $item;
            });
            $product->setMeta('videos', $videos, true);
        } else {
            $product->unsetMeta('videos');
        }

        $product->setMeta(array_filter($request->all()));

        $product->update(array_merge(
            [
                'wholesale_pricing' => 0,
                'wholesale_price' => 0,
                'shipping' => 0,
                'weight' => 0,
            ],
            $request->all()
        ));

        if ($request->has('images')) {
            $product->getMedia()->each(function ($image) use ($request) {

                if (!in_array($image['id'], $request->images)) {
                    $image->delete();
                    return;
                }

                $image_order = array_search($image->id, $request->images);

                if ($image_order !== $image->order_column) {
                    $image->order_column = $image_order;
                    $image->save();
                }
            });
        }

        

        //Variants
        $variants = [];

        $variants_count = count($product->variants);
        $added_variants = collect([]);

        foreach ($request->get('variants', []) as $variant) {

            if (empty($variant['variant_attributes'])) {
                SEThrowvalidationException('variants', 'Invalid variant "Attributes" value');
            }

            try {
                $variant_first = explode(':', explode('|', $variant['variant_attributes'])[0])[0];
            } catch (\Exception $e) {
                SEThrowvalidationException('variants', 'Invalid variant "Attributes" value');
            }

            $sku = $variant['sku'] ?? $product->sku . '-' . $variants_count;

            if (isset($variant['id']) && $product->variants->where('id', $variant['id'])->count()) {
                $product->variants->where('id', $variant['id'])->first()->update([
                    'variant_attributes' => $variant['variant_attributes'],
                    'type' => 'variant',
                    'sku' => $sku,
                    'name' => $product->name . ' ' . $variant['variant_attributes'],
                    'variant_cover' => $variant['variant_cover'],
                    'price' => $variant['price'],
                ]);

                $added_variants->push($variant['id']);

            } else {
                $variants_count++;
                $new_variant = Product::create(
                    array_merge(
                        $product->toArray(),
                        [
                            'parent_id' => $product->id,
                            'variant_attributes' => $variant['variant_attributes'],
                            'type' => 'variant',
                            'name' => $product->name . ' ' . $variant['variant_attributes'],
                            'price' => $variant['price'],
                            'sku' => $sku,
                            'slug' => $product->slug . '-' . $sku,
                            'variant_cover' => $variant['variant_cover'],
                            'status' => 'active',
                            'setup' => 'single',
                        ]
                    )
                );

                $added_variants->push($new_variant->id);
            }
        }

        $product->variants->whereNotIn('id', $added_variants)->each(function ($item) {
            $item->delete();
        });

        //Update variants wholesale pricing
        $product->variants()->update(['wholesale_pricing' => $product->wholesale_pricing, 'wholesale_price' => $product->wholesale_price]);

        //Categories

        $collection = collect($request->input('categories'));
        $product->categories()->sync($collection->all());

        //Tags

        $tags = [];
        foreach ($request->get('tags', []) as $tag) {
            $tags[] = Tag::firstOrCreate(['name' => str_slug($tag)])->id;
        }

        $product->tags()->sync($tags);

        flash('Product updated.')->success();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect(route('admin.products.index'));
    }

    public function images(Request $request, Product $product)
    {
        $this->validate($request, [
            'file' => 'mimes:jpeg,bmp,png',
        ]);

        $newImage = $product->addMedia($request->file)->toMediaCollection();

        return ['url' => $newImage->getUrl('thumb'), 'id' => $newImage->id];
    }
}
