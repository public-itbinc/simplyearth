<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Discounts\Discount;
use App\Invitation;
use App\Shop\Subscriptions\Subscription;
use App\DiscountClick;
use App\Shop\Reports\CustomerShare;
use App\Shop\Reports\SubscriptionShare;
use App\Http\Controllers\Frontend\Referrals\ReferralController;


class ReportController extends Controller
{
    function index()
    {
        return view('admin.reports.index');
    }

    function email_statistics()
    {
    	$data = [];
    	$success = true;
    	$errors = [];

        // Percentage of Subscribers Sending Email Invites
        $subscription_data = Subscription::all();
        $invitation_data = Invitation::all();
        $subscriber_email_invites_number = 0;
        $total_email_coversion = 0;
        $past_one_month = date('Y-m-d', strtotime("-1 month"));
        $graph_date = $past_one_month;
        $subscriber_graph_date_arr = [];
        $subscriber_graph_number_arr = [];

        $email_sent_number_arr = [];
        $email_sent_date_arr = [];
        $email_sent_invitation_date = [];
        
        $total_email_coversion_number_arr = [];
        $total_email_coversion_date_arr = [];
        $total_email_coversion_date = [];

        $subscriber_number_arr = [];
        $subscriber_date_arr = [];
        $subscriber_date = [];

        $invitation_date = [];
        foreach ($subscription_data as $key => $value) {
            $total_email_coversion = 0;
            foreach ($invitation_data->toArray() as $_key => $_value) {
                if ($_value['source'] != 'facebook') {
                    if ($_value['is_signed_up']) {
                        $total_email_coversion++;
                        array_push($total_email_coversion_date, date('Y-m-d', strtotime($_value['updated_at'])));
                    }

                    if ($value->account_id == $_value['customer_id']) {
                        $subscriber_email_invites_number++;
                        array_push($invitation_date, date('Y-m-d', strtotime($_value['updated_at'])));
                    }
                }
            }
            array_push($subscriber_date, date('Y-m-d', strtotime($value['updated_at'])));
        }

        foreach ($invitation_data->toArray() as $_key => $_value) {
            array_push($email_sent_invitation_date, date('Y-m-d', strtotime($_value['updated_at'])));
        }

        // Set subcriber email invites graph
        $invitation_date_array = array_count_values($invitation_date);
        $email_sent_invitation_date_arr = array_count_values($email_sent_invitation_date);
        $total_email_coversion_date = array_count_values($total_email_coversion_date);
        $subscriber_date = array_count_values($subscriber_date);

        for ($i=0; $i < 30; $i++) {
            $graph_date = date('Y-m-d', strtotime($graph_date. ' + 1 days'));
            if (isset($invitation_date_array[$graph_date])) {
                array_push($subscriber_graph_date_arr, $graph_date);
                array_push($subscriber_graph_number_arr, $invitation_date_array[$graph_date]);
            } else {
                array_push($subscriber_graph_date_arr, $graph_date);
                array_push($subscriber_graph_number_arr, 0);
            }

            if (isset($email_sent_invitation_date_arr[$graph_date])) {
                array_push($email_sent_date_arr, $graph_date);
                array_push($email_sent_number_arr, $email_sent_invitation_date_arr[$graph_date]);
            } else {
                array_push($email_sent_date_arr, $graph_date);
                array_push($email_sent_number_arr, 0);
            }

            if (isset($total_email_coversion_date[$graph_date])) {
                array_push($total_email_coversion_date_arr, $graph_date);
                array_push($total_email_coversion_number_arr, $total_email_coversion_date[$graph_date]);
            } else {
                array_push($total_email_coversion_date_arr, $graph_date);
                array_push($total_email_coversion_number_arr, 0);
            }

            if (isset($subscriber_date[$graph_date])) {
                array_push($subscriber_date_arr, $graph_date);
                array_push($subscriber_number_arr, $subscriber_date[$graph_date]);
            } else {
                array_push($subscriber_date_arr, $graph_date);
                array_push($subscriber_number_arr, 0);
            }
        }

        // new subscribers for email
        $data['subscriber_date_arr'] = $subscriber_date_arr;
        $data['subscriber_number_arr'] = $subscriber_number_arr;

        // total email conversion
        $data['total_email_coversion_date_arr'] = $total_email_coversion_date_arr;
        $data['total_email_coversion_number_arr'] = $total_email_coversion_number_arr;

        // subcriber email invites set value 
        $data['subscriber_graph_date_arr'] = $subscriber_graph_date_arr;
        $data['subscriber_graph_number_arr'] = $subscriber_graph_number_arr;

        // email sent
        $data['email_sent_date_arr'] = $email_sent_date_arr;
        $data['email_sent_number_arr'] = $email_sent_number_arr;

        $data['subscriber_number'] = count($subscription_data);
        $data['total_email_coversion'] = $total_email_coversion;
        $data['subscriber_email_invites_number'] = $subscriber_email_invites_number;

        // Numbers of Emails Sent per Active Inviter
        $data['email_sent_number'] = $invitation_data->count();


        $click_number_data = DiscountClick::all();
        $click_date = [];
        $click_numbers = 0;
        
        $click_graph_date_arr = [];
        $click_graph_number_arr = [];
        foreach ($click_number_data as $key => $value) {
            if ($value['source'] != 'facebook') {
                $click_numbers++;
                array_push($click_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }
        $past_one_month = date('Y-m-d', strtotime("-1 month"));
        $graph_date = $past_one_month;
        $click_date_array = array_count_values($click_date);
        for ($i=0; $i < 30; $i++) {
            $graph_date = date('Y-m-d', strtotime($graph_date. ' + 1 days'));
            if (isset($click_date_array[$graph_date])) {
                array_push($click_graph_date_arr, $graph_date);
                array_push($click_graph_number_arr, $click_date_array[$graph_date]);
            } else {
                array_push($click_graph_date_arr, $graph_date);
                array_push($click_graph_number_arr, 0);
            }
        }

        //Email Click Through Rate
        $data['click_numbers'] = $click_numbers;
        $data['click_graph_date_arr'] = $click_graph_date_arr;
        $data['click_graph_number_arr'] = $click_graph_number_arr;

    	return compact('data', 'success', 'errors');
    }

    function facebook_statistics()
    {
        $data = [];
        $success = true;
        $errors = [];

        $invitation_data = Invitation::all();
        $subscription_data = Subscription::all();
        $subcriber_share_data = CustomerShare::all();
        $facebook_subscribers_data = SubscriptionShare::all();

        $past_one_month = date('Y-m-d', strtotime("-1 month"));
        $graph_date = $past_one_month;

        $facebook_conversion_rate_number_arr = [];
        $facebook_conversion_rate_date_arr = [];
        $facebook_conversion_rate_date = [];
        $facebook_conversion_rate = 0;
        
        $facebook_shares_active_subscriber_number_arr = [];
        $facebook_shares_active_subscriber_date_arr = [];
        $facebook_shares_active_subscriber_date = [];
        $facebook_shares_active_subscriber = 0;

        $facebook_subscribers_number_arr = [];
        $facebook_subscribers_date_arr = [];
        $facebook_subscribers_date = [];
        $facebook_subscribers = 0;

        $facebook_click_share = 0;

        foreach ($facebook_subscribers_data as $key => $value) {
            if ($value['source'] == 'facebook') {
                $facebook_subscribers++;
                array_push($facebook_subscribers_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }

        foreach ($subcriber_share_data as $key => $value) {
            if ($value['source'] == 'facebook') {
                $facebook_shares_active_subscriber++;
                array_push($facebook_shares_active_subscriber_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }

        foreach ($subscription_data as $key => $value) {
            $facebook_conversion_rate = 0;
            foreach ($invitation_data->toArray() as $_key => $_value) {
                if ($_value['source'] == 'facebook') {
                    if ($_value['is_signed_up']) {
                        $facebook_conversion_rate++;
                        array_push($facebook_conversion_rate_date, date('Y-m-d', strtotime($_value['updated_at'])));
                    }
                }
            }
        }
        
        $facebook_conversion_rate_date = array_count_values($facebook_conversion_rate_date);
        $facebook_shares_active_subscriber_date = array_count_values($facebook_shares_active_subscriber_date);
        $facebook_subscribers_date = array_count_values($facebook_subscribers_date);

        for ($i=0; $i < 32; $i++) {
            $graph_date = date('Y-m-d', strtotime($graph_date. ' + 1 days'));
            if (isset($facebook_conversion_rate_date[$graph_date])) {
                array_push($facebook_conversion_rate_date_arr, $graph_date);
                array_push($facebook_conversion_rate_number_arr, $facebook_conversion_rate_date[$graph_date]);
            } else {
                array_push($facebook_conversion_rate_date_arr, $graph_date);
                array_push($facebook_conversion_rate_number_arr, 0);
            }

            if (isset($facebook_shares_active_subscriber_date[$graph_date])) {
                array_push($facebook_shares_active_subscriber_date_arr, $graph_date);
                array_push($facebook_shares_active_subscriber_number_arr, $facebook_shares_active_subscriber_date[$graph_date]);
            } else {
                array_push($facebook_shares_active_subscriber_date_arr, $graph_date);
                array_push($facebook_shares_active_subscriber_number_arr, 0);
            }

            if (isset($facebook_subscribers_date[$graph_date])) {
                array_push($facebook_subscribers_date_arr, $graph_date);
                array_push($facebook_subscribers_number_arr, $facebook_subscribers_date[$graph_date]);
            } else {
                array_push($facebook_subscribers_date_arr, $graph_date);
                array_push($facebook_subscribers_number_arr, 0);
            }
        }
        
        $facebook_click_number_data = DiscountClick::all();
        $facebook_click_date = [];
        $facebook_click_share = 0;
        
        $facebook_click_graph_date_arr = [];
        $facebook_click_graph_number_arr = [];
        foreach ($facebook_click_number_data as $key => $value) {
            if ($value['source'] == 'facebook') {
                $facebook_click_share++;
                array_push($facebook_click_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }
        $past_one_month = date('Y-m-d', strtotime("-1 month"));
        $graph_date = $past_one_month;
        $facebook_click_date_array = array_count_values($facebook_click_date);
        for ($i=0; $i < 32; $i++) {
            $graph_date = date('Y-m-d', strtotime($graph_date. ' + 1 days'));
            if (isset($facebook_click_date_array[$graph_date])) {
                array_push($facebook_click_graph_date_arr, $graph_date);
                array_push($facebook_click_graph_number_arr, $facebook_click_date_array[$graph_date]);
            } else {
                array_push($facebook_click_graph_date_arr, $graph_date);
                array_push($facebook_click_graph_number_arr, 0);
            }
        }

        $data['facebook_subscribers_date_arr'] = $facebook_subscribers_date_arr;
        $data['facebook_subscribers_number_arr'] = $facebook_subscribers_number_arr;
        $data['facebook_subscribers'] = $facebook_subscribers;

        $data['facebook_shares_active_subscriber_date_arr'] = $facebook_shares_active_subscriber_date_arr;
        $data['facebook_shares_active_subscriber_number_arr'] = $facebook_shares_active_subscriber_number_arr;
        $data['facebook_shares_active_subscriber'] = $facebook_shares_active_subscriber;

        //Email Click Through Rate
        $data['facebook_click_share'] = $facebook_click_share;
        $data['facebook_click_graph_date_arr'] = $facebook_click_graph_date_arr;
        $data['facebook_click_graph_number_arr'] = $facebook_click_graph_number_arr;


        $data['facebook_conversion_rate_date_arr'] = $facebook_conversion_rate_date_arr;
        $data['facebook_conversion_rate_number_arr'] = $facebook_conversion_rate_number_arr;

        $data['facebook_conversion_rate'] = $facebook_conversion_rate;
        $data['facebook_click_share'] = $facebook_click_share;
        $data['facebook_subscribers'] = $facebook_subscribers;

        return compact('data', 'success', 'errors');
    }

    function messenger_statistics()
    {   
        $data = [];
        $success = true;
        $errors = [];

        $invitation_data = Invitation::all();
        $subscription_data = Subscription::all();
        $subcriber_share_data = CustomerShare::all();
        $messenger_subscribers_data = SubscriptionShare::all();

        $past_one_month = date('Y-m-d', strtotime("-1 month"));
        $graph_date = $past_one_month;

        $messenger_conversion_rate_number_arr = [];
        $messenger_conversion_rate_date_arr = [];
        $messenger_conversion_rate_date = [];
        $messenger_conversion_rate = 0;
        
        $messenger_shares_active_subscriber_number_arr = [];
        $messenger_shares_active_subscriber_date_arr = [];
        $messenger_shares_active_subscriber_date = [];
        $messenger_shares_active_subscriber = 0;

        $messenger_subscribers_number_arr = [];
        $messenger_subscribers_date_arr = [];
        $messenger_subscribers_date = [];
        $messenger_subscribers = 0;

        $messenger_click_share = 0;
        $messenger_subscribers = 0;


        foreach ($messenger_subscribers_data as $key => $value) {
            if ($value['source'] == 'messenger') {
                $messenger_subscribers++;
                array_push($messenger_subscribers_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }

        foreach ($subcriber_share_data as $key => $value) {
            if ($value['source'] == 'messenger') {
                $messenger_shares_active_subscriber++;
                array_push($messenger_shares_active_subscriber_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }

        foreach ($subscription_data as $key => $value) {
            $messenger_conversion_rate = 0;
            foreach ($invitation_data->toArray() as $_key => $_value) {
                if ($_value['source'] == 'messenger') {
                    if ($_value['is_signed_up']) {
                        $messenger_conversion_rate++;
                        array_push($messenger_conversion_rate_date, date('Y-m-d', strtotime($_value['updated_at'])));
                    }
                }
            }
        }
        
        $messenger_conversion_rate_date = array_count_values($messenger_conversion_rate_date);
        $messenger_shares_active_subscriber_date = array_count_values($messenger_shares_active_subscriber_date);
        $messenger_subscribers_date = array_count_values($messenger_subscribers_date);
        for ($i=0; $i < 32; $i++) {
            $graph_date = date('Y-m-d', strtotime($graph_date. ' + 1 days'));
            if (isset($messenger_conversion_rate_date[$graph_date])) {
                array_push($messenger_conversion_rate_date_arr, $graph_date);
                array_push($messenger_conversion_rate_number_arr, $messenger_conversion_rate_date[$graph_date]);
            } else {
                array_push($messenger_conversion_rate_date_arr, $graph_date);
                array_push($messenger_conversion_rate_number_arr, 0);
            }

            if (isset($messenger_shares_active_subscriber_date[$graph_date])) {
                array_push($messenger_shares_active_subscriber_date_arr, $graph_date);
                array_push($messenger_shares_active_subscriber_number_arr, $messenger_shares_active_subscriber_date[$graph_date]);
            } else {
                array_push($messenger_shares_active_subscriber_date_arr, $graph_date);
                array_push($messenger_shares_active_subscriber_number_arr, 0);
            }

            if (isset($messenger_subscribers_date[$graph_date])) {
                array_push($messenger_subscribers_date_arr, $graph_date);
                array_push($messenger_subscribers_number_arr, $messenger_subscribers_date[$graph_date]);
            } else {
                array_push($messenger_subscribers_date_arr, $graph_date);
                array_push($messenger_subscribers_number_arr, 0);
            }
        }
        
        $messenger_click_number_data = DiscountClick::all();
        $messenger_click_date = [];
        $messenger_click_share = 0;
        
        $messenger_click_graph_date_arr = [];
        $messenger_click_graph_number_arr = [];
        foreach ($messenger_click_number_data as $key => $value) {
            if ($value['source'] == 'messenger') {
                $messenger_click_share++;
                array_push($messenger_click_date, date('Y-m-d', strtotime($value['updated_at'])));
            }
        }
        $past_one_month = date('Y-m-d', strtotime("-1 month"));
        $graph_date = $past_one_month;
        $messenger_click_date_array = array_count_values($messenger_click_date);
        for ($i=0; $i < 32; $i++) {
            $graph_date = date('Y-m-d', strtotime($graph_date. ' + 1 days'));
            if (isset($messenger_click_date_array[$graph_date])) {
                array_push($messenger_click_graph_date_arr, $graph_date);
                array_push($messenger_click_graph_number_arr, $messenger_click_date_array[$graph_date]);
            } else {
                array_push($messenger_click_graph_date_arr, $graph_date);
                array_push($messenger_click_graph_number_arr, 0);
            }
        }

        $data['messenger_shares_active_subscriber_date_arr'] = $messenger_shares_active_subscriber_date_arr;
        $data['messenger_shares_active_subscriber_number_arr'] = $messenger_shares_active_subscriber_number_arr;
        $data['messenger_shares_active_subscriber'] = $messenger_shares_active_subscriber;

        //Email Click Through Rate
        $data['messenger_click_share'] = $messenger_click_share;
        $data['messenger_click_graph_date_arr'] = $messenger_click_graph_date_arr;
        $data['messenger_click_graph_number_arr'] = $messenger_click_graph_number_arr;


        $data['messenger_conversion_rate_date_arr'] = $messenger_conversion_rate_date_arr;
        $data['messenger_conversion_rate_number_arr'] = $messenger_conversion_rate_number_arr;

        $data['messenger_conversion_rate'] = $messenger_conversion_rate;
        $data['messenger_click_share'] = $messenger_click_share;
        $data['messenger_subscribers'] = $messenger_subscribers;

        return compact('data', 'success', 'errors');
    }


}
