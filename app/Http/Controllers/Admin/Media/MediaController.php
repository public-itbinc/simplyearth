<?php

namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use App\Shop\Misc\ImageLibrary;
use App\Http\Resources\MediaResource;
use Spatie\MediaLibrary\Media;

class MediaController extends Controller
{

    public function index(Request $request)
    {

        $source = in_array($request->source, ['avatar']) ? $request->source : "images";

        $items = ImageLibrary::default()->first()->media()->where('collection_name', $source)->orderBy('id', 'DESC')->paginate(12);

        return MediaResource::collection($items);
    }

    public function upload(Request $request)
    {
        
        $this->validate($request, [
            'file' => 'mimes:jpeg,bmp,png',
        ]);

        $source = in_array($request->source, ['avatar']) ? $request->source : "images";

        $library = ImageLibrary::where('slug', 'default')->first();

        $newImage = $library->addMedia($request->file)->toMediaCollection($source);

        return new MediaResource($newImage);
    }

    public function destroy(Media $media, Request $request)
    {
        $media->delete();

        return response('', 200);
    }

}
