<?php

namespace App\Http\Controllers\Admin\Recipes;

use App\Shop\Products\Product;
use App\Shop\QueryFilters\RecipeFilters;
use App\Shop\Recipes\Recipe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('admin.recipes.index', ['products' => Product::pluck('name', 'id')]);
    }

    /**
     * @param Request $request
     * @param RecipeFilters $recipe_filters
     * @return mixed
     */
    public function search(Request $request, RecipeFilters $recipe_filters)
    {
        return Recipe::filter($recipe_filters)->orderBy('id', 'DESC')->paginate(20);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->all();
        $slugTitle = self::createSlug($data['name']);
        $recipe = new Recipe;
        $recipe->name = $data['name'];
        $recipe->slug = $slugTitle;
        $recipe->ingredients = json_encode($data['products']);

        $recipe->save();

        return response('', 200);

    }

    public function show(Recipe $recipe)
    {
        $recipe->products;
        return $recipe;
    }

    public function update(Request $request, Recipe $recipe)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->all();
        $ingredients = json_encode($request->products);
        $slugTitle = self::createSlug($data['name']);
        var_dump($request->products);
        $recipe->update(array_merge(['name' => $data['name'],'ingredients' => $ingredients,'slug' => $slugTitle]));

        return response('',200);
    }

    public function destroy(Recipe $recipe)
    {
        $recipe->delete();

        return response('',200);
    }


    public function createSlug($title, $id = 0)
    {

        $slug = str_slug($title);

        $allSlugs = $this->getRelatedSlugs($slug, $id);

        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Recipe::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }

}
