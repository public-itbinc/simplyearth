<?php

namespace App\Http\Controllers\Admin\Administrators;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AdministratorsController extends Controller
{
    public function index()
    {
        return view('admin.administrators.index', ['administrators' => User::with('roles')->get()]);

    }

    public function search()
    {
        return User::paginate(50);
    }

    public function detail(User $user)
    {
        return $user;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create(array_merge($request->all(), ['password' => bcrypt($request->password)]));

        $user->assignRole('admin');

        return response()->json($user);

    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        ]);

        $data = $request->all();

        if ($request->has('password')) {
            $this->validate($request, [
                'password' => 'required|string|min:6|confirmed',
            ]);

            $data['password'] = bcrypt($data['password']);
        }

        $user->update($data);

        if ($request->has('role') && !$user->hasRole('super-admin')) {

            if (!in_array($request->role, ['admin', 'author'])) {
                SEThrowvalidationException('role', 'Invalid Role');
            }

            $user->syncRoles([$request->role]);
        }

        return response()->json($user);

    }

    public function adduser(Request $request)
    {
        if ($request->has('id') && $user = User::find($request->id)) {
            $user->syncRoles(['admin']);

            return $user->toJson();
        }

    }

    public function destroy(Request $request, User $user)
    {

        if ($user->hasAnyRole(['super-admin'])) {
            SEThrowvalidationException('user', 'Super admin cant be removed');
        }

        $user->delete();
        return response('', 200);
    }
}
