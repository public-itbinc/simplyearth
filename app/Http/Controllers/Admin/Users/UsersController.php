<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Shop\QueryFilters\UserFilters;

class UsersController extends Controller
{

    function search(UserFilters $userFilter, Request $request)
    {

        if ($request->filter == 'non-staff') {
            return User::noneStaff()->filter($userFilter)->get();
        } elseif ($request->filter == 'staff') {
            return User::staff()->filter($userFilter)->get();
        }

        return response()->json(User::filter($userFilter)->get());
    }
}
