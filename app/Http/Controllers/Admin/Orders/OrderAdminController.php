<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderSearch;
use App\Rules\OrderStatusUpdate;
use App\Shop\Customers\Customer;
use App\Shop\Orders\Order;
use App\Shop\Orders\OrderBuilder;
use App\Shop\Products\Product;
use App\Shop\QueryFilters\OrderFilters;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderAdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.orders.index');
    }

    public function search(OrderFilters $order_filter)
    {

        return new OrderSearch(Order::with(['customer', 'invitation.referrer'])
                ->filter($order_filter)
                ->orderBy('needs_approval', 'DESC')
                ->orderBy('id', 'DESC')
                ->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customer' => 'required',
            'products' => 'required',
        ]);

        $products = [];

        foreach (collect($request->products) as $p) {
            $product = Product::find($p['id']);
            if ($product) {
                $product->qty = $p['qty'];
                $products[] = $product;
            }
        }

        $customer = Customer::find($request->customer['id']);

        if (!$customer) {
            SEThrowvalidationException('customer', 'Customer not found');
        }

        $order = new OrderBuilder(
            $products, $customer
        );

        if ($request->has('notes')) {
            $order->addNote($request->notes);
        }

        if ($request->discount) {
            $order->overrideDiscount($request->discount);
        }

        if (!$order->isValid()) {
            throw new ValidationException('Invalid Order', 422);
        }

        $newOrder = $order->build();

        return new OrderResource($newOrder);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order->load(['order_items', 'customer', 'shipping_address', 'billing_address', 'invitation.referrer']);

        return new OrderResource($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $new_items = [];
        foreach ($request->get('order_items', []) as $order_item) {
            if (isset($order_item['id']) && $found_order_item = $order->order_items->where('id', $order_item['id'])->first()) {
                $found_order_item->updateQuantity($order_item['quantity']);
            } elseif ($order_item['product_id']) {
                $product = Product::find($order_item['product_id']);

                if ($product) {
                    array_push($new_items, [
                        'price' => $product->price,
                        'quantity' => $order_item['quantity'],
                        'sku' => $product->sku,
                        'product_id' => $product->id,
                        'name' => $product->name,
                        'weight' => $product->shipping == 1 ? $product->weight : 0,
                    ]);
                }
            }
        }

        $order->order_items()->whereNotIn('id', collect($request->order_items)->pluck('id'))->delete();

        if (!empty($new_items)) {
            $order->order_items()->createMany($new_items);
        }

        return new OrderResource($order->reCalculate());
    }

    public function compute(Request $request)
    {
        $products = [];
        $totals = [
            'subtotal_price' => 0,
            'tax_percentage' => 0,
            'total_tax' => 0,
            'total_price' => 0,
            'total_shipping' => 0,
        ];

        if ($request->has('products')) {

            foreach (collect($request->products) as $p) {
                $product = Product::find($p['id']);
                if ($product) {
                    $product->qty = $p['qty'];
                    $products[] = $product;
                }
            }

            $order = new OrderBuilder($products);

            if ($request->discount) {
                $order->overrideDiscount($request->discount);
            }

            if ($request->has('customer') && $customer = Customer::find($request->customer['id'])) {
                $order->setCustomer($customer);
            }

            $totals['subtotal_price'] = sprintf("%.2f", $order->getSubTotal());
            $totals['discount_total'] = sprintf("%.2f", $order->getOverallDiscounts());
            $totals['total_tax'] = sprintf("%.2f", $order->getTaxTotal());
            $totals['tax_percentage'] = sprintf("%.2f", $order->getTaxPercentage());
            $totals['total_price'] = sprintf("%.2f", $order->getTotal());
            $totals['total_shipping'] = sprintf("%.2f", $order->getTotalShipping());

        }

        return [
            'products' => $products,
            'totals' => $totals,
        ];
    }

    public function update_contact(Order $order, Request $request)
    {
        $order->update($request->only(['email', 'phone']));

        return $order->shipping_address();
    }

    public function update_referrer(Order $order, Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required'
        ]);

        $order->updateReferrer($request->customer_id);

        return response('');
    }

    public function remove_referrer(Order $order)
    {

        $order->removeReferrer();

        return response('');
    }

    public function update_shipping(Order $order, Request $request)
    {
        $order->shipping_address()->updateOrCreate(['order_id' => $order->id], $request->address);

        return new OrderResource($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function updateStatus(Order $order, Request $request)
    {
        $this->validate($request, [
            'status' => ['required', new OrderStatusUpdate($order)],
        ]);

        switch ($request->status) {
            case Order::ORDER_PROCESSING:
                $order->markAsPaid();
                break;

            case Order::ORDER_COMPLETED:
                $order->markAsCompleted();
                break;

            case Order::ORDER_CANCELLED:
                $order->cancelOrder();
                break;
        }

        return $order;
    }

    public function approve(Order $order)
    {
        if ($order->needs_approval == 1) {
            $order->approve();

            Mail::to($order)->send(new \App\Mail\OrderProcessed($order));
        }

        return $order;
    }

    public function settings()
    {
        return [
            'countries' => countries_list(),
            'tax' => tax(),
        ];
    }

    public function resendOrderEmail(Order $order, Request $request)
    {
        \Mail::to($order)->send(new \App\Mail\OrderProcessed($order));

        return response('');
    }
}
