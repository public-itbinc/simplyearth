<?php

namespace App\Http\Controllers\Admin\Blogs;

use Auth;
use App\Http\Controllers\Controller;
use App\Traits\Validation;
use App\User;
use Illuminate\Http\Request;

/**
 * Blog Author class
 *
 * Add avatar and description to the user
 * who is the author of the class
 *
 * @author Fil <filjoseph22@gmail.com>
 * @date May 31, 2018
 * @date May 31, 2018 --updated
 */
class BlogAuthorController extends Controller
{
    use Validation;

    /**
     * Initiate instance
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for updating the blog author information
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/blogs/author/create');
    }

    /**
     * Store the author description and avatar
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::check($this, $request);

        $path = $request->file('avatar');
        if (! is_null($path)) {
          $path = $path->store('public/blog/authors');
          $path = str_replace('public/', '', $path);
        }

        $user = User::find(Auth::id());
        $user->about  = $request->input('description');
        $user->avatar = $path;

        if ($user->save()) {
          return back()->with([
            'success' => 'Successfully updated your information'
          ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
