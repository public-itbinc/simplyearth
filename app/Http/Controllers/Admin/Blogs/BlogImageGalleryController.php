<?php

namespace App\Http\Controllers\Admin\Blogs;

use App\Http\Controllers\Controller;
use App\Models\Blog\BlogImageGallery;
use App\Traits\BlogTraits;
use App\Traits\Validation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Gallery Controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 * @package Simply Earth
 * @date May 30, 2018
 * @date June 01, 2018 -- updated
 */
class BlogImageGalleryController extends Controller
{
    use Validation;
    use BlogTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = BlogImageGallery::paginate(20);

        return view('admin/blogs/gallery/index')->with([
            'galleries' => $gallery,
            'page' => self::paginationPage($gallery),
        ]);
    }

    /**
     * Show the form uploading gallery image
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/blogs/gallery/create');
    }

    /**
     * Store new image for gallery
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::check($this, $request);

        if ($request->has('image')) {
            $item = $request->file('image')->store('blog', 'public');
        }

        $data['image'] = Storage::url($item);
        $data['name'] = $request->input('name');
        $result = BlogImageGallery::create($data);
        if ($result->wasRecentlyCreated) {
            return redirect(route('admin.bloggallery.index'))->with(
                'success', 'Successfully added image'
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
