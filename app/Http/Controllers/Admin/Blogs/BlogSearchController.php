<?php

namespace App\Http\Controllers\Admin\Blogs;

use App\BlogPost;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Blog post searching
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class BlogSearchController extends Controller
{
  /**
   * Return search results
   *
   * @param  Request $request
   * @return Illuminate\Response
   */
  public function search(Request $request)
  {
    $posts = BlogPost::search($request->input('search'));

    return view('admin/blogs/index')->with([
      'posts' => $posts
    ]);
  }
}
