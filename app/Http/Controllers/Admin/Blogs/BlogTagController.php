<?php

namespace App\Http\Controllers\Admin\Blogs;

use App\BlogTags;
use App\Http\Controllers\Controller;
use App\Traits\Validation;
use Illuminate\Http\Request;

/**
 * Blgo tag controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class BlogTagController extends Controller
{
    /**
     * Validation
     * @var traits
     */
    use Validation;

    /**
     * Create object instance
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if (self::checkDuplication($request)) {
        return '';
      }

      self::check($this, $request);

      $data['name'] = $request->input('tag');

      $BlogTags = BlogTags::create($data);

      if ($BlogTags->wasRecentlyCreated) {
        return $BlogTags;
      }
    }

    /**
     * Check if the given tag already exist
     *
     * @param  object $request
     * @return object|null
     */
    private function checkDuplication(&$request)
    {
      return BlogTags::where('name', $request->input('tag'))
        ->exists();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
