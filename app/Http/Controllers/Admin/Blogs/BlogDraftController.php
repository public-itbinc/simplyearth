<?php

namespace App\Http\Controllers\Admin\Blogs;

use Auth;
use App\BlogPost;
use App\BlogCategory;
use App\User;
use App\Http\Controllers\Controller;
use App\Models\Blog\BlogImageGallery;
use App\Traits\Validation;
use App\Traits\BlogTraits;
use Illuminate\Http\Request;

/**
 * Blog draft controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class BlogDraftController extends Controller
{
    /**
     * Validation
     * @var object
     */
    use Validation;
    use BlogTraits;

    /**
     * Initiate this object
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Show the form for editing blog draft.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $categories = BlogCategory::all();

      $post = self::prepareEditablePost($id);

      self::alterCategory($categories, $post);

      return view('admin/blogs/draft')->with([
        'post'       => $post,
        'categories' => $categories
      ]);
    }

    /**
     * Update blog post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->update = true;

      $data = self::acceptingRequest($this, $request);

      if ($request->has('author') and $request->input('author') > 0) {
        $data['user_id'] = $request->input('author');
      } else {
        $data['user_id'] = Auth::id();
      }

      # Task 57
      if ($request->has('created_at')) {
        $data['created_at'] = date('Y-m-d h:i:s', strtotime($data['created_at']));
      }

      if ($request->has('updated_at')) {
        $data['updated_at'] = date('Y-m-d h:i:s', strtotime($data['updated_at']));
      }

      $post = BlogPost::where('id', $id)
        ->update($data);

      if ($request->has('preview')) {
        $post = BlogPost::find($id);
        return self::preview($post);
      }

      if ($request->has('status') and $request->input('status') == 'publish') {
        $message = "Successfully publish post";
      } else {
        $message = "Successfully saved as draft";
      }

      $categories = self::getBlogCategories();

      $post = self::prepareEditablePost($id);

      self::alterCategory($categories, $post);

      $authors = User::all();

      $gallery = BlogImageGallery::take(10)
        ->get();

      return view('admin/blogs/draft')->with([
        'post'       => self::prepareEditablePost($id),
        'success'    => $message,
        'categories' => $categories,
        'authors'    => $authors,
        'gallery'    => $gallery,
        'taken'      => 10,
      ]);
    }
}
