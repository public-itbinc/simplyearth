<?php

namespace App\Http\Controllers\Admin\Blogs;

use Auth;
use App\BlogPost;
use App\BlogTags;
use App\BlogTagsGroups;
use App\BlogCategoryGroup;
use App\BlogCategory;
use App\User;
use App\Models\Blog\BlogImageGallery;
use App\Traits\Validation;
use App\Traits\BlogTraits;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Shop\QueryFilters\BlogFilters;
use Illuminate\Support\Carbon;
/**
 * Blog controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class BlogController extends Controller
{
    /**
     * Validation
     * @var object
     */
    use Validation;
    use BlogTraits;

    private $update = false;

    /**
     * Make an instance
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Display a the list of all blogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BlogFilters $blog_filters)
    {
      $posts = BlogPost::filter($blog_filters)->paginate(20);

      self::formatCategory($posts);
      self::formatPost($posts);

      return view('admin/blogs/index')->with([
        'posts' => $posts
      ]);
    }

    /**
     * Show the form for creating a new blog post
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categories = self::getBlogCategories();

      $gallery = BlogImageGallery::take(10)
        ->get();

      $authors = User::all();

      return view('admin/blogs/create')->with([
        'categories' => $categories,
        'gallery'    => $gallery,
        'taken'      => 10,
        'authors'    => $authors
      ]);
    }

    /**
     * Store new blog post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = self::acceptingRequest($this, $request);

      $data['user_id'] = $request->has('user_id') ? $request->user_id : Auth::id();
      $data['published_at'] = Carbon::now();

      # Task 57
      if ($request->has('created_at')) {
        $data['created_at'] = date('Y-m-d h:i:s', strtotime($data['created_at']));
      }

      if ($request->has('updated_at')) {
        $data['updated_at'] = date('Y-m-d h:i:s', strtotime($data['updated_at']));
      }

 
      $blog = BlogPost::create($data);

      if ($blog->wasRecentlyCreated) {
        self::addPostTag($request, $blog);
        self::addPostCategory($request, $blog);

        if ($request->has('preview')) {
          return self::preview($blog);
        }

        flash('Blog updated', 'success');

        return redirect(route('admin.blogs.edit', ['id' => $blog->id]));
      }
    }

    /**
     * Display the specified resource.
     *
     * wala pa ni gina gamit
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return BlogPost::find($id);
    }

    /**
     * Show the form for editing single post of blog.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $categories = self::getBlogCategories();

      $authors = User::all();

      $post = self::prepareEditablePost($id);

      self::alterCategory($categories, $post);

      $gallery = BlogImageGallery::take(10)
        ->get();

      $data = [
        'post'       => $post,
        'categories' => $categories,
        'authors'    => $authors,
        'gallery'    => $gallery,
        'taken'      => 10,
      ];

      if($post->status == 'draft') {
        return view('admin/blogs/draft')->with($data);
      }  

      return view('admin/blogs/edit')->with($data);
    }

    /**
     * Update the blog post
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogPost $blog)
    {
      $data = self::acceptingRequest($this, $request);

      if ($request->has('preview')) {
        return self::preview($blog);
      }

      $data['user_id'] = $request->has('user_id') ? $request->user_id : Auth::id();

      if ($request->has('created_at')) {
        $data['created_at'] = date('Y-m-d h:i:s', strtotime($data['created_at']));
      }

      if ($request->has('updated_at')) {
        $data['updated_at'] = date('Y-m-d h:i:s', strtotime($data['updated_at']));
      }

      $blog->update($data);

      self::updateCategory($request, $blog->id);
      self::updateTag($request, $blog->id);
      self::removeTag($request, $blog->id); # This will remove unassigned tag

      flash('Blog updated', 'success');

      return back();
    }

    /**
     * Remove single post from the blog_posts table.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = BlogPost::find($id);
      $post->delete();

      return back()
        ->with([
          'success' => 'Successfully deleted blog post'
        ]);
    }
}
