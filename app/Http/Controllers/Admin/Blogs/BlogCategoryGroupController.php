<?php

namespace App\Http\Controllers\Admin\Blogs;

use Auth;
use App\BlogCategoryGroup;
use App\BlogCategory;
use App\Http\Controllers\Controller;
use App\Models\Blog\BlogSubCategories;
use App\Traits\BlogTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogCategoryGroupController extends Controller
{
    use BlogTraits;

    /**
     * Instance
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Display blog in specific category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parentCategory    = BlogSubCategories::getParentCategory();
        $firstSubCategory  = BlogSubCategories::getFirstSubCategory();
        $secondSubCategory = BlogSubCategories::getSecondSubCategory();

        $blogs = BlogCategoryGroup::with([
          'blogPost' => function($query) {
            $query
              ->with([
                'groups' => function($query) {
                  $query->with('blogTag')->get();
                }])
              ->get();
          },
          'blogCategory'
        ])
        ->whereCategoryId($request->input('parent_category'))
        ->orWhere('category_id', $request->input('first_sub_category'))
        ->orWhere('category_id', $request->input('second_sub_category'))
        ->get();

        self::modifyTitle($blogs);

        return view('frontend/pages/blog-category')->with([
          'blogs'             => $blogs,
          'parentCategory'    => $parentCategory,
          'firstSubCategory'  => $firstSubCategory,
          'secondSubCategory' => $secondSubCategory,
        ]);
    }
}
