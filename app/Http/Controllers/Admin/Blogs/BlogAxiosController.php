<?php

namespace App\Http\Controllers\Admin\Blogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog\BlogImageGallery;

class BlogAxiosController extends Controller
{
    /**
     * Initiate instance
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    public function getBlog($offset, $skip)
    {
      $gal = BlogImageGallery::take($offset)
        ->skip($skip)
        ->get();

      if ($gal->count() > 1) {
        return $gal->toJson();
      } else {
        return 'false';
      }
    }
}
