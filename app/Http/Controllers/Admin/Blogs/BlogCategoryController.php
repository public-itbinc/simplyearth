<?php

namespace App\Http\Controllers\Admin\Blogs;

use App\BlogCategory;
use App\Http\Controllers\Controller;
use App\Traits\BlogTraits;
use App\Traits\Validation;
use Illuminate\Http\Request;

/**
 * Blog category controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 */
class BlogCategoryController extends Controller
{
    use Validation;
    use BlogTraits;

    /**
     * Initiate instance
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Display the list of category
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $catetogories = BlogCategory::paginate();
      $catForModal  = BlogCategory::All();

      return view('admin/blogs/category/index')->with([
        'categories'  => $catetogories,
        'page'        => self::paginationPage($catetogories),
        'catForModal' => $catForModal
      ]);
    }

    /**
     * Show the form for creating new blog category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = BlogCategory::all();

        return view('admin/blogs/category/create')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Store new category
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      self::check($this, $request);

      $data = BlogCategory::create(
        $request->only([
          "name",
          "slug",
          "description",
        ])
      );

      if ($data->wasRecentlyCreated) {
        return back()->with([
          'success' => "Successfully added new category"
        ]);
      }
    }

    /**
     * Show edeting form for blog category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = BlogCategory::find($id);

      return view('admin/blogs/category/edit')->with([
        'category' => $category
      ]);
    }

    /**
     * Update category for blog.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      self::check($this, $request);

      $category = BlogCategory::whereId($id)
        ->update($request->only([
          'name', 'slug', 'description'
        ]));

      if ($category == 1) {
        return back()->with([
          'success' => 'Successfully updated category!'
        ]);
      }
    }

    /**
     * Remove category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $cat = BlogCategory::find($id);
      $cat->delete();

      return back()
        ->with([
          'success' => 'Successfully deleted blog post'
        ]);
    }
}
