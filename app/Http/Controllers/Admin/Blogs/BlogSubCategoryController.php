<?php

namespace App\Http\Controllers\Admin\Blogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog\BlogSubCategories;
use App\Models\Blog\BlogParentChildCategory;
use App\Models\Blog\BlogChildDescendantCategory;

/**
 * Controller for sub category
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 */
class BlogSubCategoryController extends Controller
{
    /**
     * Initiate this object
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store sub category.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->input('parent') == $request->input('id')) {
        return back()->with('warning', 'You can\'t this category as a child of its own');
      }

      $exists = BlogParentChildCategory::whereChild($request->input('parent'))->exists();

      if (! $exists) {
        $exists = BlogParentChildCategory::whereParent($request->input('parent'))
          ->whereChild($request->input('id'))
          ->exists();

        if (! $exists) {
          $exists = BlogParentChildCategory::whereChild($request->input('id'))
            ->exists();

          $data['parent'] = $request->input('parent');
          $data['child'] = $request->input('id');

          if ($exists) {
            $exists = BlogParentChildCategory::whereChild($request->input('id'))
              ->update($data);

          } else {
            $result = BlogParentChildCategory::create($data);
          }
        }
      } else {
        $exists = BlogParentChildCategory::whereParent($request->input('id'))->exists();

        if ($exists) {
          return back()->with('warning', 'This category cant be a descendant of its child');
        }

        $exists = BlogChildDescendantCategory::whereChild($request->input('parent'))
          ->whereDescendant($request->input('id'))
          ->exists();

        $data['child']      = $request->input('parent');
        $data['descendant'] = $request->input('id');

        if (! $exists) {
          $result = BlogChildDescendantCategory::create($data);
        } else {
          $result = BlogChildDescendantCategory::whereChild($request->input('parent'))
            ->whereDescendant($request->input('id'))
            ->update($data);
        }
      }

      return back()->with('success', 'Successfully created as sub category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
