<?php

namespace App\Http\Controllers\Admin\Pagebuilder;

use App\Models\Pagebuilder\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagebuilderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pages = Page::query();
        if($request->has('q')){
            $pages->where('title', 'LIKE', "%".$request->q."%");
        }
        $pages = $pages->paginate(10);

        return view('admin/pagebuilder/index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pagebuilder/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = Page::make($request->all());
        $page->save();

        if ($request->has('meta_title')) {
            $page->metaData()->create([
                'name' => 'title',
                'value' => $request->meta_title,
            ]);
        }

        if ($request->has('meta_description')) {
            $page->metaData()->create([
                'name' => 'description',
                'value' => $request->meta_description,
            ]);
        }

        flash('Page create', 'success');

        return redirect()->route('admin.pagebuilder.edit_template', $page ?? collect());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin/pagebuilder/edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page->fill($request->all());
        $page->save();

        if ($request->has('meta_title')) {
            $page->metaData()->updateOrCreate([
                'name' => 'title'
            ], [
                'value' => $request->meta_title,
            ]);
        }

        if ($request->has('meta_description')) {
            $page->metaData()->updateOrCreate([
                'name' => 'description',
            ], [
                'value' => $request->meta_description,
            ]);
        }

//        flash('Page updated', 'success');

        return redirect()->route('admin.pagebuilder.index')->with('Page updated', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        return redirect()->route('admin.pagebuilder.index')->with('Page deleted', 'success');
    }

    public function edit_template(Page $page){
        return view('admin/pagebuilder/template', compact('page'));
    }
}
