<?php

namespace App\Http\Controllers\Admin\Importer;

use App\Http\Controllers\Controller;
use App\Shop\Customers\Account;
use App\Shop\Customers\Customer;
use App\Shop\Importer\Shopify;
use App\Shop\Products\Product;
use DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Writer;

class ImporterController extends Controller
{
    public function productids()
    {
        return;

        $products = Product::all();

        $reader = Reader::createFromString(Storage::get('product-exports-simplyearth.csv'));

        $reader->setHeaderOffset(0);

        foreach ($reader->getRecords() as $record) {
            if (!empty($record['product_handle']) && !empty($record['product_id'])) {
                $product = $products->where('slug', $record['product_handle'])->first();

                if ($product) {
                    $product->shopify_id = $record['product_id'];
                    $product->save();
                }
            }
        }
    }
    
    public function update()
    {
        return;

        Storage::put('app/product-reviews-new.csv', '');
        $reader = Reader::createFromString(Storage::get('product-reviews.csv'));
        $writer = Writer::createFromPath(storage_path('app/product-reviews-new.csv'), 'w+');

        $products = Product::all();

        $writer->insertOne($reader->fetchOne());

        $reader->setHeaderOffset(0);

        foreach ($reader->getRecords() as $row => $record) {
            $product = $products->where('id', $record['product_id'])->first();

            if ($product) {
                echo 'Found row' . $row . '<br />';
                $record['title'] = $product->name;
                $record['productUrl'] = str_replace('simplyearth.dev', 'simplyearth.com', $product->link);
                $record['productImageUrl'] = str_replace('simplyearth.dev', 'simplyearth.com', asset($product->cover));

            } else {
                echo 'Product ' . $record['product_id'] . ' not found in row:' . $row . '<br />';
            }

            $writer->insertOne(array_values($record));
        }

    }
}
