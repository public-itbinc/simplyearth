<?php

namespace App\Http\Controllers\Admin\GiftCards;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Discounts\GiftCard;
use App\Http\Resources\GiftCardResource;
use App\Shop\QueryFilters\GiftCardFilters;
use Illuminate\Support\Facades\Mail;

class GiftCardAdminController extends Controller
{
    public function index(GiftCardFilters $gift_card_filters)
    {
        return new GiftCardResource(GiftCard::with('owner')->filter($gift_card_filters)->paginate(20));
    }

    public function resend(GiftCard $gift_card)
    {
        Mail::to($gift_card->owner)->send(new \App\Mail\NewGiftCard($gift_card));
        return response('', 200);
    }

    public function remove(GiftCard $gift_card)
    {
        $gift_card->delete();
        return response('', 200);
    }
}
