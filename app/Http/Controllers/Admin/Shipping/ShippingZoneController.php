<?php

namespace App\Http\Controllers\Admin\Shipping;

use App\Shop\Orders\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Shipping\ShippingZone;
use Facades\App\Shop\Shipping\Helpers as ShippingHelpers;
use App\Http\Resources\ShippingZoneResource;
use App\Rules\RestOfTheWorld;
use Illuminate\Support\Facades\Validator;
use App\Shop\Shipping\ShippingRatePrice;
use App\Shop\Shipping\ShippingZoneRepository;
use App\Http\Requests\Shipping\UpdateCreateShippingZone;

class ShippingZoneController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ShippingZoneResource(ShippingZone::with(['shipping_rate_prices','shipping_rate_weights'])->get());
    }

    function show(ShippingZone $shipping_zone)
    {
        return new ShippingZoneResource($shipping_zone->load('shipping_rate_prices', 'shipping_rate_weights'));
    }

    function store(UpdateCreateShippingZone $request)
    {
        $shipping_zone = (new ShippingZoneRepository(new ShippingZone))->createWithRates($request->all());

        return $shipping_zone;
    }

    function update(UpdateCreateShippingZone $request, ShippingZone $shipping_zone)
    {
        $shipping_zone = (new ShippingZoneRepository($shipping_zone))->updateWithRates($request->all());

        return $shipping_zone;
    }

    function destroy(ShippingZone $shipping_zone)
    {

        $shipping_zone->delete();

        return response('', 200);
    }

}
