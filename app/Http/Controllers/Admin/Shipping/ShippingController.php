<?php

namespace App\Http\Controllers\Admin\Shipping;

use App\Shop\Orders\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Shipping\ShippingZone;
use Facades\App\Shop\Shipping\Helpers as ShippingHelpers;
use App\Http\Resources\ShippingZoneResource;

class ShippingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.shipping.index');
    }

    function settings()
    {
        return [
            'store_name' => config('app.name'),
            'store_address' => get_option('store_address'),
            'countries' => countries_list(),
            'zone_country_list' => ShippingZone::all()->pluck(['countries'])->flatten()
        ];
    }


    function update_store_address(Request $request)
    {
        set_option('store_address',$request->all());
    }
}
