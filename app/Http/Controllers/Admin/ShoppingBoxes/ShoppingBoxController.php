<?php

namespace App\Http\Controllers\Admin\ShoppingBoxes;

use App\Http\Controllers\Controller;
use App\Shop\ShoppingBoxes\ShoppingBox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ShoppingBoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxes = ShoppingBox::all();
        return view('admin.shoppingboxes.index', ['boxes' => $boxes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShoppingBox  $shoppingBox
     * @return \Illuminate\Http\Response
     */
    public function show(ShoppingBox $shoppingBox)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShoppingBox  $shoppingBox
     * @return \Illuminate\Http\Response
     */
    public function edit(ShoppingBox $shoppingBox)
    {
        return view('admin.shoppingboxes.edit', ['box' => $shoppingBox->load(['products'])]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShoppingBox  $shoppingBox
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShoppingBox $shoppingBox)
    {

        $shoppingBox->products()->sync($request->products);

        $recipes = collect([]);

        if ($request->has('recipes')) {
            $videos = collect($request->recipes)->map(function ($item) {

                if (Storage::exists($item['cover'])) {
                    $item['url'] = item['cover'];
                } else {
                    $item['url'] = '';
                }
                return $item;
            });
            $recipes = $videos;
        }

        $shoppingBox->update(array_merge($request->all(), ['recipes' => $recipes, 'features' => $request->features ?? []]));

        flash('Shopping box updated', 'success');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShoppingBox  $shoppingBox
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShoppingBox $shoppingBox)
    {
        //
    }
}
