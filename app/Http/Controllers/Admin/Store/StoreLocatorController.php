<?php

namespace App\Http\Controllers\Admin\Store;

use App\Store;
use App\Http\Controllers\Controller;
use App\Traits\Validation;
use Illuminate\Http\Request;

/**
 * Store Locator controller
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class StoreLocatorController extends Controller
{
    use Validation;

    /**
     * Create instance
     */
    public function __construct()
    {
      return $this->middleware('auth')->except('index');
    }

    /**
     * Display the page where the user/searcher can search
     * the nearest store to them.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('frontend/store/index');
    }

    /**
     * Show the form for creating adding new store.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin/store/create');
    }

    /**
     * Store new store address and other store information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      # Validate entry
      self::check($this, $request);

      # Store image  in public folder
      $path = $request->file('store_image');
      if (! is_null($path)) {
        $store_image_path = $path->store('store');
      }

      $path = $request->file('marker_image');
      if (! is_null($path)) {
        $marker_image_path = $path->store('store');
      }

      $data = $request->only([
        'name', 'address', 'city', 'zipcode',
        'country', 'state', 'latitude',
        'longitude', 'description', 'phone',
        'email', 'fax', 'web', 'schedule'
      ]);

      if (isset($store_image_path)) {
        $data['store_image']  = $store_image_path;
      }

      if (isset($marker_image_path)) {
        $data['marker_image'] = $marker_image_path;
      }

      if (! is_null($request->input('store_tag'))) {
        $data['tags'] = $request->input('store_tag');
      }

      $data = Store::create($data);

      if ($data->wasRecentlyCreated) {
        return back()->with([
          'success' => 'Successfully added new store!'
        ]);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing store information.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $store = Store::find($id);

      return view('admin/store/edit')->with([
        'store' => $store
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      # Validate entry
      self::check($this, $request);

      # Use when deleting image in storage, because there's
      # new upload image
      $tempStore = Store::find($id);

      # Store image  in public folder
      $path = $request->file('store_image');
      if (! is_null($path)) {
        $store_image_path = $path->store('store');
      }

      $path = $request->file('marker_image');
      if (! is_null($path)) {
        $marker_image_path = $path->store('store');
      }

      $data = $request->only([
        'name', 'address', 'city', 'zipcode',
        'country', 'state', 'latitude',
        'longitude', 'description', 'phone',
        'email', 'fax', 'web', 'schedule'
      ]);

      if (isset($store_image_path)) {
        $data['store_image']  = $store_image_path;
      }

      if (isset($marker_image_path)) {
        $data['marker_image'] = $marker_image_path;
      }

      if (! is_null($request->input('store_tag'))) {
        $data['tags'] = $request->input('store_tag');
      }

      $data = Store::whereId($id)
        ->update($data);

      if ($data == 1) {
        return back()->with([
          'success' => "Successfully updated {$tempStore->name}!"
        ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $storelocator)
    {
      $storelocator->delete();

      return response('', 200);
    }
}
