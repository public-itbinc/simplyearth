<?php

namespace App\Http\Controllers\Admin\Store;

use App\Http\Controllers\Controller;
use App\Traits\Validation;
use Illuminate\Http\Request;
use App\Store;

/**
 * Class for importing data from csv
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
class StoreImportController extends Controller
{
    use Validation;

    /**
     * Create instance
     */
    public function __construct()
    {
      return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin/storeimport/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      self::check($this, $request);

      $path = $request->file('csv_file')->getRealPath();
      $data = array_map('str_getcsv', file($path));
      $header = $data[0];
      array_shift($data);

      foreach ($data as $key => $value) {
        $temp = [
          'name'      => $value[0],
          $header[1]  => $value[1],
          $header[2]  => $value[2],
          $header[3]  => $value[3],
          $header[4]  => $value[4],
          $header[5]  => $value[5],
          $header[6]  => $value[6],
          $header[7]  => $value[7],
          $header[8]  => $value[8],
          $header[9]  => $value[9],
          $header[10] => $value[10],
          $header[12] => $value[12],
          $header[13] => $value[13],
          $header[14] => $value[14],
          $header[15] => $value[15],
          $header[16] => $value[16],
          $header[17] => $value[17],
        ];

        Store::create($temp);
      }

      return back()->with([
        'success' => 'Successfully imported locations'
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
