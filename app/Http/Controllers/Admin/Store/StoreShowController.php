<?php

namespace App\Http\Controllers\Admin\Store;

use App\Http\Controllers\Controller;
use App\Store;
use App\Services\StoresService;
use App\Traits\PaginationTrait;
use Illuminate\Http\Request;

/**
 * Used to display the list of locations
 *
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0.0
 */
class StoreShowController extends Controller
{
    use PaginationTrait;

    protected $storesService;

    /**
     * Create instance
     */
    public function __construct(StoresService $storesService)
    {
        $this->storesService = $storesService;

        return $this->middleware('auth');
    }

    /**
     * Display the list of stores, stored in
     * store table
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = $this->storesService->get()->paginate();

        return view('admin/store/index')->with([
          'stores' => $stores,
          'page'  => self::paginationPage($stores)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $q = $request->input('search');

        $stores = $this->storesService->get($q)->paginate();

        return view('admin/store/index')->with([
          'stores' => $stores,
          'page'  => self::paginationPage($stores)
        ]);
    }
}
