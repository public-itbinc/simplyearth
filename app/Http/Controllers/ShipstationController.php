<?php

namespace App\Http\Controllers;

use App\Shop\Orders\Order;
use App\Shop\Orders\OrderShipNotify;
use App\Shop\Orders\OrderXMLConverter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class ShipstationController extends Controller
{

    /**
     * Shipstation Product Listing
     *
     * @param Request $request request
     *
     * @return void
     */
    public function export(Request $request)
    {

        Log::info('Shipstation Request', $request->all());

        if (!$this->credsCheck($request)) {
            return response(null, 401);
        }

        switch ($request->action) {

            case 'export':

                $orders = Order::with(['customer', 'order_items'])
                    ->where('id', '>=', 40101) //Exclude past orders from being imported to shipstation
                    ->where('processed_at', '>=', Carbon::createFromFormat('m/d/Y H:i', $request->start_date))
                    ->where('processed_at', '<=', Carbon::createFromFormat('m/d/Y H:i', $request->end_date))
                    ->paginate(100, ['*'], 'page', $request->page ?? 1);

                $content = (new OrderXMLConverter($orders))->convertToXMLString();

                return Response::make($content, '200')
                    ->header('Content-Type', 'text/xml');

                break;
        }

        return response(null, 501);
    }

    public function notify(Request $request)
    {
        try {
            Log::info('Shipstation Request', $request->all());

            if (!$this->credsCheck($request)) {
                return response(null, 401);
            }

            switch ($request->action) {

                case 'shipnotify':

                    $xml = simplexml_load_string($request->getContent());

                    (new OrderShipNotify($xml, $request->all()))->process();

                    return response(null, 200);

                    break;
            }

        } catch (\Exception $e) {
            Log::info('Shipstation notify failed');
        }
        return response('', 501);
    }

    private function credsCheck(Request $request)
    {
        if (empty(config('shipstation.store.username'))
            || config('shipstation.store.username') != $request->get('SS-UserName')
            || config('shipstation.store.password') != $request->get('SS-Password')
        ) {
            Log::notice('Shipstation Credcheck Failed');

            return false;
        }

        return true;
    }

}
