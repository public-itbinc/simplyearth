<?php

namespace App\Http\Controllers;

use App\Models\Pagebuilder\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __invoke($slug, Request $request)
    {
        $page = Page::query()
            ->whereSlug($slug)
            ->with('contents')
            ->firstOrFail();

        $redirects = collect($page->redirects)->pluck('url', 'utm_content');
        if($url = $redirects->get($request->get('utm_content'))) {
            return redirect()->to($url);
        }

        return view('frontend.page', compact('page'));
    }
}
