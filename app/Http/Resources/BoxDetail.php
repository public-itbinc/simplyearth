<?php

namespace App\Http\Resources;

use App\Shop\ShoppingBoxes\ShoppingBoxBuilder;
use Illuminate\Http\Resources\Json\Resource;

class BoxDetail extends Resource
{

    public function __construct($resource)
    {
        self::$wrap = null;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $subscription_builder = $this->getBuilder();

        $currency = currency();

        $shipping_total = $subscription_builder->getTotalShipping();

        return [
            'is_first_not_skipped' => $this->isFirstNotSkipped(),
            'monthKey' => $this->monthKey,
            'date' => $this->date->toW3cString(),
            'price' => $this->getSubscription()->product->priceStringSimple,
            'products' => $this->getProductsParsed(),
            'shopping_box' => $this->shopping_box,
            'addons' => $this->getAddons(),
            'item_count' => $this->getItemCount(),
            'exchanges' => $this->getExchanges(),
            'currency' => currency(),
            'is_free' => $this->isFree(),
            'has_gift_card' => $this->hasGiftCardApplied(),
            'has_discount' => $subscription_builder->getDiscount(),
            'gift_card' => ['id' => $this->getGiftCardId(), 'amount' => sprintf('%s%.2f', $currency['symbol'], $this->getGiftCardDiscountTotal()) , 'code' => $this->getGiftCardMaskedCode()],
            'sub_total' => sprintf('%s%.2f', $currency['symbol'], $subscription_builder->getSubTotal()),
            'discount_total' => sprintf('%s%.2f', $currency['symbol'], max(0, $subscription_builder->getDiscountTotal() - $this->getGiftCardDiscountTotal())),
            'shipping' => $shipping_total <= 0 ? 'FREE' : sprintf('%s%.2f', $currency['symbol'], $subscription_builder->getTotalShipping()),
            'tax_total' => sprintf('%s%.2f', $currency['symbol'], $subscription_builder->getTaxTotal()),
            'total' => sprintf('%s%.2f', $currency['symbol'], $subscription_builder->getGrandTotal()),
            'monthCount' => $this->months,
            'bonus' => $this->getBonus(),
            'bonus_discount' => count($this->getBonus()) ? sprintf('%s%.2f', $currency['symbol'], collect($this->getBonus())->sum('price')) : null,
        ];
    }
}
