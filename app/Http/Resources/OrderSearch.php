<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Shop\Customers\Customer;

class OrderSearch extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);

        if ($request->has('customer')) {
            $array['customer'] = Customer::find($request->customer);
        }

        return $array;
    }
}
