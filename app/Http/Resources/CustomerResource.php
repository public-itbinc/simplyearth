<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CustomerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);

        $array['tags'] = $this->tags ? $this->tags->map(
            function ($tag) {
                return ['text' => $tag->name];
            }
        ) : [];

        $array['is_wholesaler'] = $this->isWholesaler();

        return $array;
    }
}
