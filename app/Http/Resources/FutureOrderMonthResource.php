<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FutureOrderMonthResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->date->format('M jS'),
            'date_zone' => $this->date->toW3cString(),
            'monthKey' => $this->monthKey,
            'months_from_now' => $this->monthsFromNow()];
    }
}
