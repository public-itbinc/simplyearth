<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductItem extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        if (customer() && customer()->isWholesaler()) {
            $array = $this->only(['id', 'cover', 'name', 'price', 'priceString',
            'sku', 'wholesale_pricing', 'wholesale_price', 'wholesalePriceString', 'variant_attributes']);
        } else {
            $array = $this->only(['id', 'cover', 'name', 'price', 'priceString', 'sku', 'variant_attributes']);
        }

        if($this->type=='variant'){
            $array['cover'] = $this->variant_cover;
        }

        return $array;
    }
}
