<?php

namespace App\Http\Resources;

use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use Illuminate\Http\Resources\Json\Resource;

class DiscountResource extends Resource
{

    public function __construct($resource)
    {
        self::$wrap = null;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);

        if (in_array($array['type'], ['free_addon', 'percentage', 'fixed_amount']) && isset($this->options['addons'])) {
            $array['addons_list'] = Product::whereIn('id', $this->options['addons'])->get();
        }

        if ($array['type'] == 'tiered_discount' && $this->options['tiered_discount']['type']=='addon') {
            $array['tiered_discount_addons_list'] = collect($this->options['tiered_discount']['free_addons'])->map(function($product_list) {
                return Product::whereIn('id', $product_list)->get();
            });
        }

        if (isset($this->options['applies_to'])) {
            if ($this->options['applies_to'] == 'products' && isset($this->options['products'])) {
                $array['product_list'] = Product::whereIn('id', $this->options['products'])->get();
            }

            if ($this->options['applies_to'] == 'categories' && isset($this->options['categories'])) {
                $array['category_list'] = Category::whereIn('id', $this->options['categories'])->get();
            }
        }

        $array['start_date'] = $this->start_date ? dateToW3cString($this->start_date->setTimeZone('America/Chicago')) : null;
        $array['end_date'] = $this->end_date ? dateToW3cString($this->end_date->setTimeZone('America/Chicago')) : null;

        return $array;
    }
}
