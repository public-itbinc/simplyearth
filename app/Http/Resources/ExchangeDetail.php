<?php

namespace App\Http\Resources;

use App\Shop\Products\Product;
use Illuminate\Http\Resources\Json\Resource;

class ExchangeDetail extends Resource
{
    protected $from;

    public function __construct($resource, Product $from)
    {
        self::$wrap = null;
        $this->from = $from;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($exchange = $this->getExchanges()->where('from', $this->from->id)->first()) {
            $exchange_product = $exchange->product;
        } else {
            $exchange_product = $this->from;
        }

        return [
            'monthKey' => $this->monthKey,
            'exchange_product' => $exchange_product,
            'original' => $exchange ? false : true,
            'choices' => Product::defaults()->active()->where('id', '!=', $this->from->id)
                ->whereHas('categories', function($query){
                    $query->where('slug', config('subscription.exchange_category'));
                })
                ->where('price', '<=', $this->from->price + 5)->orderBy('name', 'ASC')->get()->map(function ($item) {
                    return $item->only([
                        'id',
                        'name',
                        'link',
                        'cover'
                    ]);
                }),
        ];
    }
}
