<?php

namespace App\Services;

use App\Store;

class StoresService {

    /**
     * Returns the list of stores
     * @param string $q The query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function get($q = '')
    {
        $stores = Store::query();

        if ($q) {
            foreach ([
                'name',
                'address',
                'city',
                'zipcode',
                'country',
                'description',
                'phone',
                'email'
            ] as $field) {
                $stores->orWhere($field, 'LIKE', "%$q%");
            }
        } else {
            // 
        }

        return $stores;
    }
}