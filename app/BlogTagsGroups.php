<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTagsGroups extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
     "blog_post_id", 'blog_tag_id'
  ];

  /**
   * every record in group belongs to blog post
   *
   * @return object
   */
  public function blogPost()
  {
    return $this->belongsTo('App\BlogPost');
  }

  /**
   * Every tags recorded in group belongs to
   * tags
   *
   * @return object
   */
  public function blogTag()
  {
    return $this->belongsTo('App\BlogTags');
  }

  /**
   * Return all the tags assigned to a blog post
   *
   * @param  int $id Post ID
   * @return object|null
   */
  public static function getPostTags($id)
  {
    return static::with('blogTag')
      ->whereBlogPostId($id)
      ->get();
  }

  /**
   * Check if the given tag exists in the group
   *
   * @param  int $id
   * @param  object $tag
   * @return boolean
   */
  public static function checkExistingTag($id, $tag)
  {
    return static::whereBlogPostId($id)
      ->whereBlogTagId($tag->id)
      ->exists();

  }
}
