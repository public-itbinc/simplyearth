<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubscriptionSkipped
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $subscription;
    public $old_box;
    public $new_box;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($subscription, $old_box, $new_box)
    {
        $this->subscription = $subscription;
        $this->old_box = $old_box;
        $this->new_box = $new_box;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
