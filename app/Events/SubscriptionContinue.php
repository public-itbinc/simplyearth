<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubscriptionContinue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $subscription;
    public $stopped_date;
    public $continue_date;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($subscription, $stopped_date, $continue_date)
    {
        $this->subscription = $subscription;
        $this->stopped_date = $stopped_date;
        $this->continue_date = $continue_date;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
