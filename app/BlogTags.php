<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
     "name"
  ];

  /**
   * For every tag, it has corresponding post on
   * blog
   *
   * @return object
   */
  public function groups()
  {
    return $this->hasMany('App\BlogTagsGroups');
  }

  /**
   * Check if the given tag name exist
   *
   * @param  string $tag
   * @return boolean
   */
  public static function checkExistingTag($tag)
  {
    return static::whereName($tag)->exists();
  }
}
