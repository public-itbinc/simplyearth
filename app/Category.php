<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'slug', 'description', 'parent_id'
  ];

  /**
   * has many group
   */
  public function BlogCategoryGroup()
  {
    return $this->hasMany('App\BlogCategoryGroup');
  }
}
