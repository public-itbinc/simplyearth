<?php

use App\Shop\History\History;
use App\Shop\Subscriptions\FutureOrderMonth;
use Facades\App\Shop\Cart\Cart;
use Facades\App\Shop\Checkout\Checkout;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

if (!function_exists('sub_category')) {
    function sub_category($category)
    {
        echo "<ul>";
        foreach ($category as $child) {
            $checked = isset($child['postCategory']) ? 'checked' : '';
            $input = '<input type="checkbox" name="category[]" value="' . $child['child']['id'] . '" ' . $checked . '> ' . ucwords($child['child']['name']) . ' <br>';

            echo "<li>{$input}</li>";

            if (isset($child['descendant'])) {
                sub_category_descendant($child['descendant']);
            }
        }
        echo "</ul>";
    }
}

if (!function_exists('sub_category_descendant')) {
    function sub_category_descendant($category)
    {
        echo "<ul>";
        foreach ($category as $child) {
            $checked = isset($child['postCategory']) ? 'checked' : '';
            $input = '<input type="checkbox" name="category[]" value="' . $child['descendant']['id'] . '" ' . $checked . '> ' . ucwords($child['descendant']['name']) . ' <br>';
            echo "<li>{$input}</li>";
        }
        echo "</ul>";
    }
}

function checkout()
{
    return Checkout::getFacadeRoot();
}

function getCart()
{
    $array = [
        'requires_shipping' => Cart::requiresShipping(),
        'is_free_shipping' => Cart::isFreeShipping(),
        'is_free_shipping_discount' => Cart::isFreeShippingDiscount(),
        'count' => Cart::count(),
        'items' => (customer() && customer()->isWholesaler()) ? Cart::getProducts() : Cart::getProducts()->map(function ($product) {

            if($product->type == 'subscription') {
                $product->name = sprintf("%s: %s", $product->name, Cart::getSubscriptionBoxKey());
            }

            return collect($product)->except(['wholesale_pricing', 'wholesale_price', 'wholesalePriceString']);
        }),
        'subtotal' => sprintf('%.2f', Cart::getSubTotal()),
        'discount' => Cart::getDiscount() ? Cart::getDiscount()->only([
            'id',
            'type',
            'code'
        ]) : null,
        'gift_card' => Cart::getGiftCard(),
        'available_shipping' => Cart::availableShippingMethods(),
        'discount_total' => sprintf('%.2f', Cart::getDiscountTotal()),
        'gift_card_total' => sprintf('%.2f', Cart::getGiftCardTotal()),
        'grand_total' => sprintf('%.2f', Cart::getGrandTotal()),
        'tax' => sprintf('%.2f', Cart::getTaxTotal()),
        'currency' => currency(),
        'shipping' => Cart::getShipping(),
        'shipping_total' => sprintf('%.2f', Cart::getTotalShipping()),
        'addons' => Cart::getFreeAddons(),
        'bonus' => Cart::getBonus(),
        'bonus_discount' => sprintf('%.2f', Cart::getBonusTotal()),
        'has_subscription' => Cart::hasSubscriptionProduct(),
        'is_commitment' => Cart::hasCommitmentSubscription(),
        'is_free_box_option' => Cart::isFreeFirstMonthDiscount(),
        'checkout' => [
            'shipping_method_key' => Cart::getShippingMethodKey(),
            'shipping_address' => Cart::getShippingAddress(),
        ],
    ];

    if (customer() && customer()->isWholesaler()) {
        $array['wholesale_discount_total'] = sprintf('%.2f', Cart::wholesaleDiscountTotal());

        if (!Cart::metWholesaleMinimum() && !Cart::hasPlanProduct()) {
            $array['wholesale_message'] = wholesaleErrorMessage();
        }
    }
    return collect($array);
}

function wholesaleErrorMessage()
{
    return sprintf('Minimum subtotal required for wholesale order: %s%.2f', currency()['symbol']
        , floatval(collect(get_option('settings_wholesale'))->get('wholesale_minimum_order', 0)));
}

function paginateResults($data, int $perPage = 9)
{
    $page = request()->get('page', 1);
    $offset = ($page * $perPage) - $perPage;

    return (new LengthAwarePaginator(
        array_slice($data, $offset, $perPage, false),
        count($data),
        $perPage,
        $page,
        [
            'path' => request()->url(),
            'query' => request()->query(),
        ]
    ));
}

function countries_list($code = null)
{
    $countries = array(
        "AF" => "Afghanistan",
        "AL" => "Albania",
        "DZ" => "Algeria",
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua and Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AU" => "Australia",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BD" => "Bangladesh",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia and Herzegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "BN" => "Brunei Darussalam",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "CV" => "Cape Verde",
        "KY" => "Cayman Islands",
        "CF" => "Central African Republic",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China",
        "CX" => "Christmas Island",
        "CC" => "Cocos (Keeling) Islands",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CG" => "Congo",
        "CD" => "Congo, the Democratic Republic of the",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Cote D'Ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands (Malvinas)",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French Southern Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GN" => "Guinea",
        "GW" => "Guinea-Bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard Island and Mcdonald Islands",
        "VA" => "Holy See (Vatican City State)",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran, Islamic Republic of",
        "IQ" => "Iraq",
        "IE" => "Ireland",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "KP" => "Korea, Democratic People's Republic of",
        "KR" => "Korea, Republic of",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Lao People's Democratic Republic",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libyan Arab Jamahiriya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macao",
        "MK" => "Macedonia, the Former Yugoslav Republic of",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia, Federated States of",
        "MD" => "Moldova, Republic of",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "AN" => "Netherlands Antilles",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Northern Mariana Islands",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau",
        "PS" => "Palestinian Territory, Occupied",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn",
        "PL" => "Poland",
        "PT" => "Portugal",
        "PR" => "Puerto Rico",
        "QA" => "Qatar",
        "RE" => "Reunion",
        "RO" => "Romania",
        "RU" => "Russian Federation",
        "RW" => "Rwanda",
        "SH" => "Saint Helena",
        "KN" => "Saint Kitts and Nevis",
        "LC" => "Saint Lucia",
        "PM" => "Saint Pierre and Miquelon",
        "VC" => "Saint Vincent and the Grenadines",
        "WS" => "Samoa",
        "SM" => "San Marino",
        "ST" => "Sao Tome and Principe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "CS" => "Serbia and Montenegro",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia and the South Sandwich Islands",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syrian Arab Republic",
        "TW" => "Taiwan, Province of China",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania, United Republic of",
        "TH" => "Thailand",
        "TL" => "Timor-Leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad and Tobago",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks and Caicos Islands",
        "TV" => "Tuvalu",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "US" => "United States",
        "UM" => "United States Minor Outlying Islands",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VE" => "Venezuela",
        "VN" => "Viet Nam",
        "VG" => "Virgin Islands, British",
        "VI" => "Virgin Islands, U.s.",
        "WF" => "Wallis and Futuna",
        "EH" => "Western Sahara",
        "YE" => "Yemen",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe",
    );

    if ($code) {
        return isset($countries[$code]) ? $countries[$code] : "";
    }

    return $countries;

}

function parseCountry($country_name)
{
    return collect(countries_list())->filter(function ($country, $index) use ($country_name) {
        return $country == $country_name || $index == $country_name;
    })->keys()->first() ?? 'US';
}

/**
 * Returns logged in customer
 *
 * @return \App\Shop\Customers\Customer
 */
function customer()
{
    if (!Auth::guard('customer')->check()) {
        return false;
    }

    return Auth::guard('customer')->user()->customer;
}

/**
 * Account
 *
 * @return \App\Shop\Customers\Account
 */
function account()
{
    if (!Auth::guard('customer')->user()) {
        return false;
    }

    return Auth::guard('customer')->user();
}

function tax()
{
    return .05;
}

function currency()
{
    return config('cart.currency');
}

function get_option($option, $default = null)
{
    return array_get(app('settings'), $option, $default);
}

function set_option($option, $value)
{
    \App\Shop\Settings\Setting::updateOrCreate(['option' => $option], ['value' => $value]);
}

function dateToW3cString($datetime)
{
    if (is_a($datetime, Carbon::class)) {
        return $datetime->toW3cString();
    }

    return $datetime;
}

function SEThrowvalidationException($username = 'default', $msg)
{
    throw ValidationException::withMessages([
        $username => [$msg],
    ])->status(422);
}

function is_wholesaler()
{
    return customer() && customer()->isWholesaler();
}

function SEPriceFormat($price = 0)
{
    return sprintf('$%.2f', $price);
}

/**
 * Convert boxkey to Carbon date
 *
 * @param string $box_key
 * @return Carbon
 */
function boxKeyToDate(string $box_key)
{
    return Carbon::createFromFormat('F Y d', $box_key . ' 1');
}

function processBox(FutureOrderMonth $box)
{
    App\Jobs\ProcessShoppingBox::dispatch($box)->onQueue('box');
}

function history($type, $model_id, array $data = [], $model_type = 'customer')
{
    History::create(
        [
            'type' => $type,
            'model_id' => $model_id,
            'model_type' => $model_type,
            'data' => $data,
        ]
    );
}
/**
 * Returns ordinal number value
 *
 * @param int $num
 * @return void
 */
function addOrdinalNumberSuffix($num)
{
    if (!in_array(($num % 100), array(11, 12, 13))) {
        switch ($num % 10) {
            // Handle 1st, 2nd, 3rd
            case 1:return $num . 'st';
            case 2:return $num . 'nd';
            case 3:return $num . 'rd';
        }
    }
    return $num . 'th';
}

function isMobile()
{
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4))) {
        return true;
    }

    return false;
}
