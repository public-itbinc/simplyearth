<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\OrderCreated;
use Illuminate\Support\Facades\DB;
use App\Shop\Cart\SessionCart;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $extra_data;
    protected $customer;
    protected $cart_detail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $customer, $cart_detail)
    {
        $this->extra_data = $data;
        $this->customer = $customer;
        $this->cart_detail = $cart_detail;
        $this->tries = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $session_cart = new SessionCart($this->extra_data, $this->customer, $this->cart_detail);

        DB::beginTransaction();

        try {

            $order = $session_cart->build();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        if ($order) {
            event(new OrderCreated($order));
        }
    }
}
