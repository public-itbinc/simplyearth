<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Shop\Conversion\Klaviyo;

class KlaviyoSubscriptionStarted implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $subscription;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subscription)
    {
        $this->subscription = $subscription->load('owner');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Klaviyo $klaviyo)
    {
        $klaviyo->track('Started Subscription', [
            "customer_properties" => [
                '$email' => $this->subscription->owner->email,
                'Subscription' => $this->subscription->product->name,
            ],
        ]);
    }
}
