<?php

namespace App\Jobs;

use App\Shop\Checkout\ConversionTracker;
use App\Shop\Conversion\Klaviyo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class KlaviyoTrack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $event;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($event, $data)
    {
        $this->event = $event;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Klaviyo $klaviyo)
    {
        if (env('DISABLE_KLAVIYO')) {
            return;
        }

        switch ($this->event) {

            case 'card-declined':

                if (isset($this->data['subscription'])) {
                    $subscription = $this->data['subscription'];
                    $klaviyo->track('Failed Charge', [
                        "customer_properties" => [
                            '$email' => $subscription->owner->email,
                            'Subscription' => $subscription->product->name,
                        ],
                        'properties' => [
                            'failed_since' => $subscription->failed_at,
                            'failed_attempts' => $subscription->failed_attempts,
                        ],
                    ]);
                }

                break;

            case 'subscription-cancelled':

                if (isset($this->data['subscription'])) {
                    $subscription = $this->data['subscription'];
                    $klaviyo->track('Subscription Cancelled', [
                        "customer_properties" => [
                            '$email' => $subscription->owner->email,
                            'Subscription' => $subscription->product->name,
                        ],
                    ]);
                }

                break;

            case 'subscription-restarted':

                if (isset($this->data['subscription'])) {
                    $subscription = $this->data['subscription'];
                    $klaviyo->track('Subscription Restarted', [
                        "customer_properties" => [
                            '$email' => $subscription->owner->email,
                            'Subscription' => $subscription->product->name,
                        ],
                    ]);
                }

                break;

            case 'order-fulfilled':

                if (isset($this->data['order'])) {
                    $klaviyo->track('Fulfilled Order', (new ConversionTracker($this->data['order']))->klaviyo(['time' => $this->data['order']->completed_at->timestamp]));
                }

                break;

            case 'reward':

                $klaviyo->track('Earned a Reward', [
                    "customer_properties" => [
                        '$email' => $this->data['customer']->email,
                    ],
                    "properties" => [
                        'reward' => $this->data['type'],
                        'quantity' => $this->data['quantity'],
                        'code' => $this->data['customer']->share_code,
                    ],
                ]);

                break;

            case 'referral-points-earned':

                $klaviyo->track('Referral Successful', [
                    "customer_properties" => [
                        '$email' => $this->data['customer']->email,
                    ],
                    "properties" => [
                        'remaining_referrals' => $this->data['customer']->remaining_referrals,
                        'code' => $this->data['customer']->share_code,
                    ],
                ]);

                break;
        }
    }

    public function getEvent()
    {
        return $this->event;
    }
}
