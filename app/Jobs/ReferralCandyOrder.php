<?php

namespace App\Jobs;

use App\Shop\Checkout\ConversionTracker;
use App\Shop\Conversion\ReferralCandy;
use App\Shop\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ReferralCandyOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ReferralCandy $referral_candy)
    {
        $result = $referral_candy->request('purchase', (new ConversionTracker($this->order))->referralCandy());

        if ($result['success'] && $result['response']['message'] == ReferralCandy::MESSAGE_SUCCESS) {
            // Purchase has been successfully registered at ReferralCandy
        } else {
            Log::error('Order ' . $this->order->order_number . ' was not sent to Referral candy');
        }
    }
}
