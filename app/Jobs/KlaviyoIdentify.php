<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Shop\Conversion\Klaviyo;
use App\Shop\Customers\Customer;

class KlaviyoIdentify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $customer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Klaviyo $klaviyo)
    {
        $this->customer->refresh(); //Lets refresh the customer data

        $this->customer->prepareShareCode();

        $klaviyo->updateCustomerProperties($this->customer->email, [
            'share_code' => $this->customer->share_code,
            'share_link' => $this->customer->share_link,
        ]);
    }
}
