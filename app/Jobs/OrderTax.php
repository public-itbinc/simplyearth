<?php

namespace App\Jobs;

use App\Shop\Tax\TaxCalculator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class OrderTax implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;

    public $mode;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order, $mode = 'new')
    {
        $this->order = $order;

        $this->mode = $mode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TaxCalculator $tax)
    {

        if (config('app.disable_order_tax')) {
            return;
        }

        try {
            switch ($this->mode) {
                case 'update':
                    $tax->client()->updateOrder($this->args());
                    break;

                case 'new':

                    if (!$this->order->shipping_address) {
                        Log::error(sprintf('Order #%s tax not submitted - Reason: invalid missing shipping address', $this->order->id));

                        return;
                    }

                    $tax->client()->createOrder($this->args());
                    break;

                case 'delete':
                    $tax->client()->deleteOrder($this->order->id);
                    break;
            }
        } catch (\Exception $e) {
            Log::error('OrderTax: '.$e->getMessage());
        }
    }

    private function args()
    {
        return [
            'transaction_id' => $this->order->id,
            'transaction_date' => $this->order->processed_at->format('Y/m/d'),
            'to_country' => $this->order->shipping_address->country,
            'to_zip' => $this->order->shipping_address->zip,
            'to_state' => $this->order->shipping_address->region,
            'to_city' => $this->order->shipping_address->city,
            'to_street' => $this->order->shipping_address->address1,
            'amount' => (float) $this->order->total_price - (float) $this->order->total_tax,
            'shipping' => $this->order->total_shipping,
            'sales_tax' => $this->order->total_tax,
        ];
    }
}
