<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Shop\Subscriptions\SubscriptionGift;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailBoxGift;
use Illuminate\Support\Carbon;

class NotifyBoxGift implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $gift;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SubscriptionGift $gift)
    {
        $this->gift = $gift;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (Carbon::createFromFormat('F d Y', str_replace(' ', ' 1 ', $this->gift->schedule))->lt(Carbon::parse('First day of this month')->format('Y-m-d'))) {

            $this->gift->sent++;
            $this->gift->save();
            return;
        }

        Mail::to($this->gift->email)->send(new MailBoxGift($this->gift));
        $this->gift->sent++;
        $this->gift->save();
    }
}
