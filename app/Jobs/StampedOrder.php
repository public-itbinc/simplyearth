<?php

namespace App\Jobs;

use App\Shop\Checkout\ConversionTracker;
use App\Shop\Conversion\Stamped;
use App\Shop\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class StampedOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $stamped = new Stamped();
            $conversion = new ConversionTracker($this->order);
            $stamped->track($conversion->stamped());
        } catch (\Throwable $e) {
            Log::error('Stampede Failed:' . $e->getMessage());
        }
    }
}
