<?php

namespace App\Jobs;

use App\Jobs\BuildShoppingBox;

use App\Mail\FailedPaymentCharge2;
use App\Mail\FailedPaymentCharge3;
use App\Mail\FailedPaymentCharge4;
use App\Mail\FailedPaymentCharge5;
use App\Mail\FailedPaymentCharge6;
use App\Mail\FailedPaymentCharge;
use App\Shop\Customers\Account;
use App\Shop\Subscriptions\FutureOrderMonth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ProcessShoppingBox implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $monthly_box;

    protected $original_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FutureOrderMonth $monthly_box, Carbon $original_date = null)
    {
        $this->monthly_box = $monthly_box;
        $this->original_date = $original_date ?? Carbon::today();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $builder = $this->monthly_box->getBuilder();
        $subscription = $this->monthly_box->getSubscription();

        $account = Account::find($this->monthly_box->getSubscription()->account_id);

        if ($builder->getGrandTotal() <= 0) {
            $this->buildBox();
            return;
        }

        try {
            
            if (config('app.disable_charge')) {
                $result = (object) ['success' => 1];
            } else {
                $result = $account->charge($builder->getGrandTotal());
            }

        } catch (\Exception $e) {

            Log::warning('Charge failed:' . $account->email . ' = ' . $e->getMessage());

            $now = Carbon::now();

            $subscription->failed_attempts++;

            if (is_null($subscription->failed_at)) {
                $subscription->failed_at = $now;
            }

            $retry_date = $this->original_date->copy();
            switch ($subscription->failed_attempts) {
                case 1:
                    $subscription->override_schedule = $retry_date->addDay(1)->format('d'); //Add  1 day so this will be attempted tomorrowx
                    Mail::to($account)->send(new FailedPaymentCharge($account));
                    break;

                case 2:
                    $subscription->override_schedule = $retry_date->modify('next saturday')->format('d');
                    Mail::to($account)->send(new FailedPaymentCharge2($account));
                    break;

                case 3:
                    $subscription->override_schedule = $retry_date->modify('next saturday')->format('d');
                    Mail::to($account)->send(new FailedPaymentCharge3($account));
                    break;

                case 4:
                    $subscription->override_schedule = $retry_date->modify('next saturday')->format('d');
                    Mail::to($account)->send(new FailedPaymentCharge4($account));
                    break;

                case 5:
                    $subscription->override_schedule = $retry_date->modify('next saturday')->format('d');
                    Mail::to($account)->send(new FailedPaymentCharge5($account));
                    break;

                case 6:
                    $subscription->override_schedule = $retry_date->modify('next saturday')->format('d');
                    Mail::to($account)->send(new FailedPaymentCharge6($account));
                    break;

                default:

                    //stop the subscription if not yet after 6 attempts
                    if (!$subscription->stopped()) {
                        $subscription->stop('Failed Charge');
                        KlaviyoTrack::dispatch('failed-charge', ['subscription' => $subscription]);
                    }

                    break;
            }

            $subscription->save();

            history('box_charge_failed', $account->customer->id, [
                'month_key' => $this->monthly_box->month_key
            ]);

            return;
        }

        if ($result->success) {

            $this->buildBox();
        }
    }

    private function buildBox()
    {
        //Reset Failed Attempts values
        $subscription = $this->monthly_box->getSubscription();
        $subscription->failed_at = null;
        $subscription->failed_attempts = 0;
        $subscription->save();

        //Process box
        BuildShoppingBox::dispatch($this->monthly_box)->onQueue('box');
    }
}
