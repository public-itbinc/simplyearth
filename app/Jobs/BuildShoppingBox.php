<?php

namespace App\Jobs;

use App\Shop\Orders\Order;
use App\Shop\Subscriptions\FutureOrderMonth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class BuildShoppingBox implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $monthly_box;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FutureOrderMonth $monthly_box)
    {
        $this->monthly_box = $monthly_box;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Processing Box: ' . $this->monthly_box->month_key . ' for subscription: ' . $this->monthly_box->getSubscription()->id);
        $order = $this->monthly_box->getBuilder()->setData(['status' => Order::ORDER_PROCESSING])->build();

        history('box_processed', $order->customer_id, [
            'month_key' => $this->monthly_box->month_key,
            'order_id' => $order->id,
        ]);

        Log::info('Box Processed');
    }
}
