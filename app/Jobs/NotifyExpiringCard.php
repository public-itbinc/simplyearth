<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Shop\Customers\Account;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailCreditCardExpiring;
use Illuminate\Support\Carbon;

class NotifyExpiringCard implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $account;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->account)->send(new MailCreditCardExpiring($this->account));
        $this->account->expiring_notified_at = Carbon::now();
        $this->account->save();
    }
}
