<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Shop\Orders\Order;
use App\Shop\Discounts\Discount;
use App\Shop\Customers\Invitation;
use Illuminate\Support\Facades\DB;

class ProcessReferral implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;
    protected $discount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, Discount $discount)
    {
        $this->order = $order;
        $this->discount = $discount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $invitation = Invitation::firstOrCreate([
                'customer_id' => $this->discount->referrer->id,
                'code' => $this->discount->code,
                'email' => $this->order->customer->email
            ]);

            $this->order->forceFill([
                'invitation_id' => $invitation->id
            ])->save();

            $invitation_status = $this->order->customer->account->subscription->isCommitment() ? 'processing' : 'waiting';

            $invitation->update(['status' => $invitation_status]);
        });
    }
}
