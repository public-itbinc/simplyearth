<?php

namespace App\Jobs;

use App\Shop\Checkout\ConversionTracker;
use App\Shop\Conversion\Klaviyo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Shop\Orders\Order;

class KlaviyoOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Klaviyo $klaviyo)
    {
        if (env('DISABLE_KLAVIYO')) {
            return;
        }
        
        $conversion = new ConversionTracker($this->order);

        $klaviyo->track("Placed Order", $conversion->klaviyo(['time' => $this->order->processed_at->timestamp]));

        foreach ($this->order->order_items as $order_item) {
            $klaviyo->track("Ordered Product", $conversion->klaviyoOrderItem($order_item, ['time' => $this->order->processed_at->timestamp]));
        }
    }
}
