<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
  protected $fillable = [
    "name",
    "description",
    "phone",
    "email",
    "fax",
    "web",
    "tags",
    "schedule",
    "store_image",
    "marker_image",
    "address",
    "address2",
    "country",
    "state",
    "city",
    "zipcode",
    "latitude",
    "longitude",
    "extrafield",
  ];
}
