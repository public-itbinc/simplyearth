<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait HasSimpleAttributes
{

    public function __get($key)
    {
        if (in_array($key, $this->attributes)) {
            return $this->{$key};
        }

        if ($this->hasGetMutator($key)) {
            return $this->mutateAttribute($key);
        }

        return;
    }
    
    protected function mutateAttribute($key)
    {
        return call_user_func_array(array($this, 'get' . Str::studly($key) . 'Attribute'), []);
    }

    protected function hasGetMutator($key)
    {
        return method_exists($this, 'get' . Str::studly($key) . 'Attribute');
    }
}