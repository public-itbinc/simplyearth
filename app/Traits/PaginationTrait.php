<?php
namespace App\Traits;

/**
 *
 */
trait PaginationTrait
{
  /**
   * Return the number of page for pagination
   *
   * @param  object $posts
   * @return int
   */
  private function paginationPage(&$posts)
  {
    $page = $posts->total() % $posts->perPage();
    if ($page >= 1) {
      $page = $posts->total() - $page;
      $page = ($page / $posts->perPage()) + 1;
    } else {
      $page = $posts->total() / $posts->perPage();
    }

    return $page;
  }
}
