<?php

namespace App\Traits;

trait HasSubscriptions
{
    public function scopeSubscriptions($query)
    {
        return $query->where('type', 'subscription');
    }

    public function scopeDefaults($query)
    {
        return $query->where('type', 'default');
    }
}
