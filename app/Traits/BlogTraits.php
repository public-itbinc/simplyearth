<?php
namespace App\Traits;

use Auth;
use App\BlogPost;
use App\BlogTags;
use App\BlogTagsGroups;
use App\BlogCategoryGroup;
use App\BlogCategory;
use App\User;
use App\Models\Blog\BlogParentChildCategory;
use App\Models\Blog\BlogChildDescendantCategory;
use App\Models\Blog\BlogSubCategories;

/**
 * Excess method in blog controller that can be used
 * also by some other controller related to blog controller
 *
 * @author Fil <filjoseph22@email.com>
 * @version 1.0
 */
trait BlogTraits
{
  /**
   * This will make the title of the blog short, by limiting
   * to two words
   *
   * @param  object $blogs
   * @return void
   */
  private function modifyTitle(&$blogs)
  {
    foreach ($blogs as $key => $blog) {
      $title = explode(' ', $blog->title);

      if (count($title) == 1) {
        $blogs[$key]->title = "{$title[0]}";
      }
      if (count($title) == 2) {
        $blogs[$key]->title = "{$title[0]} {$title[1]}";
      }
      if (count($title) > 2) {
        $blogs[$key]->title = "{$title[0]} {$title[1]} ..";
      }
    }
  }

  /**
   * Return blog categries
   *
   * Task 54.
   *
   * @return array
   */
  public function getBlogCategories()
  {
    $categories = BlogCategory::all()->toArray();

    foreach ($categories as $key => $category) {
      $haschild = BlogParentChildCategory::hasChild($category['id']);

      if ($haschild) {
        # get the child category if any
        $child = BlogParentChildCategory::getChild($category['id']);

        foreach ($child as $pkey => $pcategory) {
          $hasdescendant = BlogChildDescendantCategory::hasDescendant($pcategory['child']['id']);

          if ($hasdescendant) {
            # Get the descendant
            $descendant = BlogChildDescendantCategory::getDescendant($pcategory['child']['id']);

            $child[$pkey]['descendant'] = $descendant;
            
            # remove descendant from $categories
            foreach ($descendant as $value) {
              $skey = array_search($value['descendant']['id'], array_column($categories, 'id'));
              unset($categories[$skey]);
            }
          }

          $skey = array_search($pcategory['child']['id'], array_column($categories, 'id'));
          unset($categories[$skey]);
        }

        $categories[$key]['child'] = $child;
      }
    }

    return $categories;
  }

  /**
   * Accepting the submitted request
   * This method is used both by method store() and update()
   *
   * @param  object $object
   * @param  object $request
   * @return array
   */
  private function acceptingRequest(&$object, &$request)
  {
    # Validate entry
    self::check($object, $request);

    $data = $request->only([
      "title", "slug", "content",
      "excerpt", "seo_title", "seo_description",
      "seo_Keywords", 'status', 'created_at', 'image',
      'updated_at','published_at'
    ]);

    return $data;
  }

  /**
   * Make sure slug is unique
   *
   * @param  array $data
   * @return
   */
  private function makeSureSlugIsUnique(&$data)
  {
    if ($this->update === true) {
      return;
    }

    if (BlogPost::checkSlug($data['slug'])) {
      $i      = 1;
      $result = false;

      do {
        $slug = explode('-', $data['slug']);
        $count = count($slug);

        if (is_numeric($slug[$count - 1])) {
          $slug[$count - 1] += 1;
        } else {
          $slug[$count - 1] = $slug[$count - 1] . "-{$i}";
        }

        $slug = implode('-', $slug);

        if (BlogPost::checkSlug($slug)) {
          $result = true;
        } else {
          $result = false;
        }
      } while ($result === true);

      $data['slug'] = $slug;
    }
  }

  /**
   * Check if the given tag already exist
   *
   * @param  object $request
   * @return object|null
   */
  private function checkDuplication($tag)
  {
    return BlogTags::where('name', $tag)
      ->exists();
  }

  /**
   * Add tag to a post
   *
   * @param object $request
   * @param string $blog
   * @return void
   */
  private function addPostTag(&$request, $blog)
  {
    if ($request->has('tags')) {
      $tags = explode(',', $request->input('tags'));

      foreach ($tags as $key => $tag) {
        if (self::checkDuplication(trim($tag)) === false) {
          BlogTags::create(['name' => $tag]);
        }

        $id = BlogTags::where('name', $tag)->get()->first()->id;

        BlogTagsGroups::create([
          'blog_post_id' => $blog->id,
          'blog_tag_id'  => $id
        ]);
      }
    }
  }

  /**
   * Add a category for blog post
   *
   * @param object $request
   * @param object $blog
   */
  private function addPostCategory(&$request, $blog)
  {
    if ($request->has('category')) {
      if (count($request->input('category')) > 0) {
        $categries = $request->input('category');

        foreach ($categries as $key => $cat) {
          self::addDefaultCategory($blog, $cat);
        }
      }
    } else {
      if (self::checkDefaultCategory() === false) {
        $cat = BlogCategory::create([
          'name'        => 'uncategorized',
          'slug'        => 'uncategorized',
          'description' => 'Assign to a blog or product that has no categories'
        ]);

        self::addDefaultCategory($blog, $cat->id);
      } else {
        $cat = BlogCategory::whereName('uncategorized')
          ->get()
          ->first();

        self::addDefaultCategory($blog, $cat->id);
      }
    }
  }

  /**
   * Add a default category for blog post
   *
   * @param object $blog
   * @param int $id
   */
  private function addDefaultCategory($blog, $id)
  {
    BlogCategoryGroup::create([
      'blog_post_id' => $blog->id,
      'category_id'  => $id
    ]);
  }

  /**
   * This will check if the default category exist on
   * categories table
   *
   * @return object
   */
  private function checkDefaultCategory()
  {
    return BlogCategory::whereName('uncategorized')
      ->exists();
  }

  /**
   * Return the data after saving the draft
   *
   * @param  int $id
   * @return Illuminate\Response
   */
  private function draft($id)
  {
    $categories = BlogCategory::all();

    $post = self::prepareEditablePost($id);

    $authors = User::all();

    self::alterCategory($categories, $post);

    return view('admin/blogs/draft')->with([
      'post'       => $post,
      'categories' => $categories,
      'authors'    => $authors,
      'success'    => "Successfully saved as draft"
    ]);
  }

  /**
   * Alter category by adding marking of what is assigned
   * post category
   *
   * @param  object $categories
   * @param  object $post
   * @return void
   */
  private function alterCategory(&$categories, &$post)
  {
    if (is_array($categories)) {
      self::alterArrayCateogry($categories, $post);
    } else {
      # Task 52
      foreach ($categories as $key => $cat) {
        foreach ($post->category as $pkey => $p) {
          if ($cat->id == $p->blogCategory->id) {
            $categories[$key]->postCategory = true;
          }
        }
      }
    }
  }

  /**
   * This will determine the category used by blogpost
   * to be able to determine if should be checked or not
   *
   * @param  array $categories The array of categories
   * @param  object $post The post to be edited
   * @return void
   */
  private function alterArrayCateogry(&$categories, &$post)
  {
    foreach ($categories as $key => $cat) {
      if (isset($cat['child'])) {
        $categories[$key]['child'] = self::alterChildCategory($cat['child'], $post);
      }

      foreach ($post->category as $p) {
        if ($cat['id'] == $p->blogCategory->id) {
          $categories[$key]['postCategory'] = true;
        }
      }
    }
  }

  /**
   * Determine if the child category of the post should be checked also
   *
   * @param  array $cat Array of child categories
   * @param  object $post The post to be edited
   * @return array
   */
  private function alterChildCategory($cat, &$post)
  {
    if (count($cat) == 0) {
      return;
    }

    foreach ($cat as $key => $child) {
      if (isset($child['descendant'])) {
        $cat[$key]['descendant'] = self::alterdescendantCategory($child['descendant'], $post);
      }
      foreach ($post->category as $p) {
        if ($child['child']['id'] == $p->blogCategory->id) {
          $cat[$key]['postCategory'] = true;
        }
      }
    }

    return $cat;
  }

  /**
   * Return the descendant category of the post
   *
   * @param  array $cat Categories
   * @param  object $post Post to be edited
   * @return array
   */
  private function alterdescendantCategory($cat, &$post)
  {
    if (count($cat) == 0) {
      return;
    }

    foreach ($cat as $key => $child) {
      foreach ($post->category as $p) {
        if ($child['descendant']['id'] == $p->blogCategory->id) {
          $cat[$key]['postCategory'] = true;
        }
      }
    }

    return $cat;
  }

  /**
   * Prepare editable post
   *
   * @param  int $id
   * @return object
   */
  private function prepareEditablePost($id)
  {
    $post = BlogPost::getEditablePost($id);

    $tags = [];
    foreach ($post->groups as $key => $group) {
      $tags[] = $group->blogTag->name;
    }

    $post->tags = implode(',', $tags);

    return $post;
  }

  /**
   * Return a preview blade
   *
   * @param  object $request
   * @return Illuminate\Response
   */
  private function preview($request)
  {
    $request->author = Auth::user();

    return view('admin/blogs/blog-preview')->with([
      'post' => $request
    ]);
  }

  /**
   * Update the tag assigned to a blog post
   *
   * @param  object $request
   * @param  int $id
   * @return void
   */
  private function updateTag(&$request, $id)
  {
    $tags = explode(',', $request->input('tags'));

    foreach ($tags as $key => $tag) {
      if (! BlogTags::checkExistingTag($tag)) {
        $tag = BlogTags::create([
          'name' => $tag
        ]);

        BlogTagsGroups::create([
          'blog_post_id' => $id,
          'blog_tag_id'  => $tag->id
        ]);
      } else {
        $tag = BlogTags::whereName($tag)->get()->first();

        if (! BlogTagsGroups::checkExistingTag($id, $tag)) {
          BlogTagsGroups::create([
            'blog_post_id' => $id,
            'blog_tag_id'  => $tag->id
          ]);
        }
      }
    }
  }

  /**
   * This will remove tag on update
   *
   * @param  object $request
   * @param  int $id Blog Post Id
   * @return void
   */
  private function removeTag(&$request, $id)
  {
    $blogTags = BlogTagsGroups::getPostTags($id)->toArray();
    $tags = explode(',', $request->input('tags'));
    foreach ($tags as $key => $tag) {
      self::popTagFromArray($blogTags, $tag);
    }
    foreach ($blogTags as $key => $blogTag) {
      BlogTagsGroups::find($blogTag['id'])->delete();
    }
  }

  /**
   * Update the category
   *
   * @param  object $request
   * @param  int $id
   * @return void
   */
  private function updateCategory(&$request, $id)
  {
    if(!is_array($request->category)) {
      return;
    }

    $cat = BlogCategoryGroup::getCategoriesByPost($id)->toArray();

    if (! empty($cat)) {
      foreach ($request->category as $key => $catId) {
        if (! BlogCategoryGroup::checkExistingCategory($id, $catId)) {
          BlogCategoryGroup::create([
            'blog_post_id' => $id,
            'category_id'  => $catId
          ]);
        }

        self::popCategoryFromArray($cat, $catId);
      }

      self::removeCategoryFromGroup($cat);
    }
  }

  /**
   * Remove the category from array
   *
   * @param  array $cat
   * @param  int $catId
   * @return void
   */
  private function popCategoryFromArray(&$cat, $catId)
  {
    foreach ($cat as $key => $value) {
      if ($value['category_id'] == $catId) {
        unset($cat[$key]);
      }
    }
  }

  /**
   * remove tag in array that is not subjetc for removal in the group
   *
   * @param  array $blogTags
   * @param  string $tag
   * @return void
   */
  private function popTagFromArray(&$blogTags, $tag)
  {
    foreach ($blogTags as $key => $blogTag) {
      if ($blogTag['blog_tag']['name'] == $tag) {
        unset($blogTags[$key]);
      }
    }
  }

  /**
   * Delete the remaining category after poping
   *
   * @param  array $cat
   * @return void
   */
  private function removeCategoryFromGroup(&$cat)
  {
    foreach ($cat as $key => $value) {
      BlogCategoryGroup::find($value['id'])->delete();
    }
  }

  /**
   * convert the array of category into a string of comma separated
   * link category
   *
   * @param  object $posts
   * @return void
   */
  private function formatCategory(&$posts)
  {
    foreach ($posts as $key => $post) {
      if ($post->category->count() > 0) {
        $cat = [];
        foreach ($post->category as $category) {
          $cat[] = '<a href="#">'.$category->blogCategory->name.'</a>';
        }
        $posts[$key]->cat = implode(',', $cat);
      }
    }
  }

  /**
   * Convert the tag array into string of comma separated link
   *
   * @param  object $posts
   * @return void
   */
  private function formatPost(&$posts)
  {
    foreach ($posts as $key => $post) {
      if (count($post->groups) > 0) {
        $tag = [];
        foreach ($post->groups as $group) {
          $tag[] = '<a href="#">'. ucwords($group->blogTag->name) .'</a>';
        }
      }
      $posts[$key]->posttag = implode(',', $tag);
    }
  }

  /**
   * return the total page for pagination
   *
   * @param  object $posts
   * @return int
   */
  private function paginationPage(&$posts)
  {
    $page = $posts->total() % $posts->perPage();
    if ($page >= 1) {
      $page = $posts->total() - $page;
      $page = ($page / $posts->perPage()) + 1;
    } else {
      $page = $posts->total() / $posts->perPage();
    }

    return $page;
  }
}
