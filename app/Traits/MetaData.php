<?php
namespace App\Traits;

trait MetaData{

    public function metaData()
    {
        return $this->morphMany(\App\Models\Pagebuilder\MetaData::class, 'taggable');
    }

    public function getMetaTitleAttribute()
    {
        return $this->metaData()->whereName('title')->first();
    }

    public function getMetaDescriptionAttribute()
    {
        return $this->metaData()->whereName('description')->first();
    }
}
