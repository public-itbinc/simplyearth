<?php

namespace App\Traits;

use App\Shop\Referrals\Reward;
use App\Jobs\KlaviyoTrack;


trait HasRewards
{
    public function unclaimed_rewards()
    {
        return $this->hasMany(Reward::class, 'customer_id')->whereNull('claimed_at');
    }

    public function claimed_rewards()
    {
        return $this->hasMany(Reward::class, 'customer_id')->whereNotNull('claimed_at');
    }

    public function getUnclaimedFreeBoxesCountAttribute()
    {
        return $this->unclaimed_rewards->where('type', 'freebox')->count();
    }

    public function rewards()
    {
        return $this->hasMany(Reward::class);
    }

    public function reward(string $type, int $quantity = 1)
    {
        $attribute = ['type' => $type];

        $this->rewards()->createMany(array_map(function () use ($attribute) {
            return $attribute;
        }, range(1, $quantity)));

        KlaviyoTrack::dispatch('reward', ['type' => $type, 'quantity' => $quantity, 'customer' => $this]);
    }
}   