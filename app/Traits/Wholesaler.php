<?php

namespace App\Traits;

use App\Shop\Tags\Tag;
use App\Shop\Customers\WholesalerRegistration;

trait Wholesaler
{

    public function isWholesaler()
    {
        return $this->hasTags('wholesale');
    }

    public function makeWholesaler()
    {
        $this->assignTags('wholesale');

        return $this;
    }
}
