<?php

namespace App\Traits;

trait HasUniqueSlug
{

    protected function getSlugColumnName()
    {
        return "slug";
    }

    protected static function processSlug()
    {
        static::creating(function ($model) {

            $model->{$model->getSlugColumnName()} = $model->generateSlug();

        });

        static::updating(function ($model) {

            $model->{$model->getSlugColumnName()} = $model->generateSlug(true);

        });
    }

    public function generateSlug($exclude = false)
    {
        $slug = str_slug(!empty($this->{$this->getSlugColumnName()}) ? $this->{$this->getSlugColumnName()} : $this->name);

        // check to see if any other slugs exist that are the same & count them
        $builder = static::whereRaw("{$this->getSlugColumnName()} RLIKE '^{$slug}(-[0-9]+)?$'");

        if ($exclude) {
            $builder->where('id', '<>', $this->id); //->where('type', '!=', 'variant');
        }

        $count = $builder->count();

        // if other slugs exist that are the same, append the count to the slug
        return $count ? "{$slug}-{$count}" : $slug;
    }
}