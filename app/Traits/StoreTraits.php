<?php
namespace App\Traits;

use App\Store;

/**
 * @author Fil <filjoseph22@gmail.com>
 * @version 1.0
 */
trait StoreTraits
{

  public function StoreToXml()
  {
    $stores = Store::all();

    $dom        = new \DOMDocument("1.0");
    $node       = $dom->createElement("markers");
    $parentNode = $dom->appendChild($node);

    foreach ($stores as $key => $value) {
      $node    = $dom->createElement("marker");
      $newnode = $parentNode->appendChild($node);

      $newnode->setAttribute("id",        $value->id);
      $newnode->setAttribute("name",      $value->name);
      $newnode->setAttribute("address",   $value->address);
      $newnode->setAttribute("city",   $value->city);
      $newnode->setAttribute("zipcode",   $value->zipcode);
      $newnode->setAttribute("country",   $value->country);
      $newnode->setAttribute("state",   $value->state);
      $newnode->setAttribute("phone",   $value->phone);
      $newnode->setAttribute("latitude",  $value->latitude);
      $newnode->setAttribute("longitude", $value->longitude);
      $newnode->setAttribute("distance",  '50');
    }

    return $dom->saveXML();
  }
}
