<?php

namespace App\Traits;


trait HasImages
{

    public function images(String $size = 'cover')
    {
        return $this->getMedia()->map(function ($image) use ($size) {
            return ['url' => asset($image->getUrl($size)), 'id' => $image->id];
        });
    }

    public function image(String $size = 'cover')
    {
        return $this->getFirstMediaUrl('', $size);
    }
}