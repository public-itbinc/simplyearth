<?php

namespace App\Traits;

use App\Shop\Customers\Customer;
use App\Shop\Discounts\Discount;
use App\Shop\Referrals\Reward;
use App\Jobs\KlaviyoIdentify;
use App\Shop\Customers\Invitation;
use Illuminate\Support\Facades\DB;

trait HasReferral
{
    /**
     * Ensures share code is generated for the model. Generates one if need to.
     *
     * @return void
     */
    public function prepareShareCode()
    {
        if (isset($this->share_code) && !empty($this->share_code)) {
            return $this;
        }

        $this->generateShareCode();

        return $this;
    }

    /**
     * Ensures share code is generated for the model. Generates one if need to.
     *
     * @return void
     */
    public function setShareCode()
    {
        if (isset($this->share_code) && !empty($this->share_code)) {
            return $this;
        }

        KlaviyoIdentify::dispatch($this);

        return $this;
    }

    /**
     * The discount linked to the referral code
     *
     */
    public function referral_discount()
    {
        return $this->hasOne(Discount::class, 'code', 'share_code');
    }

    private function generateShareCode($count = null)
    {
        $suffix = $count ?? '';

        $first_name = preg_replace("/[^A-Za-z]/", '', $this->first_name);
        $last_name = !empty($this->last_name) ? preg_replace("/[^A-Za-z]/", '', @$this->last_name[0]) : '';

        $code = strtoupper(str_replace(' ', '', substr($first_name, 0, 20))) . strtoupper($last_name) . 'FREE' . $suffix;

        DB::beginTransaction();

        try {
            Discount::create(['code' => $code, 'type' => 'referral']);
            $this->forceFill(['share_code' => $code])->save();
        } catch(\Throwable $e) {
            DB::rollback();
            return $this->generateShareCode(intval($count) + 1);
        }

        DB::commit();

        return $code;
    }

    /**
     * Remaining Referrals before getting the next free box reward
     *
     * @return int
     */
    public function getRemainingReferralsAttribute()
    {
        $referral_factor = config('app.referral_factor');

        if ($this->points < $referral_factor) {
            return $referral_factor - $this->points;
        }

        return $referral_factor - ($this->points % $referral_factor);
    }

    public function addPoints(int $points = 0)
    {
        $this->points += $points;

        $this->processRewards();

        $this->save();
    }

    protected function processRewards()
    {
        //Referral Rewards
        $referral_factor = config('app.referral_factor');

        if ($this->points >= $referral_factor) {
            $rewards_count = (int)floor($this->points / $referral_factor);
            $this->points = $this->points % $referral_factor;

            $this->reward('freebox', $rewards_count);
        }
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }
}
