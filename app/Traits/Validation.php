<?php

namespace App\Traits;

use Auth;
use Hash;

/**
 * Written by Fil for common validation process
 *
 * @author Fil Joseph <filjoseph22@gmail.com>
 * @version 1.0
 * @date 09-28-2017
 * @date 05-08-2018 - updated
 */
trait Validation
{
    /**
     * New validation
     *
     * @param  object $object
     * @param  object $request
     * @param  string $options
     * @return void
     */
    public static function check(&$object, &$request)
    {
        $data = [];

        if ($request->has('title')) {
            $data['title'] = 'required|string';
        }

        if ($request->has('csv_file')) {
            $data['csv_file'] = 'required|file';
        }

        if ($request->has('tag')) {
            $data['tag'] = 'required|string';
        }

        if ($request->has('content')) {
            $data['content'] = 'required|string';
        }

        if ($request->has('name')) {
            $data['name'] = 'required|string';
        }

        if ($request->has('address')) {
            $data['address'] = 'required|string';
        }

        if ($request->has('city')) {
            $data['city'] = 'required|string';
        }

        if ($request->has('zipcode')) {
            $data['zipcode'] = 'required|string';
        }

        if ($request->has('country')) {
            $data['country'] = 'required|string';
        }

        if ($request->has('state')) {
            $data['state'] = 'required|string';
        }

        if ($request->has('latitude')) {
            $data['latitude'] = 'required|string';
        }

        if ($request->has('longitude')) {
            $data['longitude'] = 'required|string';
        }

        if (!is_null($request->file('image'))) {
            if ($request->has('image')) {
                $data['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:5000';
            }
        }

        if (!is_null($request->file('avatar'))) {
            if ($request->has('avatar')) {
                $data['avatar'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:5000';
            }
        }

        $object->validate($request, $data);
    }
}
