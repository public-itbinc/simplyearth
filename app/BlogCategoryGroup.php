<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryGroup extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'blog_post_id', 'category_id'
  ];

  /**
   * every record in group belongs to blog post
   *
   * @return object
   */
  public function blogPost()
  {
    return $this->belongsTo('App\BlogPost');
  }

  /**
   * Every tags recorded in group belongs to
   * category
   *
   * @return object
   */
  public function blogCategory()
  {
    return $this->belongsTo('App\BlogCategory', 'category_id');
  }

  /**
   * Reurn category of the given post ID
   *
   * @param  int $id Post Id
   * @return object
   */
  public static function getCategoriesByPost($id)
  {
      return static::whereBlogPostId($id)
        ->get();
  }

  /**
   * Check if the given category Id is already exist
   *
   * @param  int $cat
   * @return boolean
   */
  public static function checkExistingCategory($postId, $catId)
  {
    return static::whereCategoryId($catId)
      ->whereBlogPostId($postId)
      ->exists();
  }
}
