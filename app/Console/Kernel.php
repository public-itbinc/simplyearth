<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Shopping Box shedule
        $schedule->command('box:run')->daily()->at('07:00');
        $schedule->command('box:trial')->daily()->at('10:00');
        $schedule->command('gift:notify')->daily()->at('08:00');
        $schedule->command('card:expiring-notify')->monthlyOn(1,'09:00');
        $schedule->command('backup:clean')->daily()->at('06:00');
        $schedule->command('backup:run --only-db')->daily()->at('06:30');
        $schedule->command('installments:run')->daily()->at('08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
