<?php

namespace App\Listeners;

use App\Events\Registered;
use Illuminate\Support\Facades\Mail;
use App\Mail\Registered as RegisteredMail;
use App\Mail\SubscribedMail;
use App\Shop\Conversion\Klaviyo;
use App\Jobs\KlaviyoSubscriptionStarted;
use App\Jobs\KlaviyoTrack;
use App\Mail\MailSubscriptionCancelled;
use App\Mail\MailSubscriptionRestarted;
use App\Mail\MailSubscriptionSkipped;
use App\Mail\MailSubscriptionContinue;
use App\Mail\MailSubscriptionUnskipped;
use App\Mail\MailSubscriptionGifted;
use App\Mail\MailCommitMentStopAutoRenew;
use App\Jobs\KlaviyoIdentify;

class CustomerEventSubscriber
{
    public function onRegistered($event)
    {
        KlaviyoIdentify::dispatch($event->account->customer);
        
        Mail::to($event->account)->send(new RegisteredMail($event->account->customer));
    }

    public function onSubscribed($event)
    {
        //Mail::to($event->subscription->owner->email)->send(new SubscribedMail($event->subscription));
        KlaviyoSubscriptionStarted::dispatch($event->subscription);
    }

    public function onSubscriptionCancelled($event)
    {
        Mail::to($event->subscription->owner->email)->send(new MailSubscriptionCancelled($event->subscription));

        KlaviyoTrack::dispatch('subscription-cancelled', [
            'subscription' => $event->subscription
        ]);
    }

    public function onSubscriptionRestarted($event)
    {
        Mail::to($event->subscription->owner->email)->send(new MailSubscriptionRestarted($event->subscription));

        KlaviyoTrack::dispatch('subscription-restarted', [
            'subscription' => $event->subscription
        ]);
    }

    public function onSubscriptionSkipped($event)
    {
        Mail::to($event->subscription->owner)->send(new MailSubscriptionSkipped($event->subscription, $event->old_box, $event->new_box));
    }

    public function onSubscriptionContinue($event)
    {
        Mail::to($event->subscription->owner)->send(new MailSubscriptionContinue($event->subscription, $event->stopped_date, $event->continue_date));
    }

    public function onSubscriptionUnskipped($event)
    {
        Mail::to($event->subscription->owner)->send(new MailSubscriptionUnskipped($event->subscription));
    }

    public function onSubscriptionGifted($event)
    {
        Mail::to($event->subscription->owner)->send(new MailSubscriptionGifted($event->subscription, $event->gift));
    }

    public function onCommitmentStopAutoRenew($event)
    {
        Mail::to($event->subscription->owner)->send(new MailCommitMentStopAutoRenew($event->subscription));
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Registered',
            'App\Listeners\CustomerEventSubscriber@onRegistered'
        );

        $events->listen(
            'App\Events\Subscribed',
            'App\Listeners\CustomerEventSubscriber@onSubscribed'
        );

        $events->listen(
            'App\Events\SubscriptionCancelled',
            'App\Listeners\CustomerEventSubscriber@onSubscriptionCancelled'
        );

        $events->listen(
            'App\Events\SubscriptionRestarted',
            'App\Listeners\CustomerEventSubscriber@onSubscriptionRestarted'
        );

        $events->listen(
            'App\Events\SubscriptionSkipped',
            'App\Listeners\CustomerEventSubscriber@onSubscriptionSkipped'
        );

        $events->listen(
            'App\Events\SubscriptionContinue',
            'App\Listeners\CustomerEventSubscriber@onSubscriptionContinue'
        );

        $events->listen(
            'App\Events\SubscriptionUnskipped',
            'App\Listeners\CustomerEventSubscriber@onSubscriptionUnskipped'
        );

        $events->listen(
            'App\Events\SubscriptionGifted',
            'App\Listeners\CustomerEventSubscriber@onSubscriptionGifted'
        );

        $events->listen(
            'App\Events\CommitmentStopAutoRenew',
            'App\Listeners\CustomerEventSubscriber@onCommitmentStopAutoRenew'
        );
    }
}
