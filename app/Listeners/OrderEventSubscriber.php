<?php

namespace App\Listeners;

use App\Jobs\KlaviyoOrder;
use App\Jobs\KlaviyoTrack;
use App\Jobs\OrderTax;
use App\Jobs\ReferralCandyOrder;
use App\Jobs\StampedOrder;
use App\Mail\OrderProcessed;
use App\Mail\OrderShipped;
use App\Shop\Orders\InstallmentPlan;
use App\Shop\Orders\Order;
use Illuminate\Support\Facades\Mail;
use App\Shop\Customers\Invitation;

class OrderEventSubscriber
{

    public function onOrderCreated($event)
    {
        if ($event->order->customer->isWholesaler() && $event->order->customer->orders->count() == 1) {
            //Do not send the orderProcessed immediately for wholesaler's first order
            $event->order->needs_approval = 1;
            $event->order->save();
        } else {
            Mail::to($event->order)->send(new OrderProcessed($event->order));
        }

        if ($event->order->status == Order::ORDER_PROCESSING) {
            //Process cards if any
            $event->order->processGiftCards();
        }

        //Send order data to klaviyo
        KlaviyoOrder::dispatch($event->order);

        //Send order data to Stamped
        StampedOrder::dispatch($event->order);

        //Send tax to taxjar
        OrderTax::dispatch($event->order)->onQueue('normal');
    }

    public function onOrderStatusUpdated($event)
    {
        switch ($event->order->status) {
            case Order::ORDER_PROCESSING:

                //Process cards if any
                $event->order->processGiftCards();

                //Mark order as complete if theres no product that requires shipping
                if (!$event->order->order_items->where('product.shipping', true)->count()) {
                    $event->order->markAsCompleted();
                }

                break;

            case Order::ORDER_COMPLETED:

                KlaviyoTrack::dispatch('order-fulfilled', ['order' => $event->order]);

                if ($event->order->order_items->where('product.shipping', true)->count()) {
                    Mail::to($event->order)->send(new OrderShipped($event->order));
                }

                if (
                    $event->order->isSubscriptionPurchase() &&
                    $invitation = Invitation::whereIn('status', ['processing', 'waiting'])
                    ->where('email', $event->order->customer->email)->first()
                ) {
                    
                    if ($invitation->status == 'processing' || ($invitation->status == 'waiting' && $event->order->customer->subscriptionOrders()->count() >= 3)) {
                        $invitation->referrer->addPoints(3);
                        $invitation->update(['status' => 'earned']);
                    }
                }

                break;

            case Order::ORDER_CANCELLED:

                if ($installment_plan = InstallmentPlan::where('order_id', $event->order->id)->first()) {
                    $installment_plan->update(['status' => 'cancelled']);
                }

                break;
        }
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\OrderCreated',
            'App\Listeners\OrderEventSubscriber@onOrderCreated'
        );

        $events->listen(
            'App\Events\OrderStatusUpdated',
            'App\Listeners\OrderEventSubscriber@onOrderStatusUpdated'
        );
    }
}
