<?php

namespace App;

class CustomerName
{
    protected $first_name;
    protected $last_name;

    private function __construct($first_name, $last_name)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
    }

    /**
     * Parse Customer Name from Address data
     *
     * @param array $address
     * @return static
     */
    public static function parseFromAddress(array $address)
    {
        if (!isset($address['last_name']) || !is_string($address['last_name']) || empty($address['last_name'])) {
            $fullname = explode(" ", $address['first_name']);
            $address['first_name'] = $fullname[0];
            $address['last_name'] = @$fullname[1];
        }

        return new static($address['first_name'], $address['last_name']);
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    private function splitFirstName($first_name)
    {
        return explode(' ', $first_name);
    }
}
