<?php

namespace App\Shop\Processes;

use App\Shop\Orders\InstallmentPlan;
use Illuminate\Support\Carbon;

final class ProcessInstallments
{
    public function run()
    {
        $this->installmentsToday()->each(function ($installment) {
            $installment->charge();
        });
    }

    public function installmentsToday()
    {
        $now = Carbon::now();

        return InstallmentPlan::has('account')->where('status', 'active')
            ->where('next_schedule_date', '<=', $now->format('Y-m-d'))
            ->incomplete()
            ->get();
    }
}
