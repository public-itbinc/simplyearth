<?php

namespace App\Shop\Discounts;

use App\Shop\Customers\Customer;
use App\Shop\Orders\Order;
use App\Shop\Products\Product;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Discount extends Model
{
    use Filterable;

    private $order;
    private $type_processor;
    private $discount_error_message = "Invalid code";
    protected $table = 'discounts';

    protected $dates = [
        'start_date',
        'end_date',
    ];

    protected $casts = [
        'options' => 'array',
    ];

    protected $fillable = [
        'code',
        'type',
        'options',
        'start_date',
        'end_date',
        'referral_clicks',
    ];

    protected $attributes = [
        'options' => "{}",
    ];

    protected $hidden = ['users'];

    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    public function compute()
    {
        $discount_total = 0;

        switch ($this->type) {
            case 'percentage':
                $discount_value = $this->options['discount_value'] ?? 0;
                $discount_total =
                    (float)$this->applicableOrderSubTotal() * ((float)$discount_value / 100);
                break;

            case 'fixed_amount':
                $discount_value = $this->options['discount_value'] ?? 0;
                $discount_total =
                    min((float)$this->applicableOrderSubTotal(), (float)$discount_value);
                break;

            case 'tiered_discount':
                if (
                    isset($this->options['tiered_discount']['tiers'])
                    && isset($this->options['tiered_discount']['type'])
                    && in_array($this->options['tiered_discount']['type'], ['percentage', 'fixed_amount'])
                ) {
                    $tiered_type = $this->options['tiered_discount']['type'];
                    $sub_total = (float)$this->applicableOrderSubTotal();
                    $tiered_min = 0;

                    foreach ($this->options['tiered_discount']['tiers'] as $tier) {

                        if ((float)$tier['min'] > $sub_total || $tiered_min > $tier['min']) {
                            continue;
                        }

                        $tiered_min = (float)$tier['min'];

                        if ($tiered_type == 'percentage') {
                            $tier_discount = $sub_total * ((float)$tier['discount_value'] / 100);
                        } else {
                            $tier_discount = min($sub_total, (float)$tier['discount_value']);
                        }

                        $discount_total = $tier_discount;
                    }
                }
                break;
        }

        return $discount_total;
    }

    public function applicableOrderSubTotal()
    {
        $applies_to = $this->options['applies_to'] ?? 'order';
        if ($applies_to == 'products') {
            return $this->order->getDiscountableSubTotal($this->options['products']);
        } elseif ($applies_to == 'categories') {
            //Get the product IDs under the specified categories
            $products = Product::select(['id'])->whereHas('categories', function ($query) {
                return $query->whereIn('id', $this->options['categories']);
            })->get();

            return $this->order->getDiscountableSubTotal($products->pluck(['id'])->toArray());
        }

        return $this->order->getDiscountableSubTotal();
    }

    public function productExists()
    {
        //Check if subscription, if it is, it should be applicable to other subscription product
        if (count($this->options['products']) == 1) {

            $subscription = Product::find($this->options['products'][0]);

            if ($subscription && $subscription->isSubscription() && $this->order->getProducts()->filter(function ($product) {
                return $product->type == 'subscription';
            })->count() > 0) {
                return true;
            }
        }

        foreach ($this->order->getProducts() as $product) {
            if (in_array($product->id, $this->options['products'] ?? [])) {
                return true;
            }
        }
        return false;
    }

    public function orderPassed()
    {

        //Active

        if (!$this->isActive()) {
            $this->discount_error_message = 'Code no longer available';
            return false;
        }

        if (
            $this->order->getCustomer()
            && $this->order->getCustomer()->isWholesaler() && !in_array('wholesale', $this->options['customer_tags'] ?? [])
        ) {
            $this->discount_error_message = 'Sorry, you are not eligible for this code';
            return false;
        }

        //If it is a referral coupon, check if the referrer still exists

        if ($this->isReferral()) {

            if (!$this->referrer) {
                $this->discount_error_message = 'Code no longer available';

                return false;
            }

            if (!$this->order->hasSubscriptionProduct()) {
                $this->discount_error_message = 'Products in your cart are not eligible for the coupon code';
                return false;
            }
        }

        //FREE FIRST MONTH

        if ($this->type == 'freefirstmonth') {
            if (!$this->order->hasCommitmentSubscription()) {
                $this->discount_error_message = 'Sorry, you are not eligible for this code';
                return false;
            }

            if (
                $this->order->getCustomer()
                && $this->order->getCustomer()->hasSubscription()
            ) {
                $this->discount_error_message = 'Sorry, you are not eligible for this code';
                return false;
            }
        }

        //Product exists

        $applies_to = $this->options['applies_to'] ?? 'order';

        if ($applies_to == 'products') {

            if (!$this->productExists()) {
                $this->discount_error_message = 'Products in your cart are not eligible for the coupon code';
                return false;
            }
        }

        //Usage Limits

        if (isset($this->options['usage_limits']) && !empty($this->options['usage_limits']) && $this->used >= $this->options['usage_limits']) {
            $this->discount_error_message = 'Code no longer available';
            return false;
        }

        //Customer Eligibility
        if (isset($this->options['customer_eligibility']) && $this->options['customer_eligibility'] == 'specific_groups') {
            if (!$this->order->getCustomer() || count($this->order->getCustomer()->tags()->whereIn('name', $this->options['customer_tags'])->get()) <= 0) {
                $this->discount_error_message = 'Sorry, you are not eligible for this code';
                return false;
            }
        }

        //PER CUSTOMER

        if (isset($this->options['per_customer']) && $this->options['per_customer'] == true && $this->order->getCustomer()) {
            if (count($this->users->where('id', $this->order->getCustomer()->id))) {
                $this->discount_error_message = 'Code can be used once per customer only';
                return false;
            }
        }

        //Minimum order

        if (isset($this->options['minimum_requirement'])) {
            if (
                $this->options['minimum_requirement'] == 'minimum_quantity_items'
                && $this->order->count() < $this->options['minimum_quantity_items']
            ) {
                $this->discount_error_message = sprintf(
                    'Code requires minimum of %d items',
                    $this->options['minimum_quantity_items']
                );
                return false;
            }
            if (
                $this->options['minimum_requirement'] == 'minimum_purchase_amount'
                && $this->order->getSubTotal() < $this->options['minimum_purchase_amount']
            ) {
                $this->discount_error_message = sprintf(
                    'Code requires %.2f minimum purchase',
                    $this->options['minimum_purchase_amount']
                );
                return false;
            }
        }

        //Active Dates

        if ($this->expired()) {
            $this->discount_error_message = 'Code not available at this time';
            return false;
        }

        return true;
    }

    public function isActive()
    {
        return $this->active ? true : false;
    }

    public function notExpired()
    {
        if (!is_null($this->start_date) && $this->start_date > Carbon::now()) {
            return false;
        }

        if (!is_null($this->end_date) && $this->end_date < Carbon::now()) {
            return false;
        }

        return true;
    }

    public function expired()
    {
        return !$this->notExpired();
    }

    public function isReferral()
    {
        return $this->type == 'referral';
    }

    public function getDiscountErrorMessage()
    {
        return $this->discount_error_message;
    }

    public function users()
    {
        return $this->belongsToMany(Customer::class);
    }

    public function referrer()
    {
        return $this->belongsTo(Customer::class, 'code', 'share_code');
    }

    public function referred_orders()
    {
        return $this->hasManyThrough(Order::class, Invitation::class, 'code', 'invitation_id', 'code', 'id')->whereNotIn('status', [
            ORDER::ORDER_CANCELLED,
            ORDER::ORDER_FAILED,
            ORDER::ORDER_REFUNDED,
            ORDER::ORDER_ON_HOLD,
            ORDER::ORDER_PENDING,
        ]);
    }

    public function isFreeFirstMonth()
    {
        return $this->type == 'freefirstmonth' || $this->isReferral();
    }
}
