<?php

namespace App\Shop\Discounts;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DiscountGenerator
{
    protected $count = 1;
    protected $data = [];

    /**
     * Set number of discounts to be generated
     *
     * @param integer $count
     * @return self
     */
    public function setCount(int $count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Set Discount data
     *
     * @param array $data
     * @return self
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * generate Discounts;
     *
     * @return string $filepath
     */
    public function generate()
    {
        $bulk_data = collect($this->data)->only(['type', 'options', 'start_date', 'end_date'])->toArray();

        $discount_array = [];

        for ($i = 0; $i < $this->count; $i++) {
            $discount_array[] = array_merge($bulk_data,
                ['code' => str_random(10), 'options' => json_encode($this->data['options'])]);
        }

        DB::transaction (function() use($discount_array){
            DB::table('discounts')->insert($discount_array);
        });

        $filename = Carbon::now()->format('U') . '-' . str_random(5) . '.csv';
        Storage::put('bulk-discounts/' . $filename, implode(PHP_EOL, collect($discount_array)->pluck(['code'])->toArray()));

        return $filename;
    }
}
