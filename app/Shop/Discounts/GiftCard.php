<?php

namespace App\Shop\Discounts;

use App\Shop\Customers\Customer;
use App\Shop\Orders\OrderItem;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Filterable;

class GiftCard extends Model
{
    use Filterable;
    
    protected $casts = [
        'details',
    ];

    protected $appends = ['masked_code'];

    protected $fillable = [
        'token',
        'code',
        'remaining',
        'customer_id',
        'details',
        'initial_value'
    ];

    protected $hidden = ['token', 'code'];

    public function owner()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function getMaskedCodeAttribute()
    {
        return '****-****-****-' . substr($this->code, -4);
    }
}
