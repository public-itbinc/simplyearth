<?php

namespace App\Shop\Discounts;

class GiftCardGenerator
{
    private static $length = 16;

    /**
     * Generate code.
     *
     * @return string
     */
    public static function generateCode()
    {
        $characters = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $mask = '****-****-****-****';
        $promocode = '';
        $random = [];
        for ($i = 1; $i <= self::$length; $i++) {
            $character = $characters[rand(0, strlen($characters) - 1)];
            $random[] = $character;
        }
        shuffle($random);
        $length = count($random);
        for ($i = 0; $i < $length; $i++) {
            $mask = preg_replace('/\*/', $random[$i], $mask, 1);
        }
        $promocode .= $mask;
        return $promocode;
    }
}
