<?php

namespace App\Shop\Settings;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['option', 'value'];

    function setValueAttribute($value)
    {
        $this->attributes['value'] = serialize($value);
    }

    function getValueAttribute($original)
    {
        return @unserialize($original);
    }
}
