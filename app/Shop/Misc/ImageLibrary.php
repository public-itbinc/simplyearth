<?php

namespace App\Shop\Misc;

use App\Traits\HasUniqueSlug;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

class ImageLibrary extends Model implements HasMediaConversions
{
    use HasUniqueSlug, HasMediaTrait;

    protected $fillable = [
        'name',
    ];

    public static function boot()
    {
        parent::boot();

        static::processSlug();
    }

    /**
     * Register media conversions e.g. thumb
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('large')
            ->width(865)
            ->nonQueued();

        $this->addMediaConversion('cover')
            ->fit('crop', 640, 640)
            ->nonQueued();

        $this->addMediaConversion('medium')
            ->fit('crop', 255, 255)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->fit('crop', 150, 150)
            ->nonQueued();
    }

    public function scopeDefault($query)
    {
        return $query->where('slug', 'default');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('images');

            $this
            ->addMediaCollection('avatar');
    }
}
