<?php

namespace App\Shop\Misc;

use Illuminate\Support\Carbon;


class DateTimeUtility extends Carbon
{
    public static function tzEndDay($date, $timezone = 'UTC')
    {
        return (new static(substr($date, 0, 10).' 23:59:59', new \DateTimeZone($timezone)))->setTimezone('UTC');
    }

    public static function tzStartDay($date, $timezone = 'UTC')
    {
        return (new static(substr($date, 0, 10).' 00:00:00', new \DateTimeZone($timezone)))->setTimezone('UTC');
    }
}