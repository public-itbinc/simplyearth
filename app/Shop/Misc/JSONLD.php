<?php 

namespace App\Shop\Misc;

use App\Shop\Products\ProductInterface;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\Offer;


class JSONLD
{
    protected $item;
    protected $type;

    private function __construct($item, $type)
    {
        $this->item = $item;
        $this->type = $type;
    }

    /**
     * Product type JSON-LD
     *
     * @param ProductInterface $product
     * @return static
     */
    public static function product(ProductInterface $product)
    {
        $product = new static($product, 'product');

        return $product;
    }

    public function generate()
    {
        return call_user_func(array($this, 'generate'.studly_case($this->type)));
    }

    protected function generateProduct()
    {
        $schema = Schema::product();
        $schema->name($this->item->name)
        ->description($this->item->short_description)
        ->image(\URL::asset($this->item->cover))
        ->brand($this->brand())
        ->offers([
            '@type' => 'Offer',
            'price' => $this->item->price,
            'priceCurrency' => 'USD',
            'itemCondition' => Schema::offerItemCondition()::NewCondition
        ]);
        
        return $schema;
    }

    protected function brand()
    {
        
        $brand = Schema::thing();
        $brand->url(url('/'))
            ->name('Simply Earth');

        return $brand;
    }
}