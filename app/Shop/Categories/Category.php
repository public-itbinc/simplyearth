<?php

namespace App\Shop\Categories;

use App\BasicModel as Model;
use App\Shop\Products\Product;
use App\Traits\HasImages;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

class Category extends Model implements HasMediaConversions
{

    use HasMediaTrait, HasImages;

    protected $appends = ['cover'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'cover',
    ];

    public function getCoverAttribute()
    {
        return $this->image('cover');
    }

    public function editLink()
    {
        return sprintf('/admin/categories/%d/edit', $this->id);
    }

    public function link()
    {
        if($this->slug == 'monthly-recipe-box'){
            return route('subscription');
        }

        return sprintf('/categories/%s', $this->slug);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    /**
     * Register media conversions e.g. thumb
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('cover')
            ->fit('crop', 640, 640)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->fit('crop', 150, 150)
            ->nonQueued();
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function scopeParents($query)
    {
        return $query->where('parent_id', 0);
    }
}
