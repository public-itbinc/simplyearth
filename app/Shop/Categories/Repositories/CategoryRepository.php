<?php

namespace App\Shop\Categories\Repositories;

use App\Shop\Base\BaseRepository;
use App\Shop\Categories\Category;
use App\Shop\Categories\Exceptions\CategoryInvalidArgumentException;
use App\Shop\Categories\Exceptions\CategoryNotFoundException;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface, HasMediaConversions
{

    use HasMediaTrait;
    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        parent::__construct($category);
        $this->model = $category;
    }

    /**
     * List all the categories
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listCategories(string $order = 'id', string $sort = 'desc', $except = [])
    {
        return $this->model->orderBy($order, $sort)->with(['media'])->get()->except($except);
    }

    /**
     * Create the category
     *
     * @param array $params
     * @return Category
     * @throws CategoryInvalidArgumentException
     */
    public function createCategory(array $params) : Category
    {
        try {
            $collection = collect($params);
            if (!isset($params['slug']) && isset($params['name'])) {
                $slug = str_slug($params['name']);
            }

            if (request()->hasFile('cover')) {
                /*$cover = $this->uploadOne(
                    request()->file('cover', 'products'),
                    'categories'
                );*/
                //@TODO media library
            }

            $merge = $collection->merge(compact('slug'));

            $category = new Category($merge->all());

            if (isset($params['parent'])) {
                $parent = $this->findCategoryById($params['parent']);
                $category->parent()->associate($parent);
            }

            $category->save();
            return $category;
        } catch (QueryException $e) {
            throw new CategoryInvalidArgumentException($e->getMessage());
        }
    }

    /**
     * Update the category
     *
     * @param array $params
     * @return Category
     */
    public function updateCategory(array $params) : Category
    {
        $category = $this->findCategoryById($this->model->id);
        $collection = collect($params)->except('_token');
        if (!isset($params['slug']) && isset($params['name'])) {
            $slug = str_slug($params['name']);
        }

        if (request()->hasFile('cover')) {
            /*$cover = $this->uploadOne(
                request()->file('cover', 'products'),
                'categories'
            );*/

            //@TODO medialibrary
        }

        $merge = $collection->merge(compact('slug'));
        if (isset($params['parent'])) {
            $parent = $this->findCategoryById($params['parent']);
            $category->parent()->associate($parent);
        }

        $category->update($merge->all());
        return $category;
    }

    /**
     * @param int $id
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function findCategoryById(int $id) : Category
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CategoryNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a category
     *
     * @return bool
     */
    public function deleteCategory() : bool
    {
        return $this->model->delete();
    }

    /**
     * Associate a product in a category
     *
     * @param Product $product
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function associateProduct(Product $product)
    {
        return $this->model->products()->save($product);
    }

    /**
     * Return all the products associated with the category
     *
     * @return mixed
     */
    public function findProducts()
    {
        //@TODO find products
        
        return;
        return collect($this->model->products)->map(function (Product $product) {
            return $this->transformProduct($product);
        })->sortByDesc('id');
    }

    /**
     * @param array $params
     */
    public function syncProducts(array $params)
    {
        $this->model->products()->sync($params);
    }


    /**
     * Detach the association of the product
     *
     */
    public function detachProducts()
    {
        $this->model->products()->detach();
    }

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        //@TODO medialibrary
        return '';
        return $this->update(['cover' => null], $file['category']);
    }

    /**
     * Return the category by using the slug as the parameter
     *
     * @param array $slug
     * @return Category
     */
    public function findCategoryBySlug(array $slug) : Category
    {
        try {
            return $this->findOneByOrFail($slug);
        } catch (ModelNotFoundException $e) {
            throw new CategoryNotFoundException($e->getMessage());
        }
    }

    /**
     * Find products under a specific category
     *
     * @param int $id
     * @return mixed
     */
    public function findProductsInCategory(int $id)
    {
        $category = $this->findCategoryById($id);
        return $category->products;
    }

    /**
     * @return mixed
     */
    public function findParentCategory()
    {
        return $this->model->parent;
    }

    /**
     * @return mixed
     */
    public function findChildren()
    {
        return $this->model->children;
    }


    /**
     * Register media conversions e.g. thumb
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('cover')
            ->fit('crop', 640, 640)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->fit('crop', 150, 150)
            ->nonQueued();
    }
}
