<?php

namespace App\Shop\History;

use App\Shop\Customers\Customer;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Shop\Products\Product;

class History extends Model
{
    protected $table = 'history';
    protected $fillable = [
        'model_type',
        'model_id',
        'type',
        'data',
    ];

    protected $appends = ['summary', 'fromnow'];

    protected $casts = [
        'data' => 'array',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($history) {

            if (Auth::guard('web')->check()) {
                $history->initiator = Auth::guard('web')->id();
                $history->initiator_type = 'admin';
            } elseif ($customer = customer()) {
                $history->initiator_type = 'customer';
            } else {
                $history->initiator_type = 'system';
            }

        });
    }

    public function getSummaryAttribute()
    {
        switch ($this->type) {
            case 'subscription_created':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Customer subscribed to plan %s', $this->data['plan']);
                } else {
                    $summary = sprintf('Subscription created with plan %s', $this->data['plan']);
                }
                break;

            case 'subscription_stopped':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Subscription canceled by customer (%s)', @$this->data['reason']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Subscription canceled by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Subscription stopped by system (%s)', @$this->data['reason']);
                }
                break;

            case 'subscription_resumed':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Subscription resumed by customer');
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Subscription resumed by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Subscription resumed by system');
                }
                break;

            case 'box_skipped':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box %s skipped by customer', $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box %s skipped by %s', $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Box %s skipped by system', $this->data['month_key']);
                }
                break;

            case 'box_gifted':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box %s gifted by customer', $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box %s gifted by %s', $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Box %s gifted by system', $this->data['month_key']);
                }
                break;

            case 'box_updated_gift':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box %s updated gift data by customer', $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box %s updated gift data by %s', $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Box %s updated gift data by system', $this->data['month_key']);
                }
                break;

            case 'box_deleted_gift':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box %s canceled gift by customer', $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box %s canceled gift by %s', $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Box %s canceled gift by system', $this->data['month_key']);
                }
                break;

            case 'box_resumed':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box %s resumed by customer', $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box %s resumed by %s', $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Box %s resumed by system', $this->data['month_key']);
                }
                break;

            case 'box_continue':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box skipped until %s by customer', $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box skipped until %s by %s', $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Box skipped until %s by system', $this->data['month_key']);
                }
                break;

            case 'box_updated_exchanges':

                if ($this->data['exchange_type'] == 'delete') {
                    if ($this->isTypeCustomer()) {
                        $summary = sprintf('Box %s: removed exchange for product #%s by customer', $this->data['month_key'], $this->data['from_id']);
                    } elseif ($this->isTypeAdmin()) {
                        $summary = sprintf('Box %s: removed exchange for product #%s by %s', $this->data['month_key'], $this->data['from_id'], $this->admin_name);
                    } else {
                        $summary = sprintf('Box %s: removed exchange for product #%s by system', $this->data['month_key'], $this->data['from_id']);
                    }
                } else {
                    if ($this->isTypeCustomer()) {
                        $summary = sprintf('Box %s: Exchange product #%s with #%s by customer', $this->data['month_key'], $this->data['from_id'], $this->data['product_id']);
                    } elseif ($this->isTypeAdmin()) {
                        $summary = sprintf('Box %s: Exchange product #%s with #%s by %s', $this->data['month_key'], $this->data['from_id'], $this->data['product_id'], $this->admin_name);
                    } else {
                        $summary = sprintf('Box %s: Exchange product #%s with #%s by system', $this->data['month_key'], $this->data['from_id'], $this->data['product_id']);
                    }
                }

                break;

            case 'box_updated_addons':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Box %s: Updated addons - product %s qty %d by customer', $this->data['month_key'], $this->parseProductName($this->data['product_id']), $this->data['qty']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Box %s: Updated addons - product %s qty %d by %s', $this->data['month_key'], $this->parseProductName($this->data['product_id']), $this->data['qty'], $this->admin_name);
                } else {
                    $summary = sprintf('Box %s: Updated addons - product %s qty %d by system', $this->data['month_key'], $this->parseProductName($this->data['product_id']), $this->data['qty']);
                }

                break;

            case 'subscription_updated_schedule':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Updated subscription schedule to %d by customer', $this->data['schedule']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Updated subscription schedule to %d by %s', $this->data['schedule'], $this->admin_name);
                } else {
                    $summary = sprintf('Updated subscription box schedule to %d by system', $this->data['schedule']);
                }
                break;

            case 'subscription_updated_plan':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Updated subscription plan to %s by customer', $this->data['plan']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Updated subscription plan to %s by %s', $this->data['plan'], $this->admin_name);
                } else {
                    $summary = sprintf('Updated subscription plan to %s by system', $this->data['plan']);
                }
                break;

            case 'subscription_override_schedule':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Override schedule to %d by customer', $this->data['schedule']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Override schedule to %d by %s', $this->data['schedule'], $this->admin_name);
                } else {
                    $summary = sprintf('Override schedule to %d by system', $this->data['schedule']);
                }
                break;

            case 'box_applied_giftcard':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Gift card with ending %s applied to Box %s by customer', substr($this->data['code'], -4), $this->data['month_key']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Gift card with ending %s applied to Box %s by %s', substr($this->data['code'], -4), $this->data['month_key'], $this->admin_name);
                } else {
                    $summary = sprintf('Gift card with ending %s applied to Box %s by system', substr($this->data['code'], -4), $this->data['month_key']);
                }
                break;

            case 'box_removed_giftcard':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Gift card with ending %s was unapplied', substr($this->data['code'], -4));
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Gift card with ending %s was unapplied', substr($this->data['code'], -4), $this->admin_name);
                } else {
                    $summary = sprintf('Gift card with ending %s was unapplied', substr($this->data['code'], -4));
                }
                break;

            case 'updated_shipping_address':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Customer updated shipping address', json_encode($this->data['shipping_address']));
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('%s updated shipping address', $this->admin_name, json_encode($this->data['shipping_address']));
                } else {
                    $summary = sprintf('Updated shipping address', json_encode($this->data['shipping_address']));
                }
                break;

            case 'updated_email':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Customer updated email to %s', $this->data['email']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('%s updated email to %s', $this->admin_name, $this->data['email']);
                } else {
                    $summary = sprintf('Updated email to %s', $this->data['email']);
                }
                break;

            case 'updated_payment_method':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Customer updated payment method');
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('%s updated payment method', $this->admin_name);
                } else {
                    $summary = sprintf('Updated payment method');
                }
                break;

            case 'updated_password':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Password updated by customer');
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Password updated by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Password updated');
                }
                break;

            case 'deleted_address':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Address deleted by customer');
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Address deleted by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Address deleted');
                }
                break;

            case 'updated_address':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Address updated by customer');
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Address updated by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Address updated');
                }
                break;

            case 'updated_tags':

                if ($this->isTypeCustomer()) {
                    $summary = sprintf('Updated tags (%s) by customer', $this->data['tags']);
                } elseif ($this->isTypeAdmin()) {
                    $summary = sprintf('Updated tags (%s) by %s', $this->data['tags'], $this->admin_name);
                } else {
                    $summary = sprintf('Updated tags (%s) ', $this->data['tags']);
                }
                break;

            case 'sent_invitation':

                if ($this->isTypeAdmin()) {
                    $summary = sprintf('Invitation sent by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Invitation sent');
                }
                break;

            case 'account_created':

                if ($this->isTypeAdmin()) {
                    $summary = sprintf('Account created by %s', $this->admin_name);
                } else {
                    $summary = sprintf('Account created');
                }
                break;

            case 'box_processed':

                $summary = sprintf('Box %s processed #%s', $this->data['month_key'], $this->data['order_id']);
                break;

            case 'box_charge_failed':

                $summary = sprintf('Box %s charge failed', $this->data['month_key']);
                break;

            default:
                $summary = sprintf('Type: %s - Data: %s', $this->type, json_encode($this->data));
                break;
        }

        return $summary;
    }

    public function isTypeCustomer()
    {
        return $this->initiator_type == 'customer';
    }

    public function isTypeAdmin()
    {
        return $this->initiator_type == 'admin';
    }

    public function getAdminNameAttribute()
    {
        if (!$this->isTypeAdmin()) {
            return '';
        }

        if (!$this->person) {
            return 'Shop admin';
        }

        return $this->person->name;
    }

    public function person()
    {
        if ($this->initiator_type == 'admin') {
            return $this->hasOne(User::class, 'id', 'initiator');
        } elseif ($this->initiator_type == 'customer') {
            return $this->hasOne(Customer::class, 'id', 'initiator');
        }

        return null;
    }

    public function getFromnowAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    protected function parseProductName($id)
    {
        $product = Product::find($id);

        return $product ? $product->name : '#'.$id;
    }
}
