<?php

namespace App\Shop\Tax;

use Illuminate\Support\Facades\Cache;
use TaxJar\Client;

class TaxCalculator
{

    protected $client;

    protected $store_address;

    protected $location_tax = [];

    public function __construct()
    {
        $this->client = Client::withApiKey(config('app.tax_jar_token'));

        $this->store_address = app('settings')['store_address'] ?? [];
    }

    public function getTaxByLocation($from_country, $from_zip = null, $from_region = null)
    {
        $location_string = 'tax_' . $from_country . '_' . $from_zip . '_' . $from_region;
        if (!isset($this->location_tax[$location_string])) {
            $this->location_tax[$location_string] = Cache::remember($location_string, 60 * 24, function () use ($from_country, $from_zip, $from_region) {
                try {
                    return $this->getTaxFromTaxjar($from_country, $from_zip, $from_region);
                } catch (\Throwable $e) {
                    return $this->getTaxManually($from_country, $from_zip, $from_region);
                }
            });
        }

        return $this->location_tax[$location_string];
    }

    protected function getTaxFromTaxjar($from_country = null, $from_zip = null, $from_region = null)
    {
        $order_taxes = $this->client->taxForOrder([
            'from_country' => $this->store_address['country'],
            'from_zip' => $this->store_address['zip'],
            'from_state' => $this->store_address['region'],
            'to_country' => $from_country,
            'to_zip' => $from_zip,
            'to_state' => $from_region,
            'amount' => 100,
            'shipping' => 0,
            'line_items' => [
                [
                    'id' => '1',
                    'quantity' => 1,
                    'unit_price' => 100,
                ],
            ],
        ]);

        return $order_taxes->rate;
    }

    protected function getTaxManually($from_country = null, $from_zip = null, $from_region = null)
    {
        if ($from_region == 'WI' && $from_country == 'US') {
            return tax();
        }

        return 0;
    }

    public function client()
    {
        return $this->client;
    }
}
