<?php

namespace App\Shop\ShoppingBoxes;

use App\Events\OrderCreated;
use App\Shop\Customers\Account;
use App\Shop\Customers\Customer;
use App\Shop\Orders\OrderBuilder;
use App\Shop\Products\Product;
use App\Shop\Shipping\ShippingZone;
use App\Shop\Subscriptions\FutureOrderMonth;
use App\Shop\Subscriptions\SubscriptionGiftcard;

class ShoppingBoxBuilder extends OrderBuilder
{
    protected $box;
    protected $subscription;
    protected $account;
    static $available_shipping_methods = [];
    protected $country;

    /**
     * ShoppingBox Builder
     *
     * @param FutureOrderMonth $box
     */

    public function __construct(FutureOrderMonth $box)
    {
        $this->is_checkout = false;
        $this->box = $box;
        $this->subscription = $this->box->getSubscription();
        $this->account = $this->subscription->owner;
        $this->country = $this->account->customer->default_address ? $this->account->customer->default_address->country : config('app.country');
        parent::__construct([], $this->account->customer, ['order_name' => $this->account->customer->id.time()]);
        $this->initProducts();
        $this->bonuses = $this->box->getBonus();
        $this->autoSetShipping();
        if ($this->box->discount) {
            $this->discount_override = collect([
                'reason' => $this->box->discount->code,
            ])->merge($this->box->discount);
        }
    }

    public function getTotalShipping()
    {
        if (!$this->shipping_method) {
            return 0;
        }

        if ($this->isFreeShipping()) {
            return 0;
        }

        return $this->shipping_method->rate;
    }

    /**
     * Get Discountable Sub Total
     *
     * @param array $only
     * @return float
     */
    public function getDiscountableSubTotal($only = null)
    {
        $discount_total = 0.00;

        foreach ($this->box->getAddons() as $addon) {
            if (!$addon->product || (isset($only) && !in_array($addon->id, $only))) {
                continue;
            }
            $discount_total += $addon->product->price * max(1, abs($addon->qty));
        }

        return $discount_total;
    }

    public function applyDiscount(string $code, $checkDiscountEligibility = true)
    {
        parent::applyDiscount($code, $checkDiscountEligibility);

        if ($this->discount_applied) {
            $normal_discount = $this->subscription->subscriptionNormalDiscounts()->firstOrNew([]);
            $normal_discount->code = $code;
            $normal_discount->save();

            return $this;
        }

        $this->subscription->subscriptionNormalDiscounts()->delete(); //Delete subscription discount if any

        return $this;
    }

    public function deleteDiscount()
    {
        parent::deleteDiscount();

        $this->subscription->subscriptionNormalDiscounts()->delete(); //Delete subscription discount if any

        return $this;
    }

    public function getDiscountTotal()
    {
        $discount_total = 0; 
        
        if ($this->box->isFree()) {
            $discount_total = $this->products->reduce(function ($carry, $item) {
                return ($item->type == 'subscription') ? $carry + $item->price : $carry;
            });
        }
        
        $discount_total += parent::getDiscountTotal() + $this->box->getGiftCardDiscountTotal();

        return $discount_total;
    }

    public function getDiscountDetails()
    {
        $discount_details = parent::getDiscountDetails();

        if ($this->box->hasGiftCardApplied()) {
            $discount_details['gift_card'] = [
                'code' => $this->box->getGiftCardMaskedCode(),
            ];
        }

        return $discount_details;
    }

    public function autoSetShipping()
    {
        $available = $this->availableShippingMethods();

        if (count($available)) {
            //Check if there is free
            if ($free = $available->firstWhere('is_free', 1)) {
                $this->setShippingMethod($free);
            } else {
                $this->setShippingMethod($available->first());
            }

        } else {
            $this->setShippingMethod(null);
        }
        return $this;
    }

    public function availableShippingMethods()
    {
        if (!isset(self::$available_shipping_methods[$this->country])) {

            self::$available_shipping_methods[$this->country] = ShippingZone::where('countries', 'LIKE', '%"' . $this->country . '"%')
                ->first();
            self::$available_shipping_methods[$this->country] = ShippingZone::where('countries', 'LIKE', '%"' . $this->country . '"%')
                ->orWHere('countries', '["*"]')
                ->orderBy('countries', 'DESC')->first();
        }

        if (!self::$available_shipping_methods[$this->country]) {
            return collect([]);
        }

        return self::$available_shipping_methods[$this->country]->getAvailableShipping($this);
    }

    public function build()
    {
        $order = parent::build();

        if ($order) {

            if ($this->box->gifted()) {
                $order->gift = true;
                $order->gift_message = $this->box->gift->message;
                $order->gift_details = $this->box->gift;
            }

            $order->save();

            if ($this->box->hasGiftCardApplied()) {
                $subscription_giftcard = SubscriptionGiftcard::find($this->box->discount_giftcard['id']);
                if ($subscription_giftcard) {
                    $subscription_giftcard->amount -= (float) $this->box->discount_giftcard['amount'];
                    $subscription_giftcard->save();
                }
            }

            if ($this->box->discount) {
                $this->box->discount->decrement('limit');
            }

            if ($this->subscription->subscriptionNormalDiscounts) {
                $this->subscription->subscriptionNormalDiscounts()->delete();
            }

            $this->subscription->maybeIncrementCommitment($order);

            event(new OrderCreated($order));
        }

        return $order;
    }

    public function getShippingAddress()
    {
        if ($this->box->gifted()) {
            return [
                'first_name' => $this->box->gift->address_first_name,
                'last_name' => $this->box->gift->address_last_name,
                'phone' => $this->box->gift->address_phone,
                'company' => $this->box->gift->address_company,
                'address1' => $this->box->gift->address_address1,
                'address2' => $this->box->gift->address_address2,
                'city' => $this->box->gift->address_city,
                'zip' => $this->box->gift->address_zip,
                'region' => $this->box->gift->address_region,
                'country' => $this->box->gift->address_country,
            ];
        }

        return parent::getShippingAddress();
    }

    public function getSubscriptionBoxKey()
    {
        return $this->box->month_key;
    }

    protected function initProducts()
    {
        $products = collect();

        $subscription_product = $this->box->getSubscription()->product;

        $products->push((object) array_merge($subscription_product->only(
            $this->fields
        ), ['qty' => max($this->box->getSubscription()->quantity, 1)]));

        foreach ($this->box->getExchanges() as $exchange) {

            $exchange->product->price = config('subscription.exchange_price', 2);
            $exchange->product->wholesale_price = config('subscription.exchange_price', 2);

            $products->push((object) array_merge($exchange->product->only(
                $this->fields
            ), ['qty' => 1]));

            $this->addNote('Exchange ' . $exchange->original->name . ' with ' . $exchange->product->name);
        }

        foreach ($this->box->getAddons() as $add_on) {
            if (!$add_on->product) {
                continue;
            }

            $products->push((object) array_merge($add_on->product->only(
                $this->fields
            ), ['qty' => max($add_on->qty, 1)]));
        }

        $this->products = $products;
    }

    public function checkDiscount()
    {
        return;
    }
}
