<?php

namespace App\Shop\ShoppingBoxes;
use App\Shop\ShoppingBoxes\ShoppingBoxBuilder;

class FutureBoxDetail
{
    protected $box;

    public function __construct($box)
    {
        $this->box = $box;
    }

    public function toArray()
    {
        $currency = currency();

        return [
            'date' => $this->box->date->format('F j, Y'),
            'date_zone' => dateToW3cString($this->box->date),
            'gift' => $this->box->gift ? array_merge(
                $this->box->gift->toArray(),
                [
                    'gift_date' => dateToW3cString($this->box->gift->gift_date),
                    'email_date' => dateToW3cString($this->box->gift->email_date),
                ]
            ) : [
                'gift_date' => dateToW3cString($this->box->date),
            ],
            'addons' => $this->box->getAddons(),
            'cover' => $this->box->shopping_box->cover,
            'name' => $this->box->name,
            'skipped' => $this->box->skipped(),
            'monthKey' => $this->box->monthKey,
            'gifted' => $this->box->gifted(),
            'price' => $this->box->total_string,
            'status' => $this->box->status,
            'monthcount' => $this->box->months,
            'bonus' => $this->box->getBonus(),
            'total' => sprintf('%s%.2f', $currency['symbol'], $this->box->getBuilder()->getGrandTotal()),
            'discount' => $this->box->discount,
            'is_free' => $this->box->isFree(),
            'free_boxes_count' => customer()->unclaimed_free_boxes_count,
            'commitment' => $this->box->getCommitment(),
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
