<?php

namespace App\Shop\ShoppingBoxes;

use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;
use App\Shop\Reports\MonthlyBox;
use Illuminate\Support\Facades\Cache;

class ShoppingBox extends Model
{

    protected $table = 'shopping_boxes';

    protected $fillable = [
        'name',
        'key',
        'cover',
        'description',
        'recipes',
        'features',
        'box_video_cover',
        'box_video_id',
        'stock',
    ];

    protected $casts = ['features' => 'array'];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function simple()
    {
        return $this->belongsToMany(Product::class)->only(['id','name','cover']);
    }

    public static function getByKey($slug, $with = [])
    {
        return ShoppingBox::with($with)->where('key', str_slug($slug))->first();
    }

    public function getRecipesAttribute($value)
    {
        return collect($value ? json_decode($value) : []);
    }

    public function getTotalPriceAttribute()
    {
        return $this->products->sum('price');
    }

    public function getRemainingBoxesAttribute()
    {
        $box_key = ucwords(str_replace('-', ' ', $this->key));

        return Cache::remember('month-remaining-boxes-'.$box_key, 60, function () use ($box_key) {

            return (new MonthlyBox($box_key))->getRemainingBoxes()->count();         

        });
    }
    
    /**
     * Decrement the stock availability
     *
     * @return static
     */
    public function decrementStock()
    {
        if (!is_null($this->stock)) {
            $this->decrement('stock');
        }

        return $this;
    }

    /**
     * Checks if the shopping box availability
     *
     * @return boolean
     */
    public function available()
    {
        return is_null($this->stock) || $this->stock > 0;
    }
}
