<?php

namespace App\Shop\Products;

use App\Shop\Products\Product;
use Illuminate\Support\Facades\DB;


class ProductProcessor
{
    public static function calculateTotalSales()
    {
        $products = Product::all();

        foreach($products as $product)
        {
            $product->calculateSales()->save();
        }
    }
}