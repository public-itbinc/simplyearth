<?php

namespace App\Shop\Products;

use App\BasicModel;
use App\Http\Resources\ProductResourceCollection;
use App\Shop\Categories\Category;
use App\Shop\Orders\Order;
use App\Shop\Orders\OrderItem;
use App\Shop\Tags\Tag;
use App\Shop\Recipes\Recipe;
use App\Traits\Filterable;
use App\Traits\HasDecorations;
use App\Traits\HasImages;
use App\Traits\HasSubscriptions;
use App\Traits\HasVariants;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Kodeine\Metable\Metable;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;
use Illuminate\Support\Facades\DB;
use App\Traits\HasPlans;

class Product extends BasicModel implements Buyable, HasMediaConversions, ProductInterface
{

    protected $table = 'products';
    protected $keyType = 'double';

    use Metable {
        setMeta as protected traitSetMeta;
    }

    use HasVariants;

    use Searchable;

    use HasMediaTrait, HasImages, HasDecorations, Filterable, HasSubscriptions, HasPlans;

    static $monthly_subscription_id;

    protected $metaTable = 'products_meta';

    protected $appends = ['currency', 'link', 'cover', 'priceString', 'review_id'];

    protected $attributes = [
        'shipping' => 0,
    ];
    private static $validStatuses = [
        'active' => 'Active',
        'draft' => 'Draft',
    ];

    protected $metaFillable = [
        'quality_certification',
        'quality_certification_title',
        'quality_certification_image',
        'show_recipe',
        'recipe_title',
        'recipe',
        'subscription_months',
    ];

    protected $fillable = [
        'slug',
        'sku',
        'name',
        'short_description',
        'description',
        'quantity',
        'price',
        'wholesale_pricing',
        'wholesale_price',
        'status',
        'shipping',
        'weight',
        'sort_order',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'type',
        'setup',
        'variant_attributes',
        'variant_cover',
        'extra_attributes',
        'parent_id',
        'related_products',
        'available_plans',
        'recipe_id'
    ];

    protected $hidden = ['media', 'metas'];

    protected $casts = [
        'extra_attributes' => 'array',
        'wholesale_pricing' => 'boolean',
        'id' => 'float',
        'related_products' => 'array',
        'available_plans' => 'array',
    ];

    protected $with = ['metas', 'media'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($product) {

            $product->slug = $product->generateSlug();

        });

        static::updating(function ($product) {

            $product->slug = $product->generateSlug(true);

        });

    }

    public function isActive()
    {
        return $this->status == 'active';
    }

    public function isVariant()
    {
        return $this->type == 'variant';
    }

    public function isSubscription()
    {
        return $this->type == 'subscription';
    }

    public function isGiftCard()
    {
        return $this->type == 'gift_card' || ($this->parent && $this->parent->type == 'gift_card');
    }

    public function getIdAttribute($value)
    {
        return sprintf('%.0f', $value);
    }

    public function generateSlug($exclude = false)
    {
        $slug = str_slug(!empty($this->slug) ? $this->slug : $this->name);

        // check to see if any other slugs exist that are the same & count them
        $builder = static::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'");

        if ($exclude) {
            $builder->where('id', '<>', $this->id); //->where('type', '!=', 'variant');
        }

        $count = $builder->count();

        // if other slugs exist that are the same, append the count to the slug
        return $count ? "{$slug}-{$count}" : $slug;
    }

    public function getBuyableDescription($options = null): string
    {
        return $this->name;
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyablePrice($options = null): float
    {
        return $this->price;
    }

    public static function statuses()
    {
        return self::$validStatuses;
    }

    /**
     * Get the fillable meta attributes of a given array.
     *
     * @param  array  $attributes
     * @return array
     */
    protected function metaFillableFromArray(array $attributes)
    {
        if (count($this->getMetaFillable()) > 0) {
            return array_intersect_key($attributes, array_flip($this->getMetaFillable()));
        }

        return $attributes;
    }

    protected function getMetaFillable()
    {
        return $this->metaFillable;
    }

    /**
     * Set Product Meta
     * @param array $array
     */
    public function setMeta($key, $value = null, $skip = false)
    {

        if (is_string($key)) {
            $key = [$key => $value];
        }

        if ($skip) {
            $this->traitSetMeta($key);
        }

        $this->traitSetMeta($this->metaFillableFromArray($key));
    }

    public function getWholesalePriceStringAttribute()
    {
        return sprintf('%s%s', config('cart.currency')['symbol'], $this->wholesale_price);
    }

    public function getPriceStringAttribute()
    {
        $currency = config('cart.currency')['symbol'];

        $priceString = '';

        switch ($this->type) {
            case 'subscription':
                $months = max(1, (int) $this->subscription_months);

                if ($months == 1) {
                    $priceString .= sprintf('%s%s/month', $currency, $this->price);
                } else {
                    $priceString .= sprintf('%s%s/every %d months', $currency, $this->price, $months);
                }

                break;

            default:
                $priceString .= $currency . $this->price;
                break;
        }

        return $priceString;
    }

    public function getPriceStringSimpleAttribute()
    {
        return config('cart.currency')['symbol'] . $this->price;
    }

    public function getCurrencyAttribute()
    {
        return config('cart.currency');
    }

    /**
     * Register media conversions e.g. thumb
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('product')
            ->fit('crop', 716, 405)
            ->nonQueued();

        $this->addMediaConversion('cover')
            ->fit('crop', 640, 640)
            ->nonQueued();

        $this->addMediaConversion('medium')
            ->fit('crop', 255, 255)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->fit('crop', 150, 150)
            ->nonQueued();
    }

    public function editLink()
    {
        return sprintf('/admin/products/%s/edit', $this->id);
    }

    public function link()
    {
        return route('products.show', ['param' => $this->slug]);
    }

    public function getLinkAttribute()
    {
        return $this->link();
    }

    public function getCoverAttribute()
    {
        if ($this->type == 'variant') {
            return $this->variant_cover ?? $this->parent->cover ?? null;
        }

        return $this->image('medium');
    }

    public function getCoverLargeAttribute()
    {
        return $this->image('cover');
    }

    public function getSubscriptionMonthsAttribute()
    {
        return max(1, (int) $this->getMeta('subscription_months'));
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public static function getBySKU($sku)
    {
        return self::where('sku', $sku)->first();
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeExcludeSpecials($query)
    {
        return $query->whereNotIn('sku', [
            config('subscription.bonus_box'),
            config('subscription.onetime_box'),
        ]);
    }

    public function scopeLimitByTags($query)
    {
        $tags = [];

        if ($customer = customer()) {
            $tags = $customer->tags->map(function ($tag) {
                return $tag['name'];
            });
        }

        return $query->whereDoesntHave('tags')->OrWhereHas('tags', function ($r) use ($tags) {
            $r->whereIn('name', $tags);
        });
    }

    public function scopePopular($query)
    {
        return $query->orderBy('total_sales', 'desc');
    }

    public function canAccessByTags()
    {
        //IF the product doesnt have any tags, it means everyone can access it
        if (!$this->tags->count()) {
            return true;
        }

        $customer = customer();

        //If the user is not loggedin OR
        //If user doesnt have a tag, it should fail right away
        if (!$customer || !$customer->tags->count()) {
            return false;
        }

        //Lets check if the user has a matching tags
        $customer_tags = $customer->tags->map(function ($tag) {
            return $tag->name;
        });

        return count($this->tags->whereIn('name', $customer_tags)) ? true : false;
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'products_index';
    }

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'cover' => $this->cover,
            'name' => $this->name,
            'short_description' => $this->short_description,
            'sku' => $this->sku,
            'slug' => $this->slug,
            '_tags' => $this->tags->count() ? $this->tags->pluck('name') : ['default'],
        ];

        return $array;
    }

    public function shouldBeSearchable()
    {
        return $this->isActive() && !$this->isVariant() && !($this->isSubscription() && $this->sku!='REC-MONTHLY');
    }

    public function getReviewIdAttribute()
    {
        if ($this->type == 'subscription') {

            if (!isset(static::$monthly_subscription_id)) {
                $subscription = DB::table('products')->where('sku', config('subscription.monthly'))->first();
                static::$monthly_subscription_id = $subscription->id;
            }

            return static::$monthly_subscription_id;
        }

        return $this->id;
    }

    public function order_items()
    {
        return $this->hasMany(OrderItem::class, 'product_id', 'id');
    }

    public function calculateSales()
    {
        $this->total_sales = $this->order_items()->whereHas('order', function ($query) {
            $query->whereNotIn('status', [
                ORDER::ORDER_CANCELLED,
                ORDER::ORDER_FAILED,
                ORDER::ORDER_REFUNDED,
                ORDER::ORDER_ON_HOLD,
                ORDER::ORDER_PENDING,
            ]);
        })->get()->sum('quantity');

        return $this;
    }

    /**
     * @param $param
     * @return ProductResourceCollection
     */
    public function getRelatedProducts() {

        $related_products_ids = collect($this->related_products)->map(function ($item) {
            return $item['id'];
        });

        return new ProductResourceCollection(self::with(['metas', 'media'])
            ->whereIn('id', $related_products_ids->toArray())
            ->paginate(999));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function recipes(){
        return $this->belongsToMany(Recipe::class);
    }
    
    
    /**
     * Checks if the product has the tags
     *
     * @param mixed $tags
     * @return boolean
     */
    public function hasTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        return count($this->tags->filter(function ($tag) use ($tags) {
            return in_array($tag->name, $tags);
        })) == count($tags);
    }

    /**
     * Checks if the product has the tags
     *
     * @param mixed $tags
     * @return boolean
     */
    public function hasEitherTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        return count($this->tags->filter(function ($tag) use ($tags) {
            return in_array($tag->name, $tags);
        })) > 0;
    }


    /**
     * Assign product tags
     *
     * @param mixed $tags
     * @return static
     */
    public function assignTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        $this->tags()->syncWithoutDetaching(collect($tags)->map(function ($tag) {

            return Tag::firstOrCreate(['name' => $tag])->id;

        })->toArray());

        return $this;
    }

    /**
     * Remove product tags
     *
     * @param mixed $tags
     * @return static
     */
    public function removeTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        $this->tags()->whereIn('name', $tags)->detach();

        return $this;
    }

    public function getDefaultCategoryAttribute()
    {
        if ($this->type=='variant') {
            return $this->parent->default_category;
        }

        $first_category = $this->categories->first();

        return $first_category ?? new Category(['name' => 'Other']);
    }
}
