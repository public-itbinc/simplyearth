<?php

namespace App\Shop\Products;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class GCMS
{
    protected $api_key;
    protected $client;
    protected $api_url = 'https://simplyearth.azure-api.net/api';

    public function __construct()
    {
        $this->api_key = config('app.gcms_key');
        $this->client = new Client();
    }

    public function get_products($limit = 50)
    {
        $result = $this->client->get($this->api_url . '/products', [
            'headers' => $this->getHeaders(),
        ]);

        return $result->getBody()->getContents();
    }

    public function getGCMS($product_sku, $lot = null)
    {
        $lot = (int)$lot;
        $gcms = Cache::get('gcms:' . $product_sku . ':' . $lot, function () use ($product_sku, $lot) {
            
            try {

                $query = [
                    'SKU' => $product_sku,
                ];

                if (!empty($lot)) {                    
                    $query['Lot'] = $lot;
                }

                $result = $this->client->get($this->api_url . '/gcms', [
                    'headers' => $this->getHeaders(),
                    'query' => $query
                ]);

                $gcms_collection = collect(json_decode($result->getBody()->getContents()));

                Cache::put('gcms:' . $product_sku . ':' . $lot, $gcms_collection, 60 * 24);

                return $gcms_collection;
            } catch (\Throwable $th) { }

            return collect([]);
        })->map(function ($item) {
            $item->percentage = sprintf('%.2f', $item->percentage * 100);
            return $item;
        })->groupBy('sku');

        $products = Product::whereIn('sku', $gcms->keys()->flatten())->get();
        $buildDate = $gcms->first()->first()->buildDate;
        $lotExists = $gcms->first()->first()->lotExists;
        $gcms = $gcms->map(function ($component) use ($products) {
            return [
                'product' => $products->where('sku', $sku = $component->first()->sku)->first(),
                'items' => $component->groupBy('componentFamily')
            ];
        });

        return collect([
            'lotExists' => $lotExists,
            'buildDate' => $buildDate,
            'data' => $gcms
        ]);
    }

    protected function getHeaders()
    {
        return [
            'Ocp-Apim-Subscription-Key' => $this->api_key,
            'Accept'     => 'application/json',
        ];
    }
}
