<?php

namespace App\Shop\Rewards;

use App\Shop\Customers\Customer;
use App\Shop\Referrals\Reward;
use Illuminate\Support\Carbon;

class ProcessReward
{
    public static function claim(Reward $reward)
    {
        $reward->claimed_at = Carbon::now();
        $reward->save();
    }

    public static function claimFreebox(Customer $customer, $month_key)
    {
        if ($customer->unclaimed_free_boxes_count > 0) {

            $reward = $customer->unclaimed_rewards->where('type', 'freebox')->first();
            $reward->claimed_at = Carbon::now();
            $reward->schedule = $month_key;
            $reward->save();

        }
    }

    public static function unclaimFreebox(Customer $customer, $month_key)
    {

        if ($reward = $customer->claimed_rewards
            ->where('type', 'freebox')
            ->where('schedule', $month_key)
            ->where('order_id', null)->first()) {

            $reward->claimed_at = null;
            $reward->schedule = null;
            $reward->save();

        }
    }
}
