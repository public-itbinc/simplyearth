<?php

namespace App\Shop\Recipes;

use App\Shop\Products\Product;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\RecipeCollection;
use Kodeine\Metable\Metable;
class Recipe extends Model
{
    protected $table = 'recipes';
    use Filterable;

    /**
     * @var array
     */
    protected $fillable = ['name','ingredients','slug'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products() {
        return $this->belongsToMany(Product::class);
    }

    public static function getRelatedProducts($param) {
        $recipe = self::where('slug', $param)
            ->orWhere('name', $param)
            ->firstOrFail();
            
        $x = json_decode($recipe->ingredients,true);
          

        $related_products_ids = collect($x)->map(function ($item) {

             return $item['id'];
        });

        return new RecipeCollection(Product::with(['metas', 'media'])
            ->whereIn('id', $related_products_ids->toArray())
            // ->where('id', '<>', $param)
            // ->where('slug', '<>',$param)
            ->paginate(999));
    }
}
