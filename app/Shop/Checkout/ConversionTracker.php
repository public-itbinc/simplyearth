<?php

namespace App\Shop\Checkout;

use App\Shop\Orders\Order;
use App\Shop\Orders\OrderItem;
use Illuminate\Support\Carbon;
use App\Shop\Products\Product;

class ConversionTracker
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function referralCandy()
    {
        return [
            'first_name' => $this->order->customer->first_name,
            'last_name' => $this->order->customer->last_name,
            'email' => $this->order->customer->email,
            'discount_code' => @$this->order->discount_details['discount']['code'],
            'accepts_marketing' => $this->order->buyer_accepts_marketing,
            'order_timestamp' => $this->order->processed_at->timestamp,
            'browser_ip' => $this->order->ip_address,
            'user_agent' => @$this->order->client_details['user_agent'],
            'invoice_amount' => $this->order->total_price,
            'currency_code' => 'USD',
            'external_reference_id' => $this->order->order_number,
        ];
    }

    public function klaviyo($data = [])
    {
        return array_merge([
            "customer_properties" => [
                '$email' => $this->order->customer->email,
                '$first_name' => $this->order->customer->first_name,
                '$last_name' => $this->order->customer->last_name,
                'Has Account' => $this->order->customer->account ? true : false,
                'Accepts Marketing' => $this->order->buyer_accepts_marketing ? true : false,
            ],

            "properties" => [
                '$event_id' => $this->order->order_number,
                '$value' => $this->order->total_price,
                "Discount Code" => @$this->order->discount_details['discount']['code'],
                "Discount Value" => $this->order->total_discounts,
                "Subscription" => $this->order->subscription,
                "Items" => $this->order->order_items->map(function ($order_item) {
                    return [
                        "SKU" => $order_item->sku,
                        "ProductID" => $order_item->product_id,
                        "ProductName" => $order_item->name,
                        "Quantity" => $order_item->quantity,
                        "ItemPrice" => $order_item->price,
                        "RowTotal" => $order_item->price * $order_item->quantity,
                        "ProductURL" => $order_item->product ? $order_item->product->link : '',
                        "ImageURL" => asset($order_item->cover),
                        "Categories" => $order_item->product ? $order_item->product->categories->only(['name'])->toArray() : [],
                    ];
                })->toArray(),
            ],
            "time" => Carbon::now()->timestamp,
        ], $data);
    }

    public function klaviyoOrderItem(OrderItem $order_item, $data = [])
    {
        return [
            "customer_properties" => [
                '$email' => $this->order->customer->email,
                '$first_name' => $this->order->customer->first_name,
                '$last_name' => $this->order->customer->last_name,
            ],
            "properties" => [
                '$event_id' => $order_item->sku,
                '$value' => $order_item->price,
                "ProductName" => $order_item->name,
                "Quantity" => $order_item->quantity,
                "ProductURL" => $order_item->product ? $order_item->product->link : '',
                "ImageURL" => asset($order_item->cover),
                "ProductCategories" => $order_item->product ? $order_item->product->categories->only(['name'])->toArray() : [],
            ],
            "time" => Carbon::now()->timestamp,
        ];
    }

    public function google()
    {
        $subscription_skus = Product::where('type', 'subscription')->get()->pluck('sku');

        $subscription_product = $this->order->order_items->filter(function($item) use ($subscription_skus){
            return in_array($item->sku, $subscription_skus->toArray());
        })->first();

        $array = [
            //'event' => 'purchase',
            'ecommerce' => [
                'purchase' => [
                    'actionField' => [
                        'id' => $this->order->order_number,
                        'revenue' => $this->order->total_price,
                        'tax' => $this->order->total_tax,
                        'shipping' => $this->order->total_shipping,
                        'coupon' => @$this->order->discount_details['discount']['code'],
                    ],
                    'products' => $this->order->order_items->map(function ($item) {
                        $item_name_array = explode(':', $item->name);
                        return [
                            'name' => $item->type=='subscription' ?  $item_name_array[0] ?? $item->name: $item->name ,
                            'id' => $item->sku,
                            'price' => $item->price,
                            'quantity' => $item->quantity,
                        ];
                    }),
                    'subscription' => $subscription_product ? [
                        'price' => $subscription_product->price,
                        'plan' => $subscription_product->sku
                    ] : null
                ],
            ],
        ];

        return $array;
    }

    public function stamped()
    {
        $data = [
            [
                "email" => $this->order->email,
                "firstName" => $this->order->customer->first_name,
                "lastName" => $this->order->customer->last_name,
                "location" => $this->order->customer->default_address ? $this->order->customer->default_address->country_string : 'United States',
                "orderNumber" => $this->order->order_number,
                "orderId" => $this->order->id,
                "orderCurrencyISO" => "USD",
                "orderTotalPrice" => $this->order->total_price,
                "orderSource" => "website",
                "orderDate" => $this->order->processed_at->toW3cString(),
                "itemsList" => $this->order->order_items->map(function ($order_item) {
                    return [
                        "productId" => $order_item->product_id,
                        "productBrand" => "Simply Earth",
                        "productDescription" => $order_item->product ? $order_item->product->short_description : '',
                        "productTitle" => $order_item->name,
                        "productImageUrl" => \URL::asset($order_item->cover),
                        "productPrice" => $order_item->price,
                        "productType" => "Default",
                        "productUrl" => $order_item->product ? $order_item->product->link : '',
                    ];
                }),
            ],
        ];

        return $data;
    }

    public function refersion()
    {
        return [
            'cart_id' => str_random(30),
            'order_id' => $this->order->order_number,
            'shipping' => $this->order->total_shipping,
            'tax' => $this->order->total_tax,
            'discount' => $this->order->total_discounts,
            'discount_code' => $this->order->discount_details["discount"]["code"],
            'currency_code' => 'USD',
            'customer' => [
                'first_name' => $this->order->customer->first_name,
                'last_name' => $this->order->customer->last_name,
                'email' => $this->order->customer->email,
                'ip_address' => $this->order->ip_address,
            ],
            'items' => $this->order->order_items->map(function($item){
                return [
                    'price' => $item->sale_price ?: $item->price,
                    'quantity' => $item->quantity,
                    'sku' => $item->sku,
                    'name' => $item->name
                ];
            })
        ];
    }
}
