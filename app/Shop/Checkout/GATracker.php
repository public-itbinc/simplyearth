<?php

namespace App\Shop\Checkout;

class GATracker
{
    protected $purchased_data;

    function __construct($purchased_data)
    {
        $this->purchased_data = $purchased_data;
    }
    public function purchased()
    {
        $array = [
            'ecommerce' => [
                'purchase' => [
                    'actionField' => [
                        'id' => $this->purchased_data['transaction_id'],
                        'revenue' => $this->purchased_data['total'],
                        'tax' => $this->purchased_data['total_tax'],
                        'shipping' => $this->purchased_data['total_shipping'],
                        'coupon' => $this->purchased_data['discount_code'],
                    ],
                    'products' => collect($this->purchased_data['products'])->map(function ($item) {
                        $item_name_array = explode(':', $item->name);
                        return [
                            'name' => $item->type=='subscription' ?  $item_name_array[0] ?? $item->name: $item->name ,
                            'id' => $item->sku,
                            'price' => $item->price,
                            'quantity' => $item->qty,
                        ];
                    }),
                    'subscription' => $this->purchased_data['subscription'] ? [
                        'price' => $this->purchased_data['subscription']->price,
                        'plan' => $this->purchased_data['subscription']->sku
                    ] : null
                ],
            ],
        ];

        return $array;
    }
}