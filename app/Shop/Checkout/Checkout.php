<?php

namespace App\Shop\Checkout;

use Facades\App\Shop\Cart\Cart;
use Illuminate\Validation\ValidationException;
use App\Shop\Customers\Customer;
use Facades\App\Shop\Customers\CustomerRepository;
use Illuminate\Support\Facades\DB;
use App\Events\OrderCreated;
use App\Shop\Orders\InstallmentBuilder;
use App\Shop\Products\Product;
use Illuminate\Support\Carbon;
use App\Jobs\KlaviyoIdentify;

class Checkout
{

    protected $data = [];
    protected $customer;

    public function availableShippingMethods()
    {
        return Cart::availableShippingMethods();
    }

    public function setData(array $data)
    {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    /**
     *  @return App/Shop/Orders/Order
     *  @throws \Exception
     */
    public function processCheckout()
    {

        if (Cart::count() <= 0) {
            return false;
        }

        $order = Cart::setCustomer($this->customer)
            ->setData($this->data)
            ->build();

        if ($order) {
            event(new OrderCreated($order));
        }

        Cart::clear();

        return $order;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    public function getOrCreateCustomer()
    {

        if ($customer = customer()) {

            $customer = CustomerRepository::updateOrCreateAddress($customer, array_merge($this->data['checkout']['shipping_address'] ?? [], ['primary' => 1]));

            return $this->customer = $customer;
        }
        
        $this->customer = CustomerRepository::createCustomerFromCheckout($this->data['checkout']);

        return $this->customer;
    }

    public function parseShippingMethod($key)
    {
        return Cart::parseShippingMethod($key);
    }

    public function validateEmail($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function validateRequired($value)
    {
        if (is_null($value)) {
            return false;
        } elseif (is_string($value) && trim($value) === '') {
            return false;
        } elseif ((is_array($value) || $value instanceof Countable) && count($value) < 1) {
            return false;
        } elseif ($value instanceof File) {
            return (string) $value->getPath() !== '';
        }

        return true;
    }
}
