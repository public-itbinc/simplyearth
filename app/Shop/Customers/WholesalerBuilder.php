<?php

namespace App\Shop\Customers;

use Illuminate\Support\Facades\Password;
use Facades\App\Shop\Customers\CustomerRepository;
use App\Jobs\KlaviyoCustomerTags;

class WholesalerBuilder
{
    protected $email;

    protected $data;

    /**
     * Sets Email
     *
     * @param string $email
     * @return static
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Sets Data
     *
     * @param array $data
     * @return static
     */
    public function setData(array $data)
    {
        $this->data = self::normalizeData($data);

        if (isset($data['email'])) {
            $this->email = $data['email'];
        }

        return $this;
    }

    /**
     * Creates the wholesaler
     *
     * @return string
     */
    public function build()
    {

        $customer = Customer::where('email', $this->email)->first();

        if (!$customer) {

            $customer = CustomerRepository::getOrCreateCustomer($this->data);

        } 

        $customer->makeWholesaler();

        KlaviyoCustomerTags::dispatch($customer);
        
        if (!$customer->account) {

            $customer->findOrCreateAccount();

            return Password::broker('customers')->createToken($customer->account);

        }

        return $customer;
    }

    /**
     * Normalizes the data
     *
     * @param array $data
     * @return array
     */
    public static function normalizeData(array $data)
    {
        $data['address'] = [
            'first_name' => $data['first_name'],
            'last_name' => @$data['last_name'],
            'phone' => @$data['phone'],
            'address1' => @$data['address1'],
            'address2' => @$data['address2'],
            'region' => @$data['region'],
            'zip' => @$data['zip'],
            'city' => @$data['city'],
            'country' => parseCountry(@$data['country']),
        ];

        return $data;
    }
}
