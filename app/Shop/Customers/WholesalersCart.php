<?php

namespace App\Shop\Customers;

use App\BasicModel;

class WholesalersCart extends BasicModel
{
    protected $fillable = [
        'content',
    ];

    protected $casts = ['content' => 'array'];
}
