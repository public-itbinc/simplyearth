<?php

namespace App\Shop\Customers;

use Illuminate\Database\Eloquent\Model;

class TaxDocument extends Model
{
    protected $guarded = [];
}
