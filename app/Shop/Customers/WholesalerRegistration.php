<?php

namespace App\Shop\Customers;

use Illuminate\Database\Eloquent\Model;
use Facades\App\Shop\Customers\CustomerRepository;

class WholesalerRegistration extends Model
{

    protected $fillable = [
        'token',
        'email',
        'data'
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public static function createAndSendVerification(array $data)
    {
        if (!Customer::where('email', $data['email'])->first()) {
            CustomerRepository::getOrCreateCustomer(WholesalerBuilder::normalizeData($data));
        }

        $registration = self::create([
            'email' => $data['email'],
            'token' => str_random(30),
            'data' => $data,
        ]);

        return $registration;
    }
}
