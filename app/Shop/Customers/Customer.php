<?php

namespace App\Shop\Customers;

use App\BasicModel;
use App\Events\Registered as RegisteredCustomer;
use App\Mail\Activation as ActivationMail;
use App\Notifications\AccountManagement;
use App\Shop\Customers\Account;
use App\Shop\Customers\Activation;
use App\Shop\Customers\Address;
use App\Shop\History\History;
use App\Shop\Orders\Order;
use App\Shop\Tags\Tag;
use App\Traits\Filterable;
use App\Traits\HasReferral;
use App\Traits\HasRewards;
use App\Traits\Wholesaler;
use Braintree\Transaction as BraintreeTransaction;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Password;
use App\Jobs\KlaviyoCustomerTags;

class Customer extends BasicModel
{
    use Notifiable, SoftDeletes, Filterable, Wholesaler, HasReferral, HasRewards;

    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = [
        'deleted_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
    ];

    protected $appends = ['name', 'default_address', 'total_spent', 'total_orders', 'latest_order'];

    /**
     * Customer Name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        if (!empty($this->first_name) || !empty($this->last_name)) {
            return trim(ucwords($this->first_name . ' ' . $this->last_name));
        } else {
            return $this->email;
        }
    }

    /**
     * Addresses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * Customer Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activation()
    {
        return $this->hasOne(Activation::class, 'email', 'email');
    }

    public function wholesalers_cart()
    {
        return $this->hasOne(WholesalersCart::class);
    }

    /**
     * Returns the Account Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function account()
    {
        return $this->hasOne(Account::class, 'email', 'email');
    }

    public function getPaymentMethodAttribute()
    {
        return $this->account->hasBraintreeId() ? $this->account->only(['paypal_email', 'card_brand', 'card_last_four']) : null;
    }

    public function getDefaultAddressAttribute()
    {
        return $this->addresses->where('primary', 1)->first();
    }

    /**
     * Orders count
     *
     * @return int
     */
    public function getTotalOrdersAttribute()
    {
        return $this->orders->count();
    }

    /**
     * Total orders spent
     *
     * @return float
     */
    public function getTotalSpentAttribute()
    {
        return $this->orders->whereIn('status', [
            ORDER::ORDER_COMPLETED,
            ORDER::ORDER_PROCESSING,
        ])->sum('total_price');
    }

    /**
     * Latest Order
     *
     * @return Order
     */
    public function getLatestOrderAttribute()
    {
        return $this->orders()->latest()->first();
    }

    /**
     * Returns Orders of the customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function saveDefaultAddress($data = [])
    {
        $this->addresses()->firstOrCreate($data, ['primary' => 1]);
        return $this;
    }

    public function charge($amount, $token = null, $options = [])
    {
        if ($this->account) {
            return $this->account->chargeAndSave(
                $token,
                $amount,
                $options
            );
        }

        //Charge as guest customer
        $response = BraintreeTransaction::sale(array_merge(
            [
                'amount' => $amount,
                'paymentMethodNonce' => $token,
                'options' => [
                    'submitForSettlement' => true,
                ],
            ],
            $options
        ));

        if (!$response->success) {
            throw new Exception('Braintree was unable to perform a charge: ' . $response->message);
        }

        return $response;
    }

    public function sendInvite(array $data, $token = null)
    {
        $activation = $this->activation()->updateOrCreate(['email' => $this->email], ['token' => $token ?? str_random(30), 'created_at' => Carbon::now()]);

        \Mail::to($this)->send(new ActivationMail(
            $this,
            $activation,
            $data
        ));

        return $this;
    }

    public function sendAccountManagement()
    {
        $this->findOrCreateAccount();

        $token = Password::broker('customers')->createToken($this->account);

        $this->account->notify(new AccountManagement($token));

        return $this;
    }

    public function findOrCreateAccount()
    {
        if (!$this->account) {
            $this->account()->create(['email' => $this->email, 'password' => bcrypt(str_random(30))]);
            $this->setRelations(['account']);
        }

        return $this->account;
    }

    public function accountActivate($token)
    {

        $this->findOrCreateAccount();

        $token = Password::broker('customers')->createToken($this->account);

        $this->activation()->delete();

        event(new RegisteredCustomer($this->account));

        return $token;
    }

    /**
     * Customer Tags
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Returns the numbers of subscription boxes created
     *
     * @return void
     */
    public function subscriptionOrders()
    {
        return $this->orders->filter(function ($item) {
            return !is_null($item->box_key) && !is_null($item->subscription) && !empty($item->subscription) && !in_array($item->status, [
                ORDER::ORDER_CANCELLED,
                ORDER::ORDER_FAILED,
                ORDER::ORDER_REFUNDED,
                ORDER::ORDER_ON_HOLD,
                ORDER::ORDER_PENDING,
            ]);
        });
    }

    public function canGetBonusBox($count = null)
    {
        $factor = 6;

        $count = $count ?? $this->subscriptionOrders()->count() + 1;

        $count += $this->bonus_adjustment;

        if ($count == 1 || ($count > $factor && ($count - 1) % 6 == 0)) {
            return true;
        }

        return false;
    }

    public function hasBoxKey($box_key)
    {
        return $this->subscriptionOrders()->where('box_key', $box_key)->count() ? true : false;
    }

    public function hasSubscription()
    {
        return $this->account && $this->account->subscription;
    }

    public function history()
    {
        return $this->hasMany(History::class, 'model_id', 'id')->where('model_type', 'customer')->orderBy('created_at', 'DESC');
    }

    public function installmentPlans()
    {
        return $this->hasManyThrough(
            \App\Shop\Orders\InstallmentPlan::class,
            Account::class,
            'email',
            'account_id',
            'id',
            'id'
        );
    }

    /**
     * Checks if the customer has the tags
     *
     * @param mixed $tags
     * @return boolean
     */
    public function hasTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        return count($this->tags->filter(function ($tag) use ($tags) {
            return in_array($tag->name, $tags);
        })) == count($tags);
    }

    /**
     * Checks if the customer has the tags
     *
     * @param mixed $tags
     * @return boolean
     */
    public function hasEitherTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        return count($this->tags->filter(function ($tag) use ($tags) {
            return in_array($tag->name, $tags);
        })) > 0;
    }

    /**
     * Assign customer tags
     *
     * @param mixed $tags
     * @return static
     */
    public function assignTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        $this->tags()->syncWithoutDetaching(collect($tags)->map(function ($tag) {

            return Tag::firstOrCreate(['name' => $tag])->id;
        })->toArray());

        KlaviyoCustomerTags::dispatch($this);

        return $this;
    }

    /**
     * Remove customer tags
     *
     * @param mixed $tags
     * @return static
     */
    public function removeTags($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        $this->tags()->whereIn('name', $tags)->delete();

        KlaviyoCustomerTags::dispatch($this);

        return $this;
    }

    public function owns($model)
    {
        if ($this->id == $model->customer_id) {
            return true;
        }

        return false;
    }

    public function tax_documents()
    {
        return $this->hasMany(TaxDocument::class);
    }

    public function getShareLinkAttribute()
    {
        return route('subscription', ['coupon' => $this->share_code]);
    }

    public function scopeWithInstallments($query)
    {
        return $query->whereHas('account', function ($query) {
            return $query->whereHas('installment');
        });
    }
}
