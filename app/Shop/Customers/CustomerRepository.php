<?php

namespace App\Shop\Customers;

use App\Mail\Activation as ActivationMail;
use App\Shop\Base\BaseRepository;
use App\Shop\Customers\Activation;
use App\Shop\Customers\Customer;

class CustomerRepository extends BaseRepository
{

    public function __construct(Customer $customer)
    {
        parent::__construct($customer);
        $this->model = $customer;
    }

    public function getOrCreateCustomer(array $data)
    {
        $customer = Customer::firstOrCreate(['email' => $data['email']], $this->sanitizeData($data));

        $customer->setShareCode();

        return $this->updateOrCreateAddress($customer, $data['address'] ?? []);
    }

    public function createCustomerFromCheckout(array $data)
    {
        $customer = $this->updateOrCreateAddress(
            Customer::firstOrCreate(['email' => $data['email']], $this->sanitizeData($data)), 
            array_merge($data['shipping_address'] ?? [], ['primary' => 1]));

        
        \App\Jobs\KlaviyoIdentify::dispatch($customer);

        return $customer;
    }

    public function getCustomerWithAddresses($id)
    {
        return Customer::with(['addresses', 'account', 'activation'])->find($id);
    }

    public function updateOrCreateAddress(Customer $customer, array $data = []): Customer
    {

        //Make first address as default address

        $data = array_merge([
            'first_name' => $customer->first_name,
            'last_name' => $customer->last_name,
        ], $data);

        if (!$customer->addresses()->exists()) {
            $data['primary'] = 1;
        }

        $address = $customer->addresses()->updateOrCreate(collect($data)->only([
            'first_name',
            'last_name',
            'company',
            'phone',
            'address1',
            'address2',
            'city',
            'zip',
            'country',
            'region',
        ])->toArray(), $data);

        if (isset($data['primary']) && $data['primary'] == 1) {
            $customer->addresses()
                ->where('id', '!=', $address->id)->update(['primary' => 0]);
        }

        return $customer;
    }

    public function removeAddress(Customer $customer, $address_id): Customer
    {
        $customer->addresses()
            ->where('primary', '!=', 1)
            ->where('id', $address_id)->delete();

        return $customer;
    }

    public function sendInvite(array $data)
    {
        $activation = Activation::create(['email' => $this->model->email, 'token' => str_random(30)]);
        \Mail::to($this->model)->send(new ActivationMail(
            $this->model,
            $activation,
            $data
        ));

    }

    public function disableAccount(Customer $customer)
    {
        return $customer->account()->delete();
    }

    private function sanitizeData(array $data)
    {
        //Set default first_name and Last name
        if (empty($data['first_name'])) {
            $default = array_merge(
                isset($data['billing_address']) && isset($data['billing_address']['first_name'])
                ? $data['billing_address']
                : $data['shipping_address'] ?? ['first_name' => '', 'last_name' => '']
            );

            $data['first_name'] = $default['first_name'];
            $data['last_name'] = @$default['last_name'];
        }

        return $data;
    }
}
