<?php

namespace App\Shop\Customers;

use App\BasicModel;
use App\Shop\Customers\Customer;

class Address extends BasicModel
{
    protected $fillable = [
        'first_name',
        'last_name',
        'company',
        'phone',
        'address1',
        'address2',
        'city',
        'zip',
        'country',
        'region',
        'primary'
    ];

    protected $appends = ['name','country_string'];

    function user()
    {
        return $this->belongsTo(Customer::class);
    }

    function getCountryStringAttribute()
    {
        if(!$this->country) return '';
        return countries_list($this->country);
    }

    function getNameAttribute()
    {
        if (!empty($this->first_name) || !empty($this->last_name))
            return trim(ucwords($this->first_name . ' ' . $this->last_name));
        else {
            return $this->email;
        }

    }
}
