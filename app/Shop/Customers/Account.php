<?php

namespace App\Shop\Customers;

use App\Mail\ResetPasswordNotification;
use App\Shop\Customers\Customer;
use App\Shop\Subscriptions\Subscribable;
use App\Traits\Billable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Shop\Orders\InstallmentPlan;

class Account extends Authenticatable
{
    use Notifiable;
    use Billable;
    use Subscribable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'braintree_id',
    ];

    protected $dates = [
        'expiring_notified_at',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'email', 'email');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        \Mail::to($this)->send(new ResetPasswordNotification($this->customer, $token));
    }

    public function getNameAttribute()
    {
        return $this->customer ? $this->customer->name : $this->email;
    }

    public function installment()
    {
        return $this->hasOne(InstallmentPlan::class);
    }
}
