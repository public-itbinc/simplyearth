<?php

namespace App\Shop\Customers;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    /**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'customer_id',
		'code',
		'status',
		'email'
	];

	public function referrer()
	{
		return $this->belongsTo(Customer::class, 'customer_id');
	}
}
