<?php

namespace App\Shop\Customers;

use Illuminate\Database\Eloquent\Model;
use App\Shop\Customers\Customer;
use Illuminate\Support\Carbon;

class Activation extends Model
{

    protected $primaryKey = 'email';
    
    public $incrementing = false;

    public $timestamps = false;
    
    protected $table = 'customer_activations';

    protected $hidden = ['token','email'];

    protected $fillable = ['token','email'];

    public function setUpdatedAt($value)
    {

    }

    function getLinkAttribute()
    {
        return route('activation',['token' => $this->token]);
    }

    function customer()
    {
        return $this->belongsTo(Customer::class, 'email', 'email');
    }

    function setCreatedAtAttribute()
    {
        $this->attributes['created_at'] = Carbon::now();
    }
}