<?php

namespace App\Shop\Cart;

use App\CustomerName;
use App\Shop\Customers\Address;
use App\Shop\Customers\Customer;
use App\Shop\Orders\OrderBuilder;
use App\Shop\Products\Product;
use App\Shop\Products\ProductInterface;
use App\Shop\Shipping\ShippingZone;
use App\Shop\ShoppingBoxes\ShoppingBox;
use Illuminate\Session\SessionManager;

class Cart extends OrderBuilder
{
    protected $country;
    protected $region;
    protected $available_shipping_methods = [];

    public function __construct()
    {
        parent::__construct();

        if (session()->has('cart.items')) {
            $this->loadProductsFromSession(collect(session()->get('cart.items')));
        }

        $this->customer = customer();

        if ($this->customer) {
            $this->country = $this->customer->default_address->country ?? config('app.country');
            $this->region = $this->customer->default_address ? $this->customer->default_address->region : null;
        } else {
            $this->country = session()->get('cart.shipping_address.country') ?? config('app.country');
            $this->region = session()->get('cart.shipping_address.region') ?? null;
        }

        if (session()->has('cart.promocode')) {
            $this->applyDiscount(session()->get('cart.promocode'));
        }

        if (session()->has('cart.gift_card')) {
            $this->applyGiftCard(session()->get('cart.gift_card'));
        }

        if (session()->has('cart.shipping_method_key')) {
            $this->setShippingMethod($this->parseShippingMethod(session()->get('cart.shipping_method_key')));
        }

        if (session()->has('cart.shipping_address')) {

            $this->setData(['checkout' => [
                'shipping_address' => session()->get('cart.shipping_address'),
            ]]);

            $this->setShippingLocation(session()->get('cart.shipping_address'));
        }

        $this->processBonus();
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setCountryByCustomer(Customer $customer)
    {
        if (!is_a($customer->default_address, Address::class)) {
            return;
        }

        $this->setCountry($customer->default_address->country);
    }

    public function setShippingLocation(array $shipping_address)
    {

        if (isset($shipping_address['first_name'])) {
            $fullname = CustomerName::parseFromAddress($shipping_address);

            $shipping_address = array_merge($shipping_address, [
                'first_name' => $fullname->getFirstName(),
                'last_name' => $fullname->getLastName(),
            ]);
        }

        session()->put('cart.shipping_address', $shipping_address);
        $this->setData(['checkout' => [
            'shipping_address' => $shipping_address,
        ]]);

        $this->setCountry($shipping_address['country']);

        return $this;
    }

    public function setRegion($region)
    {
        session()->put('cart.shipping_address.region', $region);

        $this->region = $region;

        return $this;
    }

    public function setCountry($country)
    {
        if (!countries_list($country)) {
            return;
        }

        session()->put('cart.shipping_address.country', $country);

        $this->country = $country;

        return $this;
    }

    public function applyDiscount(string $code, $checkDiscountEligibility = true)
    {
        parent::applyDiscount($code, $checkDiscountEligibility);

        session()->put('cart.promocode', $code);

        return $this;
    }

    public function applyGiftCard(string $code)
    {
        parent::applyGiftCard($code);

        session()->put('cart.gift_card', $code);

        return $this;
    }

    public function deleteDiscount()
    {
        parent::deleteDiscount();

        session()->forget('cart.promocode');

        return $this;
    }

    public function deleteGiftCard()
    {
        parent::deleteGiftCard();

        session()->forget('cart.gift_card');

        return $this;
    }

    public function update($product, $quantity, $checkdiscount = true)
    {
        parent::update($product, $quantity, $checkdiscount);
        $this->saveCart();

        return $this;
    }

    public function add(ProductInterface $product, $quantity = 1, $checkdiscount = true)
    {
        parent::add($product, $quantity, $checkdiscount);

        $this->saveCart();

        return $this;
    }

    public function addPlan(ProductInterface $product, $cycles = null, $checkdiscount = true)
    {
        parent::addPlan($product, $cycles, $checkdiscount);

        $this->saveCart();

        return $this;
    }

    public function remove(ProductInterface $product)
    {

        parent::remove($product);
        $this->saveCart();

        return $this;
    }

    public function getProductById($product_id)
    {
        return $this->products->firstWhere('id', $product_id);
    }

    public function autoSetShipping()
    {
        if (!$this->requiresShipping()) {
            session()->forget('cart.shipping_method_key');
            return $this->setShippingMethod(null);
        }

        $available = $this->availableShippingMethods();

        if (count($available)) {
            //Check if there is free

            if ($free = $available->firstWhere('is_free', 1)) {
                $this->setShippingMethod($free);
            } else {
                $this->setShippingMethod($available->first());
            }

            session()->put('cart.shipping_method_key', $this->getShipping()->shipping_method_key);
        } else {
            $this->setShippingMethod(null);
            session()->forget('cart.shipping_method_key');
        }
        return $this;
    }

    private function saveCart()
    {
        session()->put('cart.items', $this->products->map(function ($product) {

            $data = ['id' => $product->id, 'qty' => $product->qty ?: 1];

            //save the plan if the cart item has one
            if (isset($product->plan)) {
                $data['plan'] = $product->plan;
            }

            return $data;
        }));

        return $this;
    }

    public function count()
    {
        return $this->products->sum('qty');
    }

    public function availableShippingMethods()
    {
        if (!isset($this->available_shipping_methods[$this->country])) {

            $this->available_shipping_methods[$this->country] = ShippingZone::where('countries', 'LIKE', '%"' . $this->country . '"%')
                ->orWHere('countries', '["*"]')
                ->orderBy('countries', 'DESC')->first();
        }

        if (!$this->available_shipping_methods[$this->country]) {
            return collect([]);
        }

        return $this->available_shipping_methods[$this->country]->getAvailableShipping($this);
    }

    public function getAvailbleShippingMethods()
    {
        return $this->available_shipping_methods;
    }

    public function build()
    {
        $order = parent::build();

        if (isset($order->box_key)) {
            try {
                ShoppingBox::getByKey($order->box_key)->decrementStock();
            } catch (\Exception $e) { }
        }

        return $order;
    }

    public function clear()
    {
        session()->forget('cart');
        $this->products = collect([]);
        $this->deleteDiscount();
        $this->deleteGiftCard();

        return $this;
    }

    public function getInstance()
    {
        return $this;
    }

    public function parseShippingMethod($key)
    {
        foreach ($this->availableShippingMethods() as $shipping_method) {
            if ($key == $shipping_method->shipping_method_key) {
                return $shipping_method;
            }
        }

        return null;
    }

    private function loadProductsFromSession($items)
    {
        if ($found_products = Product::whereIn('id', $items->pluck(['id']))->with(['metas'])->get()) {
            foreach ($found_products as $key => $product) {

                $current_item = $items->firstWhere('id', $product->id);

                if ($product->hasPlans() && isset($current_item['plan'])) {

                    $this->addPlan($product, $current_item['plan']->cycles, false);

                    continue;
                }

                $this->add(
                    $product,
                    $current_item['qty'] ?: 1,
                    false
                );
            }

            $this->checkDiscount();
        }
    }

    public function getDetail()
    {
        return [
            'data' => $this->getData(),
            'products' => $this->getProducts(),
            'country' => $this->getCountry(),
            'region' => $this->getRegion(),
            'available_shipping_methods' => $this->getAvailbleShippingMethods(),
            'shipping_method' => $this->getShipping(),
            'discount_applied' => $this->getDiscount(),
            'gift_card_applied' => $this->getGiftCard(),
            'discount_override' => $this->getDiscountOverride(),
            'free_addons' => $this->getFreeAddons(),
            'notes' => $this->getNotes(),
        ];
    }
}
