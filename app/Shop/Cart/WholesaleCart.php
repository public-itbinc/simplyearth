<?php

namespace App\Shop\Cart;

use App\Shop\Cart\Cart;

class WholesaleCart extends Cart
{
    public function addBulkProducts(array $products)
    {
        session()->put('cart.items', $products);

        return $this;
    }
}