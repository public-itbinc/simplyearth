<?php

namespace App\Shop\Cart;

use App\Shop\Orders\OrderBuilder;
use App\Shop\ShoppingBoxes\ShoppingBox;

class SessionCart extends OrderBuilder
{
    protected $session;
    protected $country;
    protected $region;
    protected $available_shipping_methods = [];

    public function __construct($data, $customer, $cart_detail)
    {
        parent::__construct();

        foreach ($cart_detail as $key => $value) {
            $this->$key = $value;
        }

        $this->customer = $customer;

        $this->setData(array_merge(['client_details' => [
            'user_agent' => request()->server('HTTP_USER_AGENT'),
            'ip_address' => request()->ip(),
        ]], $data));

        $this->processBonus();
    }

    function build()
    {
        $order = parent::build();

        if (isset($order->box_key)) {
            try {
                ShoppingBox::getByKey($order->box_key)->decrementStock();
            } catch (\Exception $e) { }
        }

        return $order;
    }
}
