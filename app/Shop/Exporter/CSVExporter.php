<?php

namespace App\Shop\Exporter;

use League\Csv\Writer;
use SplTempFileObject;

class CSVExporter
{
    protected $writer;

    protected $csv_name;

    protected $header;

    protected $contents;

    public function __construct()
    {
        $this->writer = Writer::createFromFileObject(new SplTempFileObject());
        $this->writer->setDelimiter("\t"); //the delimiter will be the tab character
        $this->writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $this->writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output
        $this->csv_name =  'csv-' . time().'.csv';
    }

    public function setCSV($csv_name)
    {

        $this->csv_name =  $csv_name;
        return $this;
    }

    public function setHeader($header)
    {

        $this->header = $header;
        return $this;
    }

    public function setContents($contents)
    {
        $this->contents = $contents;
        return $this;
    }

    public function generate()
    {
        if ($this->header) {
            $this->writer->insertOne($this->header);
        }

        $this->writer->insertAll($this->contents ?? []);

        $this->writer->output($this->csv_name);
        exit();
    }

    public function getWriter()
    {
        return $this->writer;
    }
}
