<?php

namespace App\Shop\Tags;

use App\Shop\Customers\Customer;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;
use App\Shop\Orders\Order;

class Tag extends Model
{
    protected $fillable = ['name'];

    public function products()
    {
        return $this->morphedByMany(Product::class, 'taggable');
    }

    public function customers()
    {
        return $this->morphedByMany(Customer::class, 'taggable');
    }

    public function orders()
    {
        return $this->morphedByMany(Order::class, 'taggable');
    }

    public function editLink()
    {
        return sprintf('/admin/tags/%d/edit', $this->id);
    }

    public function link()
    {
        return sprintf('/tags/%s', $this->slug);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = str_slug($value);
    }
}
