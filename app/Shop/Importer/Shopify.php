<?php

namespace App\Shop\Importer;

use GuzzleHttp\Client;


class Shopify
{
    protected $username;
    protected $password;
    protected $base_uri;
    protected $client;

    function __construct()
    {
        $this->username = env('SHOPIFY_USERNAME','');
        $this->password = env('SHOPIFY_PASSWORD','');
        $this->base_uri = env('SHOPIFY_URI','');
        $this->client = new Client([
            'base_uri' => $this->base_uri,
            'timeout' => 0,
        ]);
    }

    public function getCustomersCount()
    {
        $response = $this->client->get('admin/customers/count.json', [
            'auth' => $this->auth()]);

        $response = json_decode($response->getBody()->getContents());

        return $response->count;
    }

    public function getCustomers($page = 1, $limit = 50)
    {
        $response = $this->client->get('admin/customers.json', [
            'auth' => $this->auth(),
            'query' => [
                'page' => $page,
                'limit' => $limit,
            ]]);

        $response = json_decode($response->getBody()->getContents());

        return $response->customers;
    }

    public function getOrders($page = 1, $limit = 50)
    {
        $response = $this->client->get('admin/orders.json', [
            'auth' => $this->auth(),
            'query' => [
                'page' => $page,
                'limit' => $limit,
                'status' => 'any'
            ]]);

        $response = json_decode($response->getBody()->getContents());

        return $response->orders;
    }

    public function getOrdersCount()
    {
        $response = $this->client->get('admin/orders/count.json', [
            'auth' => $this->auth(),
            'query' => [
                'status' => 'any'
            ]]);

        $response = json_decode($response->getBody()->getContents());

        return $response->count;
    }

    public function getOrdersMarch($page = 1, $limit = 50)
    {
        $response = $this->client->get('admin/orders.json', [
            'auth' => $this->auth(),
            'query' => [
                'page' => $page,
                'limit' => $limit,
                'status' => 'any',
                'processed_at_min' => '2018-03-1',
                'processed_at_max' => '2018-03-31'
            ]]);

        $response = json_decode($response->getBody()->getContents());

        return $response->orders;
    }

    public function getOrdersMarchCount()
    {
        $response = $this->client->get('admin/orders/count.json', [
            'auth' => $this->auth(),
            'query' => [
                'status' => 'any',
                'processed_at_min' => '2018-03-1',
                'processed_at_max' => '2018-03-31'
            ]]);

        $response = json_decode($response->getBody()->getContents());

        return $response->count;
    }

    public function getGiftCards($page = 1, $limit = 50)
    {
        $response = $this->client->get('admin/gift_cards.json', [
            'auth' => $this->auth(),
            'query' => [
                'page' => $page,
                'limit' => $limit,
                'status' => 'any'
            ]]);

        $response = json_decode($response->getBody()->getContents());

        return $response->gift_cards;
    }

    public function getGiftCardsCount()
    {
        $response = $this->client->get('admin/gift_cards/count.json', [
            'auth' => $this->auth()]);

        $response = json_decode($response->getBody()->getContents());

        return $response->count;
    }

    private function auth()
    {
        return [$this->username, $this->password];
    }

}