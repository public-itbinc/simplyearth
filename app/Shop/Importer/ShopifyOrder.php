<?php

namespace App\Shop\Importer;

use App\Shop\Orders\Order;
use Illuminate\Support\Carbon;
use DateTime;
use App\Shop\Orders\OrderItem;
use Illuminate\Support\Facades\Log;
use App\Shop\Products\Product;

class ShopifyOrder
{

    protected $order;
    protected static $big_bonus_box;

    public function __construct($order)
    {
        Order::unsetEventDispatcher();

        $this->order = $order;
        self::$big_bonus_box = Product::where('sku', config('subscription.bonus_box'))->first();
        //dump($this->order);
    }

    public function import() : Order
    {
        $discount_details = [];
        $tags = [];

        $real_order = Order::firstOrNew(['id' => $this->order->order_number]);
        $real_order->forceFill(['id' => $this->order->order_number]);
        $real_order->order_name = str_replace('#','',$this->order->name);
        $real_order->email = $this->order->email;
        $real_order->completed_at = $this->order->closed_at ? Carbon::createFromFormat(DateTime::ISO8601, $this->order->closed_at) : null;        
        $real_order->processed_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->processed_at);
        $real_order->created_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->created_at);
        $real_order->updated_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->updated_at);
        $real_order->note = $this->order->note;
        $real_order->token = $this->order->token;
        $real_order->total_price = $this->order->total_price_usd;
        $real_order->subtotal_price = $this->order->subtotal_price;
        $real_order->note = $this->order->note;
        $real_order->total_weight = (float)$this->order->total_weight * 0.00220462;
        $real_order->total_tax = $this->order->total_tax;

        if(isset($this->order->customer)){
            $real_order->customer_id = $this->order->customer->id;
        }
        

        if($this->order->financial_status == 'pending') {
            $real_order->status = Order::ORDER_PENDING;
        } elseif($this->order->financial_status == 'paid') {
            if($this->order->closed_at || $this->order->fulfillment_status=='fulfilled' || $this->order->fulfillment_status == 'shipped') {
                $real_order->status = Order::ORDER_COMPLETED;
            } elseif($this->order->fulfillment_status == 'restocked') {
                $real_order->status = Order::ORDER_CANCELLED;
            }else {
                $real_order->status = Order::ORDER_PROCESSING;
            }
        } elseif($this->order->financial_status == 'refunded') {
            $real_order->status = Order::ORDER_REFUNDED;
        } elseif($this->order->financial_status == 'voided') {
            $real_order->status = Order::ORDER_CANCELLED;
        } else {
            $real_order->status = $this->order->financial_status;
        }

        //SUBSCRIPTION

        $subscription = null;

        foreach($this->order->line_items as $line_item) {
            if($line_item->sku=='REC-MONTHLY') {
                $real_order->subscription = 'Monthly Subscription Box';
                $real_order->box_key = $real_order->processed_at->format('F Y');
                break;
            }
        }

       

        $real_order->total_discounts = $this->order->total_discounts;
        $real_order->buyer_accepts_marketing = $this->order->buyer_accepts_marketing;
        $real_order->cancelled_at = $this->order->cancelled_at ? Carbon::createFromFormat(DateTime::ISO8601, $this->order->cancelled_at) : null;
        $real_order->cancel_reason = $this->order->cancel_reason;
        $real_order->phone = $this->order->phone;
        $real_order->ip_address = $this->order->browser_ip;
        
        $discount_details['discount'] = $this->order->discount_codes;

        $real_order->discount_details = $discount_details;        

        if (count($this->order->shipping_lines)) {
            $real_order->requested_shipping_service = $this->order->shipping_lines[0]->title;
        }

        

        //Client Details
        if (isset($this->order->client_details)) {
            $real_order->client_details = $this->order->client_details;
        }

        //Payment Details

        if (isset($this->order->payment_details)) {
            $real_order->payment_details = [
                'maskedNumber' => $this->order->payment_details->credit_card_number.' '.$this->order->payment_details->credit_card_company
            ];
        }

        $has_bonus_box = false;

        if (collect($this->order->discount_codes)->where('code', 'BONUSBOX')->count()) {
            $has_bonus_box = true;
            $real_order->subtotal_price += self::$big_bonus_box->price;
            $real_order->total_discounts += self::$big_bonus_box->price;
        }
        

        $real_order->save();

        $products = [];
        $order_items = [];

        foreach ($this->order->line_items as $line_item) {

            //GET THE PRODUCT
            if(!isset($products[$line_item->sku])) {
                $product = Product::where('sku', $line_item->sku)->first();
                if(!$product) {
                    echo 'Product not found:'.$line_item->product_id.':'.$line_item->sku.PHP_EOL;
                    continue; //skip if product is not found;
                } else {
                    $products[$product->sku] = $product;
                }
            }
            

            $order_item = OrderItem::firstOrNew(['id' => $line_item->id]);
            $order_item->forceFill(['id' => $line_item->id]);

            $order_item->weight = (float)$line_item->grams * 0.00220462;
            $order_item->price = $line_item->price;
            $order_item->quantity = $line_item->quantity;
            $order_item->requires_shipping = $line_item->requires_shipping;
            $order_item->sku = $line_item->sku;
            $order_item->name = $line_item->name;
            $order_item->product_id = $line_item->product_id;
            $order_item->taxable = $line_item->taxable;
            $order_item->image_url = $products[$line_item->sku]->cover;
            $order_item->created_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->created_at);
            $order_item->updated_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->updated_at);
            $order_items[] = $order_item;


        }

        if ($has_bonus_box) {
            $order_item = OrderItem::firstOrNew(['sku' => self::$big_bonus_box->sku, 'order_id' => $this->order->order_number]);
            $order_item->weight = self::$big_bonus_box->weight;
            $order_item->price = self::$big_bonus_box->price;
            $order_item->quantity = 1;
            $order_item->sku = self::$big_bonus_box->sku;
            $order_item->name = self::$big_bonus_box->name;
            $order_item->product_id = self::$big_bonus_box->product_id;
            $order_item->image_url = self::$big_bonus_box->cover;
            $order_item->created_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->created_at);
            $order_item->updated_at = Carbon::createFromFormat(DateTime::ISO8601, $this->order->updated_at);
            $order_items[] = $order_item;
        }

        
        $real_order->order_items()->saveMany($order_items);
        //Billing Address

        if(isset($this->order->billing_address)) {

            $real_order->billing_address()->updateOrCreate([
                'first_name' => $this->order->billing_address->first_name,
                'last_name' => $this->order->billing_address->last_name,
                'phone' => $this->order->billing_address->phone,
                'company' => $this->order->billing_address->company,
                'address1' => $this->order->billing_address->address1,
                'address2' => $this->order->billing_address->address2,
                'city' => $this->order->billing_address->city,
                'zip' => $this->order->billing_address->zip,
                'country' => $this->order->billing_address->country_code,
                'region' => $this->order->billing_address->province_code,
            ]);            
        }

        //Shipping Address

        if(isset($this->order->shipping_address)) {

            $real_order->shipping_address()->updateOrCreate([
                'first_name' => $this->order->shipping_address->first_name,
                'last_name' => $this->order->shipping_address->last_name,
                'phone' => $this->order->shipping_address->phone,
                'company' => $this->order->shipping_address->company,
                'address1' => $this->order->shipping_address->address1,
                'address2' => $this->order->shipping_address->address2,
                'city' => $this->order->shipping_address->city,
                'zip' => $this->order->shipping_address->zip,
                'country' => $this->order->shipping_address->country_code,
                'region' => $this->order->shipping_address->province_code,
            ]);            
        }

        // Tags

        if(isset($this->order->tags)) {

            $order_tags = [];
            
            foreach (array_filter(array_map('trim', explode(',',$this->order->tags))) as $tag) {

                $slug_tag = str_slug($tag);

                if(!isset($tags[$slug_tag])) {
                    $tags[$slug_tag] = \App\Shop\Tags\Tag::firstOrCreate(['name' => $slug_tag]);
                }

                $order_tags[] = $tags[$slug_tag]->id;
            }

            $real_order->tags()->sync($order_tags);
        }

        return $real_order;
    }
}