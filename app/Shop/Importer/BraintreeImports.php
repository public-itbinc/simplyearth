<?php

namespace App\Shop\Importer;

use App\Shop\Customers\Account;
use Illuminate\Support\Facades\Log;
use Braintree\Customer as BraintreeCustomer;
use Braintree\CustomerSearch;
use Braintree\PayPalAccount;
use League\Csv\Reader;
use Illuminate\Support\Facades\Storage;

class BraintreeImports
{
    private static $csv_records;

    public static function importAccountPaymentMethod(Account $account)
    {
        if ($account = self::pullFromCSV($account)) {
            return $account;
        }

        try {
            $customers = BraintreeCustomer::search([CustomerSearch::email()->is($account->email)]);
        } catch (\Exception $e) {
            Log::warning($e->getMessage());
            return false;
        }

        if(count($customers->getIds()) == 0) {
            return false;
        }

        $customer = $customers->firstItem();

        if(!$customer || empty($customer->paymentMethods)) {
            return false;
        }

        foreach ($customer->paymentMethods as $p) {
            if ($p->isDefault()) {
                $paymentMethod = $p;
            }
        }

        if (!isset($paymentMethod)) {
            $paymentMethod = $customer->paymentMethods[0];
        }

        $paypalAccount = $paymentMethod instanceof PayPalAccount;

        $account->forceFill([
            'braintree_id' => $customer->id,
            'paypal_email' => $paypalAccount ? $paymentMethod->email : null,
            'card_brand' => !$paypalAccount ? $paymentMethod->cardType : null,
            'card_last_four' => !$paypalAccount ? $paymentMethod->last4 : null,
            'expiring_notified_at' => null
        ])->save();

        return $account;
    }

    public static function pullFromCSV(Account $account)
    {
        try{

            if(!isset(self::$csv_records)) {
                $reader = Reader::createFromString(Storage::get('payment_methods.csv'));
                $reader->setHeaderOffset(0);
                self::$csv_records = collect($reader->getRecords());
            }

            $found = self::$csv_records->where('Customer Email', $account->email)->first();

            if($found) {

                $paypalAccount = $found['Payment Method Type']=='Paypal Account' ? true : false;
                $account->forceFill([
                    'braintree_id' => $found['Customer ID'],
                    'paypal_email' => $paypalAccount ? $found['Customer Email'] : null,
                    'card_brand' => !$paypalAccount ? $found['Card Type'] : null,
                    'card_last_four' => !$paypalAccount ? $found['Last Four of the Credit Card'] : null,
                    'expiring_notified_at' => null
                ])->save();

                return $account;
            }

        } catch (\Exception $e) {
            Log::warning($e->getMessage());
            return false;
        }

        return false;
    }
    
}
