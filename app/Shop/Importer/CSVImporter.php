<?php

namespace App\Shop\Importer;
use League\Csv\Reader;

class CSVImporter
{
    protected $csv;
    protected $reader;

    public function setCSV($csv)
    {
        $this->csv =  $csv;

        if (!file_exists($this->csv)) {
            throw new \Exception('CSV not found');
        }

        $this->reader = Reader::createFromPath($this->csv);

        $this->reader->setHeaderOffset(0);

        return $this;
    }

    public function getItems()
    {
        return $this->reader->getRecords();
    }

    public function getReader()
    {
        return $this->reader;
    }
}
