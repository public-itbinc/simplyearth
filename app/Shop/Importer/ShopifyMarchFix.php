<?php

namespace App\Shop\Importer;

use App\Shop\Orders\Order;

class ShopifyMarchFix
{

    protected $order;

    public function __construct($order)
    {
        Order::unsetEventDispatcher();

        $this->order = $order;
    }

    public function fix()
    {
        $discount_details = [];
        $tags = [];

        $real_order = Order::find(['id' => $this->order->order_number])->first();

        if (!$real_order) {
            echo 'F' . $this->order->order_number;
            return false;
        }

        if (!isset($this->order->tags) || strpos($this->order->tags, 'Subscription')===false) {

            echo 'N' . $this->order->order_number;
            return false;
        }

        $real_order->subscription = 'Subscription Box';
        $real_order->box_key = $real_order->processed_at->format('F Y');
        $real_order->save();

        echo 'S'. $this->order->order_number;

        return $real_order;
    }
}
