<?php

namespace App\Shop\Importer;

use App\Shop\Customers\Account;
use App\Shop\Customers\Customer;
use App\Shop\Products\Product;
use App\Shop\Subscriptions\Subscription;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use Illuminate\Support\Carbon;
use App\Shop\Subscriptions\SubscriptionBuilder;
use App\Shop\Importer\BraintreeImports;

class ImportRechargeFailed
{
    protected $csv;

    public function __construct()
    {
        $this->csv = Storage::get('failed-subscriptions.csv');
    }

    public function process()
    {
        Account::unsetEventDispatcher();
        Subscription::unsetEventDispatcher();
        $reader = Reader::createFromString($this->csv);

        $reader->setHeaderOffset(0);

        echo "COUNT: " . $reader->count() . PHP_EOL;

        $errors = [];

        $counter = 0;

        foreach ($reader->getRecords() as $record) {

            if($record['status'] != 'Active, has Order Error') {
                continue;
            }

            $customer = Customer::where('email', $record['customer email'])->first();

            if (!$customer) {
                $errors[] = 'Missing customer:' . $record['customer email'];
                echo "C";
                continue;
            }

            $plan = Product::subscriptions()->where('sku', config('subscription.monthly'))->first();

            if (!$plan) {
                $errors[] = "Missing plan:" . $record['product title'];
                echo 'A';
                continue;
            }

            if (!$customer->account) {
                $customer->account()->create(['password' => bcrypt(str_random(16))]);
            }

            $customer->refresh()->load('account.subscription');

            $subscription = $customer->account->subscription;

            if (!$subscription) {
                $subscription = $customer->account->subscription()->create([
                    'plan' => $plan->sku,
                ]);
            } else {
                continue;
            }

            $subscription->schedule = SubscriptionBuilder::parseConsolidateSchedule(4);
            $subscription->failed_attempts = 5;
            $subscription->save();

            BraintreeImports::importAccountPaymentMethod($customer->account);

            $counter++;
            echo '.';

        }
        
        echo 'Total Imported:'.$counter.PHP_EOL;

        echo implode(PHP_EOL, $errors);
        
    }

    public function getPlanByTitle($product_title)
    {
        $sku = null;
        
        if ($product_title == 'Monthly Subscription Box' || $product_title == 'Your 1st Essential Oil Recipe Box  - Cancel at any time  Auto renew' || $product_title == 'Your 1st Essential Oil Recipe Box - Cancel at any time' || $product_title=='Monthly Subscription Box  Auto renew') {
            $sku = config('subscription.monthly');
        } elseif ($product_title == 'Quarterly Subscription Box' || $product_title == 'Quarterly Subscription Box Auto renew  Auto renew' || $product_title = 'Quarterly Subscription Box Auto renew') {
            $sku = config('subscription.quarterly');
        }

        if (!$sku) {
            return false;
        }

        return Product::subscriptions()->where('sku', $sku)->first();
    }
}
