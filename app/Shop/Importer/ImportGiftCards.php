<?php

namespace App\Shop\Importer;

use App\Shop\Customers\Customer;
use App\Shop\Discounts\GiftCard;
use App\Shop\Discounts\GiftCardGenerator;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use Illuminate\Support\Carbon;
use DateTime;


class ImportGiftCards
{
    protected $csv;

    public function __construct()
    {
        $this->csv = Storage::get('gift_cards.csv');
    }

    public function process()
    {

        $reader = Reader::createFromString($this->csv);

        $reader->setHeaderOffset(0);

        foreach ($reader->getRecords() as $record) {
                if ($record['Balance']<=0 || $record['Enabled?']=='false' || $record['Expired?']=='true') {
                    continue;
                }

                //Get customer

                $customer = Customer::where('email', $record['Email'])->first();

                if (!$customer) {
                    echo 'Missing customer on giftcard ID:'.$record['Id'].PHP_EOL;
                    continue;
                }

                $gift_card = GiftCard::firstOrNew(['id' => $record['Id']]);
                $gift_card->forceFill([
                    'id' => $record['Id'],    
                    'initial_value' => $record['Balance'],
                    'remaining' => $record['Balance'],
                    'customer_id' => $customer->id]);

                if(!$gift_card->code) {
                    $gift_card->code = GiftCardGenerator::generateCode();
                    $gift_card->token = str_random(30);
                }

                $gift_card->save();
            }
    }
}