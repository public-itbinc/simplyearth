<?php

namespace App\Shop\Importer;

use App\Shop\Discounts\Discount;
use Illuminate\Support\Facades\DB;

class DiscountsConversion extends CSVImporter
 {
     public function updateCouponTo40()
     { 
         foreach($this->reader->getRecords() as $record) {
             $result = DB::update('update discounts set options = ? where code = ?', ['{"usage_limits":0,"minimum_requirement":"none","customer_eligibility":"everyone","applies_to":"products","customer_tags":[],"addons":["607979405364"],"products":["5720099269","607979405398","607979405397","607979405419"],"per_customer":true}', $record['code']] );
             
             if (!$result) {
                 dump($record['code']);
             }
         }
     }
 }