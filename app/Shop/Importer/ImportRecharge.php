<?php

namespace App\Shop\Importer;

use App\Shop\Customers\Account;
use App\Shop\Customers\Customer;
use App\Shop\Products\Product;
use App\Shop\Subscriptions\Subscription;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use Illuminate\Support\Carbon;
use App\Shop\Subscriptions\SubscriptionBuilder;

class ImportRecharge
{
    protected $csv;

    public function __construct()
    {
        $this->csv = Storage::get('subscriptions.csv');
    }

    public function process()
    {
        Account::unsetEventDispatcher();
        Subscription::unsetEventDispatcher();
        $reader = Reader::createFromString($this->csv);

        $reader->setHeaderOffset(0);

        echo "COUNT: " . $reader->count() . PHP_EOL;

        $errors = [];

        foreach ($reader->getRecords() as $record) {

            $customer = Customer::where('email', $record['email'])->first();

            if (!$customer) {
                $errors[] = 'Missing customer:' . $record['email'];
                echo "C";
                continue;
            }

            $plan = $this->getPlanByTitle($record['product title']);

            if (!$plan) {
                $errors[] = "Missing plan:" . $record['product title'];
                echo 'A';
                continue;
            }

            if (!$customer->account) {
                $customer->account()->create(['password' => bcrypt(str_random(16))]);
            }

            $customer->refresh()->load('account.subscription');

            $subscription = $customer->account->subscription;

            if (!$subscription) {
                $subscription = $customer->account->subscription()->create([
                    'plan' => $plan->sku,
                ]);
            } else {
                continue;
            }

            $charge_date = Carbon::createFromFormat('Y-m-d g:i:s', $record['charge date']);

            $subscription->continue_at = $charge_date;

            $subscription->schedule = SubscriptionBuilder::parseConsolidateSchedule($charge_date->format('d'));

            $subscription->save();

            if ((float)$record['discount amount'] > 0) {
                $subscription->SubscriptionDiscounts()->create(
                    [
                        'amount' => $record['discount amount'],
                        'code' => $record['discount code']
                    ]
                );
            }

            echo '.';

        }

        echo implode(PHP_EOL, $errors);
        
    }

    public function getPlanByTitle($product_title)
    {
        $sku = null;
        
        if ($product_title == 'Monthly Subscription Box' || $product_title == 'Your 1st Essential Oil Recipe Box  - Cancel at any time  Auto renew' || $product_title == 'Your 1st Essential Oil Recipe Box - Cancel at any time' || $product_title=='Monthly Subscription Box  Auto renew') {
            $sku = config('subscription.monthly');
        } elseif ($product_title == 'Quarterly Subscription Box' || $product_title == 'Quarterly Subscription Box Auto renew  Auto renew' || $product_title = 'Quarterly Subscription Box Auto renew') {
            $sku = config('subscription.quarterly');
        }

        if (!$sku) {
            return false;
        }

        return Product::subscriptions()->where('sku', $sku)->first();
    }
}
