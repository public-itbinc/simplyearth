<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class GiftCardFilters extends QueryFilters {
    
    function q($name)
    {
        return $this->query->where('code', 'LIKE', '%'.$name .'%')->orWhereHas('owner', function($query) use ($name){
            $query->where('first_name', 'LIKE', '%'.$name .'%')
            ->orWhere('last_name', 'LIKE', '%' . $name . '%')
            ->orWhere('email', 'LIKE', '%' . $name . '%');
        });
    }

}
