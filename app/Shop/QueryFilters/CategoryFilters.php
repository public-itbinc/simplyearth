<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class CategoryFilters extends QueryFilters
{

    public function q($name)
    {
        return $this->query->where('name', 'LIKE', '%' . $name . '%');
    }

    public function slug($slug)
    {
        return $this->query->where('slug', '=', $slug);
    }

    public function sortby($orderString)
    {
        $orderArray = explode('.', $orderString);

        $orderBy = $orderArray[0] ?: 'created_at';
        $order = $orderArray[1] ?: 'desc';

        return $this->query->orderBy($orderBy, $order);
    }

}
