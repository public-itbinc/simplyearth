<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class BlogFilters extends QueryFilters {
    
    function q($name)
    {
        return $this->query->where('title', 'LIKE', '%'.$name .'%')->orWhere('slug', 'LIKE', '%' . $name . '%');
    }

}
