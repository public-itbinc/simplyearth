<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class DiscountFilters extends QueryFilters {
    
    function q($name)
    {
        return $this->query->where('code', 'LIKE', '%'.$name .'%');
    }

}
