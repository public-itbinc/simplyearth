<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class OrderFilters extends QueryFilters
{

    public function q($name)
    {
        return $this->query
            ->where('email', 'LIKE', '%' . $name . '%')
            ->orWhere('id', 'LIKE', '%' . (int) $name . '%');
    }

    public function customer($customer_id)
    {
        return $this->query->whereHas('customer', function ($query) use ($customer_id) {
            $query->where('id', $customer_id);
        })->get();
    }

}
