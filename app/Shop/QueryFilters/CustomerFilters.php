<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class CustomerFilters extends QueryFilters {
    
    function q($name)
    {
        return $this->query
        ->where('first_name', 'LIKE', '%'.$name .'%')
        ->orWhere('last_name', 'LIKE', '%' . $name . '%')
        ->orWhere('email', 'LIKE', '%' . $name . '%');
    }

}
