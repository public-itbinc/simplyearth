<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class ProductFilters extends QueryFilters {
    
    function q($name)
    {
        return $this->query->where('name', 'LIKE', '%'.$name .'%')->orWhere('sku', 'LIKE', '%' . $name . '%');
    }

    function status($status) {
        return $this->query->where('status', '=', $status);
    }

    function sortby($orderString)
    {
        $orderArray = explode('.', $orderString);

        $orderBy = $orderArray[0]?:'total_sales';
        $order = $orderArray[1]?:'desc';

        return $this->query->orderBy($orderBy,$order);
    }

}
