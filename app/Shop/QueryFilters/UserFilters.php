<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class UserFilters extends QueryFilters {
    
    function q($name)
    {
        return $this->query->where('name', 'LIKE', '%'.$name .'%')->orWhere('email', 'LIKE', '%' . $name . '%');
    }

}
