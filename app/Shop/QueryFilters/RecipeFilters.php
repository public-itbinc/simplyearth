<?php

namespace App\Shop\QueryFilters;

use App\QueryFilters;

class RecipeFilters extends QueryFilters {

    function q($name)
    {
        return $this->query->where('name', 'LIKE', '%'.$name .'%');
    }

}
