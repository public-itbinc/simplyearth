<?php

namespace App\Shop\Referrals;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $appends = ['text', 'icon'];
    protected $guarded = [];

    protected $casts = [
        'claimed_at' => 'date'
    ];

    public function getTextAttribute()
    {
        switch($this->type)
        {
            case 'freebox':
                return 'Free Box';
        }

        return '';
    }

    public function getIconAttribute()
    {
        switch($this->type)
        {
            case 'freebox':
                return 'box-small.png';
        }

        return '';
    }
}
