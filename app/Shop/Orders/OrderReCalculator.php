<?php

namespace App\Shop\Orders;

use App\Shop\Discounts\Discount;
use App\Shop\Customers\Customer;
use App\Shop\Products\Product;
use Illuminate\Support\Collection;
use App\Jobs\OrderTax;

class OrderRecalculator extends OrderBuilder
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->customer = $order->customer;
        $this->products = new Collection;
        $found_products = Product::whereIn('id', $this->order->order_items->pluck(['product_id']))->with(['metas'])->get();
            if ($found_products) {
                foreach ($found_products as $key => $product) {
                    $this->products->push((object) array_merge(
                        ['qty' => $this->order->order_items->firstWhere('product_id', $product->id)->quantity],
                        $product->only($this->fields)
                    ));
                }
            }

        $this->discount_applied = isset($this->order->discount_details['discount']) ? Discount::hydrate($this->order->discount_details['discount'])->first() : null;
    }

    public function build()
    {
        $this->order->subtotal_price = $this->getSubTotal();
        $this->order->total_tax = $this->getTaxTotal();
        $this->order->total_price = $this->getGrandTotal();
        $this->order->total_shipping = $this->getTotalShipping();
        $this->order->total_discounts = $this->getOverallDiscounts();
        $this->order->save();

        OrderTax::dispatch($this->order, 'update')->onQueue('normal');

        return $this->order;
    }

    public function getTaxPercentage()
    {
        return $this->order->tax_percentage;
    }

    public function getShippableWeightTotal()
    {
        $shippable_weight_total = 0.00;

        foreach ($this->products as $product) {
            $shippable_weight_total += (float) $product->weight * max(1, abs($product->qty));
        }

        return $shippable_weight_total;
    }
}
