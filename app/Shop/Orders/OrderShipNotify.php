<?php

namespace App\Shop\Orders;

use App\Shop\Orders\Order;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\data;
use App\Events\OrderStatusUpdated;

class OrderShipNotify
{
    protected $xml;
    protected $data;

    public function __construct($xml, $data)
    {
        $this->xml = $xml;
        $this->data = $data;
    }

    public function process()
    {

        // load some objects
        $order = Order::getOrderByNumber($this->xml->OrderNumber);

        if (!$order) {
            throw new \Exception('Ship Notify: Order '.$this->xml->OrderNumber.' not found', 404);
        }

        $order->tracking_number = (string)$this->xml->TrackingNumber ?? $this->data['tracking_number'];
        $order->shipping_carrier = (string)$this->xml->Carrier ?? $this->data['carrier'];
        $order->shipping_service = (string)$this->xml->Service ?? $this->data['service'];
        $order->shipping_date = Carbon::createFromFormat('m/d/Y', $this->xml->ShipDate);
        $order->status = Order::ORDER_COMPLETED;
        $order->completed_at = Carbon::now();
        $order->save();

        event(new OrderStatusUpdated($order));

    }

}
