<?php

namespace App\Shop\Orders;

use Spatie\ArrayToXml\ArrayToXml;

class OrderXMLConverter
{

    public $orders;

    public function __construct($orders)
    {
        $this->orders = $orders;
    }

    public function convertToArray()
    {
        $array = [];

        if (count($this->orders) > 0) {

            $array['Order'] = $this->orders->filter(function($order){

                return $order->needsShipping();

            })->map(
                function ($order) {

                    $gift_message = $order->gift_details['message'] ?? "";

                    $data = [
                        'OrderID' => ['_cdata' => "$order->id"],
                        'OrderNumber' => ['_cdata' => "$order->order_number"],
                        'OrderDate' => $order->processed_at->format('m/d/Y g:i A'),
                        'OrderStatus' => ['_cdata' => "$order->status"],
                        'LastModified' => $order->updated_at->format('m/d/Y g:i A'),
                        'ShippingMethod' => $order->requested_shipping_service,
                        'PaymentMethod' => null,
                        'OrderTotal' => $order->total_price,
                        'TaxAmount' => $order->total_tax,
                        'ShippingAmount' => $order->total_shipping,
                        'CustomerNotes' => ['_cdata' => "$order->note"],
                        'InternalNotes' => ['_cdata' => "$order->InternalNotes"],
                        'Gift' => $order->gift ? 'true' : 'false',
                        'GiftMessage' => ['_cdata' => "$gift_message"],
                        'Customer' => [
                            'CustomerCode' => ['_cdata' => "{$order->customer->id}"],
                        ],
                    ];

                    $billing_address = $order->billing_address ?? $order->shipping_address;

                    if ($billing_address) {
                        $data['Customer']['BillTo'] = [
                            'Name' => ['_cdata' => "{$billing_address->name}"],
                            'Company' => ['_cdata' => "{$billing_address->company}"],
                            'Address1' => ['_cdata' => "{$billing_address->address1}"],
                            'Address2' => ['_cdata' => "{$billing_address->address2}"],
                            'City' => ['_cdata' => "{$billing_address->city}"],
                            'State' => ['_cdata' => "{$billing_address->region}"],
                            'PostalCode' => ['_cdata' => "{$billing_address->zip}"],
                            'Country' => ['_cdata' => "{$billing_address->country}"],
                            'Phone' => ['_cdata' => "{$billing_address->phone}"],
                        ];

                    }

                    $shipping_address = $order->shipping_address;

                    if ($shipping_address) {
                        $data['Customer']['ShipTo'] = [
                            'Name' => ['_cdata' => "{$shipping_address->name}"],
                            'Company' => ['_cdata' => "{$shipping_address->company}"],
                            'Address1' => ['_cdata' => "{$shipping_address->address1}"],
                            'Address2' => ['_cdata' => "{$shipping_address->address2}"],
                            'City' => ['_cdata' => "{$shipping_address->city}"],
                            'State' => ['_cdata' => "{$shipping_address->region}"],
                            'PostalCode' => ['_cdata' => "{$shipping_address->zip}"],
                            'Country' => ['_cdata' => "{$shipping_address->country}"],
                            'Phone' => ['_cdata' => "{$shipping_address->phone}"],
                        ];
                    }

                    if (count($order->order_items) > 0) {

                        $data['Items'] = ['Item' => $order->order_items->map(
                            function ($order_item) {
                                $image_url = config('app.url').$order_item->image_url;
                                $item = [
                                    'SKU' => ['_cdata' => "{$order_item->sku}"],
                                    'LineItemID' => ['_cdata' => "{$order_item->id}"],
                                    'Name' => ['_cdata' => "{$order_item->name}"],
                                    'ImageUrl' => ['_cdata' => "{$image_url}"],
                                    'Weight' => $order_item->weight,
                                    'WeightUnits' => 'lbs',
                                    'Quantity' => $order_item->quantity,
                                    'UnitPrice' => $order_item->sale_price ?: $order_item->price,
                                ];

                                //Attributes

                                if ($order_item->extra_attributes) {
                                    $item['Options'] = [
                                        'Option' => collect($order_item->extra_attributes)->map(
                                            function ($option) {
                                                return [
                                                    'Name' => ['_cdata' => "{$option['label']}"],
                                                    'Value' => ['_cdata' => "{$option['value']}"],
                                                ];
                                            }
                                        )->toArray(),
                                    ];
                                }

                                return $item;

                            }
                        )->toArray()];

                    } else {
                        $data['Items'] = [];
                    }

                    return $data;

                }
            )->toArray();

        }

        return $array;
    }

    public function convertToXMLString()
    {

        $content = ArrayToXml::convert(
            $this->convertToArray(), [
                'rootElementName' => 'Orders',
                '_attributes' => [
                    'pages' => $this->orders->lastPage(),
                ],
            ], true, 'utf-8'
        );

        return $content;
    }

}
