<?php

namespace App\Shop\Orders;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    protected $table = 'billing_addresses';

    protected $appends = ['name', 'country_string'];

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'company',
        'phone',
        'address1',
        'address2',
        'city',
        'country',
        'zip',
        'region',
    ];

    public function getNameAttribute()
    {
        if (!empty($this->first_name) || !empty($this->last_name)) {
            return trim(ucwords($this->first_name . ' ' . $this->last_name));
        } else {
            return $this->email;
        }
    }

    public function getCountryStringAttribute()
    {
        if (!$this->country) {
            return '';
        }

        return countries_list($this->country);
    }

}
