<?php

namespace App\Shop\Shipping;

use Illuminate\Database\Eloquent\Model;


abstract class ShippingMethod extends Model
{

    protected $fillable = [
        'name',
        'min',
        'max',
        'rate',
        'is_free'
    ];


    protected $appends = [
        'shipping_method_key',
        'description'
    ];

    abstract function getDescriptionAttribute();

    function getShippingMethodKeyAttribute()
    {
        return $this->getTable() . '-' . $this->id;
    }

    function shipping_zone()
    {
        return $this->belongsTo(ShippingZone::class);
    }

    function isAvailable($value)
    {
        $value = (float)$value;

        if ((float)$this->min > $value) return false;

        if (is_numeric($this->max) && (float)$this->max < $value) return false;

        return true;
    }



}