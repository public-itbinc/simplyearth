<?php

namespace App\Shop\Shipping;

class Helpers
{

    function sanitize_shipping_zone(array $data)
    {
        $data['countries'] = $this->sanitize_countries($data['countries']);
        return $data;
    }

    function sanitize_countries($countries)
    {
        if (is_string($countries)) {
            $countries = explode(',', $countries);
        }

        return collect($countries)->filter(function ($country) {
            return $country == '*' || countries_list($country);
        })->values();
    }
}