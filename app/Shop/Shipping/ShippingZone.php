<?php

namespace App\Shop\Shipping;

use Illuminate\Database\Eloquent\Model;
use App\Shop\Shipping\ShippingRatePrice;
use App\Shop\Shipping\ShippingRateWeight;
use App\Shop\Orders\OrderBuilder;
use Illuminate\Database\Eloquent\Collection;

class ShippingZone extends Model
{
    protected $fillable = [
        'name',
        'countries'
    ];

    function getCountriesAttribute($value)
    {
        $result = json_decode($value);

        if (json_last_error() === JSON_ERROR_NONE) {
            return $result;
        }

        return $value;
    }

    function shipping_rate_prices()
    {
        return $this->hasMany(ShippingRatePrice::class);
    }

    function shipping_rate_weights()
    {
        return $this->hasMany(ShippingRateWeight::class);
    }

    function getAvailableShipping($order)
    {
        $shippable_price_total = $order->getShippablePriceTotal()-$order->getDiscountTotal();
        $shippable_weight_total = $order->getShippableWeightTotal();
        $skip_free = $order->isFreeFirstMonthDiscount();

        $available_shippings = new Collection;

        $this->shipping_rate_prices->map(function ($shipping_rate_price) use ($shippable_price_total, $available_shippings, $skip_free) {
            if ($shipping_rate_price->isAvailable($shippable_price_total) && (!$skip_free || !$shipping_rate_price->is_free)) {
                $available_shippings->push($shipping_rate_price);
            }
        });

        $this->shipping_rate_weights->map(function ($shipping_rate_weight) use ($shippable_weight_total, $available_shippings, $skip_free) {
            if ($shipping_rate_weight->isAvailable($shippable_weight_total) && (!$skip_free || !$shipping_rate_weight->is_free)) {
                $available_shippings->push($shipping_rate_weight);
            }
        });

        if ($skip_free && $order->getShippingAddress()['country']=='US') {
            $available_shippings->push(new ShippingRatePrice([
                'name' => 'Flat Shipping',
                'min' => 0,
                'rate' => 0,
            ]));
        }

        return $available_shippings->sortBy('rate');
    }
}