<?php

namespace App\Shop\Shipping;

use App\Shop\Shipping\ShippingMethod;

class ShippingRatePrice extends ShippingMethod
{
    function getDescriptionAttribute()
    {
        if($this->is_free) return 'Free shipping';
        
        return sprintf("$%.2f – %s", $this->min, ($this->max) ? sprintf('$%.2f', $this->max) : 'No limit');
    }
}
