<?php

namespace App\Shop\Shipping;

use App\Shop\Base\BaseRepository;
use App\Shop\Shipping\ShippingZone;
use Facades\App\Shop\Shipping\Helpers as ShippingHelpers;


class ShippingZoneRepository extends BaseRepository
{
    protected $model;

    function __construct(ShippingZone $model)
    {
        $this->model = $model;
    }

    function createWithRates(array $data)
    {

        $this->model = $this->model->create(ShippingHelpers::sanitize_shipping_zone($data));


        if (isset($data['shipping_rate_prices'])) {
            $this->syncShippingRatePrices($data['shipping_rate_prices']);
        }

        if (isset($data['shipping_rate_weights'])) {
            $this->syncShippingRateWeights($data['shipping_rate_weights']);
        }

        return $this->model;
    }

    function updateWithRates(array $data)
    {

        $this->model->update(ShippingHelpers::sanitize_shipping_zone($data));


        if (isset($data['shipping_rate_prices'])) {
            $this->syncShippingRatePrices($data['shipping_rate_prices']);
        }

        if (isset($data['shipping_rate_weights'])) {
            $this->syncShippingRateWeights($data['shipping_rate_weights']);
        }

        return $this->model;
    }

    function syncShippingRatePrices(array $shipping_rate_prices)
    {
        $updated_item_ids = [];

        if ($this->model->id) {

            foreach ($this->cleanShippingRatePrices($shipping_rate_prices) as $item) {
                $item['shipping_zone_id'] = $this->model->id;
                array_push($updated_item_ids, $this->model->shipping_rate_prices()->updateOrCreate($item)->id);
            }

        } else {
            array_push($updated_item_ids, $this->model->shipping_rate_prices()->createMany($this->cleanShippingRatePrices($shipping_rate_prices))->id);
        }

        $delete = array_diff($this->model->shipping_rate_prices()->get()->pluck(['id'])->all(), $updated_item_ids);

        if (count($delete) > 0) {
            $this->model->shipping_rate_prices()->whereIn('id', $delete)->delete();
        }

    }


    function syncShippingRateWeights(array $shipping_rate_weights)
    {
        $updated_item_ids = [];

        if ($this->model->id) {

            foreach ($this->cleanShippingRateWeights($shipping_rate_weights) as $item) {
                $item['shipping_zone_id'] = $this->model->id;
                array_push($updated_item_ids, $this->model->shipping_rate_weights()->updateOrCreate($item)->id);
            }

        } else {
            array_push($updated_item_ids, $this->model->shipping_rate_weights()->createMany($this->cleanShippingRateWeights($shipping_rate_weights))->id);
        }

        $delete = array_diff($this->model->shipping_rate_weights()->get()->pluck(['id'])->all(), $updated_item_ids);

        if (count($delete) > 0) {
            $this->model->shipping_rate_weights()->whereIn('id', $delete)->delete();
        }


    }

    function cleanShippingRatePrices(array $items) : array
    {
        return collect($items)->map(function ($item) {
            return collect($item)->only(['name', 'min', 'max', 'rate', 'is_free'])
                ->merge(['shipping_zone_id' => $this->model->id])->all();
        })->all();
    }

    function cleanShippingRateWeights(array $items) : array
    {
        return collect($items)->map(function ($item) {
            return collect($item)->only(['name', 'min', 'max', 'rate', 'is_free'])
                ->merge(['shipping_zone_id' => $this->model->id])->all();
        })->all();
    }
}