<?php

namespace App\Shop\Shipping;

use App\Shop\Shipping\ShippingMethod;

class ShippingRateWeight extends ShippingMethod
{
    function getDescriptionAttribute()
    {
        if ($this->is_free) return 'Free shipping';
        
        return sprintf("%.2flb – %s", $this->min, ($this->max) ? sprintf('%.2flb', $this->max) : 'No limit');
    }
}
