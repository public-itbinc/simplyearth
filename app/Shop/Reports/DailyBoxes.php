<?php

namespace App\Shop\Reports;

use App\Shop\Customers\Account;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;

class DailyBoxes
{

    /**
     * Start Date
     *
     * @var Carbon
     */
    protected $start_date;

    /**
     * End Date
     *
     * @var Carbon
     */
    protected $end_date;

    public function __construct($start_date, $end_date = null)
    {
        $this->start_date = Carbon::parse($start_date);
        $this->end_date = Carbon::parse($end_date ?? $start_date);

        if ($this->end_date->lt($this->start_date)) {
            $this->end_date = clone ($this->start_date);
        }
    }

    public function process()
    {
        do {

            $this->getSubscribersByDate($this->start_date)->each(function($account){
                Storage::append(sprintf('boxes/%s%s', $this->start_date->format('Y-m-d'), '.txt'), $account->email);
            });
            
            $this->start_date->addDay(1);
        } while ($this->start_date->lte($this->end_date));
    }

    protected function getSubscribersByDate($date)
    {
        Carbon::setTestNow($date);

        return Account::subscribedUsers()
            ->whereHas('subscription', function ($query) use ($date) {
                $query->where('schedule', $date->format('d'));
            })
            ->whereHas('customer')
            ->get()->filter(function($account){
                $nextbox = $account->nextBox();
                return ($nextbox && $nextbox->date->isToday());
            })->map(function($account){
                
                $nextbox = $account->nextBox();
               

                if($nextbox->skipped()){
                    $account->email .= ' Skipped';
                }

                return $account;
            });
    }
}
