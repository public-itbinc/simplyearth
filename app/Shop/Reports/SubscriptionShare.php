<?php

namespace App\Shop\Reports;

use Illuminate\Database\Eloquent\Model;

class SubscriptionShare extends Model
{
    /**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'source'
	];
}
