<?php

namespace App\Shop\Reports;

use App\Shop\Customers\Account;
use Illuminate\Support\Carbon;
use App\Shop\Orders\Order;
use Illuminate\Support\Facades\Cache;

class MonthlyBox
{
    protected $box_key;

    private $this_month;

    public function __construct(string $box_key)
    {
        $this->box_key = $box_key;

        $date_array = explode(' ', $this->box_key);
        $this->this_month = Carbon::createFromFormat('F:Y:j', $date_array[0] . ':' . $date_array[1] . ':' . Carbon::today()->format('d'));
    }

    public function getRemainingBoxes()
    {
        return Account::whereHas('subscription', function ($query) {

            $query->whereNull('ends_at');
            //Not before continue_at
            $query->where(function ($query) {

                $query->whereNull('continue_at');
                $query->orWhereRaw('continue_at <= "'. $this->this_month->format('Y-m-d' . '"'));

            });
            
            //Not paused
            $query->whereDoesntHave('subscriptionPauses', function ($query) {
                $query->where('schedule', $this->box_key);
            });

        })
        ->whereHas('customer')
        ->whereDoesntHave('customer.orders', function ($query) {

            $query->whereNotNull('subscription');
            $query->where('box_key', $this->box_key);
            $query->whereNotIn('status', [
                Order::ORDER_CANCELLED,
                Order::ORDER_FAILED,
                Order::ORDER_REFUNDED,
                Order::ORDER_ON_HOLD,
                Order::ORDER_PENDING,
            ]);
        })
        ->get();

    }
}
