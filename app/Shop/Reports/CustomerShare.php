<?php

namespace App\Shop\Reports;

use Illuminate\Database\Eloquent\Model;

class CustomerShare extends Model
{
    /**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'customer_id', 
		'is_subscribe', 
		'source'
	];
}
