<?php

namespace App\Shop\Subscriptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use App\Shop\Orders\Order;

class Commitment extends Model 
{

    protected $guarded = [];

    protected $attributes = [];

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withTimestamps();
    }

    public function getRemainingCyclesAttribute()
    {
        return min($this->cycles, $this->cycles - $this->completed_cycles);
    }

    public function isCurrent()
    {
        return $this->status == 'current';
    }
}
