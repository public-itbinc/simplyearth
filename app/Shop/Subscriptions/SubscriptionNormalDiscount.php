<?php

namespace App\Shop\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class SubscriptionNormalDiscount extends Model
{
    protected $table = 'subscription_normal_discounts';

    protected $fillable = [
        'code',
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
