<?php

namespace App\Shop\Subscriptions;

use App\Shop\Discounts\GiftCard;
use App\Shop\ShoppingBoxes\ShoppingBox;
use App\Shop\ShoppingBoxes\ShoppingBoxBuilder;
use App\Traits\CanAddBonus;
use App\Traits\HasSimpleAttributes;
use Illuminate\Support\Carbon;

class FutureOrderMonth
{
    use HasSimpleAttributes, CanAddBonus;

    static $first_not_skipped;
    protected $count;
    protected $subscription;
    protected $date;
    protected $shopping_box;
    protected $subscription_pause;
    protected $subscription_gift;
    protected $subscription_addons;
    protected $subscription_exchanges;
    protected $is_free;
    protected $product_addons;
    protected $product_exchanges;
    protected $discount;
    protected $discount_giftcard;
    protected $attributes = [
        'date',
        'shopping_box',
    ];

    protected $builder;

    protected $fields = ['id', 'sku', 'name', 'price', 'priceString', 'cover', 'shipping',
        'weight', 'type', 'extra_attributes', 'wholesale_pricing', 'wholesale_price'];

    protected $commitment;

    public function __construct(
        Subscription $subscription,
        ShoppingBox $shopping_box,
        Carbon $date,
        int $count,
        $subscription_data
    ) {
        $this->count = $count;
        $this->subscription = $subscription;
        $this->shopping_box = $shopping_box;
        $this->date = $date;
        $this->subscription_pause = $subscription_data['pauses']->first();
        $this->subscription_gift = $subscription_data['gifts']->first();
        $this->subscription_addons = $subscription_data['addons'];
        $this->subscription_exchanges = $subscription_data['exchanges'];
        $this->is_free = !!$subscription_data['rewards'];

        if (!isset(static::$first_not_skipped) && !$this->skipped()) {
            static::$first_not_skipped = $this->month_key;
        }

        if ($this->isFirstNotSkipped() && $this->subscription->subscriptionNormalDiscounts->count()) {
            $this->applyDiscount($this->subscription->subscriptionNormalDiscounts->first()->code, false);
        }
    }

    public function isFirstNotSkipped()
    {
        return (static::$first_not_skipped === $this->month_key);
    }

    public function isFree()
    {
        return $this->is_free;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

    public function setCommitment($commitment)
    {
        $this->commitment = $commitment;

        return $this;
    }

    public function getCommitment()
    {
        return $this->commitment;
    }

    public function getBox()
    {
        return $this->shopping_box;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getProductCountAttribute()
    {
        return $this->getItemCount();
    }

    public function getMonthsAttribute()
    {
        return $this->count * (int) $this->subscription->product->subscription_months;
    }

    public function getNameAttribute()
    {
        return $this->shopping_box->name;
    }

    public function getPriceStringAttribute()
    {
        return $this->subscription->product->priceString;
    }

    public function getDiscountAttribute()
    {
        return $this->discount;
    }

    public function getDiscountGiftcardAttribute()
    {
        return $this->discount_giftcard;
    }

    public function getDiscountTotalAttribute()
    {
        return ($this->discount) ? $this->discount->amount : 0;
    }

    public function getTotalStringAttribute()
    {
        return sprintf('%s%s', config('cart.currency')['symbol'], $this->price - $this->discount_total);
    }

    public function getPriceAttribute()
    {
        return $this->subscription->product->price;
    }

    public function getGiftAttribute()
    {
        return $this->subscription_gift;
    }

    public function getPriceStringSimpleAttribute()
    {
        return currency()['symbol'] . $this->subscription->product->price;
    }

    public function getStatusAttribute()
    {
        if ($this->skipped()) {
            return 'skipped';
        }

        return 'pending';
    }

    public function getDateZoneAttribute()
    {
        return $this->date->toW3cString();
    }

    public function getMonthKeyAttribute()
    {
        return $this->date->format('F Y');
    }

    public function skipped()
    {
        return !is_null($this->subscription_pause) || $this->subscriptionSkipped();
    }

    public function subscriptionSkipped()
    {
        if (is_null($this->subscription->continue_at)) {
            return false;
        }

        $date_array = explode(' ', $this->month_key);

        $this_month = Carbon::createFromFormat('F:Y:j', $date_array[0] . ':' . $date_array[1] . ':1');

        $continue = Carbon::create($this->subscription->continue_at->format('Y'), $this->subscription->continue_at->format('m'), 1);

        return $this_month < $continue;
    }

    public function gifted()
    {
        return !is_null($this->subscription_gift) ? true : false;
    }

    public function monthsFromNow()
    {
        return $this->date->diffInMonths(Carbon::now());
    }

    public function updateAddons($product_id, $qty)
    {
        $this->subscription->updateAddons($this->monthKey, $product_id, $qty);

        //Refresh box addons
        $this->subscription_addons = SubscriptionAddons::where('subscription_id', $this->subscription->id)->where('schedule', $this->monthKey)->get();
        $this->product_addons = null;
    }

    public function updateExchanges($from_id, $product_id)
    {
        $this->subscription->updateExchanges($this->monthKey, $from_id, $product_id);

        //Refresh box exchanges
        $this->subscription_exchanges = SubscriptionExchanges::where('schedule', $this->monthKey)->get();
        $this->product_exchanges = null;
    }

    public function getAddons()
    {
        if (!isset($this->product_addons)) {

            $this->product_addons = collect([]);

            if (count($this->subscription_addons)) {
                $this->product_addons = SubscriptionAddons::with('product')->whereIn(
                    'id',
                    $this->subscription_addons->pluck(['id'])
                )->get();
            }
        }
        return $this->product_addons;
    }

    public function getExchanges()
    {
        if (!isset($this->product_exchanges)) {

            if ($this->subscription_exchanges->count() == 0) {
                $this->product_exchanges = collect([]);
                return $this->product_exchanges;
            }

            $this->product_exchanges = SubscriptionExchanges::with('product')->whereIn(
                'id',
                $this->subscription_exchanges->pluck(['id'])
            )->get()->map(function ($item) {
                $item->price = config('subscription.exchange_price');
                $item->priceString = sprintf('%s%.2f', currency()['symbol'], $item->price);
                return $item;
            });
        }
        return $this->product_exchanges;
    }

    public function getProducts()
    {
        return $this->shopping_box->products;
    }

    public function getProductsParsed()
    {
        $products = collect($this->getProducts())->map(function ($item) {
            $item['exchanged'] = $this->getExchanges()->contains('from', $item['id']);
            $item['is_oil'] = str_contains($item->categories()->pluck('name'), 'Oils');
            $item['categories'] = $item->categories()->pluck('name');
            return $item;
        });

        return $products;
    }

    public function getItemCount()
    {
        $count = count($this->getProducts());
        if ($this->subscription_addons) {
            foreach ($this->subscription_addons as $addon) {
                $count += max(1, (int) $addon->qty);
            }
        }

        return $count;
    }

    public function applySubscriptionDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    public function applyDiscount($code, $checkDiscountEligibility = true)
    {
        $this->getBuilder()->applyDiscount($code);

        return $this;
    }

    public function deleteDiscount()
    {

        $this->getBuilder()->deleteDiscount();

        return $this;
    }

    public function applyDiscountGiftCard(array $gift_card)
    {
        $this->discount_giftcard = $gift_card;

        return $this;
    }

    public function clearData()
    {
        SubscriptionAddons::where('schedule', $this->month_key)->delete();
        SubscriptionGift::where('schedule', $this->month_key)->delete();
        SubscriptionExchanges::where('schedule', $this->month_key)->delete();
        SubscriptionPause::where('schedule', $this->month_key)->delete();
    }

    public function applyGiftCard(GiftCard $gift_card)
    {

        $builder = $this->getBuilder();

        $discount_amount = min($gift_card->remaining, $builder->getGrandTotal());

        $this->getSubscription()->subscriptionGiftCards()->create([
            'code' => $gift_card->code,
            'amount' => $discount_amount,
            'schedule' => $this->month_key,
        ]);

        $gift_card->remaining -= $discount_amount;
        $gift_card->save();

    }

    /**
     * Get Shoppingboxbuilder
     *
     * @return ShoppingBoxBuilder
     */
    public function getBuilder()
    {
        if (!isset($this->builder)) {
            $this->builder = new ShoppingBoxBuilder($this);
        }

        return $this->builder;
    }

    public function getGiftCardDiscountTotal()
    {
        return ($this->discount_giftcard) ? max(0, (float) $this->discount_giftcard['amount']) : 0;
    }

    public function hasGiftCardApplied()
    {
        return !!$this->discount_giftcard;
    }

    public function getGiftCardMaskedCode()
    {
        return ($this->discount_giftcard) ? '****-****-****-' . substr($this->discount_giftcard['code'], -4) : '';
    }

    public function getGiftCardId()
    {
        return ($this->discount_giftcard) ? $this->discount_giftcard['id'] : null;
    }

    public function resetBuilder()
    {
        $this->builder = null;
        
        return $this;
    }
}
