<?php

namespace App\Shop\Subscriptions;

use App\Shop\Products\Product;
use App\Shop\ShoppingBoxes\ShoppingBox;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class FutureOrders
{
    protected $subscription;
    protected $months;
    private $prepareMonths;
    protected $shopping_boxes;
    protected $today;
    protected $bonus;
    protected $month_count;
    protected $schedule;
    protected $unclaimed_free_boxes;

    public function __construct()
    {
        $this->bonus = Product::where('sku', config('subscription.bonus_box'))->first();
    }

    public function getInstance()
    {
        return $this;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

    public function initSubscription(Subscription $subscription)
    {
        if (!$this->subscription || $this->subscription->id != $subscription->id) {
            return $this->setSubscription($subscription);
        }

        return $this;
    }

    public function setSubscription(Subscription $subscription, $reload = false)
    {
        /*if ($this->subscription && $this->subscription->id == $subscription->id && !$reload ) {
        return $this;
        }*/

        $this->today = Carbon::today();

        $this->subscription = $subscription;

        $this->shopping_boxes = ShoppingBox::all();

        $this->prepareMonths = new Collection;

        $this->months = new Collection;

        $this->schedule = $this->subscription->final_schedule;

        $this->month_count = max(1, (int) $this->subscription->product->subscription_months);

        if (!is_null($this->subscription->continue_at) && $this->subscription->continue_at->format('Y-m-d') >= $this->today->format('Y-m-d')) {
            $next = Carbon::create($this->subscription->continue_at->format('Y'), $this->subscription->continue_at->format('m'), $this->subscription->continue_at->format('d'), 12);
        } elseif (count($this->subscription->owner->customer->subscriptionOrders())) {

            $last_subscription_order_date = Carbon::createFromFormat('F Y d H', sprintf('%s %d %d', $this->subscription->owner->customer->subscriptionOrders()->last()->box_key, $this->schedule, 12));
            $next = $last_subscription_order_date->addMonthNoOverflow($this->month_count);
        } else {
            $next = Carbon::create(null, null, $this->schedule, 12);
        }

        $skipped = 0;
        $commitments = null;
        $current_cycle = 1;

        if ($this->subscription->isCommitment()) {
            $commitments = $this->subscription->commitments->whereIn('status', ['current', 'pending'])->getIterator();
        }

        $subscriptionDiscounts = $this->subscription->SubscriptionDiscounts;

        $subscriptionGiftcards = $this->subscription->subscriptionGiftcards;
        
        $this->unclaimed_free_boxes = $this->subscription->owner->customer->unclaimed_rewards->where('type', 'freebox');

        for ($i = 0, $box_count = 0; $i < $this->subscription->future_box_count; $i++) {
            //If the schedule overlaps e.g. 31 on February we will set it to the last day of the month
            $this->maybeAddFutureBox($next, $skipped, $subscriptionDiscounts, $subscriptionGiftcards, $commitments, $current_cycle, $i, $box_count);

            $next = $next->copy()->addMonthNoOverflow(max(1, (int) $this->subscription->product->subscription_months));
        }

        return $this;
    }

    public function refresh()
    {
        if ($this->subscription) {
            $this->setSubscription($this->subscription->refresh());
        }

        return $this;
    }

    protected function maybeAddFutureBox($date, &$skipped, &$subscriptionDiscounts, &$subscriptionGiftcards, &$commitments, &$current_cycle = null, &$loop, &$box_count)
    {
        //We will skip if the current date has passed the schedule.
        if ($date->format('Y-m-d') < $this->today->format('Y-m-d')) {
            return;
        }

        //We will skip if the user already has order for current month

        if ($this->subscription->owner->customer->hasBoxKey($date->format('F Y'))) {
            return;
        }

        $box = $this->getShoppingBoxByDate($date);

        $commitment = null;

        if ($this->subscription->isCommitment() && $commitments) {

            $current_commitment = current($commitments);

            if (!$current_commitment) {
                $commitment = null;
            } else if ($current_cycle <= $current_commitment->remaining_cycles) {
                $commitment = $current_commitment;
            } else {
                $current_cycle = 1;
                $commitment = next($commitments);
            }

            if (!$commitment) {
                return;
            }

            //We're gonna skip if auto renew is disabled
            if (!$this->subscription->isAutoRenew() && ($commitment && $commitment->status != 'current') &&
                (is_null($this->subscription->resumed_at) || $this->subscription->resumed_at > $date)) {

                $commitment = null;
                $current_cycle--;

                //adjust the loop to accomodate more boxes
                if (!is_null($this->subscription->resumed_at)) {
                    $loop--;
                }
            }
        }

        $subscription_data = collect([
            'pauses' => $this->subscription->subscriptionPauses->whereIn('schedule', $date->format('F Y')),
            'gifts' => $this->subscription->subscriptionGifts->whereIn('schedule', $date->format('F Y')),
            'addons' => $this->subscription->subscriptionAddons->whereIn('schedule', $date->format('F Y')),
            'exchanges' => $this->subscription->subscriptionExchanges->whereIn('schedule', $date->format('F Y')),
            'rewards' => $this->unclaimed_free_boxes->pop(),
        ]);

        $new_box = new FutureOrderMonth($this->subscription, $box, $date, ++$box_count, $subscription_data);

        $new_box->setCommitment($commitment);

        if (!$new_box->skipped()) {
            $current_cycle++;
        } else {
            $loop--;
        }

        if ($new_box->skipped() || $new_box->gifted()) {
            $skipped++;
        } else {

            if ($this->subscription->owner->customer->canGetBonusBox($this->subscription->owner->customer->subscriptionOrders()->count() + $new_box->getCount() - $skipped)) {

                for ($bonus_loop = 0; $bonus_loop < $this->subscription->quantity; $bonus_loop++) {
                    $new_box->addBonus($this->bonus)->resetBuilder();
                }

            }

            $subscriptionDiscounts = $subscriptionDiscounts->filter(function ($discount) {
                return $discount->limit > 0 || $discount->unlimited == true;
            });

            //Apply Discount if available
            if (count($subscriptionDiscounts)) {
                $new_box->applySubscriptionDiscount($subscriptionDiscounts->first())->resetBuilder();

                $subscriptionDiscounts->first()->limit--;
            }

            $subscriptionGiftcards = $subscriptionGiftcards->reject(function ($gift_card) {
                return $gift_card->amount <= 0;
            });

            if ($subscription_gift_card = $subscriptionGiftcards->filter(function ($item) use ($new_box) {
                return boxKeyToDate($item->schedule)->format('Y-m-1') <= $new_box->getDate()->format('Y-m-1');
            })->first()) {
                $grand_total = $new_box->getBuilder()->getGrandTotal();
                $discount_amount = min($grand_total, $subscription_gift_card->amount);

                $new_box->applyDiscountGiftCard([
                    'id' => $subscription_gift_card->id,
                    'amount' => $discount_amount,
                    'code' => $subscription_gift_card->code]);

                $subscriptionGiftcards->map(function ($item) use ($subscription_gift_card, $discount_amount) {
                    if ($item->id == $subscription_gift_card->id) {
                        $item->amount -= $discount_amount;
                    }
                    return $item;
                });
            }
        }

        $this->months->push($new_box);
    }

    private function getShoppingBoxByDate($date)
    {
        $box = $this->filterBoxByKey($date->format('F-Y'));

        if (!$box) {

            $box = new ShoppingBox([
                'key' => strtolower($date->format('F-Y')),
                'Name' => $date->format('F Y'),
            ]);

        }

        return $box;
    }

    private function filterBoxByKey($key)
    {
        $key = strtolower($key);

        foreach ($this->shopping_boxes as $box) {
            if (strtolower($box->key) == $key) {
                return $box;
            }
        }
    }

    /**
     * Returns the monthly future orders
     *
     * @return \Illuminate\Support\Collection
     */
    public function getMonths()
    {
        return $this->months;
    }

    public function getMonth($month)
    {
        return $this->getMonths()->first(function ($item) use ($month) {
            return strtolower($month) === strtolower($item->date->format('F Y'));
        });
    }

    public function getNextBox()
    {
        return $this->getMonths()->filter(function ($item) {

            if ($this->subscription->isCommitment()) {
                return !$item->skipped() && ($this->subscription->isAutoRenew() || ($item->getCommitment() && $item->getCommitment()->status == 'current') ||
                    (!is_null($this->subscription->resumed_at) && $this->subscription->resumed_at <= $item->getDate()));
            }

            return !$item->skipped();
        })->first();
    }
}
