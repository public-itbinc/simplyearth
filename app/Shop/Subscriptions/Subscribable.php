<?php

namespace App\Shop\Subscriptions;

use Facades\App\Shop\Subscriptions\FutureOrders;

trait Subscribable
{
    protected $future_subscription;
    /**
     * Determine if the model has a given subscription.
     *
     * @return bool
     */
    public function subscribed()
    {

        if (is_null($this->subscription)) {
            return false;
        }

        return $this->subscription->valid();
    }

    /**
     * Has a paused subscription
     *
     * @return bool
     */
    public function paused()
    {

        if (is_null($this->subscription)) {
            return false;
        }

        return !$this->subscription->valid();
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    /**
     * Subscription Builder
     *
     * @param string $plan
     * @return SubscriptionBuilder
     */
    public function subscribe($plan)
    {
        return new SubscriptionBuilder($this, $plan);
    }

    /**
     * Next Subscription Box
     *
     * @return FutureOrderMonth
     */
    public function nextBox()
    {
        return $this->futureOrders()->getNextBox();
    }

    public function scopeSubscribedUsers($query)
    {
        return $query->whereHas('subscription', function ($s) {
            $s->whereNull('ends_at');
        });
    }

    public function futureOrders()
    {
        if (is_null($this->future_subscription)) {
            $this->future_subscription = FutureOrders::setSubscription($this->subscription);
        }
        return $this->future_subscription;
    }

    public function refreshBox()
    {
        $this->future_subscription = null;

        return $this;
    }

    public function getBoxes()
    {
        if (!$this->subscribed()) {
            return collect([]);
        }

        return $this->futureOrders()->getMonths();
    }
}
