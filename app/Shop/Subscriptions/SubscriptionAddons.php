<?php

namespace App\Shop\Subscriptions;

use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;

class SubscriptionAddons extends Model
{
    protected $table = 'subscription_addons';

    protected $fillable = [
        'schedule',
        'product_id',
        'qty',
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
