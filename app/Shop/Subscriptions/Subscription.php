<?php

namespace App\Shop\Subscriptions;

use App\Events\SubscriptionGifted;
use App\Shop\Customers\Account;
use App\Shop\Orders\Order;
use App\Shop\Products\Product;
use App\Shop\Products\ProductInterface;
use Facades\App\Shop\Subscriptions\FutureOrders;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Subscription extends Model implements ProductInterface
{

    protected $fillable = [
        'plan',
        'starts_at',
        'paused_at',
        'ends_at',
        'schedule',
    ];

    protected $attributes = [
        'schedule' => 1,
    ];

    protected $dates = [
        'starts_at',
        'resumed_at',
        'ends_at',
        'paused_at',
        'trial_ends_at',
        'continue_at',
    ];

    protected $casts = [
        'auto_renew' => 'boolean',
    ];

    protected $appends = ['final_schedule'];

    public function subscriptionDiscounts()
    {
        return $this->hasMany(SubscriptionDiscount::class);
    }

    public function subscriptionNormalDiscounts()
    {
        return $this->hasMany(SubscriptionNormalDiscount::class);
    }

    public function subscriptionGifts()
    {
        return $this->hasMany(SubscriptionGift::class);
    }

    public function subscriptionPauses()
    {
        return $this->hasMany(SubscriptionPause::class);
    }

    public function subscriptionAddons()
    {
        return $this->hasMany(SubscriptionAddons::class);
    }

    public function subscriptionExchanges()
    {
        return $this->hasMany(SubscriptionExchanges::class);
    }

    public function subscriptionGiftcards()
    {
        return $this->hasMany(SubscriptionGiftcard::class);
    }

    public function subscriptionFreeboxes()
    {
        return $this->hasMany(SubscriptionFreebox::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'sku', 'plan')->where('type', 'subscription');
    }

    /**
     * Get the Account related to the subscription.
     */
    public function owner()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function commitments()
    {
        return $this->hasMany(Commitment::class);
    }

    /**
     * Determine if the subscription is active
     *
     * @return bool
     */
    public function valid()
    {
        return $this->active();
    }

    /**
     * Determine if the subscription is active.
     *
     * @return bool
     */
    public function active()
    {
        return is_null($this->ends_at);
    }

    /**
     * Determine if the subscription is no longer active.
     *
     * @return bool
     */
    public function stopped()
    {
        return !is_null($this->ends_at);
    }

    public function stop($reason = null)
    {
        $this->ends_at = Carbon::now();
        $this->cancel_reason = $reason;

        $this->save();

        history('subscription_stopped', $this->owner->customer->id, ['reason' => $reason]);

        return $this;
    }

    public function resume()
    {
        $this->ends_at = null;
        $this->failed_at = null;
        $this->failed_attempts = 0;
        $this->resumed_at = null;
        $this->cancel_reason = null;
        $this->auto_renew = true;
        $this->overrideSchedule(Carbon::today()->addDay(1)->format('d'));

        $this->save();

        history('subscription_resumed', $this->owner->customer->id);

        return $this;
    }

    public function commitmentUnpause()
    {
        $this->forceFill([
            'auto_renew' => true,
            'resumed_at' => null,
        ])->save();

        history('subscription_unpaused', $this->owner->customer->id);

        return $this;
    }

    public function futureOrders()
    {
        return FutureOrders::initSubscription($this);
    }

    public function skipMonth($month_year)
    {
        $this->subscriptionPauses()->firstOrCreate(['schedule' => $month_year]);
        $this->subscriptionGifts()->where('schedule', $month_year)->delete();
        //Restore Giftcards
        $this->subscriptionGiftcards()->where('schedule', $month_year)->each(function ($subscription_giftcard) {
            $subscription_giftcard->restore();
        });

        return $this;
    }

    public function giftMonth($month_year, array $data)
    {
        $this->subscriptionPauses()->where('schedule', $month_year)->delete();

        if ($existing = $this->subscriptionGifts()->where(['schedule' => $month_year])->first()) {
            $existing->update($data);

            history('box_updated_gift', $this->owner->customer->id, [
                'month_key' => $month_year,
                'gift_data' => $data,
            ]);

        } else {
            $gift = $this->subscriptionGifts()->create(['schedule' => $month_year]);
            $gift->fill($data)->save();

            history('box_gifted', $this->owner->customer->id, [
                'month_key' => $month_year,
                'gift_data' => $data,
            ]);

            event(new SubscriptionGifted($this, $gift));

        }

        return $this;
    }

    public function updateAddons($month_year, $product_id, $qty)
    {

        $product_id = is_numeric($product_id) ? $product_id : $product_id['id'];

        if ($qty <= 0) {
            $this->subscriptionAddons()
                ->where('schedule', $month_year)
                ->where('product_id', $product_id)->delete();
        } else {
            $this->subscriptionAddons()->updateOrCreate(
                ['schedule' => $month_year, 'product_id' => $product_id],
                ['qty' => (int) $qty]
            );
        }

        history('box_updated_addons', $this->owner->customer->id, [
            'month_key' => $month_year,
            'product_id' => $product_id,
            'qty' => $qty,
        ]);

        return $this;
    }

    public function updateExchanges($month_year, $from_id, $product_id)
    {
        $type = 'exchange';
        if ($product_id == $from_id) {
            $this->subscriptionExchanges()
                ->where('schedule', $month_year)
                ->where('from', $from_id)->delete();
            $type = 'delete';
        } else {
            $this->subscriptionExchanges()->updateOrCreate(
                ['schedule' => $month_year, 'from' => $from_id],
                ['product_id' => $product_id]
            );
        }

        history('box_updated_exchanges', $this->owner->customer->id, [
            'month_key' => $month_year,
            'from_id' => $from_id,
            'product_id' => $product_id,
            'exchange_type' => $type,
        ]);

        return $this;
    }

    public function resumeMonth($month_year)
    {
        $this->subscriptionPauses()->where('schedule', $month_year)->delete();
        $this->subscriptionGifts()->where('schedule', $month_year)->delete();
        $this->continue_at = null;
        $this->save();

        return $this;
    }

    public function nextboxContinue($month_year)
    {
        $stopped = null;

        foreach ($this->futureOrders()->getMonths() as $item) {
            if ($item->monthKey == $month_year) {
                $this->resumeMonth($month_year);
                break;
            }

            $this->skipMonth($item->monthKey);
            $stopped = $item;
        }

        history('box_continue', $this->owner->customer->id, [
            'month_key' => $month_year,
        ]);

        return [$stopped, $this->refresh()->futureOrders()->getNextBox()];
    }

    public function setSchedule($schedule)
    {
        $new_schedule = SubscriptionBuilder::parseConsolidateSchedule($schedule);
        $this->schedule = $new_schedule;
        $this->override_schedule = null;

        return $this;
    }

    public function getFinalScheduleAttribute()
    {
        return $this->override_schedule ?? SubscriptionBuilder::parseConsolidateSchedule($this->schedule);
    }

    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    public function overrideSchedule($schedule)
    {
        $this->override_schedule = min(max(1, (int) $schedule), 31);

        return $this;
    }

    public function isCommitment()
    {
        return in_array($this->plan, [config('subscription.commitment_box'), config('subscription.commitment_box_3')]);
    }

    public function isAutoRenew()
    {
        return $this->auto_renew;
    }

    public function getIsCommitmentAttribute()
    {
        return $this->isCommitment();
    }

    public function getFutureBoxCountAttribute()
    {
        if (!$this->isCommitment()) {
            return 12;
        }

        $commitment_count = $this->commitments->reduce(function ($carry, $commitment) {
            return $carry + $commitment->remaining_cycles;
        });

        return $commitment_count;
    }

    public function getCurrentCommitmentAttribute()
    {
        return $this->commitments->where('status', 'current')->first();
    }

    public function maybeIncrementCommitment(Order $order)
    {
        if (!$this->isCommitment()) {
            return $this;
        }

        //We are going to reset the commitment if it is created from resumed at reached
        if (!$this->current_commitment && $this->resumed_at <= Carbon::now()) {

            $this->forceFill([
                'auto_renew' => 1,
                'resumed_at' => null,
            ])->save();

            if ($pending = $this->commitments->where('status', 'pending')->first()) {
                $pending->status = 'current';
                $pending->save();
            }
        }

        if ($commitment = $this->current_commitment) {

            $commitment->increment('completed_cycles');
            $commitment->orders()->attach($order->id); //Attach order to the current commitment

            // If current  is complete, set first pending into current and create new pending

            if ($commitment->remaining_cycles < 1) {

                $commitment->status = 'completed';
                $commitment->save();

                $this->commitments()->create(
                    [
                        'status' => 'pending',
                        'cycles' => $this->commitment_months,
                    ]
                );

                if ($this->isAutoRenew() && $pending = $this->commitments->where('status', 'pending')->first()) {
                    $pending->status = 'current';
                    $pending->save();
                } else {
                    $this->stop();
                }
            }
        }

        return $this;
    }

    public function getCommitmentLastBoxAttribute()
    {
        if (!$this->isCommitment()) {
            return null;
        }

        return $this->futureOrders()->getMonths()->filter(function ($box) {
            return !$box->skipped() && $box->getCommitment() && $box->getCommitment()->status == 'current';
        })->last();
    }

    public function getCommitmentAllowedSkipsAttribute()
    {
        if (!$this->isCommitment()) {
            return collect([]);
        }

        $lastbox_from_now = $this->commitment_last_box ? $this->commitment_last_box->monthsFromNow() : 0;
        return $this->getNextSchedulesBySkips([$lastbox_from_now + 4, $lastbox_from_now + 7]);
    }

    public function commitmentPause(string $key)
    {
        $this->auto_renew = false;
        $this->resumed_at = Carbon::parse($key);
        $this->save();
    }

    public function commitmentStop()
    {
        $this->auto_renew = false;
        $this->resumed_at = null;
        $this->save();
    }

    public function getNextSchedulesBySkips($skips)
    {
        if (!is_array($skips)) {
            $skips = [$skips];
        }

        return $this->futureOrders()->getMonths()->filter(function ($box) use ($skips) {
            return in_array($box->monthsFromNow(), $skips);
        });
    }

}
