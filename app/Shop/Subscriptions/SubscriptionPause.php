<?php

namespace App\Shop\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPause extends Model
{
    protected $table = 'subscription_pauses';

    protected $fillable = [
        'schedule'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
