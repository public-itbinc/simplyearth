<?php

namespace App\Shop\Subscriptions;

use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;

class SubscriptionExchanges extends Model
{
    protected $table = 'subscription_exchanges';

    protected $fillable = [
        'schedule',
        'product_id',
        'from',
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function original()
    {
        return $this->hasOne(Product::class, 'id', 'from');
    }
}
