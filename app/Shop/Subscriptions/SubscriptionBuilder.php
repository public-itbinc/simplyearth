<?php

namespace App\Shop\Subscriptions;

use Carbon\Carbon;

class SubscriptionBuilder
{
    protected $plan;
    protected $owner;
    protected $schedule;
    protected $commitment_months = 0;

    public function __construct($owner, $plan)
    {
        $this->owner = $owner;
        $this->plan = $plan;
        $this->schedule = self::parseConsolidateSchedule(Carbon::now()->format('d'));
    }

    public function setSchedule($schedule)
    {
        $this->schedule = (int) $schedule;

        return $this;
    }

    public function setCommitment(int $commitment_months)
    {
        $this->commitment_months = $commitment_months;

        return $this;
    }

    public function create()
    {
        $subscription = Subscription::firstOrNew(
            [
                'account_id' => $this->owner->id,
            ]
        );

        $subscription->forceFill(
            [
                'commitment_months' => $this->commitment_months,
                'plan' => $this->plan,
                'paused_at' => null,
                'ends_at' => null,
                'schedule' => $this->schedule,
            ]
        );

        $subscription = $this->owner->subscription()->save($subscription);

        if ($subscription) {
            history('subscription_created', $this->owner->customer->id, [
                'plan' => $this->plan,
            ]);
        }

        if (!is_null($this->commitment_months) && $this->commitment_months > 0) {
            $subscription->commitments()->createMany(
                [
                    [
                        'status' => 'current',
                        'cycles' => $this->commitment_months,
                    ],
                    [
                        'status' => 'pending',
                        'cycles' => $this->commitment_months,
                    ],
                    [
                        'status' => 'pending',
                        'cycles' => $this->commitment_months,
                    ]
                ]
            );
        }

        return $subscription;
    }

    public static function parseConsolidateSchedule($day)
    {
        $day = (int) $day;

        if ($day > 20) {
            return 20;
        }

        return $day;
    }

    public function getSchedule()
    {
        return $this->schedule;
    }
}
