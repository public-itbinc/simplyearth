<?php

namespace App\Shop\Subscriptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class SubscriptionGift extends Model
{
    protected $table = 'subscription_gifts';

    protected $fillable = [
        'schedule',
        'first_name',
        'last_name',
        'phone',
        'email',
        'email_date',
        'gift_date',
        'contact_by_email',
        'address_first_name',
        'address_last_name',
        'address_company',
        'address_phone',
        'address_address1',
        'address_address2',
        'address_city',
        'address_zip',
        'address_country',
        'address_region',
        'message',
    ];

    protected $dates = [
        'gift_date',
        'email_date',
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function scopeEmailGiftToday($query)
    {
        $today = Carbon::today();

        return $query->whereDate('email_date', '=', $today->toDateString())
            ->where('sent', 0)
            ->whereHas('subscription', function ($subscription) use ($today) {
                $subscription->whereNull('ends_at');
            });
    }
}
