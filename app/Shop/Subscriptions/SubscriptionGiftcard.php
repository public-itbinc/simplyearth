<?php

namespace App\Shop\Subscriptions;

use Illuminate\Database\Eloquent\Model;
use App\Shop\Discounts\GiftCard;

class SubscriptionGiftcard extends Model
{
    protected $fillable = [
        'code',
        'amount',
        'schedule'
    ];

    public function restore()
    {
        if ($gift_card = GiftCard::where('code', $this->code)->first()) {
            $gift_card->remaining += $this->amount;
            $gift_card->save();
        }

        $this->delete();
    }
}
