<?php

namespace App\Shop\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class SubscriptionDiscount extends Model
{
    protected $table = 'subscription_discounts';

    protected $fillable = [
        'amount',
        'code',
        'type',
        'limit'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
