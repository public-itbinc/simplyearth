<?php

namespace App\Shop\Conversion;

use GuzzleHttp\Client;

class Stamped
{
    protected $url = "https://stamped.io/api/simplyearth.com/survey/reviews/bulk";
    private $client;
    private $client_id;
    private $client_secret;

    public function __construct()
    {
        $this->client = new Client();
        $this->client_id = config('conversion.stamped.client_id');
        $this->client_secret = config('conversion.stamped.client_secret');
    }

    public function track(array $data = [])
    {
        $data = array_merge([], $data);

        $result = $this->client->post($this->url, [
            'auth' => [$this->client_id, $this->client_secret],
            'json' => $data,
            'timeout' => 2000,
        ]);

        return $result;
    }

    public function getReview($product_id = '')
    {
        
        $result = $this->client->get('https://stamped.io/api/simplyearth.com/richsnippet?productid='.$product_id, [
            'auth' => [$this->client_id, $this->client_secret]
        ]);
    }

}
