<?php

namespace App\Shop\Conversion;

use GuzzleHttp\Client;

class Refersion
{
    const url = 'https://inbound-webhooks.refersion.com/tracker/orders/paid';
    protected $public_key;
    protected $secret_key;
    private $client;

    public function __construct()
    {
        $this->public_key = config('conversion.refersion.api_key');
        $this->secret_key = config('conversion.refersion.secret_key');

        $this->client = new Client();
    }

    public function sendOrder($data)
    {
        $this->client->post($this->api_url, [
            'json' => $data,
            'headers' => $this->getHeaders(),
        ]);
    }

    protected function getHeaders()
    {
        return [
            'Refersion-Public-Key' => $this->public_key,
            'Refersion-Secret-Key' => $this->secret_key,
        ];
    }
}
