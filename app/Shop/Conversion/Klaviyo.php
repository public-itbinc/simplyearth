<?php

namespace App\Shop\Conversion;

use GuzzleHttp\Client;

class Klaviyo
{
    const url = 'https://a.klaviyo.com';
    protected $api_key;
    protected $private_api_key;
    protected $event = '';
    protected $options;
    private $client;

    public function __construct($api_key, $private_api_key, $options = [])
    {
        $this->api_key = $api_key;
        $this->private_api_key = $private_api_key;
        $this->options = $options;
        $this->client = new Client();
    }

    public function track(string $event_name, array $data = [])
    {
        $data = array_merge([
            'token' => $this->api_key,
            'event' => $event_name,
        ], $data);

        $result = $this->client->get(self::url . '/api/track?data='.base64_encode(json_encode($data)));

        return $result;
    }

    public function updateCustomerProperties($email, array $properties = [])
    {
        $data = [
            'token' => $this->api_key,
            'properties' => array_merge( $properties, [
                '$email' => $email,
            ])
        ];

        $result = $this->client->get(self::url . '/api/identify?data='.base64_encode(json_encode($data)));

        return $result;
    }

    public function updateCustomerTags($email, array $tags = [])
    {
        $data = [
            'token' => $this->api_key,
            'properties' =>[
                '$email' => $email,
                'Tags' => !empty($tags) ? implode(' ', $tags) : ' ',
            ],            
        ];

        $result = $this->client->get(self::url . '/api/identify?data='.base64_encode(json_encode($data)));

        return $result;
    }

    public function subscribe($list_id, $email, $other_data = [])
    {
        $data = [
            'api_key' => $this->private_api_key,
            'profiles' => [
                [
                    'email' => $email,
                    'web_origin' => 'Simply Earth footer'
                ]
            ]
        ];

        $result = $this->client->POST(self::url . '/api/v2/list/'.$list_id.'/members', [
            'json' => $data
        ]);

        return $result;
    }

    public function sendEmail($templated_id, $email, array $email_data)
    {
        $data = array_merge($email_data, [
            'api_key' => $this->private_api_key,
            'to' => $email,
            'from_email' => 'hello@simplyearth.com',
            'from_name' => 'Simply Earth',
        ]);

        $result = $this->client->POST(self::url . '/api/v1/email-template/'.$templated_id.'/send', [
            'form_params' => $data
        ]);

        return $result;
    }
}
