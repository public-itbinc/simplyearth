<?php

use Faker\Generator as Faker;
use App\Shop\Subscriptions\Subscription;
use App\Shop\Customers\Account;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'account_id' => factory(Account::class)->create()->id,
        'schedule' => $faker->numberBetween(1, 15)
    ];
});
