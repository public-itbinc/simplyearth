<?php

use Faker\Generator as Faker;

$factory->define(App\Shop\Customers\Customer::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,        
    ];
});
