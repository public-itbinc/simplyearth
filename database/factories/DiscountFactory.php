<?php

use Faker\Generator as Faker;
use App\Shop\Subscriptions\SubscriptionDiscount;
use App\Shop\Discounts\Discount;

$factory->define(SubscriptionDiscount::class, function (Faker $faker) {
    return [
        'amount' => $faker->price,
        'code' => str_random(10),
        'type' => 'fixed',
    ];
});
