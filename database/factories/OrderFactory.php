<?php

use Faker\Generator as Faker;
use App\Shop\Orders\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'currency' => 'USD',
        'email' => $faker->email        
    ];
});
