<?php

use Faker\Generator as Faker;

$factory->define(App\Shop\ShoppingBoxes\ShoppingBox::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'key' => $faker->slug,
    ];
});
