<?php

use Faker\Generator as Faker;

$factory->define(App\Shop\Customers\Account::class, function (Faker $faker) {
    return [
        'email' => factory(App\Shop\Customers\Customer::class)->create()->email,
        'password' => bcrypt('password'),
    ];
});
