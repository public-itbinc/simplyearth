<?php

use Faker\Generator as Faker;

$factory->define(App\Shop\Products\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->catchPhrase(),
        'price' => $faker->numberBetween(10, 20),
        'sku' => $faker->creditCardNumber(),
        'status' => 'active',
        'description' => $faker->paragraph,
        'short_description' => $faker->paragraph,
        'meta_title' => $faker->title,
        'meta_description' => $faker->sentence,
        'meta_keywords' => implode(',', $faker->words)
    ];
});
