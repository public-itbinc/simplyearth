<?php

use Faker\Generator as Faker;

$factory->define(App\Shop\Customers\Activation::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'token' => \Password::getRepository()->createNewToken()
    ];
});
