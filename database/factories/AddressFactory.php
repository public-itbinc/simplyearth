<?php

use Faker\Generator as Faker;

$factory->define(App\Shop\Customers\Address::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'company' => $faker->company,
        'address1' => $faker->streetAddress,
        'address2' => $faker->address,
        'city' => $faker->city,
        'zip' => $faker->postcode,
        'country' => $faker->countryCode,
        'region' => $faker->state,
    ];
});
