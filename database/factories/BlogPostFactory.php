<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\BlogPost::class, function (Faker $faker) {
    $title = $faker->title;
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'content' => $faker->sentence,
        'status' => 'publish',
        'seo_title' => $faker->title,
        'seo_description' => $faker->sentence,
        'seo_keywords' => implode(',', $faker->words),
        'user_id' => User::create(['name' => $faker->name, 'email' => $faker->email, 'password' => str_random(10)])->id
    ];
});
