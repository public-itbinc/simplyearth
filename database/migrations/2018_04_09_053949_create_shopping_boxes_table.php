<?php

use App\Shop\ShoppingBoxes\ShoppingBox;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_boxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique();
            $table->string('name');
            $table->string('cover')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        $monthly_boxes = [
            [
                'name' => 'January Box',
                'key' => 'january',
            ],
            [
                'name' => 'February Box',
                'key' => 'february',
            ],
            [
                'name' => 'March Box',
                'key' => 'march',
            ],
            [
                'name' => 'April Box',
                'key' => 'april',
            ],
            [
                'name' => 'May Box',
                'key' => 'may',
            ],
            [
                'name' => 'June Box',
                'key' => 'june',
            ],
            [
                'name' => 'July Box',
                'key' => 'july',
            ],
            [
                'name' => 'August Box',
                'key' => 'august',
            ],
            [
                'name' => 'September Box',
                'key' => 'september',
            ],
            [
                'name' => 'October Box',
                'key' => 'october',
            ],
            [
                'name' => 'November Box',
                'key' => 'november',
            ],
            [
                'name' => 'December Box',
                'key' => 'december',
            ],
        ];

        foreach ($monthly_boxes as $box) {

            ShoppingBox::create($box);

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_boxes');
    }
}
