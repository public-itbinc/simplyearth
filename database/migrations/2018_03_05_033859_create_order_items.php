<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantity')->default(1);
            $table->decimal('price')->default(0.00);
            $table->mediumText('extra_attributes')->nullable();
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->boolean('requires_shipping')->default(1);
            $table->decimal('weight')->default(0.00);
            $table->bigInteger('product_id')->nullable();
            $table->string('sku')->nullable();
            $table->mediumText('image_url')->nullable();
            $table->string('name');
            $table->boolean('taxable')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
