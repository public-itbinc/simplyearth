<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->unsigned()->index()->nullable();
            $table->integer('first_sub_category')->unsigned()->index()->nullable();
            $table->integer('second_sub_category')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('parent')->references('id')->on('blog_categories')->onDelete('cascade');
            $table->foreign('first_sub_category')->references('id')->on('blog_categories')->onDelete('cascade');
            $table->foreign('second_sub_category')->references('id')->on('blog_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_sub_categories');
    }
}
