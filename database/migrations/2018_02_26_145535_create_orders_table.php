<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->dateTime('closed_at')->nullable();
            $table->dateTime('cancelled_at')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->string('cancel_reason', 50)->nullable();
            $table->string('status', 50)->default('pending');
            $table->dateTime('processed_at')->nullable();
            $table->string('token')->nullable();
            $table->string('cart_token')->nullable();
            $table->boolean('gift')->default(false);
            $table->mediumText('gift_message')->nullable();
            $table->string('currency', 4)->default('USD');
            $table->mediumText('note')->nullable();
            $table->decimal('total_weight')->default(0.00);
            $table->decimal('total_shipping')->default(0.00);
            $table->decimal('subtotal_price')->default(0.00);
            $table->decimal('total_discounts')->default(0.00);
            $table->decimal('total_tax')->default(0.00);
            $table->decimal('tax_percentage')->default(0.00);
            $table->decimal('total_price')->default(0.00);
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->text('client_details')->nullable();
            $table->mediumText('requested_shipping_service')->nullable();
            $table->dateTime('shipping_date')->nullable();
            $table->mediumText('shipping_service')->nullable();
            $table->string('shipping_carrier')->nullable();
            $table->string('tracking_number')->nullable();
            $table->text('payment_details')->nullable();
            $table->string('landing_page')->nullable();
            $table->boolean('buyer_accepts_marketing')->default(1);
            $table->timestamps();
        });

        $startId = config('database.orders.start_id', 10000);

        DB::table('orders')->insert(['id' => $startId - 1]);
        DB::table('orders')->where('id', $startId - 1)->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
