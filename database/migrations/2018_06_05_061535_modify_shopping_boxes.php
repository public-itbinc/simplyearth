<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyShoppingBoxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping_boxes', function (Blueprint $table) {
            $table->mediumText('features')->nullable();
            $table->string('box_video_id')->nullable();
            $table->string('box_video_cover')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping_boxes', function (Blueprint $table) {
            $table->dropColumn('features');
        });
    }
}
