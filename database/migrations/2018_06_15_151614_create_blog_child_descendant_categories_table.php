<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogChildDescendantCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_child_descendant_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('child')->unsigned()->index();
            $table->integer('descendant')->unsigned()->index();
            $table->timestamps();
            $table->foreign('child')->references('id')->on('blog_categories')->onDelete('cascade');
            $table->foreign('descendant')->references('id')->on('blog_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_child_descendant_categories');
    }
}
