<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFooterAndHeaderFieldToPagebuilderPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagebuilder_pages', function (Blueprint $table) {
            $table->string('footer')->default('main')->after('slug');
            $table->string('header')->default('main')->after('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagebuilder_pages', function (Blueprint $table) {
            //
        });
    }
}
