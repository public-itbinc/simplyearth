<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('name');
            $table->string('sku')->unique();
            $table->mediumText('short_description')->nullable();
            $table->text('description')->nullable();
            $table->integer('quantity')->default(0)->nullable();
            $table->integer('sort_order')->default(0)->nullable();
            $table->decimal('price')->default(0);
            $table->string('status')->default('draft');
            $table->boolean('shipping')->default(0);
            $table->decimal('weight')->default(0);
            $table->mediumText('meta_title')->nullable();
            $table->mediumText('meta_description')->nullable();
            $table->mediumText('meta_keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
