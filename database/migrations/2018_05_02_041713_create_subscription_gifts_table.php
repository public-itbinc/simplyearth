<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptiongiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_id')->unsigned()->index();
            $table->string('schedule');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('message')->nullable();
            $table->dateTime('gift_date')->nullable();
            $table->dateTime('email_date')->nullable();
            $table->boolean('contact_by_email')->default(0);
            $table->string('address_first_name')->nullable();
            $table->string('address_last_name')->nullable();
            $table->string('address_company')->nullable();
            $table->string('address_phone')->nullable();
            $table->string('address_address1')->nullable();
            $table->string('address_address2')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_zip')->nullable();
            $table->string('address_country', 2)->nullable();
            $table->string('address_region')->nullable();
            $table->timestamps();
            $table->unique(['schedule', 'subscription_id']);
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_gifts');
    }
}
