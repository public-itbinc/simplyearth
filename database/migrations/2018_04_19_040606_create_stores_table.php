<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->nullable();
          $table->string('address')->nullable();
          $table->string('city')->nullable();
          $table->string('zipcode')->nullable();
          $table->string('country')->nullable();
          $table->string('state')->nullable();
          $table->float('latitude', 10, 6);
          $table->float('longitude', 10, 6);
          $table->text('description')->nullable();
          $table->string('phone')->nullable();
          $table->string('email')->nullable();
          $table->string('fax')->nullable();
          $table->string('web')->nullable();
          $table->string('tags')->nullable(); # Task 17
          $table->string('schedule')->nullable();
          $table->string('store_iamge')->nullable();
          $table->string('marker_image')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
