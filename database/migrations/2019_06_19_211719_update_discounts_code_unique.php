<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDiscountsCodeUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discounts', function (Blueprint $table) {
            $table->unique('code');
        });

        
        
        Schema::table('customers', function (Blueprint $table) {
            $table->unique('share_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discounts', function (Blueprint $table) {
            $table->dropUnique('discounts_code_unique');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->dropUnique('customers_share_code_unique');
        });
    }
}
