<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubscriptionDiscountsUnlimited extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_discounts', function (Blueprint $table) {
            $table->boolean('unlimited')->default(false);
            $table->tinyInteger('limit')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_discounts', function (Blueprint $table) {
            $table->dropColumn('unlimited');
            $table->dropColumn('limit');
        });
    }
}
