<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VariableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('setup')->default('single');
            $table->bigInteger('parent_id')->nullable();
            $table->mediumText('variant_attributes')->nullable();
            $table->mediumText('variant_cover')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('setup');
            $table->dropColumn('parent_id');
            $table->dropColumn('variant_attributes');
            $table->dropColumn('variant_cover');
        });
    }
}
