<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()


    
    {
        Schema::create('history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model_type');
            $table->bigInteger('model_id');
            $table->string('type')->default('default');
            $table->bigInteger('initiator')->nullable();
            $table->string('initiator_type');
            $table->text('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history');
    }
}
