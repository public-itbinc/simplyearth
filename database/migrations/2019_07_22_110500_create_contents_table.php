<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagebuilder_contents', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('page_id');
            $table->foreign('page_id')->references('id')->on('pagebuilder_pages')->onDelete('cascade');

            $table->unsignedInteger('template_id');
            $table->foreign('template_id')->references('id')->on('pagebuilder_templates')->onDelete('cascade');

            $table->text('content');
            $table->integer('rang');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagebuilder_contents');
    }
}
