<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductShoppingBoxPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_shopping_box', function (Blueprint $table) {
            $table->bigInteger('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('shopping_box_id')->unsigned()->index();
            $table->foreign('shopping_box_id')->references('id')->on('shopping_boxes')->onDelete('cascade');
            $table->primary(['product_id', 'shopping_box_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_shopping_box');
    }
}
