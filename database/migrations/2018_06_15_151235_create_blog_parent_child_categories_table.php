<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogParentChildCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_parent_child_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->unsigned()->index();
            $table->integer('child')->unsigned()->index();
            $table->timestamps();

            $table->foreign('parent')->references('id')->on('blog_categories')->onDelete('cascade');
            $table->foreign('child')->references('id')->on('blog_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_parent_child_categories');
    }
}
