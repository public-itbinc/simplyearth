<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan');
            $table->string('status')->default('active');
            $table->tinyInteger('schedule')->unsigned();
            $table->tinyInteger('cycles')->unsigned();
            $table->tinyInteger('paid_cycles')->unsigned()->default(0);
            $table->tinyInteger('failed_attempts')->unsigned()->default(0);
            $table->decimal('amount')->default(0.00);
            $table->integer('account_id')->unsigned();
            $table->date('next_schedule_date');
            $table->timestamps();
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_plans');
    }
}
