<?php

use App\Models\Pagebuilder\Template;
use App\Models\Pagebuilder\Category;
use App\Models\Pagebuilder\Pivots\Content;
use Illuminate\Database\Seeder;

class AddSectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Create default categories
        Category::updateOrCreate([
            "slug" => 'contents',
        ], [
            "name" => 'Contents'
        ]);

        Category::updateOrCreate([
            "slug" => 'features'
        ], [
            "name" => 'Features'
        ]);

        Category::updateOrCreate([
            "slug" => 'testimonials'
        ], [
            "name" => 'Testimonials'
        ]);

        foreach (Category::all() as $category) {
            $categories = Category::query()->whereSlug($category->slug);

            if ($categories->count() >= 2) {
                $drop_category = $categories->latest()->first();
                $drop_category->delete();
            }
        }

        //Create default templates
        Template::updateOrCreate([
            'type' => 'text_and_image'
        ], [
            'name' => 'Text and Img',
            'position_inverse' => true,
            "preview" => [
                "previewDesktop" => '/builder/demo/text_and_image-desk.png',
                "previewTabledHorizontal" => '/builder/demo/text_and_image-desk.png',
                "previewTabled" => '/builder/demo/text_and_image-tabled.png',
                "previewMobile" => '/builder/demo/text_and_image-tabled.png',
            ],
            'structure' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "text" => '<h2>The most amazing essential oil recipe box</h2><p>For 100% pure essential oil + Natural Ingredients every month</p>',
                "button" => [
                    "text" => 'Visit Our Shop',
                    "line" => '#',
                    "type" => 'button'
                ],
                "img" => '/builder/img/text-and-image-prev.png',
                "video" => false,
                "video_preview" => '',
                'position_inverse' => false,
            ],
            'section' => 'contents',
        ]);

        Template::updateOrCreate([
            'type' => 'text_mixed_and_image'
        ], [
            'name' => 'Mixed text and Img',
            'position_inverse' => true,
            "preview" => [
                "previewDesktop" => '/builder/demo/text_and_image-mix.png',
                "previewTabledHorizontal" => '/builder/demo/text_and_image-mix.png',
                "previewTabled" => '/builder/demo/text_and_image-mix.png',
                "previewMobile" => '/builder/demo/text_and_image-mix.png',
            ],
            'structure' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "text" => '<h2>The most amazing essential oil recipe box</h2><p>For 100% pure essential oil + Natural Ingredients every month</p><a href="#" class="btn">CHECK OUT THIS MONTH’S BOX</a><p><small>Don’t wait to make it yours. It WILL sell out!</small></p>',
                "button" => [
                    "text" => '',
                    "line" => '#',
                    "type" => 'btn-link'
                ],
                "img" => '/builder/demo/test.png',
                "video" => false,
                "video_preview" => '',
                'position_inverse' => false,
            ],
            'section' => 'contents',
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_blue',
        ], [
            "name" => 'Text and Blue',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/text_and_blue-desk.png',
                "previewTabledHorizontal" => '/builder/demo/text_and_blue-desk.png',
                "previewTabled" => '/builder/demo/text_and_blue-desk.png',
                "previewMobile" => '/builder/demo/text_and_blue-mobile.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#00b4ca',
                    "image" => ''
                ],
                "text" => '<p>13% of our profits go to help end human trafficking.</p><h2>Check Out The Organizations We Support</h2><p>Each month we highlight one organization so that you can get involved.</p>',
                "image" => '/builder/img/avatar.png'
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'simple_content_CTA',
        ], [
            "name" => 'Simple Content CTA',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/simple-content-cta-desk.png',
                "previewTabledHorizontal" => '/builder/demo/simple-content-cta-desk.png',
                "previewTabled" => '/builder/demo/simple-content-cta-desk.png',
                "previewMobile" => '/builder/demo/simple-content-cta-desk.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>Simple Content CTA</h2><p>Dragée sweet fruitcake topping dragée carrot cake biscuit toffee jelly-o. </p>',
                "image" => '/builder/img/text-and-image-prev.png',
                "button" => [
                    "text" => 'Button',
                    "link" => '#',
                    "type" => 'button'
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'accordion',
        ], [
            "name" => 'Accordion',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/accordion.png',
                "previewTabledHorizontal" => '/builder/demo/accordion.png',
                "previewTabled" => '/builder/demo/accordion-mobile.png',
                "previewMobile" => '/builder/demo/accordion-mobile.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#F8F8F8',
                    "image" => ''
                ],
                "text" => '<h2>Frequently Asked Questions</h2>',
                "accordionItem" => [
                    [
                        "title" => '<h4>What will I get in my essential oil Recipe Box?</h4>',
                        "text" => '<p>Every month, we’ll send 4 full size 100% pure essential oils + extras to make 6 recipes. You will have everything you need to make your home natural. Each month is a new fun theme. Plus, you’ll get tips on other ways to use your oils.</p>'
                    ],
                    [
                        "title" => '<h4>Is it really only for $39 for 4 essential oils?</h4>',
                        "text" => '<p>Every month, we’ll send 4 full size 100% pure essential oils + extras to make 6 recipes. You will have everything you need to make your home natural. Each month is a new fun theme. Plus, you’ll get tips on other ways to use your oils.</p>'
                    ],
                    [
                        "title" => '<h4>When will my next box ship?</h4>',
                        "text" => '<p>Every month, we’ll send 4 full size 100% pure essential oils + extras to make 6 recipes. You will have everything you need to make your home natural. Each month is a new fun theme. Plus, you’ll get tips on other ways to use your oils.</p>'
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_blue_offer'
        ], [
            "name" => 'Text And Blue Offer',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/offer.png',
                "previewTabledHorizontal" => '/builder/demo/offer.png',
                "previewTabled" => '/builder/demo/offer.png',
                "previewMobile" => '/builder/demo/offer.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#00b4ca',
                    "image" => ''
                ],
                "text" => '<p><img src="/builder/img/box-icon.png" alt=""><strong>FREE SHIPPING</strong> on all orders with 6+ boxes</p>',
                "image" => '/builder/img/box-icon.png',
                "button" => [
                    "text" => 'Build Your Box',
                    "link" => '#',
                    "type" => 'button'
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'features_icon'
        ], [
            "name" => 'Features Icon',
            'section' => 'features',
            "preview" => [
                "previewDesktop" => '/builder/demo/feature-icon.png',
                "previewTabledHorizontal" => '/builder/demo/feature-icon.png',
                "previewTabled" => '/builder/demo/feature-icon.png',
                "previewMobile" => '/builder/demo/feature-icon-mobile.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Invite</h3><p>Sign up and share your coupon code with friends.</p>',
                        "image" => '/builder/img/invite-icon.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Subscribe</h3><p>Your friend gets their first box free when they subscribe.</p>',
                        "image" => '/builder/img/invite-icon.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Claim</h3><p>You get a FREE BOX for every friend that subscribes.</p>',
                        "image" => '/builder/img/invite-icon.png',
                        "video" => false,
                        "video_preview" => '',
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_icon',
        ], [
            "name" => 'Text and Icon',
            'section' => 'features',
            "preview" => [
                "previewDesktop" => '/builder/demo/feature-photo2.png',
                "previewTabledHorizontal" => '/builder/demo/feature-photo2.png',
                "previewTabled" => '/builder/demo/feature-photo2.png',
                "previewMobile" => '/builder/demo/feature-photo-mobile.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Subsribe and Save!</h3><p>Join for as low as $39/box. Every box is worth more than $150 compared to the big essential oil companies.</p>',
                        "image" => '/builder/img/feature-photo-1.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Free Big Bonus Box</h3><p>When you start today! It includes coconut oil, almond oil, beeswax, roller bottles, and 5ml bottles.</p>',
                        "image" => '/builder/img/feature-photo-2.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Free Shipping</h3><p>Your first box ships right away. Your next box ships on the “1st” or “15th” dependent on the current date.</p>',
                        "image" => '/builder/img/feature-photo-3.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Natural Home</h3><p>Have fun being a DIY Natural Home Master using tested 100% pure essential oils and natural ingredients.</p>',
                        "image" => '/builder/img/feature-photo-4.png',
                        "video" => false,
                        "video_preview" => '',
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_icon_three',
        ], [
            "name" => 'Text and Icon Tree',
            'section' => 'features',
            "preview" => [
                "previewDesktop" => '/builder/demo/feature-photo-tree.png',
                "previewTabledHorizontal" => '/builder/demo/feature-photo-tree.png',
                "previewTabled" => '/builder/demo/feature-photo-tree.png',
                "previewMobile" => '/builder/demo/feature-photo-mobile.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Invite</h3><p>Sign up and share your coupon code with friends.</p>',
                        "image" => '/builder/img/feature-photo-5.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Subscribe</h3><p>Your friend gets their first box free when they subscribe.</p>',
                        "image" => '/builder/img/feature-photo-6.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Claim</h3><p>You get a FREE BOX for every friend that subscribes.</p>',
                        "image" => '/builder/img/feature-photo-7.png',
                        "video" => false,
                        "video_preview" => '',
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'testimonial_v1',
        ], [
            "name" => 'Testimonial V1',
            'section' => 'testimonials',
            "preview" => [
                "previewDesktop" => '/builder/demo/testimonial-demo.png',
                "previewTabledHorizontal" => '/builder/demo/testimonial-demo.png',
                "previewTabled" => '/builder/demo/testimonial-demo-mobile.png',
                "previewMobile" => '/builder/demo/testimonial-demo-mobile.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#f8f8f8',
                    "image" => ''
                ],
                "text" => '<h2>See What Others Are Saying</h2><p>Our fans love sharing how they are using Simply Earth to make their homes natural. Join the community using #simplyearth.</p>',
                "image" => 'https://simplyearth.com/images/testimonials-group.jpg',
                "testimonials" => [
                    [
                        "text" => '<p>"I love it! Thanks @fromsimplyearth"</p>',
                        "img" => 'https://simplyearth.com/images/testimonial-megan.jpg',
                        "email" => '@meganeacuna'
                    ],
                    [
                        "text" => '<p>"I love it! Thanks @fromsimplyearth"</p>',
                        "img" => 'https://simplyearth.com/images/testimonial-happy.jpg',
                        "email" => '@meganeacuna'
                    ],
                    [
                        "text" => '<p>"I love it! Thanks @fromsimplyearth"</p>',
                        "img" => 'https://simplyearth.com/images/testimonial-gina.jpg',
                        "email" => '@meganeacuna'
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_icon_tree',
        ], [
            "name" => 'Text And Icon Tree',
            'section' => 'testimonials',
            "preview" => [
                "previewDesktop" => '/builder/demo/video-testimonial.png',
                "previewTabledHorizontal" => '/builder/demo/video-testimonial.png',
                "previewTabled" => '/builder/demo/video-testimonial.png',
                "previewMobile" => '/builder/demo/video-testimonial.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#FEFAF7',
                    "image" => ''
                ],
                "text" => '<h2>PEOPLE LOVE US</h2>',
                "blocks" => [
                    [
                        "text" => '<p>Chocolate tootsie roll candy canes cotton candy. Pastry icing jelly icing tiramisu candy canes. Candy canes halvah pie ice cream.</p><span class="name">Jordan S.</span><p style="color: #404141;">Springfield, MO</p>',
                        "image" => '/builder/img/thumbnail.png',
                        "video" => true,
                        "video_preview" => '/builder/img/thumbnail.png',
                    ],
                    [
                        "text" => '<p>Chocolate tootsie roll candy canes cotton candy. Pastry icing jelly icing tiramisu candy canes. Candy canes halvah pie ice cream.</p><span class="name">Jordan S.</span><p style="color: #404141;">Springfield, MO</p>',
                        "image" => '/builder/img/thumbnail.png',
                        "video" => true,
                        "video_preview" => '/builder/img/thumbnail.png',
                    ],
                    [
                        "text" => '<p>Chocolate tootsie roll candy canes cotton candy. Pastry icing jelly icing tiramisu candy canes. Candy canes halvah pie ice cream.</p><span class="name">Jordan S.</span><p style="color: #404141;">Springfield, MO</p>',
                        "image" => '/builder/img/thumbnail.png',
                        "video" => true,
                        "video_preview" => '/builder/img/thumbnail.png',
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'title',
        ], [
            "name" => 'Title',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/title.png',
                "previewTabledHorizontal" => '/builder/demo/title.png',
                "previewTabled" => '/builder/demo/title.png',
                "previewMobile" => '/builder/demo/title.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h1>Why Simply Earth</h1>',
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'text',
        ], [
            "name" => 'Text',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/text-desk.png',
                "previewTabledHorizontal" => '/builder/demo/text-desk.png',
                "previewTabled" => '/builder/demo/text-mobi.png',
                "previewMobile" => '/builder/demo/text-mobi.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "default" => '<p>New to Essential Oils? Not sure where to begin? We are here to help. We love essential oils so much that we have developed a FREE essential oils ebook. It will help you get started with essential oils. It includes everything from what essential oils actually are to ideas on how to incorporate essential oils into your daily life.</p>',
                "cpc" => '<p>New to Essential Oils? Not sure where to begin? We are here to help. We love essential oils so much that we have developed a FREE essential oils ebook. It will help you get started with essential oils. It includes everything from what essential oils actually are to ideas on how to incorporate essential oils into your daily life.</p>',
                "cpm" => '<p>New to Essential Oils? Not sure where to begin? We are here to help. We love essential oils so much that we have developed a FREE essential oils ebook. It will help you get started with essential oils. It includes everything from what essential oils actually are to ideas on how to incorporate essential oils into your daily life.</p>',
                "email" => '<p>New to Essential Oils? Not sure where to begin? We are here to help. We love essential oils so much that we have developed a FREE essential oils ebook. It will help you get started with essential oils. It includes everything from what essential oils actually are to ideas on how to incorporate essential oils into your daily life.</p>',
                "socia" => '<p>New to Essential Oils? Not sure where to begin? We are here to help. We love essential oils so much that we have developed a FREE essential oils ebook. It will help you get started with essential oils. It includes everything from what essential oils actually are to ideas on how to incorporate essential oils into your daily life.</p>',
            ]
        ]);


        Template::updateOrCreate([
            'type' => 'image',
        ], [
            'name' => 'Image',
            'position_inverse' => true,
            "preview" => [
                "previewDesktop" => '/builder/demo/image.png',
                "previewTabledHorizontal" => '/builder/demo/image.png',
                "previewTabled" => '/builder/demo/image.png',
                "previewMobile" => '/builder/demo/image.png',
            ],
            'structure' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "img" => '/builder/img/text-and-image-prev.png',
                "video" => false,
                'position_inverse' => true,
            ],
            'section' => 'contents',
        ]);

        Template::updateOrCreate([
            'type' => 'video',
        ], [
            'name' => 'Video',
            'position_inverse' => true,
            "preview" => [
                "previewDesktop" => '/builder/demo/video.png',
                "previewTabledHorizontal" => '/builder/demo/video.png',
                "previewTabled" => '/builder/demo/video.png',
                "previewMobile" => '/builder/demo/video.png',
            ],
            'structure' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "img" => '/builder/img/text-and-image-prev.png',
                "video" => false,
                "video_preview" => '',
                'position_inverse' => true,
            ],
            'section' => 'contents',
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_icon_slider',
        ], [
            "name" => 'Text and Icon Slider',
            'section' => 'features',
            "preview" => [
                "previewDesktop" => '/builder/demo/feature-photo2.png',
                "previewTabledHorizontal" => '/builder/demo/feature-photo2.png',
                "previewTabled" => '/builder/demo/feature-photo2.png',
                "previewMobile" => '/builder/demo/text-icon-tree-slider-mobi.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Subsribe and Save!</h3><p>Join for as low as $39/box. Every box is worth more than $150 compared to the big essential oil companies.</p>',
                        "image" => '/builder/img/feature-photo-1.png',
                        "video" => false
                    ],
                    [
                        "text" => '<h3>Free Big Bonus Box</h3><p>When you start today! It includes coconut oil, almond oil, beeswax, roller bottles, and 5ml bottles.</p>',
                        "image" => '/builder/img/feature-photo-2.png',
                        "video" => false
                    ],
                    [
                        "text" => '<h3>Free Shipping</h3><p>Your first box ships right away. Your next box ships on the “1st” or “15th” dependent on the current date.</p>',
                        "image" => '/builder/img/feature-photo-3.png',
                        "video" => false
                    ],
                    [
                        "text" => '<h3>Natural Home</h3><p>Have fun being a DIY Natural Home Master using tested 100% pure essential oils and natural ingredients.</p>',
                        "image" => '/builder/img/feature-photo-4.png',
                        "video" => false
                    ]
                ]
            ]
        ]);

        Template::updateOrCreate([
            "type" => 'text_and_icon_three_slider',
        ], [
            "name" => 'Text and Icon Tree Slider',
            'section' => 'features',
            "preview" => [
                "previewDesktop" => '/builder/demo/feature-photo-tree.png',
                "previewTabledHorizontal" => '/builder/demo/feature-photo-tree.png',
                "previewTabled" => '/builder/demo/feature-photo-tree.png',
                "previewMobile" => '/builder/demo/text-icon-slider-mobi.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Invite</h3><p>Sign up and share your coupon code with friends.</p>',
                        "image" => '/builder/img/feature-photo-5.png',
                        "video" => false
                    ],
                    [
                        "text" => '<h3>Subscribe</h3><p>Your friend gets their first box free when they subscribe.</p>',
                        "image" => '/builder/img/feature-photo-6.png',
                        "video" => false
                    ],
                    [
                        "text" => '<h3>Claim</h3><p>You get a FREE BOX for every friend that subscribes.</p>',
                        "image" => '/builder/img/feature-photo-7.png',
                        "video" => false
                    ]
                ]
            ]
        ]);
        Template::updateOrCreate([
            "type" => 'questionnaire',
        ], [
            "name" => 'Questionnaire',
            'section' => 'contents',
            "preview" => [
                "previewDesktop" => '/builder/demo/question.png',
                "previewTabledHorizontal" => '/builder/demo/question.png',
                "previewTabled" => '/builder/demo/question.png',
                "previewMobile" => '/builder/demo/question.png',
            ],
            "position_inverse" => false,
            "structure" => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "textTitle" => '<h2>Template questionnaire</h2>',
                "text1" => '<p>New to Essential Oils? Not sure where to begin? We are here to help.</p>',
                "input1" => [
                    "name" => 'unique name',
                    "placeholder" => 'Enter text',
                    "type" => 'text'
                ],
                "text2" => '<p>New to Essential Oils? Not sure where to begin? We are here to help.</p>',
                "input2" => [
                    "name" => 'unique name',
                    "placeholder" => 'Enter text',
                    "type" => 'text'
                ],
                "text3" => '<p>New to Essential Oils? Not sure where to begin? We are here to help.</p>',
                "input3" => [
                    "name" => 'unique name',
                    "placeholder" => 'Enter text',
                    "type" => 'text'
                ],
            ]
        ]);


        foreach (Template::all() as $template) {
            $templates = Template::query()->whereType($template->type);

            if ($templates->count() >= 2) {
                $drop_template = $templates->latest()->first();

                if (Content::query()->where('template_id', $drop_template->id)->count()) {
                    foreach (Content::query()->where('template_id', $drop_template->id)->get() as $content) {
                        $content->delete();
                    }
                }

                $drop_template->delete();
            }
        }
    }
}
