<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */

        $user = factory(User::class)->create([
            'email' => 'demo@demo.com',
            'password' => bcrypt('demo'),
        ]);

        $user->assignRole('Admin');
    }
}
