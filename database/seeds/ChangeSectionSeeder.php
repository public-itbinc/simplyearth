<?php

use Illuminate\Database\Seeder;
use App\Models\Pagebuilder\Template;

class ChangeSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Change type template
        $templates = Template::query()->where('type','text_and_icon')->orderBy('id', 'DESC');
        if($templates->count() >= 2){
            $template = $templates->first();
            $template->type = "text_and_icon_three";
            $template->save();
        }


        $sections = [
            'text_and_image' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "text" => '<h2>The most amazing essential oil recipe box</h2><p>For 100% pure essential oil + Natural Ingredients every month</p>',
                "button" => [
                    "text" => 'Visit Our Shop',
                    "line" => '#',
                    "type" => 'button'
                ],
                "img" => '/builder/img/text-and-image-prev.png',
                "video" => false,
                "video_preview" => '',
                'position_inverse' => true,
            ],
            'text_mixed_and_image' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "text" => '<h2>The most amazing essential oil recipe box</h2><p>For 100% pure essential oil + Natural Ingredients every month</p><a href="#" class="btn">CHECK OUT THIS MONTH’S BOX</a><p><small>Don’t wait to make it yours. It WILL sell out!</small></p>',
                "button" => [
                    "text" => '',
                    "line" => '#',
                    "type" => 'btn-link'
                ],
                "img" => '/builder/demo/test.png',
                "video" => false,
                "video_preview" => '',
                'position_inverse' => true,
            ],
            'features_icon' => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Invite</h3><p>Sign up and share your coupon code with friends.</p>',
                        "image" => '/builder/img/invite-icon.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Subscribe</h3><p>Your friend gets their first box free when they subscribe.</p>',
                        "image" => '/builder/img/invite-icon.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Claim</h3><p>You get a FREE BOX for every friend that subscribes.</p>',
                        "image" => '/builder/img/invite-icon.png',
                        "video" => false,
                        "video_preview" => '',
                    ]
                ]
            ],
            'text_and_icon' => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Subsribe and Save!</h3><p>Join for as low as $39/box. Every box is worth more than $150 compared to the big essential oil companies.</p>',
                        "image" => '/builder/img/feature-photo-1.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Free Big Bonus Box</h3><p>When you start today! It includes coconut oil, almond oil, beeswax, roller bottles, and 5ml bottles.</p>',
                        "image" => '/builder/img/feature-photo-2.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Free Shipping</h3><p>Your first box ships right away. Your next box ships on the “1st” or “15th” dependent on the current date.</p>',
                        "image" => '/builder/img/feature-photo-3.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Natural Home</h3><p>Have fun being a DIY Natural Home Master using tested 100% pure essential oils and natural ingredients.</p>',
                        "image" => '/builder/img/feature-photo-4.png',
                        "video" => false,
                        "video_preview" => '',
                    ]
                ]
            ],
            'text_and_icon_three' => [
                "background" => [
                    "color" => '#ffffff',
                    "image" => ''
                ],
                "text" => '<h2>How does the program work?</h2>',
                "item" => [
                    [
                        "text" => '<h3>Invite</h3><p>Sign up and share your coupon code with friends.</p>',
                        "image" => '/builder/img/feature-photo-5.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Subscribe</h3><p>Your friend gets their first box free when they subscribe.</p>',
                        "image" => '/builder/img/feature-photo-6.png',
                        "video" => false,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<h3>Claim</h3><p>You get a FREE BOX for every friend that subscribes.</p>',
                        "image" => '/builder/img/feature-photo-7.png',
                        "video" => false,
                        "video_preview" => '',
                    ]
                ]
            ],
            'text_and_icon_tree' => [
                "background" => [
                    "color" => '#FEFAF7',
                    "image" => ''
                ],
                "text" => '<h2>PEOPLE LOVE US</h2>',
                "blocks" => [
                    [
                        "text" => '<p>Chocolate tootsie roll candy canes cotton candy. Pastry icing jelly icing tiramisu candy canes. Candy canes halvah pie ice cream.</p><span class="name">Jordan S.</span><p style="color: #404141;">Springfield, MO</p>',
                        "image" => 'https://www.youtube.com/embed/jnLSYfObARA',
                        "video" => true,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<p>Chocolate tootsie roll candy canes cotton candy. Pastry icing jelly icing tiramisu candy canes. Candy canes halvah pie ice cream.</p><span class="name">Jordan S.</span><p style="color: #404141;">Springfield, MO</p>',
                        "image" => 'https://www.youtube.com/embed/jnLSYfObARA',
                        "video" => true,
                        "video_preview" => '',
                    ],
                    [
                        "text" => '<p>Chocolate tootsie roll candy canes cotton candy. Pastry icing jelly icing tiramisu candy canes. Candy canes halvah pie ice cream.</p><span class="name">Jordan S.</span><p style="color: #404141;">Springfield, MO</p>',
                        "image" => 'https://www.youtube.com/embed/jnLSYfObARA',
                        "video" => true,
                        "video_preview" => '',
                    ]
                ]
            ],
            'video' =>[
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "img" => '/builder/img/text-and-image-prev.png',
                "video" => false,
                "video_preview" => '',
                'position_inverse' => true,
            ],
            'image' => [
                "background" => [
                    'color' => '#ffffff',
                    'image' => '',
                ],
                "img" => '/builder/img/text-and-image-prev.png',
                "video" => false,
                'position_inverse' => true,
            ]

        ];

        foreach ($sections as $key => $section) {
            $template = Template::query()->whereType($key)->first();
            $template->structure = $section;
            $template->save();

        }
    }
}
