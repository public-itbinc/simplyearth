<?php

return [
    'apiKey' => env('SHIPSTATION_API_KEY', ''),
    'apiSecret' => env('SHIPSTATION_API_SECRET', ''),
    'store' => [
        'username' => env('SHIPSTATION_STORE_USERNAME', ''),
        'password' => env('SHIPSTATION_STORE_PASSWORD', ''),
    ],
];
