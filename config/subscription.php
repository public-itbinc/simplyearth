<?php

return [
    'monthly' => env('SUBSCRIPTION_MONTHLY_SKU', 'REC-MONTHLY'),
    'monthly2019' => env('SUBSCRIPTION_MONTHLY2019_SKU', 'REC-MONTHLY2019'),
    'quarterly' => env('SUBSCRIPTION_QUARTERLY_SKU', 'REC-QUARTERLY'),
    'exchange_price' => 2,
    'exchange_category' => env('SUBSCRIPTION_EXCHANGE_CATEGORY', 'essential-oils'),
    'bonus_box' => env('BONUS_BOX', 'REC-BONUSBOX'),
    'onetime_box' => env('ONETIME_BOX', 'REC-MONTHLY-ONETIME'),
    'commitment_box' => env('COMMITMENT_BOX', 'REC-MONTHLY-6MONTHS'),
    'commitment_box_3' => env('COMMITMENT_BOX_3', 'REC-MONTHLY-3MONTHS')
];
