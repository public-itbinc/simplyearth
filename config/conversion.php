<?php

return [
    'klaviyo' => [
        'url' => env('KLAVIYO_URL', 'https://a.klaviyo.com'),
        'api_key' => env('KLAVIYO_API_KEY', 'DEMO'),
        'private_api_key' => env('KLAVIYO_PRIVATE_API_KEY', ''),
        'list' => env('KLAVIYO_LIST', '')
    ],
    'refersion' => [
        'api_key' => env('REFERSION_API_KEY', 'DEMO'),
        'secret_key' => env('REFERSION_SECRET_KEY', 'DEMO'),
    ],        
    'referral_candy' => [
        'access_id' => env('REFERRAL_CANDY_ACCESS_ID', ''),
        'api_secret' => env('REFERRAL_CANDY_API_SECRET', ''),
    ],
    'stamped' => [
        'client_id' => env('STAMPED_CLIENT_ID', ''),
        'client_secret' => env('STAMPED_CLIENT_SECRET', ''),
        'store_url' => env('STAMPED_STOR_URL', '')
    ]
];
