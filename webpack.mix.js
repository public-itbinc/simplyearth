let mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

mix.webpackConfig({
  //plugins: [ new BundleAnalyzerPlugin()],
  module: {
    loaders: [{
      test: require.resolve("tinymce/tinymce"),
      loaders: ["imports?this=>window", "exports?window.tinymce"]
    },
    {
      test: /tinymce\/(themes|plugins)\//,
      loaders: ["imports?this=>window"]
    }
    ]
  }
});


mix
.webpackConfig({
  devtool: "source-map"
})
.sourceMaps();

//mix.options({imgLoaderOptions: {enabled: false}, processCssUrls: false})
mix
  .options({
    processCssUrls: false
  })

  //admin

  .js(["resources/assets/js/admin-shipping.js"], "public/js")
  .js(["resources/assets/js/admin-orders.js"], "public/js")
  .js(["resources/assets/js/admin-customers.js"], "public/js")
  .js(["resources/assets/js/admin-products.js"], "public/js")
  .js(["resources/assets/js/admin-blog.js"], "public/js")
  .js(["resources/assets/js/form.js"], "public/js/form.js")
  .js(["resources/assets/js/admin.js"], "public/js/admin.js")
  .js(["resources/assets/js/admin-recipes.js"], "public/js")
  .js(["resources/assets/admin/pagebuilder/src/main.js"], "public/builder/js")
  .js(["resources/assets/admin/pagebuilder/src/main2.js"], "public/builder/js")
  .sass("resources/assets/sass/admin.scss", "public/css")
  .sass("resources/assets/admin/pagebuilder/src/assets/css/main.scss", "public/builder/css")

  mix.browserSync({
    proxy: 'http://simplyearth.test'
})
  //Frontend
  .sass("resources/assets/sass/app.scss", "public/css")
  .js("resources/assets/js/app.js", "public/js")
  .js(["resources/assets/js/profile.js"], "public/js")
  .js(["resources/assets/js/checkout.js"], "public/js")
  .js('resources/assets/js/components/blog/fontend.js', 'public/js')
  .js(["resources/assets/js/invites.js"], "public/js")


if (mix.inProduction()) {
  mix.version();
}